$(document).ready(function() {
            var clicked = 0;

            $('body').on("click", ".hamburger", function() {
               $('html, body').animate({scrollTop : 0},200);
    
                if (clicked == 0) {
                    
                    $(this).addClass('is-active');
    
                    $(".menus").addClass('step1');
                    $(".menus").addClass('menusslide');
    
                    setTimeout(function() {
    
                        $(".menus").addClass('step2');
    
                    }, 600);
    
    
                    setTimeout(function() {
    
                        $(".menus").addClass('step3');
    
                        clicked = 1;
    
    
                    }, 1000);
    
                } else {
    
    
                    $(this).removeClass('is-active');
    
                    $(".menus").removeClass('menusslide');
                    $(".menus").removeClass('step3');
    
    
                    setTimeout(function() {
    
                        $(".menus").removeClass('step2');
    
                    }, 200);
    
    
                    setTimeout(function() {
    
                        $(".menus").removeClass('step1');
    
                        clicked = 0;
                        
                    }, 400);
                }
    
    
            });
            $('body').on('click','.smoothscroll',function (event) {
                var target = $('#'+$(this).data('link'));
            if (target.length) {
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000, function() {
                    var $target = $(target);
                    $target.focus();
                    if ($target.is(":focus")) {
                        return false;
                    } else {
                        $target.attr('tabindex','-1');
                        $target.focus();
                    };
                });
            }
        });
        
        $(window).scroll(function () {
         if ($(this).scrollTop() > 500) {
           $('.scrollup').fadeIn('slow');
         } else {
           $('.scrollup').fadeOut('slow');
         }
        });
        $('body').on('click','.scrollup',function () {
         $("html, body").animate({scrollTop: 0}, 1000);
         return false;
        });

        $('body').on('click','.nav-item',function () {
            $(".hamburger").removeClass('is-active');
            
                            $(".menus").removeClass('menusslide');
                            $(".menus").removeClass('step3');
            
            
                            setTimeout(function() {
            
                                $(".menus").removeClass('step2');
            
                            }, 200);
            
            
                            setTimeout(function() {
            
                                $(".menus").removeClass('step1');
            
                                clicked = 0;
                                
                            }, 400);
           });
    
        jQuery("h1").fitText(1.1, { minFontSize: '40px', maxFontSize: '120px' });
    
        });