import { Config } from './../shared/configs/general.config';
import { CloudinaryService } from './../admin-app/components/cloudinary/cloudinary.service';
import { Component } from '@angular/core';
import { HobSeoService } from '../shared/services/hobseo.service';
import { ScrollToTopService } from '../shared/services/scroll-to-top.service';

@Component({
    selector: 'frontend-cmp',
    templateUrl: './frontend-index.html'
})


export class FrontendComponent {
    
    constructor (private cloudinaryService:CloudinaryService, private hobSeoService: HobSeoService, private scroll: ScrollToTopService){}

    ngOnInit() {
        this.scroll.scrollTop();
        this.setCloudinaryName();
    }

    setCloudinaryName() {
        this.cloudinaryService.getCloudinarySettings()
            .subscribe(res=>Config.setCloudinary(res.cloudinaryCloudName),
                err=>this.handleErrorMsg(err));
    }
    
    handleErrorMsg(res:any) {
        console.error(res.message);
    }
}
