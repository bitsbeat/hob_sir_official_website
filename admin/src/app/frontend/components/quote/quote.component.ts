import { Component, OnInit } from '@angular/core';
import { NoticeService } from '../../../admin-app/components/notice/notice.service';

@Component({
    selector:'quote-bar',
    templateUrl:'./quote.html'
})

export class QuoteComponent implements OnInit{
    quote: any;

    constructor(private quoteService: NoticeService) {}

    ngOnInit() {
        this.getQuote();
    }

    getQuote() {
        this.quoteService.getQuote()
            .subscribe(res => this.bindQuote(res));
    }

    bindQuote(res: any) {
        this.quote = res;
    }
}
