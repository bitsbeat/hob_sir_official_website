import {NgModule} from "@angular/core";
import {SharedModule} from "../../../shared/shared.module";
import {CommonModule} from "@angular/common";
import { QuoteComponent } from "./quote.component";
import { NoticeService } from "../../../admin-app/components/notice/notice.service";

@NgModule({
  imports: [SharedModule],
  declarations: [QuoteComponent],
  exports: [QuoteComponent],
  providers: [NoticeService]
})

export class QuoteModule {

}
