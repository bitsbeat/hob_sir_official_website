import { FrontendComponent } from './../../frontend.component';
import {RouterModule, Routes} from "@angular/router";
import {BlogComponent} from "./blog.component";
import {BlogListComponent} from "./blog-list.component";
import {BlogDetailComponent} from "./blog-detail.component";
import {NgModule} from "@angular/core";
import { BlogHomeComponent } from './blog-home.component';
import { BlogBySlogComponent } from './blog-by-slog.component';

export const BlogRoute: Routes = [
  {path: '', component: BlogHomeComponent,
    children: [
      {path: '', component: BlogListComponent},
      {path: ':slog', component: BlogBySlogComponent},
      {path: 'detail/:urlSlog', component: BlogDetailComponent}
    ]
  }
]

@NgModule({
  imports: [
  RouterModule.forChild(BlogRoute)
  ],
  exports: [
    RouterModule
  ]
})
export class BlogRouting {
}
