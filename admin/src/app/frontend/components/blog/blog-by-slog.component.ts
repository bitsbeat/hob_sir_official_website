import {Component} from '@angular/core';
import {BlogService} from '../../../admin-app/components/blog/blog.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {BlogResponse, BlogCategoryResponse} from '../../../admin-app/components/blog/blog.model';
import {Config} from '../../../shared/configs/general.config';
import 'rxjs/add/operator/filter';
import { ScrollToTopService } from '../../../shared/services/scroll-to-top.service';
import { HobSeoService } from '../../../shared/services/hobseo.service';
import { ParamMap } from '@angular/router/src/shared';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'blog-by-slog',
  templateUrl: './blog-by-slog.component.html'
})

export class BlogBySlogComponent{
  index = 1;
  perPage = 6;
  currentPage: number;
  pageLimit: number;
  totalItems: number;
  blogBySlogList: any[] = [];
  categorySlog: string;
  blogCategoryList: BlogCategoryResponse;  
  currentBlogCategory: string;
  garbage: any;

  constructor(private blogService: BlogService, private activatedRoute: ActivatedRoute, private router: Router, private scroll: ScrollToTopService, private hobSeoService: HobSeoService) {
    this.garbage = this.router.events.filter(event => event instanceof NavigationEnd)
      .subscribe((event: NavigationEnd) => {
        this.categorySlog = this.activatedRoute.snapshot.params.slog;
        this.currentPage = parseInt(typeof this.activatedRoute.snapshot.queryParams.page != 'undefined' ? this.activatedRoute.snapshot.queryParams.page : 1);
        this.getBlogList();
        this.getBlogCategory();
        this.scroll.scroll130();
      });
    // this.garbage = activatedRoute.paramMap
    //   .switchMap((params: ParamMap) => {
    //     this.categorySlog = params.get('slog');
    //     this.currentPage = parseInt(typeof activatedRoute.snapshot.queryParams.page != 'undefined' ? activatedRoute.snapshot.queryParams.page : 1);
    //     this.getBlogList();
    //     this.getBlogCategory();
    //     this.scroll.scroll130();
    //     return Observable.of(1);
    //   })
  }

  ngOnDestroy() {
    this.garbage.unsubscribe();
  }

  getBlogCategory() {
    this.blogService.getBlogCategoryList()
        .subscribe(res => this.bindCategoryList(res));
  }

  bindCategoryList(res: BlogCategoryResponse) {
    this.blogCategoryList = res;
    this.blogCategoryList.dataList.forEach(category => {
      if(category.urlSlogCategory == this.categorySlog) {
          this.currentBlogCategory = category.categoryName;
          this.hobSeoService.setTitle("Blogs | " + category.categoryName);
      }
    })
  }
  
  getBlogList() {
    this.blogService.getBlogByCategorySlog(this.perPage, this.currentPage, this.categorySlog)
          .subscribe(resEvent => this.handleDetail(resEvent));
  }

  handleDetail(objRes: BlogResponse) {
    this.blogBySlogList = objRes.dataList;
    this.totalItems = objRes.totalItems;
    this.pageLimit = Math.ceil(this.totalItems / this.perPage);
    this.blogBySlogList = this.blogBySlogList.map((blog) => {
      blog.bannerImage = this.getSource(blog.bannerImage);
      return blog;
    });
  }

  getSource(image: string) {
    return Config.Cloudinary.url(image);
  }

  increaseIndex() {
    if ((this.pageLimit - 2) > this.index)
      this.index++;
  }

  decreaseIndex() {
    if (this.index > 1)
      this.index--;
  }
}
