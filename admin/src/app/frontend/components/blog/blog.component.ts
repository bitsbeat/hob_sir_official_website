import {Component, OnInit} from '@angular/core';
import {BlogService} from "../../../admin-app/components/blog/blog.service";
import {BlogModel} from "../../../admin-app/components/blog/blog.model";

@Component({
  selector: 'blog',
  templateUrl: './blog.html',
})

export class BlogComponent implements OnInit{
  latestBlogs: BlogModel[];

  constructor(private blogService: BlogService) {}

  ngOnInit(){
      this.getLatestBlogs();
  }

  getLatestBlogs() {
      this.blogService.getLatestBlog()
        .subscribe(res => this.bindBlogDetails(res));
  }

  bindBlogDetails(objRes: BlogModel[]) {
    this.latestBlogs = objRes;
  }
}
