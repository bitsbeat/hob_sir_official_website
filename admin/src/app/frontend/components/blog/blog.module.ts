import {NgModule} from '@angular/core';
import {BlogComponent} from "./blog.component";
import {BlogListComponent} from "./blog-list.component";
import {BlogDetailComponent} from "./blog-detail.component";
import {BlogRouting} from "./blog.route";
import {BlogService} from "../../../admin-app/components/blog/blog.service";
import { BlogHomeComponent } from './blog-home.component';
import { SharedModule } from '../../../shared/shared.module';
import { BlogBySlogComponent } from './blog-by-slog.component';
import { ScrollToTopService } from '../../../shared/services/scroll-to-top.service';
// import { GrowlModule } from 'primeng/primeng';

@NgModule({
  imports: [SharedModule, BlogRouting],
  declarations: [ BlogComponent, BlogListComponent, BlogDetailComponent, BlogHomeComponent, BlogBySlogComponent],
  exports: [BlogComponent, BlogListComponent, BlogDetailComponent, BlogHomeComponent, BlogBySlogComponent],
  providers: [BlogService, ScrollToTopService]
})

export class BlogModule {}
