import {Component, OnInit} from '@angular/core';
import {BlogService} from '../../../admin-app/components/blog/blog.service';
import {BlogCategoryResponse} from '../../../admin-app/components/blog/blog.model';
import {Config} from "../../../shared/configs/general.config";

@Component({
  selector: 'blog-home',
  templateUrl: './blog-home.component.html'
})

export class BlogHomeComponent implements OnInit{
  blogCategoryList: BlogCategoryResponse;

  constructor(private blogService: BlogService) {

  }

  ngOnInit() {
    this.getBlogCategory();
  }

  getBlogCategory() {
    this.blogService.getBlogCategoryList()
        .subscribe(res => this.bindCategoryList(res));
  }

  bindCategoryList(objRes: BlogCategoryResponse) {
    this.blogCategoryList = objRes;
  }
}
