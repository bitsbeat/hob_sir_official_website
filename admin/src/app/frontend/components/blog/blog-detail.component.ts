import { Component, OnInit } from "@angular/core";
import { BlogService } from '../../../admin-app/components/blog/blog.service';
import { ActivatedRoute } from '@angular/router';
import { Config } from "../../../shared/configs/general.config";
import { ScrollToTopService } from '../../../shared/services/scroll-to-top.service';
import { HobSeoService } from '../../../shared/services/hobseo.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Message } from 'primeng/primeng';
declare const FB: any;

@Component({
  selector: 'blog-detail',
  templateUrl: './blog-detail.html'
})

export class BlogDetailComponent implements OnInit {
  urlSlog: string;
  repoUrl: string = '';
  blogDetail: any;
  shareUrl: string = '';
  feedUrl: string = '';
  msgs: Message[] = [];

  constructor(private blogService: BlogService, private sanitizer: DomSanitizer, private activatedRoute: ActivatedRoute, private scroll: ScrollToTopService, private hobSeoService: HobSeoService) {
    FB.init({
      appId: '144121063036405',
      cookie: false,
      xfbml: true,
      version: 'v2.8'
    });
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => this.urlSlog = params['urlSlog']);
    this.getBlogDetail();
    this.scroll.scroll130();
    this.repoUrl = document.location.href;
  }

  share() {
    FB.ui({
      method: 'share_open_graph',
      action_type: 'og.shares',
      action_properties: JSON.stringify({
        object: {
          'og:url': this.repoUrl,
          'og:title': this.blogDetail.data.blogTitle,
          'og:description': this.blogDetail.data.blogSummary,
          'og:image': this.blogDetail.data.bannerImage
        }
      })
    },
     (response) => {
        if (response && !response.error_message) {
          // then get post content
          let res = 'successfully posted. Status id : ' + response.post_id;
          this.afterSubmit(res, true);
        } else {
          let error = "Something went wrong";
          this.afterSubmit(error, false);
        }
      })
  }

  afterSubmit(res, data) {
    this.msgs = [];
    this.msgs.push({
      severity: data == true ? 'success' : 'error',
      summary: data == true ? 'Success!' : 'Error!',
      detail: res
    });
  }

  getBlogDetail() {
    this.blogService.getBlogDetailByUrlSlog(this.urlSlog)
      .subscribe(res => this.bindBlogDetails(res));
  }

  bindBlogDetails(objRes: any) {
    this.hobSeoService.setTitle("Blogs | " + objRes.data.blogTitle);
    this.blogDetail = objRes;

    this.blogDetail.data.bannerImage = this.getSource(this.blogDetail.data.bannerImage);
    this.blogDetail.data.blogDescription = this.transform(this.blogDetail.data.blogDescription);
  }

  transform(content) {
    return this.sanitizer.bypassSecurityTrustHtml(content);
  }

  getSource(image: string) {
    return Config.Cloudinary.url(image);
  }
}
