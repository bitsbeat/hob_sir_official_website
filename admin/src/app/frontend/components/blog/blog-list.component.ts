import {Component} from "@angular/core";
import { BlogService } from '../../../admin-app/components/blog/blog.service';
import { BlogResponse, BlogCategoryResponse } from '../../../admin-app/components/blog/blog.model';
import { Config } from '../../../shared/configs/general.config';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/switchMap';
import { ScrollToTopService } from "../../../shared/services/scroll-to-top.service";
import { HobSeoService } from '../../../shared/services/hobseo.service';

@Component({
  selector: 'blog-list',
  templateUrl: './blog-list.html'
})

export class BlogListComponent {
  index: number = 1;
  currentPage: number = 1;
  perPage: number = 6;
  totalPage: number;
  pageLimit: number;
  totalItems: number;
  blogCategoryList: BlogCategoryResponse;
  blogList: BlogResponse;

  constructor(private blogService: BlogService, private activatedRoute: ActivatedRoute, private scroll: ScrollToTopService, private hobSeoService: HobSeoService) {
    this.activatedRoute.queryParams
        .switchMap((params: any) =>
          this.blogService.getBlogList(this.perPage, this.currentPage = parseInt(typeof params['page'] != 'undefined' ? params['page'] : 1)))
        .subscribe(resEvent => this.bindBlogContent(resEvent));
  }

  ngOnInit() {
    this.hobSeoService.setTitle("Blogs");
    this.getBlogCategory();
  }

  getBlogCategory() {
    this.blogService.getBlogCategoryList()
        .subscribe(res => this.bindCategoryList(res));
  }

  bindCategoryList(res: BlogCategoryResponse) {
    this.blogCategoryList = res;
  }

  getSelectedBlogList(category) {
    let categoryId = (<HTMLSelectElement>category.srcElement).value;
    this.currentPage = 1;
    this.blogService.getBlogList(this.perPage, this.currentPage, categoryId)
      .subscribe(res => this.bindBlogContent(res));
  }

  bindBlogContent(res: BlogResponse) {
    this.scroll.scroll130();    
    this.blogList = res;
    this.totalItems = res.totalItems;
    this.pageLimit = Math.ceil(this.totalItems / this.perPage);
    this.blogList.dataList = this.blogList.dataList.map((blog) => {
      blog.bannerImage = this.getImageSource(blog.bannerImage);
      return blog;
    })
  }

  getImageSource(image: string) {
    return Config.Cloudinary.url(image, {height: 186, width: 300, crop:'scale'});
  }

  increaseIndex() {
    if ((this.pageLimit - 2) > this.index)
      this.index++;
  }

  decreaseIndex() {
    if (this.index > 1)
      this.index--;
  }
}
