import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import { AllnewsComponent } from './all-news.component';
import { NewsDetailComponent } from './news-detail.component';

export const AllNewsRoute: Routes = [
  {path: '', 
    children: [
      {path: '', component: AllnewsComponent,},
      {path: ':urlSlog', component: NewsDetailComponent}
    ]
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(AllNewsRoute)
  ],
  exports: [
    RouterModule
  ]
})
export class AllNewsRouting {
}
