import { LinkedInService } from './../../../shared/services/linkedin.service';
import { Component, OnInit, NgZone } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { Config } from "../../../shared/configs/general.config";
import { NewsService } from '../../../admin-app/components/news/news.service';
import { NewsResponse, NewsModel } from "../../../admin-app/components/news/news.model";
import { ScrollToTopService } from '../../../shared/services/scroll-to-top.service';
import { HobSeoService } from "../../../shared/services/hobseo.service";
import { DomSanitizer } from '@angular/platform-browser';
import { Message } from 'primeng/primeng';
declare const FB: any;

@Component({
  selector: 'news-detail',
  templateUrl: './news-detail.component.html'
})

export class NewsDetailComponent implements OnInit {
  urlSlog: string;
  newsDetail: any;
  repoUrl: string = '';
  msgs: Message[] = [];

  constructor(private zone: NgZone, private newsService: NewsService, private sanitizer: DomSanitizer, private activatedRoute: ActivatedRoute, private scroll: ScrollToTopService, private hobSeoService: HobSeoService) {
    FB.init({
      appId: '144121063036405',
      cookie: false,
      xfbml: true,
      version: 'v2.8'
    });
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => this.urlSlog = params['urlSlog']);
    this.getBlogDetail();
    this.scroll.scroll130();
    this.repoUrl = document.location.href;
  }

  share() {
    FB.ui({
      method: 'share_open_graph',
      action_type: 'og.shares',
      action_properties: JSON.stringify({
        object: {
          'og:url': this.repoUrl,
          'og:title': this.newsDetail.newsTitle,
          'og:description': this.newsDetail.newsSummary,
          'og:image': this.newsDetail.image[0].imageName
        }
      })
    },
      (response) => {
        if (response && !response.error_message) {
          // then get post content
          let res = 'successfully posted. Status id : ' + response.post_id;
          this.afterSubmit(res, true);
        } else {
          let error = "Something went wrong";
          this.afterSubmit(error, false);
        }
      })
  }

  afterSubmit(res, data) {
    this.msgs = [];
    this.msgs.push({
      severity: data == true ? 'success' : 'error',
      summary: data == true ? 'Success!' : 'Error!',
      detail: res
    });
  }

  getBlogDetail() {
    this.newsService.getNewsDetailByTitleSlog(this.urlSlog)
      .subscribe(res => this.bindNewsDetails(res));
  }

  bindNewsDetails(objRes: NewsModel) {
    this.hobSeoService.setTitle("News | " + objRes.newsTitle);
    this.newsDetail = objRes;
    this.newsDetail.image[0].imageName = this.getSource(this.newsDetail.image[0].imageName);
    this.newsDetail.newsDescription = this.transform(this.newsDetail.newsDescription);
  }

  transform(content) {
    return this.sanitizer.bypassSecurityTrustHtml(content);
  }

  getSource(image: string) {
    return Config.Cloudinary.url(image);
  }
}
