import { LinkedInService } from '../../../shared/services/linkedin.service';
import { NgModule } from "@angular/core";
import { AllnewsComponent } from "./all-news.component";
import { NewsService } from "../../../admin-app/components/news/news.service";
import { AllNewsRouting } from "./all-news.route";
import { NewsDetailComponent } from "./news-detail.component";
import { SharedModule } from '../../../shared/shared.module';
import { ScrollToTopService } from '../../../shared/services/scroll-to-top.service';
import { HobSeoService } from "../../../shared/services/hobseo.service";
// import { GrowlModule } from "primeng/primeng";

@NgModule({
  imports: [SharedModule, AllNewsRouting],
  declarations: [AllnewsComponent, NewsDetailComponent],
  exports: [AllnewsComponent, NewsDetailComponent],
  providers: [NewsService, ScrollToTopService, HobSeoService, LinkedInService]
})

export class AllNewsModule {}
