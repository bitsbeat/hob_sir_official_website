import { Component, OnInit } from "@angular/core";
import { NewsService } from "../../../admin-app/components/news/news.service";
import { NewsResponse } from "../../../admin-app/components/news/news.model";
import { Config } from "../../../shared/configs/general.config";
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import { ScrollToTopService } from "../../../shared/services/scroll-to-top.service";
import { HobSeoService } from '../../../shared/services/hobseo.service';
@Component({
  selector: "all-news",
  templateUrl: "./all-news.html"
})
export class AllnewsComponent implements OnInit{
  index: number = 1;
  perPage: number = 6;
  currentPage: number = 1;
  newsList: NewsResponse;
  pageLimit: number;
  totalItems: number;

  constructor(private newsService: NewsService, private activatedRoute: ActivatedRoute, private scroll: ScrollToTopService, private hobSeoService: HobSeoService) {}

  ngOnInit() {
    this.hobSeoService.setTitle("News");
    this.activatedRoute.queryParams
    .switchMap((params: any) =>
      this.newsService.getNewsList(this.perPage, this.currentPage = parseInt(typeof params['page'] != 'undefined' ? params['page'] : 1), true))
    .subscribe(resEvent => this.bindNewsList(resEvent));
  }

  bindNewsList(objRes: NewsResponse) {
    this.scroll.scroll130();
    this.newsList = objRes;
    this.totalItems = objRes.totalItems;
    this.pageLimit = Math.ceil(this.totalItems / this.perPage);
    this.newsList.dataList = this.newsList.dataList.map(news => {
      news.image[0].imageName = this.getSource(news.image[0].imageName);
      return news;
    });
  }

  getSource(image: string) {
    return Config.Cloudinary.url(image, {
      height: 186,
      width: 298,
      crop: "scale"
    });
  }
}
