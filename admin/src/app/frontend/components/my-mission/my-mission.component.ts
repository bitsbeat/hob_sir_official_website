import { Component } from '@angular/core';
import { HtmlContentModel } from '../../../admin-app/components/html-content/html-content.model';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { HtmlContentService } from '../../../admin-app/components/html-content/html-content.service';

@Component({
    selector: 'my-mission',
    templateUrl : './my-mission.html',
    styleUrls: ['./my-mission.component.css']
})

export class MymissionComponent implements OnInit{

    constructor(private htmlService: HtmlContentService) {}

    ngOnInit() {
        
    }
}