import {NgModule} from "@angular/core";
import {SharedModule} from "../../../shared/shared.module";
import {CommonModule} from "@angular/common";
import {LatestNewsComponent} from "./latest-news.component";
import {NewsService} from "../../../admin-app/components/news/news.service";

@NgModule({
  imports: [SharedModule, CommonModule],
  declarations: [LatestNewsComponent],
  exports: [LatestNewsComponent],
  providers: [NewsService]
})

export class LatestNewsModule {

}
