import { Component, OnInit } from "@angular/core";
import { NewsService } from "../../../admin-app/components/news/news.service";
import { NewsModel } from "../../../admin-app/components/news/news.model";
import { Config } from "../../../shared/configs/general.config";
import { HobSeoService } from "../../../shared/services/hobseo.service";

@Component({
  selector: "latest-news",
  templateUrl: "./latest-news.html"
})
export class LatestNewsComponent implements OnInit {
  latestNewsList: NewsModel[];

  constructor(private newsService: NewsService) {}

  ngOnInit() {
    this.getLatestNews();
  }

  getLatestNews() {
    this.newsService.getLatestNews().subscribe(res => this.bindNewsList(res));
  }

  bindNewsList(objRes: NewsModel[]) {
    this.latestNewsList = objRes;
    this.latestNewsList.forEach(news => {
      news.image[0].imageName = this.getImageSource(news.image[0].imageName);
    });
  }

  getImageSource(image: string) {
    return Config.Cloudinary.url(image, {height: 160, width: 255, crop: 'scale'});
  }
}
