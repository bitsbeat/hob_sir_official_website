import { Component, OnInit } from "@angular/core";
import { VideoGalleryService } from "../../../admin-app/components/videoGallery/videoGallery.service";
import { VideoResponse } from "../../../admin-app/components/videoGallery/videoGallery.model";
import { DomSanitizer } from "@angular/platform-browser";
import { ActivatedRoute } from "@angular/router";
import "rxjs/add/operator/switchMap";
import { ScrollToTopService } from '../../../shared/services/scroll-to-top.service';
import { HobSeoService } from '../../../shared/services/hobseo.service';

@Component({
  selector: "video-gallery",
  templateUrl: "./video-gallery.html"
})
export class VideoGalleryComponent implements OnInit {
  index: number = 1;
  perPage: number = 6;
  currentPage: number = 1;
  totalItems: number;
  pageLimit: number;
  videoList: VideoResponse;
  youtubeUrl = "https://www.youtube.com/embed/";

  constructor(
    private videoService: VideoGalleryService,
    private sanitizer: DomSanitizer,
    private activatedRoute: ActivatedRoute,
    private scroll: ScrollToTopService,
    private hobSeoService: HobSeoService
  ) {}

  ngOnInit() {
    this.hobSeoService.setTitle("Video Gallery");
    this.activatedRoute.queryParams
      .switchMap((params: any) =>
        this.videoService.getVideoGalleryList(
          this.perPage,
          (this.currentPage = parseInt(
            typeof params["page"] != "undefined" ? params["page"] : 1
          ))
        )
      )
      .subscribe(resEvent => this.bindVideoList(resEvent));
  }

  bindVideoList(objRes: VideoResponse) {
    this.scroll.scroll130();
    this.videoList = objRes;
    this.totalItems = objRes.totalItems;
    this.pageLimit = Math.ceil(this.totalItems / this.perPage);
    this.videoList.dataList = this.videoList.dataList.map(video => {
      video.videoUrl = this.getIframeUrl(video.videoUrl);
      return video;
    });
  }

  getIframeUrl(link: string) {
    let embedUrl = link.split("=");
    return this.youtubeUrl + embedUrl[1];
  }

  increaseIndex() {
    if (this.pageLimit - 2 > this.index) this.index++;
  }

  decreaseIndex() {
    if (this.index > 1) this.index--;
  }
}
