import { FrontendComponent } from './../../frontend.component';
import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import { VideoGalleryComponent } from './video-gallery.component';

export const VideoGalleryRoute: Routes = [
  {path: '',
    children: [
      {path: '', component:VideoGalleryComponent},
    ]
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(VideoGalleryRoute)
  ],
  exports: [
    RouterModule
  ]
})
export class VideoGalleryRouting {
}