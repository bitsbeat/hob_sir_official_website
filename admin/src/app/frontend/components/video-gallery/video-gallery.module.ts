import { VideoGalleryRouting } from './video-gallery.route';
import { VideoGalleryComponent } from './video-gallery.component';
import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import { VideoGalleryService } from '../../../admin-app/components/videoGallery/videoGallery.service';
import { ScrollToTopService } from '../../../shared/services/scroll-to-top.service';

@NgModule({
  imports: [CommonModule, RouterModule, VideoGalleryRouting],
  declarations: [ VideoGalleryComponent],
  exports: [VideoGalleryComponent],
  providers: [VideoGalleryService, ScrollToTopService]
})

export class VideoGalleryModule {}
