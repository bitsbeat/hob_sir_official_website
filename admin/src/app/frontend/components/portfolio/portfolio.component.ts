import { Component } from '@angular/core';
import { PartnerService } from '../../../admin-app/components/partner/partner.service';
import { PartnerResponse } from '../../../admin-app/components/partner/partner.model';
import { Config } from '../../../shared/configs/general.config';
import { HtmlContentService } from '../../../admin-app/components/html-content/html-content.service';
import { HtmlContentModel } from '../../../admin-app/components/html-content/html-content.model';
import { ScrollToTopService } from '../../../shared/services/scroll-to-top.service';
import { HobSeoService } from '../../../shared/services/hobseo.service';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'portfolio',
    templateUrl: 'portfolio.component.html'
})

export class PortfolioComponent{
    perPage: number = 100;
    currentPage: number = 1;
    partnerList: PartnerResponse;
    portfolioId: string = "5a15640caf556765b426fc37";
    portfolioDescription: HtmlContentModel;

    constructor(private partnerService: PartnerService, private html: HtmlContentService, private hobSeoService: HobSeoService, private scroll: ScrollToTopService) {}

    ngOnInit() {
        this.hobSeoService.setTitle("Portfolio");
        this.getPartners();
        this.getPortfolioDescription();
        this.scroll.scroll130();
    }
    
    getPortfolioDescription() {
        this.html.getHtmlEditorById(this.portfolioId)
        .subscribe(res => this.bindPortfolio(res));
    }
    
    bindPortfolio(res: HtmlContentModel) {
        this.portfolioDescription = res;
    }
    
    getPartners() {
        this.partnerService.getPartnerList(this.perPage, this.currentPage)
        .subscribe(res => this.bindPartner(res));
    }
    
    bindPartner(res : PartnerResponse) {
        this.partnerList = res;
        this.partnerList.dataList = this.partnerList.dataList.map((partner) => {
            partner.imageName = this.getImageSource(partner.imageName);
            return partner;
        });
    }

    getImageSource(image: string) {
        return Config.Cloudinary.url(image);
    }
}