import { Component } from '@angular/core';
import {HtmlContentModel} from "../../../admin-app/components/html-content/html-content.model";
import {HtmlContentService} from "../../../admin-app/components/html-content/html-content.service";
import {ScrollToTopService} from "../../../shared/services/scroll-to-top.service";
import { HobSeoService } from '../../../shared/services/hobseo.service';

@Component({
  selector: "biography",
  templateUrl: "./biography.component.html"
})

export class BiographyComponent{
  biographyId: string = "5a14155628c6cf4f5491687e";
  biographyDescription: HtmlContentModel;
  
  constructor(private htmlContent: HtmlContentService, private hobSeoService: HobSeoService, private scroll: ScrollToTopService) {}
  
  ngOnInit() {
    this.hobSeoService.setTitle("Biography");    
    this.htmlContent.getHtmlEditorById(this.biographyId)
      .subscribe(res => this.bindBiography(res));
    this.scroll.scroll130();
  }

  bindBiography(objRes: HtmlContentModel) {
    this.biographyDescription = objRes;
  }
}
