import { Component } from '@angular/core';
import { OrganizationInfoService } from '../../../admin-app/components/organization-information/orginfo.service';
import { OrganizationModel } from '../../../admin-app/components/organization-information/orginfo.model';
import { Config } from '../../../shared/configs/general.config';
import { TwitterService } from "../../../shared/services/twitter.service";

@Component({
    selector: 'person-info',
    templateUrl: './person-info.component.html'
})

export class PersonInfoComponent {
    personDetail: OrganizationModel;
    twitterPost: any[];
    // postLink: string;

    constructor(private orgService: OrganizationInfoService, private twitter: TwitterService) {}

    ngOnInit() {
        this.getPersonInfo();
        this.getTwitterFeed();
    }

    getTwitterFeed() {
      this.twitter.getTwitterPosts()
        .subscribe(res => this.bindFeed(res),
          error => console.log("Error", error));
    }

    bindFeed(res: any) {
      this.twitterPost = res;
      this.twitterPost.forEach((twitter) => {
        twitter.postLink = "https://twitter.com/" + twitter.user.screen_name + "/status/" + twitter.id_str;
        return twitter;
      })
    }

    getPersonInfo() {
        this.orgService.getOrgInfoDetail()
            .subscribe(res => this.bindPersonInfo(res));
    }

    bindPersonInfo(objRes: OrganizationModel) {
        this.personDetail = objRes;
        this.personDetail.logoImageName = this.getImageSource(this.personDetail.logoImageName);
    }

    getImageSource(image: string) {
        return Config.Cloudinary.url(image);
    }
}
