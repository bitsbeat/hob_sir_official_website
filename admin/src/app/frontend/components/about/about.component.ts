import { Component, OnInit } from '@angular/core';
import { HtmlContentModel } from '../../../admin-app/components/html-content/html-content.model';
import { HtmlContentService } from '../../../admin-app/components/html-content/html-content.service';

@Component({
    selector:'about-section',
    templateUrl:'./about.html'
})

export class AboutComponent implements OnInit{
    passionHeaderId: string = "5a0da5d69d41d31b91add80c";
    passionHeaderDescription: HtmlContentModel;
    aboutMeHeaderId: string = "5a0da6ab9d41d31b91add80d";
    aboutMeHeaderDescription: HtmlContentModel;

    constructor(private htmlContent: HtmlContentService) {}

    ngOnInit() {
        this.htmlContent.getHtmlEditorById(this.passionHeaderId)
            .subscribe(res => this.bindPassionHeaderHtml(res));
        this.htmlContent.getHtmlEditorById(this.aboutMeHeaderId)
            .subscribe(res => this.bindAboutMeHeaderHtml(res));
    }

    bindPassionHeaderHtml(objRes: HtmlContentModel) {
        this.passionHeaderDescription = objRes;
    }

    bindAboutMeHeaderHtml(objRes: HtmlContentModel) {
        this.aboutMeHeaderDescription = objRes;
    }
}
