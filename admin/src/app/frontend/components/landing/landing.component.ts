import { Component, OnInit } from '@angular/core';
import { HtmlContentModel } from "../../../admin-app/components/html-content/html-content.model";
import { ActivatedRoute } from '@angular/router';
import { Router, NavigationEnd } from '@angular/router';
import { HobSeoService } from '../../../shared/services/hobseo.service';
import { ScrollToTopService } from '../../../shared/services/scroll-to-top.service';

@Component({
    selector: 'landing',
    templateUrl: './landing.html'
})

export class LandingComponent implements OnInit{

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private hobSeoService: HobSeoService, private scroll: ScrollToTopService) {
        
    }
    
    ngOnInit() {
        this.hobSeoService.setTitle('Hob Khadka | Official Website');
        this.scroll.scrollTop();
    }
}

