import { Component, OnInit } from '@angular/core';
import {ImgAlbumResponse} from "../../../admin-app/components/imgGallery/img-gallery.model";
import {ImgGalleryService} from "../../../admin-app/components/imgGallery/img-gallery.service";
import {ActivatedRoute} from "@angular/router";
import 'rxjs/add/operator/switchMap';
import { Config } from '../../../shared/configs/general.config';
import { ScrollToTopService } from '../../../shared/services/scroll-to-top.service';
import { HobSeoService } from '../../../shared/services/hobseo.service';

@Component({
    selector: 'image-gallery',
    templateUrl: './image-gallery.component.html',
    styleUrls: ['./light-box.css']
})

export class ImageGalleryComponent implements OnInit{
  index: number = 1;
  galleryList: ImgAlbumResponse;
  currentPage: number = 1;
  perPage: number = 6;
  pageLimit: number;
  totalItems: number;
  albumId: string;
  albumName: string;
  showGallery: boolean = false;
  
  constructor(private objService: ImgGalleryService, private activatedRoute: ActivatedRoute, private scroll: ScrollToTopService, private hobSeoService: HobSeoService){}

  ngOnInit() {
    this.hobSeoService.setTitle("Image Gallery");
    this.activatedRoute.queryParams
      .switchMap((params: any) =>
        this.objService.getAllAlbumWithCoverImage(this.perPage, this.currentPage = parseInt(typeof params['page'] != 'undefined' ? params['page'] : 1)))
      .subscribe(resEvent => this.bindImages(resEvent));
  }

  bindImages(objRes: ImgAlbumResponse){
    this.scroll.scroll130();
    this.galleryList = objRes;
    this.totalItems = objRes.totalItems;
    this.pageLimit = Math.ceil(this.totalItems / this.perPage);
    this.galleryList.dataList = this.galleryList.dataList.map(album => {
      album.image.imageName = this.sourceAlbumImage(album.image.imageName);
      return album;
    });
  }

  sourceAlbumImage(image: string) {
    return Config.Cloudinary.url(image, {height: 219, width: 350, crop:'scale'});
  }

  increaseIndex() {
    if ((this.pageLimit - 2) > this.index)
      this.index++;
  }

  decreaseIndex() {
    if (this.index > 1)
      this.index--;
  }

  showGalleryFunction(id: string, albumName: string) {
    this.albumId = id;
    this.albumName = albumName;
    this.showGallery = true;
  }

  GalleryImageCloseButton() {
    this.showGallery = false;
  }
}