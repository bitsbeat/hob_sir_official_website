import { ImageGalleryRouting } from './image-gallery.route';
import { ImgGalleryService } from './../../../admin-app/components/imgGallery/img-gallery.service';
import { ImageGalleryComponent } from './image-gallery.component';
import { ImageLightBoxComponent } from './image-light-box.component';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { ScrollToTopService } from '../../../shared/services/scroll-to-top.service';

@NgModule({
  imports: [SharedModule, ImageGalleryRouting],
  declarations: [ ImageGalleryComponent, ImageLightBoxComponent],
  exports: [ImageGalleryComponent, ImageLightBoxComponent],
  providers: [ImgGalleryService, ScrollToTopService]
})

export class ImageGalleryModule {}
