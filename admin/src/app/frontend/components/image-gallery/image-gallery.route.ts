import { ImageGalleryComponent } from './image-gallery.component';
import { FrontendComponent } from './../../frontend.component';
import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";

export const ImageGalleryRoute: Routes = [
  {path: '',
  // {path: 'detail', component: BlogDetailComponent}
    children: [
      {path: '', component:ImageGalleryComponent},
    ]
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(ImageGalleryRoute)
  ],
  exports: [
    RouterModule
  ]
})
export class ImageGalleryRouting {
}
