import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ImgGalleryService} from '../../../admin-app/components/imgGallery/img-gallery.service';
import {ImgGalleryResponse} from '../../../admin-app/components/imgGallery/img-gallery.model';
import {Config} from '../../../shared/configs/general.config';
import { HobSeoService } from '../../../shared/services/hobseo.service';

@Component({
    selector: 'image-light-box',
    templateUrl: './image-light-box.component.html',
    styleUrls: ['./light-box.css']
})
export class ImageLightBoxComponent implements OnInit{
  @Output() showGalleryImages: EventEmitter<boolean> = new EventEmitter();
  @Input() albumId: string;
  @Input() albumName: string;
  index = 0;
  imgListResponse: ImgGalleryResponse;

  constructor(private galleryService: ImgGalleryService, private hobSeoService: HobSeoService) {}

  ngOnInit() {
    this.hobSeoService.setTitle("Image Gallery | " + this.albumName);
    this.getAlbumImgList();
    window.addEventListener('click', (event: any) => {
      if (event.target.className.indexOf('bg-overlay') == 0) {
        this.hideGallery();
      }
    });
  }

  getAlbumImgList() {
    this.galleryService.getAlbumImgList(this.albumId)
      .subscribe(objRes => this.bindList(objRes));
  }

  bindList(objRes: ImgGalleryResponse) {
    this.imgListResponse = objRes;
    this.imgListResponse.dataList = this.imgListResponse.dataList.map((image) => {
      image.imageName = this.getSource(image.imageName);
      return image;
    });
  }

  getSource(image: string) {
    return Config.Cloudinary.url(image, {height: 600, width: 888, crop: 'limit'});
  }

  increaseIndex() {
    if (this.index < this.imgListResponse.dataList.length - 1)
      this.index++;
  }

  decreaseIndex() {
    if (this.index > 0)
      this.index--;
  }

  hideGallery() {
    const isCanceled = true;
    this.showGalleryImages.emit(isCanceled);
  }
}