import { Component } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ContactService } from '../../../admin-app/components/contact/contact.service';
import { ValidationService } from '../../../shared/services/validation.service';
import { Message } from 'primeng/primeng';

@Component({
    selector: 'contact',
    templateUrl: './contact.component.html'
})

export class ContactComponent {
    showContactStatus: boolean = false;
    contactForm: FormGroup;
    isSubmitted: boolean = false;
    msgs: Message[] = [];

    constructor(private formBuilder: FormBuilder, private contactService: ContactService) {
        this.contactForm = formBuilder.group({
            fullName: ['', Validators.required],
            email: ['', Validators.compose([Validators.required, ValidationService.emailValidator])],
            subject: ['', Validators.required],
            message: ['', Validators.required]
        });
    }

    onSubmit() {
        this.isSubmitted = true;
        if (this.contactForm.valid) {
            this.contactService.saveContactMsg(this.contactForm.value)
                .subscribe(data => {
                    this.showContact();
                    this.afterSubmit(data.message, true);
                    this.contactForm.reset();
                },
                error => {
                    this.afterSubmit(error.message, false);
                });
        }
    }

    afterSubmit(res, data) {
        this.isSubmitted = false;
        this.msgs = [];
        this.msgs.push({
            severity: data == true ? 'success' : 'error',
            summary: data == true ? 'Success!' : 'Error!',
            detail: res
        });
    }

    showContact() {
        if (this.showContactStatus != false) {
            this.isSubmitted = false;
            this.contactForm.reset();
        }
        this.showContactStatus = !this.showContactStatus;
        window.addEventListener('click', (event: any) => {
            if (event.target.className.indexOf('bg-contact') == 0) {
                this.showContactStatus = false;
            }
        })
    }
}

