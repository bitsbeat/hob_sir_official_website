import {NgModule} from "@angular/core";
import {ContactComponent} from "./contact.component";
import {ContactService} from "../../../admin-app/components/contact/contact.service";
import {SharedModule} from "../../../shared/shared.module";
// import {GrowlModule} from 'primeng/primeng';

@NgModule({
  imports: [SharedModule],
  declarations: [ContactComponent],
  exports: [ContactComponent],
  providers:[ContactService]
})

export class ContactModule{}
