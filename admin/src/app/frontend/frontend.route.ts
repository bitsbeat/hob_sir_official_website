import { BiographyComponent } from './components/biography/biography.component';
import { AllnewsComponent } from './components/all-news/all-news.component';
import { ContactComponent } from './components/contact/contact.component';
import { Routes, RouterModule} from '@angular/router';
import { NgModule } from "@angular/core";
import { FrontendComponent } from "./frontend.component";
import { LandingComponent } from "./components/landing/landing.component";
import { PortfolioComponent } from '../frontend/components/portfolio/portfolio.component';

export const FrontendRoute: Routes = [
  {
    path: '', component: FrontendComponent,
    children: [
        {path: '', component: LandingComponent},
        {path: 'contact', component: ContactComponent},
        {path: 'biography', component: BiographyComponent},
        {path: 'portfolio', component: PortfolioComponent},
        {
          path: 'news', loadChildren: 'app/frontend/components/all-news/all-news.module#AllNewsModule'
        },
        {
          path: 'video', loadChildren: 'app/frontend/components/video-gallery/video-gallery.module#VideoGalleryModule'
        },
        {
          path: 'image-gallery', loadChildren: 'app/frontend/components/image-gallery/image-gallery.module#ImageGalleryModule'
        },
        {
         path: 'blog', loadChildren: 'app/frontend/components/blog/blog.module#BlogModule'
        }
    ]
  }
];

@NgModule({
  imports: [
  RouterModule.forChild(FrontendRoute)
  ],
  exports: [
    RouterModule
  ]
})
export class FrontendRouting {
}
