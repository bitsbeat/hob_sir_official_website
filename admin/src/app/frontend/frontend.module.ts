import { BiographyComponent } from './components/biography/biography.component';
import { VideoGalleryModule } from './components/video-gallery/video-gallery.module';
import { ImageGalleryModule } from './components/image-gallery/image-gallery.module';
import { GalleryComponent } from './components/gallery/gallery.component';
import { MymissionComponent } from './components/my-mission/my-mission.component';
import { AboutComponent } from './components/about/about.component';
import { QuoteComponent } from './components/quote/quote.component';
import { NavComponent } from './components/nav/nav.component';
import { HtmlContentService } from './../admin-app/components/html-content/html-content.service';
import { HtmlContentModule } from './../admin-app/components/html-content/html-content.module';
import { LandingComponent } from './components/landing/landing.component';
import { NgModule } from '@angular/core';
import { FrontendComponent } from './frontend.component';
import { FrontendRouting } from './frontend.route';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {BlogModule} from './components/blog/blog.module';
import {OrganizationInfoService} from "../admin-app/components/organization-information/orginfo.service";
import {LatestNewsModule} from "./components/latest-news/latest-news.module";
import { AllNewsModule } from './components/all-news/all-news.module';
import { PersonInfoComponent } from './components/person-info/person-info.component';
import { ContactModule } from './components/contact/contact.module';
import { TwitterService } from '../shared/services/twitter.service';
import { MomentModule } from 'angular2-moment';
import { PortfolioComponent } from './components/portfolio/portfolio.component';
import { PartnerService } from '../admin-app/components/partner/partner.service';
import { QuoteModule } from './components/quote/quote.module';

@NgModule({
  imports: [
  FrontendRouting,
    HtmlContentModule,
    CommonModule,
    BlogModule,
    LatestNewsModule,
    ImageGalleryModule,
    VideoGalleryModule,
    AllNewsModule,
    ContactModule,
    MomentModule,
    QuoteModule,
    SharedModule.forRoot(),
  ],
  declarations: [FrontendComponent, LandingComponent, NavComponent, AboutComponent, MymissionComponent, GalleryComponent, PersonInfoComponent, BiographyComponent, PortfolioComponent],
  providers: [HtmlContentService, OrganizationInfoService, TwitterService, PartnerService]
})

export class FrontendModule {
}
