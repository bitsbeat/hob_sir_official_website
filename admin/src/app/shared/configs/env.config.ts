let URL: string = 'http://localhost:3002';

if (typeof window != "undefined")
   URL = window.location.protocol + "//" + window.location.host;

export const HOST_URL: string = URL;
export const API_URL: string = URL + "/api/";
export const JSON_URL: string = URL + '/data/';
