export var menuItem = [
  {
    headerTitle: null,
    icon: "",
    route: "",
    menuItem: [
      // {
      // route: "user-management",
      // title: "User",
      // icon: "fa-user"
      // },
      // {
      //   route: "role",
      //   title: "Role",
      //   icon: "fa-shield"
      // },
      // {
      //   route: "access",
      //   title: "Api Access",
      //   icon: "fa-user"
      // },
      // {
      //   route: "querylist",
      //   title: "Query List",
      //   icon: "fa-user"
      // },
      // {
      //   route: "admission-overlay",
      //   title: "Admission Banner",
      //   icon: "fa-file"
      // },
      // {
      //   route: "list",
      //   title: "Applicant List",
      //   icon: "fa-list"
      // },
      {
        route: "blog-management",
        title: "Blog",
        icon: "fa-newspaper-o"
      },
      {
        route: "contact",
        title: "Contact List",
        icon: "fa-list"
      },
      // {
      //   route: "course",
      //   title: "Course Management",
      //   icon: "fa-book"
      // },
      // {
      //   route: "alumni",
      //   title: "DAV Alumni",
      //   icon: "fa-user"
      // },
      // {
      //   route: "document-management",
      //   title: "Document Mgmt",
      //   icon: "fa fa-file"
      // },
      {
        route: "email-template",
        title: "Email Template",
        icon: "fa-envelope"
      },
      // {
      //   route: "event",
      //   title: "Event Management",
      //   icon: "fa-calendar"
      // },
      // {
      //   route: "event-registration",
      //   title: "Event Registration",
      //   icon: "fa-list-ol"
      // },
      // {
      //   route: "errorlog",
      //   title: "Error Log",
      //   icon: "fa-stack-overflow"
      // },
      {
        route: "html",
        title: "Html Content",
        icon: "fa-html5"
      },
      {
        //route: "imagegallery",
        route: "img-gallery",
        title: "Image Gallery",
        icon: "fa-picture-o"
      },
      // {
      //   route: "img-slider",
      //   title: "Image Slider",
      //   icon: "fa-play"
      // },
      {
        route: "news-section",
        title: "News",
        icon: "fa-newspaper-o"
      },
      {
        route: "quote",
        title: "Quote",
        icon: "fa fa-quote-left"
      },
      // {
      //   route: "newsletter",
      //   title: "Newsletter",
      //   icon: "fa-file-text-o"
      // },
      {
        route: "partner",
        title: "Partners",
        icon: "fa-link"
      },
      // {
      //   route: "team-management",
      //   title: "Team Management",
      //   icon: "fa-users"
      // },
      // {
      //   route: "testimonial",
      //   title: "Student Speak",
      //   icon: "fa-comment-o"
      // },
      // {
      //   route: "token",
      //   title: "Token",
      //   icon: "fa-ticket"
      // },
      {
        route: "videoGallery",
        title: "Video Gallery",
        icon: "fa-video-camera"
      }]
  },
  {
    headerTitle: "Settings",
    route: "",
    icon: "fa-wrench",
    menuItem: [{
      route: "cloudinary",
      title: "Cloudinary",
      icon: "fa-cloud"
    },
      {
        route: "comment",
        title: "Comments",
        icon: "fa-commenting-o"
      },
      {
        route: "email-service",
        title: "Email Service",
        icon: "fa-cogs"
      },
      {
        route: "analytics",
        title: "Google Analytics",
        icon: "fa-line-chart"
      },

      // {
      //   route: "googlemap",
      //   title: "Google Map",
      //   icon: "fa-map-o"
      // },
      {
        route: "organization",
        title: "Org. Information",
        icon: "fa-building-o"
      }
    ]
  }
];
