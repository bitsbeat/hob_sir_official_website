'user strict';
import { Component, OnInit } from '@angular/core';
import { SpinnerComponent } from './spinner.component';

@Component({
  selector: 'ajax-overlay-spinner',
  templateUrl: './spinner.html' ,
  styleUrls: ['./spinner.css']
})
export class SpinnerOverlayComponent extends SpinnerComponent implements OnInit{
  ngOnInit() {
    this.fixedPosition = true;
    this.bgOverlay     = true;
    this.topMargin     = true;
    this.detectAjax();
  }
}
