/**
 * Created by sanedev on 6/19/16.
 */
'use strict';

import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import{HOST_URL}  from '../../configs/env.config';
@Component({
  selector: 'ajax-spinner',
  templateUrl: './spinner.html' ,
  styleUrls: ['./spinner.css']
})

export class SpinnerComponent implements OnDestroy,OnInit {
  protected currentTimeout: any;
  protected isDelayedRunning: boolean = false;
  showSpinner: boolean = false;
  hide: boolean = true;
  bgOverlay: boolean = false;
  fixedPosition: boolean = false;
  topMargin: boolean = false;

  constructor() {
  }

  ngOnInit() {
    this.detectAjax();
  }

  @Input()
  public delay: number = 300;

  @Input()
  public set isRunning(value: boolean) {
    if (!value) {
      this.cancelTimeout();
      this.isDelayedRunning = false;
    }

    if (this.currentTimeout) {
      return;
    }

    this.currentTimeout = setTimeout(() => {
      this.isDelayedRunning = value;
      this.cancelTimeout();
    }, this.delay);
  }

  detectAjax() {
    let origOpen = XMLHttpRequest.prototype.open;
    let that = this;
    XMLHttpRequest.prototype.open = function (method, url, async) {
      if (url.indexOf(HOST_URL + '/api') != -1)
        that.showSpinner = true;
      that.hide = false;
      this.addEventListener('load', function () {
        // console.log('request completed!');
        // console.log(this.readyState); //will always be 4 (ajax is completed successfully)
        // console.log(this.responseText); //whatever the response was
        setTimeout(()=>
            that.showSpinner = false
          , 500
        );
        setTimeout(()=>
          that.hide = true, 200
        );
      });
      origOpen.apply(this, arguments);
      //  this.setRequestHeader("Auth","123");
    };


  }


  private cancelTimeout(): void {
    clearTimeout(this.currentTimeout);
    this.currentTimeout = undefined;
  }

  ngOnDestroy(): any {
    this.cancelTimeout();
  }
}
