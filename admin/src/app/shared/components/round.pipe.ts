import { Pipe } from "@angular/core";

// tslint:disable-next-line:use-pipe-transform-interface
@Pipe({
    name: 'round'
})

export class RoundPipe {
    transform(value: number) {
        let roundedValue = Math.round(value) == 0 ? 1: Math.round(value);
        return roundedValue;
    }
}
