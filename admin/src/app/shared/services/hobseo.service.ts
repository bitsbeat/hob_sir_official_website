import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Meta } from '@angular/platform-browser';

@Injectable()

export class HobSeoService{
  titleService: Title;
  metaService: Meta;

  constructor(titleService: Title, private meta: Meta){
    this.titleService = titleService;
    this.metaService = meta;
  }

  setTitle(newTitle: string){
    this.titleService.setTitle(newTitle);
  }

  setMetaData(title: string, type: string, url: string, summary: string, image?: string){
    this.metaService.addTags([
      {property: 'og:title', content: title},
      {property: 'og:type', content: type},
      {property: 'og:url', content: url},
      {property: 'og:description', content: summary},
      {property: 'og:site_name', content: 'DAV MBA School'},
      {itemprop: 'description', content: summary}
    ]);
    if(image){
      this.metaService.addTag({property: 'og:image', content: image});
    }
  }

  setTwitterCard(title: string, card: string, summary: string, image?:string){
    this.metaService.addTags([
      {property: 'twitter:title', content: title},
      {property: 'twitter:card', content: card},
      {property: 'twitter:description', content: summary},
    ]);
    if(image)
      this.metaService.addTag({property: 'twitter:image:src', content: image});
  }
}
