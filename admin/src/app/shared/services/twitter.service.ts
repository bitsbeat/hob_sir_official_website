import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { API_URL } from "../configs/env.config";

@Injectable()

export class TwitterService {
    twitterApiRoute: string = "twitter";

    constructor(private http: Http) {

    }

    getTwitterPosts(): any {
        return this.http.get(API_URL + this.twitterApiRoute)
            .map(res => res.json());
    }
}
