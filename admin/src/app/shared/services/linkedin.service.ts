import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { RequestOptions, Headers } from '@angular/http';
import {Observable} from "rxjs/Observable";

@Injectable()

export class LinkedInService {
    shareUrl: string = "https://api.linkedin.com/v1/people/~/shares?format=json";

    constructor(private http: Http) {

    }

    postLink(shareContent: any): Observable<any> {
        let body = JSON.stringify(shareContent);
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.shareUrl, body, options)
            .map(res => <any>res.json())
            .catch(this.handleError);
    } 

    handleError(error) {
        console.log(error.json());
        return Observable.throw(error.json() || 'server error');
    }
}