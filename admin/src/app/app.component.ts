import { Config } from './shared/configs/general.config';
import { CloudinaryService } from './admin-app/components/cloudinary/cloudinary.service';
import {Component} from '@angular/core';

@Component({
    selector: 'app',
    templateUrl: './app.html'
})
export class AppComponent {

    constructor(private cloudinaryService:CloudinaryService) {

    }

    ngOnInit() {
        this.setCloudinaryName();
    }

    setCloudinaryName() {
        this.cloudinaryService.getCloudinarySettings()
            .subscribe(res=>Config.setCloudinary(res.cloudinaryCloudName),
                err=>this.handleErrorMsg(err));
    }

    handleErrorMsg(res:any) {
        console.log(res.message);
    }

}
