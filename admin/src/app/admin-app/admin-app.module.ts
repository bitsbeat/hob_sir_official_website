import { EventRegistrationModule } from './components/event-registration/event-registration.module';
import { ImgGalleryModule } from './components/imgGallery/img-gallery.module';
import { DocumentManagementModule } from './components/document-management/document-management.module';
import { ImgSliderModule } from './components/imgSlider/imgSlider.module';
import {NgModule} from '@angular/core';
import {ApplicationLogModule} from './components/application-log/application-log.module';
import{BlogModule} from './components/blog/blog.module';

import{CloudinaryModule} from './components/cloudinary/cloudinary.module';
import{CommentSettingModule} from './components/comment-setting/comment-setting.module';
import{ContactModule} from './components/contact/contact.module';
import{DashboardModule} from './components/dashboard/dashboard.module';
import{EmailServiceModule} from './components/email-service/email-service.module';
import{EmailTemplateModule} from './components/email-template/email-template.module';
import{EventManagementModule} from './components/event-management/event-managment.module';
import{GoogleAnalyticsModuless} from './components/google-analytics/google-analytics.module';
import {GoogleMapModule} from './components/google-map/google-map.module';
import{HtmlContentModule} from './components/html-content/html-content.module';
import{NewsModule}from'./components/news/news.module';
import{OrganizationInformationModule} from './components/organization-information/organization-information.module';
import{TeamManagementModule} from './components/team-management/team-management.module';
import{TestimonialModule} from './components/testimonial/testimonial.module';
import{UserManagementModule} from './components/user-management/user-managment.module';
import{UserProfileModule} from './components/user-profile/user-profile.module';
import{AdminAppComponent} from './admin-app.component';
import{adminAppRouting} from './admin-app.route';
import {SidebarCmp} from './components/sidebar/sidebar'
import {TopNavCmp}from './components/topnav/topnav';
import {SharedModule} from "../shared/shared.module";
import {PartnerModule} from "./components/partner/partner.module";
import {CountryListService} from "../shared/services/countrylist.service";
import {RoleModule} from "./components/role-management/role.module";
import {ApiAccessModule} from "./components/api-access/api-access.module";
import {TokenModule} from "./components/token-management/token-management.module";
import {QueryListModule} from "./components/query-section/query.module";
import { CourseManagementModule } from './components/course-management/course-management.module';
import {NoticeModule} from "./components/notice/notice.module";
import { AdmissionModule } from './components/admissionform/admission.module';
import {NewsletterModule} from "./components/newsletter/newsletter.module";
import {AdmissionOverlayModule} from "./components/admission-overlay/admission-overlay.module";
import {AlumniModule} from "./components/alumni/alumni.module";
import { VideoGalleryModule } from '../frontend/components/video-gallery/video-gallery.module';

@NgModule({
  imports: [
    adminAppRouting,
    ApplicationLogModule,
    BlogModule,
    CloudinaryModule,
    CommentSettingModule,
    ContactModule,
    DashboardModule,
    EmailServiceModule,
    EmailTemplateModule,
    GoogleAnalyticsModuless,
    HtmlContentModule,
    ImgGalleryModule,
    NewsModule,
    OrganizationInformationModule,
    PartnerModule,
    UserManagementModule,
    UserProfileModule,
    RoleModule,
    ApiAccessModule,
    TokenModule,
    VideoGalleryModule,
    SharedModule.forRoot()
  ],
  declarations: [AdminAppComponent, SidebarCmp, TopNavCmp],
  providers: [CountryListService]

})

export class AdminAppModule {
}
