import { Component } from '@angular/core';

@Component({
    selector: 'testimonial',
    template: '<router-outlet></router-outlet>'
})

export class TestimonialComponent{

}