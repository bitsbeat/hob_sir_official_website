import { ActivatedRoute } from '@angular/router';
import {Component, EventEmitter, Output, Input, AfterViewInit, ViewChild, OnInit} from '@angular/core';
import {FormControlMessages} from "../../../shared/components/control-valdation-message.component";
import {TestimonialModel} from "./testimonial.model";
import {TestimonialService} from "./testimonial.service";
import{Config} from "../../../shared/configs/general.config";
import{ImageCanvasSizeEnum} from "../../../shared/configs/enum.config";
import {ValidationService} from "../../../shared/services/validation.service";
import {ImageUploader} from "../../../shared/components/image-uploader.component";
import {Validators, FormBuilder, FormGroup, FormControl} from "@angular/forms";
import { Location } from "@angular/common";
@Component({
  selector: 'testimonial-editor',
  templateUrl: './testimonial-editor.html'
  // styles: [style]
})
export class TestimonialEditorComponent implements OnInit,AfterViewInit {
  objTestimonial:TestimonialModel = new TestimonialModel();
  testimonialId:string;
  //@Output() showListEvent:EventEmitter<any> = new EventEmitter();
  testimonialForm:FormGroup;
  isSubmitted:boolean = false;

  /* Image Upload Handle*/
  imageDeleted:boolean = false;
  file:File;
  fileName:string = "";
  drawImagePath:string = Config.DefaultAvatar;
  imageFormControl:FormControl = new FormControl('', Validators.required);
  canvasSize:number = ImageCanvasSizeEnum.small;
  /* End Image Upload handle */

  constructor(private _objService:TestimonialService, private _formBuilder:FormBuilder, private location: Location, private activatedRoute: ActivatedRoute) {
    this.objTestimonial.testimonialDate = new Date().toLocaleDateString();
    this.testimonialForm = _formBuilder.group({
      "personName": ['', Validators.required],
      "organization": ['', Validators.required],
      "testimonialContent": ['', Validators.required],
      "email": ['', ValidationService.emailValidator],
      "imageFormControl": this.imageFormControl,
      "designation": [''],
      "facebookURL": ['', ValidationService.urlValidator],
      "twitterURL": ['', ValidationService.urlValidator],
      "gPlusURL": ['', ValidationService.urlValidator],
      "linkedInURL": ['', ValidationService.urlValidator],
      "active": ['']
    });
    activatedRoute.params.subscribe(params => this.testimonialId = params['id']);
  }

  ngAfterViewInit() {
    if (!this.testimonialId)
      this.drawImageToCanvas(Config.DefaultAvatar);
  }

  ngOnInit() {
    if (this.testimonialId)
      this.getTestimonialDetail();
  }

  getTestimonialDetail() {
    this._objService.getTestimonialDetail(this.testimonialId)
      .subscribe(res =>this.bindDetail(res),
        error => this.errorMessage(error));
  }

  bindDetail(objRes:TestimonialModel) {
    this.objTestimonial = objRes;
    this.objTestimonial.testimonialDate = new Date(this.objTestimonial.testimonialDate).toLocaleDateString();
    this.testimonialForm.setValue({
      "personName": objRes.personName,
      "organization": objRes.organization,
      "testimonialContent": objRes.testimonialContent,
      "email": objRes.email,
      "imageFormControl": objRes.imageName,
      "designation": objRes.designation,
      "facebookURL": objRes.facebookURL,
      "twitterURL": objRes.twitterURL,
      "gPlusURL": objRes.gPlusURL,
      "linkedInURL": objRes.linkedInURL,
      "active": objRes.active
    });
    this.objTestimonial.imageName = objRes.imageName;
    this.objTestimonial.imageProperties = objRes.imageProperties;
    this.fileName = objRes.imageName;
    (<FormControl>this.testimonialForm.controls['imageFormControl']).patchValue(this.fileName);
    let path:string = "";
    if (this.fileName) {
      var cl = Config.Cloudinary;
      path = cl.url(this.fileName);
    }
    else
      path = Config.DefaultAvatar;
    this.drawImageToCanvas(path);
  }


  saveTestimonial() {
    this.isSubmitted = true;
    (<FormControl>this.testimonialForm.controls['imageFormControl']).patchValue(this.fileName);
    if (this.testimonialForm.valid) {
      if (!this.testimonialId) {
        this._objService.saveTestimonial(this.testimonialForm.value, this.file)
          .subscribe(res => this.resStatusMessage(res),
            error => this.errorMessage(error));
      }
      else {
        this._objService.updateTestimonial(this.testimonialId, this.testimonialForm.value, this.file, this.imageDeleted)
          .subscribe(res => this.resStatusMessage(res),
            error => this.errorMessage(error));
      }
    }
  }

  resStatusMessage(objSave:any) {
    swal("Success !", objSave.message, "success")
    this.location.back();
  }

  triggerCancelForm() {
    this.location.back();
  }

  errorMessage(objResponse:any) {
    swal("Alert !", objResponse.message, "info");
  }

  /*Image handler */

  deleteImage(id:string) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Image !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      ()=> {
        this._objService.deleteImage(this.objTestimonial.imageName, this.objTestimonial.imageProperties.imageExtension, this.objTestimonial.imageProperties.imagePath)
          .subscribe(res=> {
              this.imageDeleted = true;
              this.objTestimonial.imageName = "";
              this.fileName = "";
              this.drawImageToCanvas(Config.DefaultAvatar);
              swal("Deleted!", res.message, "success");
            },
            error=> {
              swal("Alert!", error.message, "info");

            });
      });
  }

  changeFile(args) {
    this.file = args;
      this.fileName = this.file.name;
  }

  drawImageToCanvas(path:string) {
    this.drawImagePath = path;
  }

  /* End ImageHandler */
}

