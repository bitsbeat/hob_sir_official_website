import { TestimonialListComponent } from './testimonial-list.component';
import {NgModule}      from '@angular/core';
import {TestimonialService} from "./testimonial.service";
import {TestimonialEditorComponent} from"./testimonial-editor.component";
import {TestimonialComponent} from"./testimonial.component";

import {SharedModule} from '../../../shared/shared.module';

@NgModule({
    imports: [SharedModule],
    declarations: [TestimonialComponent, TestimonialListComponent, TestimonialEditorComponent],
    providers: [TestimonialService],
    exports: [TestimonialComponent, TestimonialListComponent, TestimonialEditorComponent]
})

export class TestimonialModule {
}