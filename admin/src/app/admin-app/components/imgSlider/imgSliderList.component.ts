import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { ImgSliderResponse } from "./imgSlider.model";
import { ImgSliderModel } from "./imgSlider.model";
import { ImgSliderService } from "./imgSlider.service";
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'img-list',
  templateUrl: './imgSliderList.component.html'
})

export class ImgSliderListComponent implements OnInit {
  objListResponse: ImgSliderResponse = new ImgSliderResponse();
  error: any;
  //showForm: boolean = false;
  sliderId: string;

  perPage: number = 10;
  currentPage: number = 1;
  totalPage: number = 1;
  first: number = 0;
  bindSort: boolean = false;
  preIndex: number = 0;

  ngOnInit() {
    this.getImgSliderList();
  }

  constructor(private _objService: ImgSliderService, private router: Router, private location: Location) { }

  getImgSliderList() {
    this._objService.getImageSliderList(this.perPage, this.currentPage)
      .subscribe(res => this.bindList(res),
      error => this.errorMessage(error));
  }

  errorMessage(objResponse: any) {
    swal("Alert!", objResponse.message, "info");
  }

  bindList(objRes: ImgSliderResponse) {
    this.objListResponse = objRes;
        this.preIndex = (this.perPage * (this.currentPage - 1));

        if (objRes.dataList.length > 0) {
            let totalPage = objRes.totalItems / this.perPage;
            this.totalPage = totalPage > 1 ? Math.ceil(totalPage) : 1;

            /*End Pagination */
            if (!this.bindSort) {
                this.bindSort = true;
                this.sortTable();
            }
            else
                jQuery("table").trigger("update", [true]);
        }
  }

  sortTable() {
     setTimeout(() => {
            jQuery('.tablesorter').tablesorter({
                headers: {
                    2: { sorter: false },
                    3: { sorter: false }
                }
            });
        }, 50);
  }


  edit(id: string) {
    this.router.navigate(['/admin/img-slider/img-slider-editor', id]);
  }

  addImage() {
    this.router.navigate(['/admin/img-slider/img-slider-editor']);
  }

  delete(id: string) {
    swal({
      title: "Are you sure?",
      text: "You will not be able to recover this Image !",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
      () => {
        let objSlider: ImgSliderModel = new ImgSliderModel();
        objSlider._id = id;
        objSlider.deleted = true;
        this._objService.deleteImageSlider(objSlider)
          .subscribe(res => {
            this.getImgSliderList();
            swal("Deleted!", res.message, "success");
          },
          error => {
            swal("Alert!", error.message, "info");

          });
      });

  }

  pageChanged(event) {
        this.perPage = event.rows;
        this.currentPage = (Math.floor(event.first / event.rows)) + 1;
        this.first = event.first;
        if (event.first == 0)
            this.first = 1;
        this.getImgSliderList();
    }
}
