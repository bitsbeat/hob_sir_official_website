import { Routes, RouterModule } from '@angular/router';
import {ImgSliderListComponent} from "./imgSliderList.component";
import {ImgSliderEditorComponent} from "./imgSliderEditor.component";

export const ImgSliderRoutes: Routes = [
  {
    path: 'avsek',
    children: [
      {path: '', component: ImgSliderListComponent},
      {path: 'img-slider-editor', component:ImgSliderEditorComponent},
      {path: 'img-slider-editor/:id', component: ImgSliderEditorComponent}
    ]
  }
]
