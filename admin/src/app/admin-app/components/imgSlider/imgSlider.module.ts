import { ImgSliderComponent } from './imgSlider.component';
import {NgModule} from '@angular/core';
import {ImgSliderEditorComponent} from"./imgSliderEditor.component";
import {ImgSliderService} from"./imgSlider.service";
import { RouterModule } from '@angular/router';
import {SharedModule} from '../../../shared/shared.module';
import {ImgSliderListComponent} from "./imgSliderList.component";
import {XhrService} from "../../../shared/services/xhr.service";


@NgModule({
  imports: [SharedModule, RouterModule],
  declarations: [ImgSliderEditorComponent,
    ImgSliderListComponent, ImgSliderComponent
  ],
  providers: [ImgSliderService, XhrService],
  exports: [ImgSliderEditorComponent, ImgSliderListComponent, ImgSliderComponent ]
})

export class ImgSliderModule {

}
