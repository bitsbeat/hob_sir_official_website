import { Component } from '@angular/core';

@Component({
    selector: 'img-slider',
    template: '<router-outlet></router-outlet>'
})

export class ImgSliderComponent {

}
