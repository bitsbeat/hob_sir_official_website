import { Component, EventEmitter, Output, Input, AfterViewInit, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { ImgSliderModel } from "./imgSlider.model";
import { Config } from "../../../shared/configs/general.config";
import { ImageCanvasSizeEnum } from "../../../shared/configs/enum.config";
import { ImgSliderService} from "./imgSlider.service";
import { Router, ActivatedRoute } from '@angular/router'; 
import { Location } from '@angular/common';

@Component({
  selector: 'img-slider-editor',
  templateUrl: './imgSliderEditor.component.html'
})

export class ImgSliderEditorComponent implements OnInit, AfterViewInit {
  objSlider: ImgSliderModel = new ImgSliderModel();
  sliderId: string;
  //@Output() showSliderListEvent: EventEmitter<any> = new EventEmitter();
  imgSliderForm: FormGroup;
  isSubmitted: boolean = false;

  /* Image Upload Handle*/
  imageDeleted: boolean = false;
  file: File;
  fileName: string = "";
  drawImagePath: string = Config.DefaultImage;
  imageFormControl: FormControl = new FormControl('', Validators.required);
  canvasSize: number = ImageCanvasSizeEnum.small;
  /* End Image Upload handle */

  constructor(private _objService: ImgSliderService, private _formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private location: Location) {
    this.imgSliderForm = _formBuilder.group({
      "imageTitle": ['', Validators.required],
      "imageAltText": ['', Validators.required],
      "imagePrimaryContent": [''],
      "imageSecondaryContent": [''],
      "active": [''],
      "imageFormControl": this.imageFormControl
    });
    activatedRoute.params.subscribe(params => this.sliderId = params['id']);
  }

  ngAfterViewInit() {
    if (!this.sliderId)
      this.drawImageToCanvas(Config.DefaultImage);
  }

  ngOnInit() {
    if (this.sliderId)
      this.getImageDetail();
  }

  drawImageToCanvas(path: string) {
    this.drawImagePath = path;
  }

  getImageDetail() {
    this._objService.getImageSliderDetail(this.sliderId)
      .subscribe(res =>this.bindDetail(res),
        error => this.errorMessage(error));
  }

  bindDetail( objRes: ImgSliderModel){
    this.imgSliderForm.setValue({
      "imageTitle": objRes.imageTitle,
      "imageAltText": objRes.imageAltText,
      "imagePrimaryContent": objRes.imagePrimaryContent,
      "imageSecondaryContent": objRes.imageSecondaryContent,
      "active": objRes.active,
      "imageFormControl": objRes.imageName 
    });
    this.objSlider.imageName = objRes.imageName;
    this.objSlider.imageProperties = objRes.imageProperties;
    this.fileName = objRes.imageName;
    (<FormControl>this.imgSliderForm.controls['imageFormControl']).patchValue(this.fileName);
    let path: string="";
    if(this.fileName){
      var cl = Config.Cloudinary;
      path = cl.url(this.fileName);
    }
    else {
      path = Config.DefaultImage;
    }
    this.drawImageToCanvas(path);
  }

  saveImageSlider(){
    this.isSubmitted = true;
    (<FormControl>this.imgSliderForm.controls['imageFormControl']).patchValue(this.fileName);

    if(this.imgSliderForm.valid){
      if(!this.sliderId) {
        this._objService.saveImageSlider(this.imgSliderForm.value, this.file)
          .subscribe(res => this.resStatusMessage(res),
            error => this.errorMessage(error));
      }
      else{
        this._objService.updateImageSlider(this.sliderId,this.imgSliderForm.value, this.file, this.imageDeleted)
          .subscribe(res => this.resStatusMessage(res),
            error => this.errorMessage(error));
      }
    }
  }

  resStatusMessage(objSave: any){
    swal("Success !", objSave.message, "success");
    this.location.back();
  }

  errorMessage(objResponse: any) {
    swal("Alert !", objResponse.message, "info");
  }

  triggerCancelForm() {
    this.location.back();
  }

  changeFile(args) {
    this.file = args;
    this.fileName = this.file.name;
  }

  deleteImage(id: string) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Image !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      ()=> {
        this._objService.deleteImage(this.objSlider.imageName, this.objSlider.imageProperties.imageExtension, this.objSlider.imageProperties.imagePath)
          .subscribe(res=> {
              this.imageDeleted = true;
              this.objSlider.imageName = "";
              this.fileName = "";
              this.drawImageToCanvas(Config.DefaultImage);
              swal("Deleted!", res.message, "success");
            },
            error=> {
              swal("Alert!", error.message, "info");
            });
      });
  }

}
