import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {FileOperrationService} from '../../../shared/services/fileOperation.service';
import {Observable} from "rxjs/Observable";
import { ImgSliderModel, ImgSliderResponse } from './imgSlider.model';
import{ API_URL} from "../../../shared/configs/env.config";
import {XhrService} from "../../../shared/services/xhr.service";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";

@Injectable()

export class ImgSliderService {
  apiRoute:string = "imageslider";
  progressObserver:any;
  progress:any;

  constructor(private _http:Http, private fileService:FileOperrationService, private xhrService: XhrService) {
    this.progress = Observable.create(observer => {
      this.progressObserver = observer
    }).share();
  }

  deleteImage(fileName:string, orgExt:string, path:string):Observable < any > {
    return this.fileService.deleteFile(fileName, orgExt, path, "image");
  }

  getImage():Observable<ImgSliderResponse>{
            return this._http.get('api/imageslider')
            .map(res=><ImgSliderResponse>res.json())
  }

  getAllActiveImageSlider(): Observable <ImgSliderResponse> {
    return this._http.get(API_URL + this.apiRoute + "/active")
      .map(res =><ImgSliderResponse>res.json())
      .catch(this.handleError);
  }

  getImageSliderList(perPage:number, currentPage:number):Observable < ImgSliderResponse> {

    return this._http.get(API_URL + this.apiRoute + "?perpage=" + perPage + "&page=" + currentPage)
      .map(res =><ImgSliderResponse>res.json())
      .catch(this.handleError);
  }

  getImageSliderDetail(sliderId:string):Observable < ImgSliderModel> {
    return this._http.get(API_URL + this.apiRoute + "/" + sliderId)
      .map(res =><ImgSliderModel>res.json())
      .catch(this.handleError);
  }

  saveImageSlider(objSave: ImgSliderModel, file: File): Observable<any> {
    return this.xhrService.xhrRequest<ImgSliderModel,any>('POST', this.apiRoute, 'imageName', objSave, file);
  }

  updateImageSlider(userId: string, objUpdate:ImgSliderModel, file: File, imageDeleted: boolean): Observable<any> {
    return this.xhrService.xhrRequest<ImgSliderModel,any>('PUT', this.apiRoute, 'imageName', objUpdate, file, userId, imageDeleted);
  }

  deleteImageSlider(objSlider:ImgSliderModel):Observable < any> {
    let body = JSON.stringify({});
    return this._http.patch(API_URL + this.apiRoute + "/" + objSlider._id, body)
      .map(res =><any>res.json())
      .catch(this.handleError);
  }


  handleError(error) {
    console.log(error.json());
    return Observable.throw(error.json() || 'server error');
  }
}
