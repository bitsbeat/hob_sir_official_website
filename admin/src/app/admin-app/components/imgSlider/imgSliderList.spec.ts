import { Observable } from 'rxjs/Rx';
import { ImgSliderListComponent } from './imgSliderList.component';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from './../../../shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TestBed, async, inject } from '@angular/core/testing';
import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import {ImgSliderModel} from "./imgSlider.model";
import {ImgSliderService} from "./imgSlider.service";
import { Router } from '@angular/router';

class MockService extends ImgSliderService {
    objModel: ImgSliderModel = new ImgSliderModel();
    objResponse: ImgSliderModel[] =[]; 

    constructor() {
        super(null, null, null);
        this.objModel._id = "123abcd";
        this.objModel.imageName = "newImage";
        this.objModel.active = true;
        this.objModel.imageAltText = "zxcv";
        this.objModel.imagePrimaryContent = "asjfhasjf";
        this.objModel.imageSecondaryContent = "bnasjfhasf";
        this.objModel.imageTitle = "Testing";
        this.objResponse.push(this.objModel);
    } 

    getImgSliderList(): Observable<ImgSliderModel[]> {
        return Observable.of(this.objResponse);
    }

}

describe('ImgComponentList', () => {
    beforeEach(() => {
        TestBed.configureTestingModule  ({
            imports: [BrowserAnimationsModule, SharedModule, RouterTestingModule],
            declarations: [ImgSliderListComponent],
            providers:[         {provide: ImgSliderService, useClass: MockService}
 ]
        });
    });
    beforeEach(async(() => {
        TestBed.compileComponents();
    }));

    it('should get the image slider List', () => {
        var fixture = TestBed.createComponent(ImgSliderListComponent);
        fixture.detectChanges();
        var component = fixture.debugElement.componentInstance;
        expect(component.getImgSliderList()).toContain(Array);

        // expect(mockService.getImgSliderList()).toBe(Array);
    });
})