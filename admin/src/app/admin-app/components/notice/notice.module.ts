import {NgModule} from "@angular/core";
import {SharedModule} from "../../../shared/shared.module";
import {CommonModule} from "@angular/common";
import {NoticeEditorComponent} from "./notice-editor.component";
import {NoticeService} from "./notice.service";
import {NoticeComponent} from "./notice.component";
import { NoticeRouting } from "./notice.route";

@NgModule({
  imports: [SharedModule.forRoot(), CommonModule, NoticeRouting],
  declarations: [NoticeComponent, NoticeEditorComponent],
  exports: [NoticeComponent, NoticeEditorComponent],
  providers: [NoticeService]
})

export class NoticeModule{

}
