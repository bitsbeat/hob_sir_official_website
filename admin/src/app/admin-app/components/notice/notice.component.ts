import {Component, OnInit} from "@angular/core";
import {NoticeResponse, NoticeModel} from "./notice.model";
import {NoticeService} from "./notice.service";
import {Router} from "@angular/router";
import { Location } from "@angular/common";

@Component({
  selector: 'notice',
  templateUrl: './notice.component.html'
})

export class NoticeComponent implements OnInit{
    objNoticeResponse: NoticeResponse = new NoticeResponse();
    error: any;

    perPage: number = 10;
    currentPage: number = 1;
    totalPage: number = 1;
    first: number = 0;
    bindSort: boolean = false;
    preIndex: number = 0;

    ngOnInit() {
      this.getNoticeList();
    }

    constructor(private _objService: NoticeService, private router: Router, private location: Location) { }

    getNoticeList(){
      this._objService.getNotice(this.perPage, this.currentPage)
        .subscribe(res => this.bindNoticeList(res),
        error => this.errorMessage(error));
    }

    errorMessage(objResponse: any) {
      swal("Alert!", objResponse.message, "info");
    }

    bindNoticeList(objRes: NoticeResponse){
      this.objNoticeResponse = objRes;
      this.preIndex = (this.perPage * (this.currentPage - 1));

      if (objRes.dataList.length > 0) {
        let totalPage = <any>objRes.totalItems / this.perPage;
        this.totalPage = totalPage > 1 ? Math.ceil(totalPage) : 1;

        /*End Pagination */
        if (!this.bindSort) {
          this.bindSort = true;
          this.sortTable();
        }
        else
          jQuery("table").trigger("update", [true]);
      }
    }

    addNotice() {
      this.router.navigate(['/admin/quote/quote-editor']);
    }

    edit(id: string){
      this.router.navigate(['/admin/quote/quote-editor', id]);
    }

  delete(id: string) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Notice !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      () => {
        let objNotice: NoticeModel = new NoticeModel();
        objNotice._id = id;
        objNotice.deleted = true;
        this._objService.deleteNotice(objNotice)
          .subscribe(res => {
              this.getNoticeList();
              swal("Deleted!", res.message, "success");
            },
            error => {
              swal("Alert!", error.message, "info");

            });
      });
  }

    sortTable() {
      setTimeout(() => {
        jQuery('.tablesorter').tablesorter({
          headers: {
            2: { sorter: false },
            3: { sorter: false }
          }
        });
      }, 50);
    }

    pageChanged(event) {
      this.perPage = event.rows;
      this.currentPage = (Math.floor(event.first / event.rows)) + 1;
      this.first = event.first;
      if (event.first == 0)
        this.first = 1;
      this.getNoticeList();
    }
}
