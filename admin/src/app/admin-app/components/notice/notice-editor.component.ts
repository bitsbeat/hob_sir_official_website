import {Component, OnInit} from "@angular/core";
import {NoticeModel} from "./notice.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { Location } from "@angular/common";
import {NoticeService} from "./notice.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'notice-editor',
  templateUrl: './notice-editor.component.html'
})

export class NoticeEditorComponent implements OnInit{
  objNotice: NoticeModel = new NoticeModel();
  noticeId: string;
  noticeForm: FormGroup;
  isSubmitted: boolean = false;
  today:Date = new Date();

  constructor(private _objService: NoticeService, private _formBuilder: FormBuilder, private location: Location, private activatedRoute: ActivatedRoute){
    this.noticeForm = _formBuilder.group({
      "noticeTitle": ['', Validators.required],
      "noticeDescription": ['', Validators.required],
      "active": ['']
    });
    activatedRoute.params.subscribe(params => this.noticeId = params['id']);
  }

  ngOnInit() {
    if (this.noticeId)
      this.getNoticeDetail();
  }

  getNoticeDetail(){
    this._objService.getNoticeDetail(this.noticeId)
      .subscribe(res =>this.bindDetail(res),
        error => this.errorMessage(error));
  }

  bindDetail(objRes: NoticeModel){
    this.noticeForm.setValue({
      "noticeTitle": objRes.noticeTitle,
      "noticeDescription": objRes.noticeDescription,
      "active": objRes.active
    });
  }

  saveNotice(){
    this.isSubmitted = true;
    if(this.noticeForm.valid){
      if(!this.noticeId){
          this._objService.saveNotice(this.noticeForm.value)
            .subscribe(res => this.resStatusMessage(res),
            error => this.errorMessage(error));
      }
      else{
        this._objService.updateNotice(this.noticeId, this.noticeForm.value)
          .subscribe(res => this.resStatusMessage(res),
          error => this.errorMessage(error));
      }
    }
  }

  changeDateFormatToView(data: string) {
    return new Date(data);
    //return moment(data).format("YYYY-MM-DD HH:mm");
  }

  resStatusMessage(objSave: any){
    swal("Success !", objSave.message, "success");
    this.location.back();
  }

  errorMessage(objResponse: any) {
    swal("Alert !", objResponse.message, "info");
  }

  triggerCancelForm() {
    this.location.back();
  }
}
