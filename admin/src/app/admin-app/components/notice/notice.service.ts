import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {NoticeModel, NoticeResponse} from "./notice.model";
import {Observable} from "rxjs/Observable";
import {API_URL} from "../../../shared/configs/env.config";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";

@Injectable()

export class NoticeService{
  apiRoute:string = "notice";
  quoteRoute: string = "quote";

  constructor(private _http: Http){

  }

  getQuote(): Observable<any>{
    return this._http.get(API_URL + this.quoteRoute)
      .map(res => res.json())
      .catch(this.handleError);
  }

  saveNotice(objSave: NoticeModel){
      let body = JSON.stringify(objSave);
      return this._http.post(API_URL + this.apiRoute, body)
        .map(res => res.json())
        .catch(this.handleError);
  }

  updateNotice(id: string, objUpdate: NoticeModel){
    let body = JSON.stringify(objUpdate);
    return this._http.put(API_URL + this.apiRoute + "/" + id, body)
      .map(res => res.json())
      .catch(this.handleError);
  }

  getLatestNotice(): Observable<NoticeModel[]>{
    return this._http.get(API_URL + "notice-list/latest-week")
      .map(res => <NoticeModel[]>res.json())
      .catch(this.handleError);
  }

  getNoticeDetail(id: string){
    return this._http.get(API_URL + this.apiRoute + "/" + id)
      .map(res => <NoticeModel>res.json())
      .catch(this.handleError);
  }

  getNoticeDetailBySlug(slug: string): Observable<NoticeModel>{
    return this._http.get(API_URL + "notice-detail/" + slug)
      .map(res => <NoticeModel>res.json())
      .catch(this.handleError);
  }


  getNotice(perPage: number, currentPage: number, active?: boolean): Observable<NoticeResponse>{
    let queryString: string = "";
    queryString += perPage ? "?perpage=" + perPage : "";
    queryString += currentPage ? "&page=" + currentPage : "";
    if (perPage)
      queryString += active ? "&active=true" : "";
    else
      queryString += active ? "?active=true" : "";
    return this._http.get(API_URL + this.apiRoute + queryString)
      .map(res => <NoticeResponse>res.json())
      .catch(this.handleError);
  }

  deleteNotice(objNotice: NoticeModel): Observable<any> {
    let body = JSON.stringify({});
    return this._http.patch(API_URL + this.apiRoute + "/" + objNotice._id, body)
      .map(res =><any>res.json())
      .catch(this.handleError);
  }

  handleError(error):  Observable<any>{
    return Observable.throw(error.json() || 'server error');
  }
}
