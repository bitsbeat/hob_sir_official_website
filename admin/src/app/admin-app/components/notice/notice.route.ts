import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import { NoticeComponent } from './notice.component';
import { NoticeEditorComponent } from './notice-editor.component';

export const NoticeRoute: Routes = [
  {
    path: '',
    children: [
      {path: '', component: NoticeComponent},
      {path: 'quote-editor', component: NoticeEditorComponent},
      {path: 'quote-editor/:id', component: NoticeEditorComponent}
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(NoticeRoute)
  ],
  exports: [
    RouterModule
  ]
})
export class NoticeRouting {
}
