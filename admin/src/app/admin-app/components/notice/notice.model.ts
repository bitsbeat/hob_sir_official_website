export class NoticeModel{
  constructor(){
    this.active = false;
    this.lastUpdated = false;
  }
  _id: string;
  noticeTitle: string;
  noticeDescription: string;
  active: boolean;
  addedOn: string;
  addedBy: string;
  lastUpdated: boolean;
  updatedOn: string;
  updatedBy: string;
  deletedOn: string;
  deletedBy: string;
  deleted: boolean;
}

export class NoticeResponse{
  dataList: NoticeModel[];
  currentPage: string;
  totalItems: string;
}
