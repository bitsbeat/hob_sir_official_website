import {Component, EventEmitter, Output, Input, ViewChild, OnInit} from '@angular/core';
import {ContactModel} from "./contact.model";
import {ContactService} from "./contact.service";
import * as moment from 'moment';
@Component({
  selector: 'contact-view',
  templateUrl: './contact-view.html'
  //providers: [UserService],

})

export class ContactViewComponent implements OnInit {
  //  @Input showInfo:boolean;
  @Input() contactId: string;
  @Output() viewCancelEvent: EventEmitter<any> = new EventEmitter();
  objContact: ContactModel = new ContactModel();
  error: any;
  showDefaultImage: boolean = false;
  imageSrc: string;

  ngOnInit() {
    this.getUserDetail();
  }

  constructor(private _objService: ContactService) {

  }

  getUserDetail() {
    this._objService.getContactById(this.contactId)
      .subscribe(resUser => this.handleDetail(resUser),
        error => this.error = error);
  }

  handleDetail(objContact: ContactModel) {
    // objContact.addedOn = moment(objContact.addedOn).format('LLLL');
    this.objContact = objContact;

  }

  triggerCancelView(event?: Event) {
    let showInfo = false;
    this.viewCancelEvent.emit(showInfo);
  }

  print(){
      let printContents, popupWin;
      printContents = document.getElementById('print-section').innerHTML;
      popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
      popupWin.document.open();
      popupWin.document.write(`
        <html>
          <head>
            <title>Contact Detail</title>
            <style>
            
            </style>
          </head>
      <body onload="window.print();window.close()">${printContents}</body>
        </html>`
      );
      popupWin.document.close();

    }


}

