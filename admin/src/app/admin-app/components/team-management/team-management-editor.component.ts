import { ActivatedRoute } from '@angular/router';
import { ImageProperties } from './../../../shared/models/image.model';
import {Component, EventEmitter, Output, Input, AfterViewInit, ViewChild, OnInit} from '@angular/core';
import {TeamManagementModel} from "./team-management.model";
import {TeamManagementService} from "./team-management.service";
import{Config} from "../../../shared/configs/general.config";
import{ImageCanvasSizeEnum} from "../../../shared/configs/enum.config";
import {ValidationService} from "../../../shared/services/validation.service";
import {Validators, FormBuilder, FormGroup, FormControl} from "@angular/forms";
import { Location } from "@angular/common";
@Component({
  selector: 'team-management-editor',
  templateUrl: './team-management-editor.html'
})
export class TeamManagementEditorComponent implements OnInit,AfterViewInit {
  objTeam: TeamManagementModel = new TeamManagementModel();
  memberId: string;
  //@Output() showListEvent: EventEmitter<any> = new EventEmitter();
  teamMgmtForm: FormGroup;
  isSubmitted: boolean = false;

  /* Image Upload Handle*/
  imageDeleted: boolean = false;
  file: File;
  fileName: string = "";
  drawImagePath: string = Config.DefaultAvatar;
  imageFormControl: FormControl = new FormControl('', Validators.required);
  canvasSize: number = ImageCanvasSizeEnum.small;
  /* End Image Upload handle */


  constructor(private _objService: TeamManagementService, private _formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private location: Location) {
    this.teamMgmtForm = _formBuilder.group({
      "teamMemberName": ['', Validators.required],
      "email": ['', Validators.compose([Validators.required, ValidationService.emailValidator])],
      "imageFormControl": this.imageFormControl,
      "designation": ['', Validators.required],
      "address": [''],
      "description": [''],
      "facebookURL": ['', ValidationService.urlValidator],
      "twitterURL": ['', ValidationService.urlValidator],
      "googlePlusURL": ['', ValidationService.urlValidator],
      "linkedInURL": ['', ValidationService.urlValidator],
      "active": ['']
    });
    activatedRoute.params.subscribe(params => this.memberId = params['id']);
  }

  ngAfterViewInit() {
    if (!this.memberId)
      this.drawImageToCanvas(Config.DefaultAvatar);
  }

  ngOnInit() {
    if (this.memberId)
      this.getTeamMemberDetail();
  }

  getTeamMemberDetail() {
    this._objService.getTeamMemberDetail(this.memberId)
      .subscribe(res =>this.bindDetail(res),
        error => this.errorMessage(error));
  }

  bindDetail(objRes: TeamManagementModel) {
    this.teamMgmtForm.setValue({
      "teamMemberName": objRes.teamMemberName,
      "email": objRes.email,
      "imageFormControl": objRes.imageName,
      "designation": objRes.designation,
      "address": objRes.address,
      "description": objRes.description,
      "facebookURL": objRes.facebookURL,
      "twitterURL": objRes.twitterURL,
      "googlePlusURL": objRes.googlePlusURL,
      "linkedInURL": objRes.linkedInURL,
      "active": objRes.active
    })
    this.objTeam.imageName = objRes.imageName;
    this.objTeam.imageProperties = objRes.imageProperties;
    this.fileName = objRes.imageName;
    (<FormControl>this.teamMgmtForm.controls['imageFormControl']).patchValue(this.fileName);
    let path: string = "";
    if (this.objTeam.imageName) {
      var cl = Config.Cloudinary;
      path = cl.url(this.fileName);
    }
    else
      path = Config.DefaultAvatar;
    this.drawImageToCanvas(path);
  }

  saveTeamMember() {
    this.isSubmitted = true;
    (<FormControl>this.teamMgmtForm.controls['imageFormControl']).patchValue(this.fileName);

    if (this.teamMgmtForm.valid) {
      if (!this.memberId) {
        this._objService.saveTeamMember(this.teamMgmtForm.value, this.file)
          .subscribe(res => this.resStatusMessage(res),
            error => this.errorMessage(error));
      }
      else {
        this._objService.updateTeamMember(this.memberId, this.teamMgmtForm.value, this.file, this.imageDeleted)
          .subscribe(res => this.resStatusMessage(res),
            error => this.errorMessage(error));
      }
    }
  }

  resStatusMessage(objSave: any) {
    swal("Success !", objSave.message, "success")
    this.location.back();
  }

  triggerCancelForm() {
      this.location.back();
  }

  errorMessage(objResponse: any) {
    swal("Alert !", objResponse.message, "info");
  }

  /*Image handler */
  changeFile(args) {
    this.file = args;
    this.fileName = this.file.name;
  }

  drawImageToCanvas(path: string) {
    this.drawImagePath = path;
  }

  deleteImage(id: string) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Image !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      ()=> {
        this._objService.deleteImage(this.objTeam.imageName, this.objTeam.imageProperties.imageExtension, this.objTeam.imageProperties.imagePath)
          .subscribe(res=> {
              this.imageDeleted = true;
              this.objTeam.imageName = "";
              this.fileName = "";
              this.drawImageToCanvas(Config.DefaultAvatar);
              swal("Deleted!", res.message, "success");
            },
            error=> {
              swal("Alert!", error.message, "info");

            });
      });
  }
  /* End ImageHandler */
}

