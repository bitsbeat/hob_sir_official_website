import { TeamManagementListComponent } from './team-management-list.component';
import {NgModule}      from '@angular/core';
import {TeamManagementService} from "./team-management.service";
import {TeamManagementEditorComponent} from "./team-management-editor.component";
import {TeamManagementComponent} from "./team-management.component";

import {SharedModule} from '../../../shared/shared.module';

@NgModule({
    imports: [SharedModule],
    declarations: [TeamManagementEditorComponent, TeamManagementComponent, TeamManagementListComponent],
    providers: [TeamManagementService]
})

export class TeamManagementModule {
}