import { Component } from '@angular/core';

@Component({
    selector: 'team-management',
    template: '<router-outlet></router-outlet>'
})

export class TeamManagementComponent{

}