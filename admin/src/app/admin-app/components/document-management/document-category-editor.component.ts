import { DocumentManagementService } from './document-management.service';
import { DocumentCategory } from './document-management.model';
import { Validators } from '@angular/forms';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, Input, AfterViewInit, OnInit } from '@angular/core';
import { Location } from "@angular/common";
import { Router, ActivatedRoute } from "@angular/router";
@Component({
    selector: 'document-category-editor',
    templateUrl: './document-category-editor.component.html'
})

export class DocumentCategoryEditorComponent implements OnInit{

    documentCategory: DocumentCategory = new DocumentCategory();
    documentCategoryId : string;
    documentCategoryForm: FormGroup;
    isSubmitted: boolean = false;

    constructor(private _formbuilder: FormBuilder, private activatedRoute: ActivatedRoute, private _documentmgmtService: DocumentManagementService, private _location: Location){
        this.documentCategoryForm = _formbuilder.group({
            "categoryName" : ['', Validators.required],
            "categoryDescription" : ['', Validators.required],
            "active": ['']
        });
        activatedRoute.params.subscribe(params => this.documentCategoryId =params['id']);
    }

    ngOnInit() {
        if(this.documentCategoryId)
            this.getDocumentCategoryDetails();
    }

    getDocumentCategoryDetails() {
        this._documentmgmtService.getDocumentCategoryDetails(this.documentCategoryId)
            .subscribe(res => this.bindDetail(res)),
            error => this.errorMessage(error);
    }

    bindDetail(objRes: DocumentCategory) {
        this.documentCategoryForm.setValue({
            "categoryName": objRes.categoryName,
            "categoryDescription": objRes.categoryDescription,
            "active": objRes.active
        });
    }

    saveDocumentCategory(){
        this.isSubmitted = true;
        if(this.documentCategoryForm.valid){
            if(!this.documentCategoryId){
                this._documentmgmtService.saveDocumentCategory(this.documentCategoryForm.value)
                    .subscribe(res => this.resStatusMessage(res),
                        error =>this.errorMessage(error));
            }
            else{
                this._documentmgmtService.updateDocumentCategory(this.documentCategoryId, this.documentCategoryForm.value)
                    .subscribe(res => this.resStatusMessage(res)),
                    error => this.errorMessage(error);                    
            }
        }
    }

    resStatusMessage(res:any) {
        //this.showListEvent.emit(false); // * isCanceled = false
      swal("Success !", res.message, "success");
      this.documentCategoryForm.reset();
      this._location.back();
    }

    errorMessage(objResponse:any) {
      swal("Alert !", objResponse.message, "info");
    }

    triggerCancelForm() {
        //let isCanceled = true;
        //this.showListEvent.emit(isCanceled);
        this._location.back();
    }
}