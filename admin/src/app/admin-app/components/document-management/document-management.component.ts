import { Router } from '@angular/router';
import { Component } from '@angular/core';

@Component({
    selector: 'document-management',
    templateUrl: './document-management.component.html'
})

export class DocumentManagementComponent{
    documenttab: boolean = false;
    documentCategorytab: boolean = false;

    constructor(private router: Router){ }

    public tabSwitch(args) {
        if (1 == args.index) {
            this.router.navigate(['/admin/document-management/document-category']);
        }
        else {
            this.router.navigate(['/admin/document-management/document']);
        }
    }
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                   