export class DocumentCategory{
    constructor(){
        this.active = false;
    }
    _id:string;
    categoryName: string;
    urlSlogCategory: string;
    categoryDescription: string;
    active: boolean;
    addedBy: string;
    addedOn: Date;
    updatedBy: string;
    updatedOn: Date;
    deleted: boolean;
    deletedBy: string;
    deletedOn: Date;
}

export class DocumentModel{
    constructor(){
        this.active = false;
    }
    _id: string;
    urlSlog: string;
    categoryId: string;
    //documentCategory: string;        // sanjeev lai vanna parcha change garna
    documentDescription: string;
    documentName: string;
    documentProperties: string;
    documentTitle: string;
    documentAltText: string;
    active: boolean;
    addedBy: string;
    addedOn: Date;
    updatedBy: string;
    updatedOn: Date;
    deleted: boolean;
    deletedBy: string;
    deletedOn: Date;
}

export class DocumentCategoryResponse{
    dataList:DocumentCategory[];
    totalItems:number;
    currentPage:number;
}

export class DocumentModelResponse{
    dataList:DocumentModel[];
    totalItems:number;
    currentPage:number;
}