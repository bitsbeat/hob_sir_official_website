import { Router } from '@angular/router';
import { DocumentManagementService } from './document-management.service';
import { DocumentCategory, DocumentCategoryResponse } from './document-management.model';
import { Component } from '@angular/core';

@Component({
    selector: 'document-category-list',
    templateUrl: './document-category-list.component.html'
})

export class DocumentCategoryListComponent {
    catListResponse: DocumentCategoryResponse;
    error: any;
    categoryId: string;
    first: number = 0;
    perPage: number = 10;
    currentPage: number = 1;
    totalPage: number = 1;

    preIndex: number = 0;
    bindSort: boolean = false;

    constructor(private _categoryservice: DocumentManagementService, private router: Router) { }

    ngOnInit() {
        //this.perPage = 10;
        //this.currentPage = 1;
        //   if (!this.isCanceled)
        this.getDocCategoryList();
    }

    getDocCategoryList() {
        this._categoryservice.getCategoryList(this.perPage, this.currentPage)
            .subscribe(objRes => this.bindList(objRes),
            error => this.errorMessage(error));
    }

    errorMessage(objResponse: any) {
        swal("Alert!", objResponse.message, "info");
    }

    bindList(objRes: DocumentCategoryResponse) {
        this.catListResponse = objRes;
        this.preIndex = (this.perPage * (this.currentPage - 1));
        if (objRes.dataList.length > 0) {
            let totalPage = objRes.totalItems / this.perPage;
            this.totalPage = totalPage > 1 ? Math.ceil(totalPage) : 1;
            if (!this.bindSort) {
                this.bindSort = true;
                this.sortTable();
            }
            else
                jQuery("table").trigger("update", [true]);

        }
        // if(objRes.dataList.length > 0) {
        //     let totalPage = objRes.totalItems / this.perPage;
        //     this.totalPage = totalPage > 1 ? Math.ceil(totalPage) : 1;
        //     this.sortTable();
        // }
    }

    sortTable() {
        setTimeout(() => {
            jQuery('.tablesorter').tablesorter({
                headers: {
                    2: { sorter: false },
                    3: { sorter: false }
                }
            });
        }, 50);
    }

    addDocumentCategory() {
        this.router.navigate(['/admin/document-management/document-category/document-category-editor']);
    }

    edit(id: string) {
        this.router.navigate(['/admin/document-management/document-category/document-category-editor', id]);
    }

    delete(id: string) {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this category !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },
            () => {
                let objCategory: DocumentCategory = new DocumentCategory();
                objCategory._id = id;
                objCategory.deleted = true;
                this._categoryservice.deleteDocumentCategory(objCategory)
                    .subscribe(res => {
                        this.getDocCategoryList();
                        swal("Deleted!", res.message, "success");
                    },
                    error => {
                        swal("Alert!", error.message, "info");
                    });
            });
    }

    pageChanged(event) {
        this.perPage = event.rows;
        this.currentPage = (Math.floor(event.first / event.rows)) + 1;
        this.first = event.first;
        if (event.first == 0)
            this.first = 1;
        this.getDocCategoryList();
    }
}