import { Router } from '@angular/router';
import { DocumentManagementService } from './document-management.service';
import { DocumentModel, DocumentModelResponse, DocumentCategoryResponse } from './document-management.model';
import { Component } from '@angular/core';

@Component({
    selector: 'document-list',
    templateUrl: './document-list.component.html'
})

export class DocumentListComponent {
    documentListResponse: DocumentModelResponse = new DocumentModelResponse();
    catListResponse: DocumentCategoryResponse = new DocumentCategoryResponse();
    documentId: string;
    first: number = 0;
    perPage: number = 10;
    currentPage: number = 1;
    totalPage: number = 1;

    preIndex: number = 0;
    bindSort: boolean = false;
    
    constructor(private _documentservice: DocumentManagementService, private router: Router) { }

    ngOnInit() {
        this.getDocumentList();
        this.getCategoryList();
    }

    getCategoryList(){
        this._documentservice.getCategoryList(100,1, true)
            .subscribe(res => this.catListResponse = res,
            error => this.errorMessage(error));
    }

    categoryFilter(args) {
        let categoryId = args.target.value;
        this.currentPage = 1;
        this._documentservice.getDocumentList(this.perPage, this.currentPage, categoryId)
            .subscribe(res => this.bindList(res),
            error=>this.errorMessage(error));
    }

    getDocumentList(){
        this._documentservice.getDocumentList(this.perPage, this.currentPage)
            .subscribe(objRes => this.bindList(objRes),
            error => this.errorMessage(error)); 
    }

    errorMessage(objResponse: any) {
        swal("Alert!", objResponse.message, "info");
    }

    bindList(objRes: DocumentModelResponse) {
        this.documentListResponse = objRes;
        this.preIndex = (this.perPage * (this.currentPage - 1));
        if (objRes.dataList.length > 0) {
            let totalPage = objRes.totalItems / this.perPage;
            this.totalPage = totalPage > 1 ? Math.ceil(totalPage) : 1;
            if (!this.bindSort) {
                this.bindSort = true;
                this.sortTable();
            }
            else
                jQuery("table").trigger("update", [true]);

        }
    }

    sortTable() {
        setTimeout(() => {
            jQuery('.tablesorter').tablesorter({
                headers: {
                    2: { sorter: false },
                    3: { sorter: false }
                }
            });
        }, 50);
    }

    pageChanged(event) {
        this.perPage = event.rows;
        this.currentPage = (Math.floor(event.first / event.rows)) + 1;
        this.first = event.first;
        if (event.first == 0)
            this.first = 1;
        this.getDocumentList();
    }

    addDocument() {
        this.router.navigate(['/admin/document-management/document/document-editor']);
    }

    edit(id: string) {
        this.router.navigate(['/admin/document-management/document/document-editor', id]);
    }
    
    delete(id:string) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Document !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      ()=> {
        let documentData:DocumentModel = new DocumentModel();
        documentData._id = id;
        documentData.deleted = true;
        this._documentservice.deleteDocument(documentData)
          .subscribe(res=> {
              this.getDocumentList();
              swal("Deleted!", res.message, "success");
            },
            error=> {
              swal("Alert!", error.message, "info");

            });
      });

  }
}