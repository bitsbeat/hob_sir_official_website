import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DocumentManagementService } from './document-management.service';
import { Validators } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { DocumentModel, DocumentCategoryResponse } from './document-management.model';
import { Component, OnInit, Input } from "@angular/core";
import { Location } from "@angular/common";

@Component({
    selector: 'document-editor',
    templateUrl: './document-editor.component.html'
})

export class DocumentEditorComponent implements OnInit {
    documentId: string;
    documentData: DocumentModel = new DocumentModel();
    docCategoryList: DocumentCategoryResponse = new DocumentCategoryResponse();
    documentForm: FormGroup;
    documentFormControl: FormControl = new FormControl('', Validators.required);
    isSubmitted: boolean = false;
    file: File;
    fileName: string = '';
    allowedExt: string[] = ['pdf', 'doc', 'ppt', 'pptx', 'docx'];/// For valdiation of file ext
    allowedSize: number = 3;
    fileDeleted: boolean = false;

    constructor(private _documentService: DocumentManagementService, private _formBuilder: FormBuilder, private location: Location, private activatedRoute: ActivatedRoute) {
        this.documentForm = _formBuilder.group({
            "categoryId": ['', Validators.required],
            "documentTitle": ['', Validators.required],
            "documentAltText": [''],
            "documentDescription": ['', Validators.required],
            "documentFormControl": this.documentFormControl,
            "active": ['']
        });
        activatedRoute.params.subscribe(params => this.documentId =params['id']);
    }

    ngOnInit() {
        this.getCategoryList();
        if (this.documentId)
            this.getDocumentDetails();
    }

    getDocumentDetails() {
        this._documentService.getDocumentDetails(this.documentId)
            .subscribe(res => this.bindDetail(res),
            error => this.errorMessage(error));
    }

    bindDetail(objRes: DocumentModel) {
        this.documentForm.setValue({
            "categoryId": objRes.categoryId,
            "documentTitle": objRes.documentTitle,
            "documentAltText": objRes.documentAltText,
            "documentDescription": objRes.documentDescription,
            "active": objRes.active,
            "documentFormControl": objRes.documentName
        });
        this.documentData.documentName = objRes.documentName;
        this.fileName = objRes.documentName;
        (<FormControl>this.documentForm.controls['documentFormControl']).patchValue(this.fileName);
    }

    saveDocument() {
        this.isSubmitted = true;
        this.documentFormControl.patchValue(this.fileName);
        if (this.documentForm.valid) {
            if (!this.documentId) {
                this._documentService.saveDocument(this.documentForm.value, this.file)
                    .subscribe(res => this.resStatusMessage(res),
                    error => this.errorMessage(error));
            }
            else {
                this._documentService.updateDocument(this.documentId, this.documentForm.value, this.file, this.fileDeleted)
                    .subscribe(res => this.resStatusMessage(res),
                    error => this.errorMessage(error));
            }
        }
    }

    getCategoryList() {
        this._documentService.getCategoryList(10, 1, true)
            .subscribe(res => {
                this.docCategoryList = res
            },
            error => this.errorMessage(error))
    }

    resStatusMessage(objSave: any) {
        swal("Success !", objSave.message, "success");
        this.location.back();
    }

    errorMessage(objResponse: any) {
        swal("Alert !", objResponse.message, "info");
    }

    onFileSelect(args) {
        this.file = args;
        if (this.file)
            this.fileName = this.file.name;
    }
    onDeleteFile(e){

    }

    triggerCancelForm() {
        this.location.back();
    }

}