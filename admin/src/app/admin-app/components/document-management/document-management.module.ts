import { XhrService } from './../../../shared/components/xhr.service';

import { DocumentEditorComponent } from './document-editor.component';
import { DocumentListComponent } from './document-list.component';
import { CommonModule } from '@angular/common';
import { DocumentCategoryListComponent } from './document-category-list.component';
import { DocumentManagementService } from './document-management.service';
import { DocumentCategoryEditorComponent } from './document-category-editor.component';
import { DocumentManagementComponent } from './document-management.component';
import { SharedModule } from './../../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

@NgModule({
    declarations: [DocumentListComponent, DocumentEditorComponent, DocumentManagementComponent, DocumentCategoryEditorComponent, DocumentCategoryListComponent],
    imports: [SharedModule, RouterModule, CommonModule],
    exports: [DocumentListComponent, DocumentEditorComponent, DocumentCategoryEditorComponent, DocumentManagementComponent, DocumentCategoryListComponent],
    providers: [DocumentManagementService, XhrService]
})

export class DocumentManagementModule {

}