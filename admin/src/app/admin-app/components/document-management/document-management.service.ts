
import { XhrService } from './../../../shared/components/xhr.service';
import { Config } from './../../../shared/configs/general.config';
import { Observable } from 'rxjs/Observable';
import { API_URL } from './../../../shared/configs/env.config';
import { Http } from '@angular/http';
import { DocumentCategory, DocumentCategoryResponse, DocumentModel, DocumentModelResponse } from './document-management.model';
import { Injectable } from '@angular/core';

@Injectable()

export class DocumentManagementService {
    documentCategoryAPIRoute: string = "document-category";
    documentAPIRoute: string = "document";
    progress: any;
    constructor(private _http: Http) { }

    getDocumentList(perPage: number, currentPage: number, categoryId?: string): Observable<DocumentModelResponse> {
        let queryString: string = "";
        queryString += perPage ? "?perpage=" + perPage : "";
        queryString += currentPage ? "&page=" + currentPage : "";
        queryString += categoryId ? "&categoryid=" + categoryId : "";
        return this._http.get(API_URL + this.documentAPIRoute + queryString)
            .map(res => <DocumentModelResponse>res.json())
            .catch(this.handleError);
    }

    getDocumentDetails(documentId: string): Observable<DocumentModel>{
        return this._http.get(API_URL + this.documentAPIRoute + "/" + documentId)
                    .map(res => <DocumentModel>res.json())
                    .catch(this.handleError);
    }

    saveDocument(documentData: DocumentModel, file: File): Observable<any> {
        return Observable.create(observer => {
            let formData: FormData = new FormData(),
                xhr: XMLHttpRequest = new XMLHttpRequest();

            if (file) {
                formData.append('documentName', file);
            }
            formData.append('data', JSON.stringify(documentData));
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        observer.next(JSON.parse(xhr.response));
                        observer.complete();
                    } else {
                        observer.error(JSON.parse(xhr.response));
                    }
                }
            };
            xhr.upload.onprogress = (event) => {
                this.progress = Math.round(event.loaded / event.total * 100);
                //this.progressObserver.next(this.progress);
            };
            xhr.open('POST', API_URL + this.documentAPIRoute, true);
            xhr.setRequestHeader("Authorization", Config.AuthToken);
            xhr.send(formData);
        })
            // return this.xhrservice.xhrRequest<DocumentModel, any>('POST', this.documentAPIRoute, 'documentName', documentData, file);
    }

    updateDocument(documentId: string, updateDocument: DocumentModel,file: File, fileDeleted: boolean): Observable<any>{
        // return this.xhrservice.xhrRequest<DocumentModel, any>('PUT', this.documentAPIRoute, 'documentName', updateDocument, file, documentId, fileDeleted);

        return Observable.create(observer => {
            let formData: FormData = new FormData(),
          xhr: XMLHttpRequest = new XMLHttpRequest();


        if(file){
            formData.append('documentName', file);
        }
        formData.append('data', JSON.stringify(updateDocument));
        xhr.onreadystatechange = () => {
          if(xhr.readyState === 4) {
              if(xhr.status === 200 ) {
                observer.next(JSON.parse(xhr.response));
                observer.complete();
              } else{
                  observer.error(JSON.parse(xhr.response));
              }
          }
        };
        xhr.upload.onprogress = (event) => {
          this.progress = Math.round(event.loaded / event.total * 100);
        }
        xhr.open('PUT', API_URL + this.documentAPIRoute+ "/" + documentId, true);
        xhr.setRequestHeader("Authorization", Config.AuthToken);
        xhr.send(formData);
        })
    }

    deleteDocument(documentData: DocumentModel): Observable<any>{
        let body = JSON.stringify({});
        return this._http.patch(API_URL+this.documentAPIRoute+ "/" + documentData._id, body)
                    .map(res => res.json())
                    .catch(this.handleError);
    }

    saveDocumentCategory(objCategory: DocumentCategory) {
        let body = JSON.stringify(objCategory);
        return this._http.post(API_URL + this.documentCategoryAPIRoute, body)
            .map(res => res.json())
            .catch(this.handleError);
    }

    updateDocumentCategory(catId: string, objCategory: DocumentCategory) {
        let body = JSON.stringify(objCategory);
        return this._http.put(API_URL + this.documentCategoryAPIRoute + "/" + catId, body)
            .map(res => res.json())
            .catch(this.handleError);
    }

    deleteDocumentCategory(objCategory: DocumentCategory) {
        let body = JSON.stringify({});
        return this._http.patch(API_URL + this.documentCategoryAPIRoute + "/" + objCategory._id, body)
            .map(res => <any>res.json())
            .catch(this.handleError);
    }

    getCategoryList(perPage: number, currentPage: number, active?: boolean): Observable<DocumentCategoryResponse> {
        let queryString: string = "";
        queryString += perPage ? "?perpage=" + perPage : "";
        queryString += currentPage ? "&page=" + currentPage : "";
        if (perPage)
            queryString += active ? "&active=true" : "";
        else
            queryString += active ? "?active=true" : "";
        return this._http.get(API_URL + this.documentCategoryAPIRoute + queryString)
            .map(res => <DocumentCategoryResponse>res.json())
            .catch(this.handleError);
    }

    getDocumentCategoryDetails(catId: string): Observable<DocumentCategory> {
        return this._http.get(API_URL + this.documentCategoryAPIRoute + "/" + catId)
            .map(res => <DocumentCategory>res.json())
            .catch(this.handleError);
    }

    handleError(error) {
        return Observable.throw(error.json() || 'server error');
    }
}
