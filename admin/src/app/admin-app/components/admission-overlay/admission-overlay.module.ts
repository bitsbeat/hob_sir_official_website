import {NgModule} from '@angular/core';
import { RouterModule } from '@angular/router';
import {SharedModule} from '../../../shared/shared.module';
import {XhrService} from "../../../shared/services/xhr.service";
import { AdmissionOverlayComponent } from "./admission-overlay.component";
import { AdmissionOverlayService } from "./admission-overlay.service";

@NgModule({
  imports: [SharedModule, RouterModule],
  declarations: [AdmissionOverlayComponent],
  providers: [AdmissionOverlayService, XhrService],
  exports: [AdmissionOverlayComponent ]
})

export class AdmissionOverlayModule {

}
