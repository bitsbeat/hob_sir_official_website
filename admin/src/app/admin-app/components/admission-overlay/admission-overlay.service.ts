import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {FileOperrationService} from '../../../shared/services/fileOperation.service';
import {Observable} from "rxjs/Observable";
import { AdmissionOverlayModel, AdmissionOverlayResponse } from './admission-overlay.model';
import{ API_URL} from "../../../shared/configs/env.config";
import {XhrService} from "../../../shared/services/xhr.service";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";

@Injectable()

export class AdmissionOverlayService {
  apiRoute:string = "admission-overlay";
  progressObserver:any;
  progress:any;

  constructor(private _http:Http, private fileService:FileOperrationService, private xhrService: XhrService) {
    this.progress = Observable.create(observer => {
      this.progressObserver = observer
    }).share();
  }

  deleteImage(fileName:string, orgExt:string, path:string):Observable < any > {
    return this.fileService.deleteFile(fileName, orgExt, path, "image");
  }

  getImage():Observable<AdmissionOverlayResponse>{
            return this._http.get('api/'+this.apiRoute)
            .map(res=><AdmissionOverlayResponse>res.json())
  }

  getAdmissionOverlayDetail():Observable <AdmissionOverlayResponse> {
    return this._http.get(API_URL + this.apiRoute )
      .map(res =><AdmissionOverlayResponse>res.json())
      .catch(this.handleError);
  }

  saveAdmissionOverlayImage(objSave: AdmissionOverlayModel, file: File): Observable<any> {
    return this.xhrService.xhrRequest<AdmissionOverlayModel,any>('POST', this.apiRoute, 'imageName', objSave, file);
  }

  updateAdmissionOverlayImage(overlayId: string, objUpdate:AdmissionOverlayModel, file: File, imageDeleted: boolean): Observable<any> {
    return this.xhrService.xhrRequest<AdmissionOverlayModel,any>('PUT', this.apiRoute, 'imageName', objUpdate, file, overlayId, imageDeleted);
  }

  
  handleError(error) {
    return Observable.throw(error.json() || 'server error');
  }
}
