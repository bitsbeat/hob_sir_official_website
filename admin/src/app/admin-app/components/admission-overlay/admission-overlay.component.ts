import { Component, EventEmitter, Output, Input, AfterViewInit, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { AdmissionOverlayModel, AdmissionOverlayResponse } from './admission-overlay.model';

import { Config } from "../../../shared/configs/general.config";
import { ImageCanvasSizeEnum } from "../../../shared/configs/enum.config";
import { AdmissionOverlayService} from "./admission-overlay.service";
import { Router, ActivatedRoute } from '@angular/router'; 
import { Location } from '@angular/common';

@Component({
  selector: 'admission-overlay',
  templateUrl: './admission-overlay.component.html'
})

export class AdmissionOverlayComponent implements OnInit, AfterViewInit {
  objAdmissionOverlay: AdmissionOverlayModel = new AdmissionOverlayModel();
  sliderId: string;
  admissionOverlay: FormGroup;
  isSubmitted: boolean = false;
  /* Image Upload Handle*/
  imageDeleted: boolean = false;
  file: File;
  fileName: string = "";
  drawImagePath: string = Config.DefaultImage;
  imageFormControl: FormControl = new FormControl('', Validators.required);
  canvasSize: number = ImageCanvasSizeEnum.small;
  /* End Image Upload handle */

  constructor( 
    private _objService: AdmissionOverlayService, 
    private _formBuilder: FormBuilder, 
    private activatedRoute: ActivatedRoute, 
    private location: Location
  ) {
    this.admissionOverlay = _formBuilder.group({
      "imageTitle": ['', Validators.required],
      "imageAltText": ['', Validators.required],
      "imageHeight": ['', Validators.required],
      "imageWidth": ['', Validators.required],
      "active": [''],
      "imageFormControl": this.imageFormControl
    });
    activatedRoute.params.subscribe(params => this.sliderId = params['id']);
  }

  ngAfterViewInit() {
    if (!this.sliderId)
      this.drawImageToCanvas(Config.DefaultImage);
  }

  ngOnInit() {
      this.getImageDetail();
  }

  drawImageToCanvas(path: string) {
    this.drawImagePath = path;
  }

  getImageDetail() {
    this._objService.getAdmissionOverlayDetail()
      .subscribe(res =>this.bindDetail(res),
        error => this.errorMessage(error));
  }

  bindDetail( objRes: AdmissionOverlayResponse){
    if(objRes.dataList[0]){
      this.objAdmissionOverlay = objRes.dataList[0];
      this.admissionOverlay.setValue({
        "imageTitle": objRes.dataList[0].imageTitle,
        "imageAltText": objRes.dataList[0].imageAltText,
        "imageHeight": objRes.dataList[0].imageHeight ? objRes.dataList[0].imageHeight : "",
        "imageWidth": objRes.dataList[0].imageWidth ? objRes.dataList[0].imageWidth : "",
        "active": objRes.dataList[0].active,
        "imageFormControl": objRes.dataList[0].imageName 
      });
      this.objAdmissionOverlay.imageName = objRes.dataList[0].imageName;
      this.objAdmissionOverlay.imageProperties = objRes.dataList[0].imageProperties;
      this.fileName = objRes.dataList[0].imageName;
    
    }
    
    (<FormControl>this.admissionOverlay.controls['imageFormControl']).patchValue(this.fileName);
    let path: string="";
    if(this.fileName){
      var cl = Config.Cloudinary;
      path = cl.url(this.fileName);
    }
    else {
      path = Config.DefaultImage;
    }
    this.drawImageToCanvas(path);
  }

  saveAdmissionOverlay(){
    this.isSubmitted = true;
    (<FormControl>this.admissionOverlay.controls['imageFormControl']).patchValue(this.fileName);

    if(this.admissionOverlay.valid){
      if(!this.objAdmissionOverlay._id) {
        this._objService.saveAdmissionOverlayImage(this.admissionOverlay.value, this.file)
          .subscribe(res => this.resStatusMessage(res),
            error => this.errorMessage(error));
      }
      else{
        this._objService.updateAdmissionOverlayImage(this.objAdmissionOverlay._id, this.admissionOverlay.value, this.file, this.imageDeleted)
          .subscribe(res => this.resStatusMessage(res),
            error => this.errorMessage(error));
      }
    }
  }

  resStatusMessage(objSave: any){
    swal("Success !", objSave.message, "success");
  }

  errorMessage(objResponse: any) {
    swal("Alert !", objResponse.message, "info");
  }

  triggerCancelForm() {
    this.location.back();
  }

  changeFile(args) {
    this.file = args;
    this.fileName = this.file.name;
  }

  deleteImage(id: string) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Image !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      ()=> {
        this._objService.deleteImage(this.objAdmissionOverlay.imageName, this.objAdmissionOverlay.imageProperties.imageExtension, this.objAdmissionOverlay.imageProperties.imagePath)
          .subscribe(res=> {
              this.imageDeleted = true;
              this.objAdmissionOverlay.imageName = "";
              this.fileName = "";
              this.drawImageToCanvas(Config.DefaultImage);
              swal("Deleted!", res.message, "success");
            },
            error=> {
              swal("Alert!", error.message, "info");
            });
      });
  }

}
