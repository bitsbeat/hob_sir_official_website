import { Component, OnInit } from '@angular/core';
import { VideoGalleryService } from './videoGallery.service';
import { VideoResponse, VideoModel } from './videoGallery.model';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
    selector: 'video-gallery-list',
    templateUrl: './videoGalleryList.component.html'
})

export class VideoGalleryListComponent implements OnInit{
    videoGalleryList: VideoResponse;

    perPage: number = 10;
    currentPage: number = 1;
    totalPage: number = 1;
    first: number = 0;
    bindSort: boolean = false;
    preIndex: number = 0;

    constructor(private videoService: VideoGalleryService, private router: Router, private location: Location) {}

    ngOnInit() {
        this.getVideoGalleryList();
    }

    getVideoGalleryList() {
        this.videoService.getVideoGalleryList(this.perPage, this.currentPage)
            .subscribe(res => this.bindVideoGalleryList(res))
    }

    bindVideoGalleryList(objRes: VideoResponse) {
        this.videoGalleryList = objRes;
    }

    addVideo() {
        this.router.navigate(['/admin/videoGallery/videoGalleryEditor']);
    }

    edit(id: string) {
        this.router.navigate(['./admin/videoGallery/videoGalleryEditor', id]);
    }

    delete(id: string) {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Video !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
          },
          ()=> {
            let objVideo:VideoModel = new VideoModel();
            objVideo._id = id;
            objVideo.deleted = true;
            this.videoService.deleteVideo(objVideo)
              .subscribe(res=> {
                  this.getVideoGalleryList();
                  swal("Deleted!", res.message, "success");
                },
                error=> {
                  swal("Alert!", error.message, "info");
  
                });
          });
    }
}