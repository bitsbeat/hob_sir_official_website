import {RouterModule, Routes} from "@angular/router";
import {VideoGalleryComponent} from "./videoGallery.component";
// import {BlogListComponent} from "./blog-list.component";
import {NgModule} from "@angular/core";
import { VideoGalleryListComponent } from './videoGalleryList.component';
import { VideoGalleryEditorComponent } from './videoGalleryEditor.component';

export const VideoGalleryRoute: Routes = [
  {path: '', component: VideoGalleryComponent,
    children: [
      {path: '', component:VideoGalleryListComponent},
      {path: 'videoGalleryEditor', component: VideoGalleryEditorComponent},
      {path: 'videoGalleryEditor/:id', component: VideoGalleryEditorComponent}
    //   {path: ':year/:month/:day/:urlSlog', component: BlogDetailComponent}
    ]
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(VideoGalleryRoute)
  ],
  exports: [
    RouterModule
  ]
})
export class VideoGalleryRouting {
}
