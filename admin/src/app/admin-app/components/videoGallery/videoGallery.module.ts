import { VideoGalleryComponent } from './videoGallery.component';
import {NgModule} from '@angular/core';
// import {ImgSliderEditorComponent} from"./imgSliderEditor.component";
// import {ImgSliderService} from"./imgSlider.service";
import { RouterModule } from '@angular/router';
import {SharedModule} from '../../../shared/shared.module';
// import {ImgSliderListComponent} from "./imgSliderList.component";
// import {XhrService} from "../../../shared/services/xhr.service";
import { VideoGalleryRouting } from './videoGallery.route'
import { VideoGalleryService } from './videoGallery.service';
import { VideoGalleryListComponent } from './videoGalleryList.component';
import { VideoGalleryEditorComponent } from './videoGalleryEditor.component';

@NgModule({
  imports: [ RouterModule, VideoGalleryRouting,SharedModule.forRoot()],
  declarations: [VideoGalleryComponent, VideoGalleryListComponent, VideoGalleryEditorComponent],
  exports: [VideoGalleryComponent, VideoGalleryListComponent, VideoGalleryEditorComponent],
  providers: [VideoGalleryService],
})

export class VideoGalleryModule {

}
