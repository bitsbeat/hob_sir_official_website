import { Component, OnInit } from '@angular/core';
import { VideoGalleryService } from './videoGallery.service';
import { FormGroup, FormControl, Validators, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { Config } from "./../../../shared/configs/general.config";
import { Router, ActivatedRoute } from '@angular/router'; 
import { Location } from '@angular/common';
import { VideoModel } from './videoGallery.model';

@Component({
    selector: 'video-gallery-editor',
    templateUrl: './videoGalleryEditor.component.html'
})

export class VideoGalleryEditorComponent implements OnInit{
  objVideo: VideoModel = new VideoModel();
  videoId: string;
  //@Output() showSliderListEvent: EventEmitter<any> = new EventEmitter();
  videoForm: FormGroup;
  isSubmitted: boolean = false;

  constructor(private videoService: VideoGalleryService, private _formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private location: Location) {
    this.videoForm = _formBuilder.group({
      "title": ['', Validators.required],
      "videoUrl": ['', Validators.required],
      "active": ['']
    });
    activatedRoute.params.subscribe(params => this.videoId = params['id']);
  }

  ngOnInit() {
    if (this.videoId)
      this.getVideoDetail();
  }

  getVideoDetail() {
    this.videoService.getVideoDetail(this.videoId)
      .subscribe(res =>this.bindDetail(res),
        error => this.errorMessage(error));
  }

  bindDetail( objRes: VideoModel){
    this.videoForm.setValue({
      "title": objRes.title,
      "videoUrl": objRes.videoUrl,
      "active": objRes.active,
    });
  }

  saveVideo(){
    this.isSubmitted = true;
    if(this.videoForm.valid){
      if(!this.videoId) {
        this.videoService.saveVideoGallery(this.videoForm.value)
          .subscribe(res => this.resStatusMessage(res),
            error => this.errorMessage(error));
      }
      else{
        this.videoService.updateVideoGallery(this.videoId,this.videoForm.value)
          .subscribe(res => this.resStatusMessage(res),
            error => this.errorMessage(error));
      }
    }
  }

  resStatusMessage(objSave: any){
    swal("Success !", objSave.message, "success");
    this.location.back();
  }

  errorMessage(objResponse: any) {
    swal("Alert !", objResponse.message, "info");
  }

  triggerCancelForm() {
    this.location.back();
  }
}