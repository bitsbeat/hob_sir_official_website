export class VideoModel {
    constructor() {
        this.active = false;
    }

    _id: string;
    title: string;
    videoUrl: string;
    active: boolean;
    addedBy:string;
    addedOn:string;
    updatedBy:string;
    updatedOn:string;
    deleted:boolean;
    deletedBy:string;
    deletedOn:string;
}

export class VideoResponse {
    dataList: VideoModel[];
    totalItems: number;
    currentPage: number;
}