import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {Observable} from "rxjs/Observable";
import{ API_URL} from "../../../shared/configs/env.config";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { VideoModel, VideoResponse } from './videoGallery.model';

@Injectable()

export class VideoGalleryService {
  apiRoute:string = "video";

  constructor(private _http:Http) {
  }

  getVideoGalleryList(perPage:number, currentPage:number):Observable < VideoResponse> {

    return this._http.get(API_URL + this.apiRoute + "?perpage=" + perPage + "&page=" + currentPage)
      .map(res =><VideoResponse>res.json())
      .catch(this.handleError);
  }

  getVideoDetail(videoId:string):Observable < VideoModel> {
    return this._http.get(API_URL + this.apiRoute + "/" + videoId)
      .map(res =><VideoModel>res.json())
      .catch(this.handleError);
  }

  saveVideoGallery(objSave: VideoModel): Observable<any> {
    return this._http.post(API_URL + this.apiRoute, objSave)
      .map(res => res.json())
      .catch(this.handleError);
  }

  updateVideoGallery(videoId: string, objUpdate:VideoModel): Observable<any> {
    return this._http.put(API_URL + this.apiRoute + "/" + videoId, objUpdate)
            .map(res => res.json())
            .catch(this.handleError);
  }

  deleteVideo(objVideo: VideoModel):Observable < any> {
    let body = JSON.stringify({});
    return this._http.patch(API_URL + this.apiRoute + "/" + objVideo._id, body)
      .map(res =><any>res.json())
      .catch(this.handleError);
  }


  handleError(error) {
    console.log(error.json());
    return Observable.throw(error.json() || 'server error');
  }
}
