import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import { DashboardComponent } from "./dashboard.component";

export const DashboardRoute: Routes = [
  {path: '',
    children: [
      {path: '', component: DashboardComponent},
    ]
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(DashboardRoute)
  ],
  exports: [
    RouterModule
  ]
})
export class DashboardRouting {
}