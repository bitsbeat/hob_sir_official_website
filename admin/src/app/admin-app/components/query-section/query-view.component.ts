import {Component, OnInit, Output, Input, EventEmitter} from "@angular/core";
import {QueryModel} from './query.model';
import {QueryService} from "./query.service";
import {ActivatedRoute} from "@angular/router";
import {Location} from "@angular/common";
import * as moment from 'moment';

@Component({
  selector: '<query-view>',
  templateUrl: './query-view.html'
})

export class QueryViewComponent implements OnInit{

  @Input () queryId: string;
  @Output() viewCancelEvent: EventEmitter<any> = new EventEmitter ();
  ObjQuery: QueryModel = new QueryModel();
  error: any;

  constructor( private qservice: QueryService,
               private route: ActivatedRoute ,
               private location: Location) {
    route.params.subscribe(params => this.queryId = params['id'])
  }

  ngOnInit() {
    this.getQueryDetail();
  }

  getQueryDetail() {
    this.qservice.getById(this.queryId)
      .subscribe(resp => this.handleDetail(resp))
      , error => this.error = error;
  }

  handleDetail(objModel: QueryModel) {
    // console.log(objModel)
    objModel.addedOn = moment(objModel.addedOn).format('LLLL');
    this.ObjQuery = objModel;
  }


  triggerCancelView(event?: Event) {
    let showList = false;
    this.location.back();
  }

  print(){
      let printContents, popupWin;
      printContents = document.getElementById('print-section').innerHTML;
      popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
      popupWin.document.open();
      popupWin.document.write(`
        <html>
          <head>
            <title>Query Detail</title>
            <style>
    
            </style>
          </head>
          <body onload="window.print();window.close()">${printContents}</body>
        </html>`
      );
      popupWin.document.close();

    }

}
