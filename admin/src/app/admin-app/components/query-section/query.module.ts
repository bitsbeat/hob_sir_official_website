import {NgModule} from "@angular/core";
import{SharedModule} from '../../../shared/shared.module';
// import { ReactiveFormsModule } from '@angular/forms';
// import { CommonModule } from '@angular/common';
import {QueryListComponent} from "./query-list.component";
import {QueryViewComponent} from "./query-view.component";
import {QueryService} from "./query.service";

@NgModule({
  imports: [SharedModule],
  declarations: [QueryListComponent, QueryViewComponent],
  providers: [QueryService]
})

export class QueryListModule {}
