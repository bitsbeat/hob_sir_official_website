import {Injectable, Query} from "@angular/core";
import {Http} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {QueryModel, QueryResp} from "./query.model";
import 'rxjs/add/operator/map';

@Injectable()
export class QueryService {
  constructor(private http: Http) {}

  private handleError(error: any): Observable<any> {
    return Observable.throw(error.json().error || 'Server error');

  }
  getList(perPage: number, currentPage: number): Observable<QueryResp> {
    return this.http.get('api/queries' + "?perpage" + perPage + "&page=" + currentPage)
      .map(res => <QueryResp>res.json())
      .catch(this.handleError);
  }
  getById(id: string):Observable<QueryModel> {
    return this.http.get('api/queries'+ "/" + id)
      .map(res => <QueryModel>res.json())
      .catch(this.handleError);
  }

  deleteList(obj: QueryModel) {
    let body = JSON.stringify({});
    return this.http.patch('api/queries'+ "/" + obj._id, body)
      .map(res => res.json())
      .catch(this.handleError);

  }
}
