export class QueryModel {
  _id:string;
  name:string;
  email:string;
  query:string;
  addedOn: string;
  deleted: boolean;
  deletedBy: string;
  deletedOn: string;

}
export class QueryResp {
  dataList: QueryModel[];
  currentPage: number = 1;
  totalItems: number = 0;
}
