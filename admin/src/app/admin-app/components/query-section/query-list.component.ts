import {Component, ElementRef, OnInit} from '@angular/core';
import {Paginator} from 'primeng/primeng';
import {QueryModel, QueryResp} from './query.model';
import {QueryService} from './query.service';
import {Router} from "@angular/router";

@Component({
  selector: '<query-list>',
  templateUrl: './query-list.html'
})

export class QueryListComponent implements OnInit{
 Objlist: QueryModel = new QueryModel();
 showInfo: boolean = false;
 showList: boolean = true;
 showView: boolean = false;
 Objresp: QueryResp = new QueryResp();
 qId: string;
// Pagination
  perPage:number = 10;
  currentPage:number = 1;
  totalPage:number = 1;
  first:number = 0;
  bindSort:boolean = false;
  preIndex:number = 1;
  /* End Pagination */

  constructor(private qservice: QueryService,
              private router: Router) {}

  ngOnInit() {
    this.perPage = 10;
    this.currentPage = 1;
    this.getQueryList();
  }

  getQueryList() {
    this.qservice.getList(this.perPage, this.currentPage)
      .subscribe(objQuery => this.bindList(objQuery),
      error => this.errorMessage(error));
  }

  errorMessage(objResponse:any) {
    swal("Alert !", objResponse.message, "info");
  }

  bindList(objRes:QueryResp) {
    this.Objresp = objRes;
    this.preIndex = (this.perPage * (this.currentPage - 1));
    if (objRes.totalItems > 0) {
      let totalPage = objRes.totalItems / this.perPage;
      this.totalPage = totalPage > 1 ? Math.ceil(totalPage) : 1;
      if (!this.bindSort) {
        this.bindSort = true;
        this.sortTable();
      }
      else
        jQuery("table").trigger("update", [true]);
    }
  }

  sortTable() {
    setTimeout(() => {
      jQuery('.tablesorter').tablesorter({
        headers: {
          3: {sorter: false},
          4: {sorter: false},
          5: {sorter: false}
        }
      });
    }, 50);
  }

  changeDateFormat(data:string) {
    return new Date(data).toLocaleString('en-GB', {
      month: "numeric",
      year: "numeric",
      day: "numeric",
      hour12: false,
      hour: "numeric",
      minute: "numeric"
    });
  }

  viewList() {
    // this.showList = true;
    // this.showView = false;
    this.router.navigate(['/admin/querylist']);
  }
  showDetail(id: string) {
    // this.showView = true;
    // this.showList = false;
    // this.qId = id;
    this.router.navigate(['/admin/querylist/viewquery',id]);

  }

  delete(id:string) {
    swal({
        title: "Did you reply this query?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      ()=> {
        let objTemp:QueryModel = new QueryModel();
        objTemp._id= id;
        this.qservice.deleteList(objTemp)
          .subscribe(res =>
              // console.log('****'),
            {
              this.getQueryList();
              swal("Deleted!", res.message, "success");
            },
            error=> {
              swal("Alert!", error.message, "info");
            }
          );
      });
  }

  handleCancel(args) {
    this.showList = false;
    this.sortTable();
  }

  pageChanged(event) {
    this.perPage = event.rows;
    this.currentPage = (Math.floor(event.first / event.rows)) + 1;
    this.first = event.first;
    if (event.first == 0)
      this.first = 1;
    this.getQueryList();
  }


}
