import {Component, EventEmitter, Output, Input, AfterViewInit, ViewChild, OnInit} from '@angular/core';
import {BlogDocumentModel} from "./blog.model";
import {BlogService} from "./blog.service";
import {FormGroup, FormControl, Validators, FormBuilder} from "@angular/forms";
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';


@Component({
  selector: 'blog-doc-editor',
  templateUrl: './blog-doc-editor.html'
})
export class BlogDocEditorComponent implements OnInit {
  objBlogDoc: BlogDocumentModel = new BlogDocumentModel();
  docId: string;
  blogId: string;
  @Output() showListEvent: EventEmitter<any> = new EventEmitter();
  blogDocForm: FormGroup;
  docFormControl: FormControl = new FormControl('', Validators.required);
  isSubmitted: boolean = false;
  /* File Upload Handle*/
  allowedExt: string[] = ['pdf', 'doc', 'ppt', 'pptx', 'docx'];/// For valdiation of file ext
  allowedSize: number = 3;
  fileDeleted: boolean = false;
  file: File;
  fileName: string;

  constructor(private _objService: BlogService, private _formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private location: Location) {
    this.blogDocForm = this._formBuilder.group({
      "documentTitle": ['', Validators.required],
      "docFormControl": this.docFormControl,
      "active": ['']
      // "_id": this.docId
    });
    route.params.subscribe(params => {this.blogId = params['blogid']});
    route.params.subscribe(params => {this.docId = params['docid']});
  }

  ngOnInit() {
    if (this.docId)
      this.getBlogDocDetail();
  }


  getBlogDocDetail() {
    this._objService.getBlogDocDetail(this.blogId, this.docId)
      .subscribe(res =>this.bindDetail(res),
        error => this.errorMessage(error));
  }

  bindDetail(objRes: BlogDocumentModel) {
     this.blogDocForm.setValue({
     "documentTitle":objRes.documentTitle,
     "docFormControl": objRes.documentName,
     "active": objRes.active
       // "_id": this.docId
   })
    this.objBlogDoc.documentName = objRes.documentName;
     this.objBlogDoc = objRes;
     this.fileName = objRes.documentName;
    (<FormControl>this.blogDocForm.controls['docFormControl']).patchValue(this.fileName);
  }

  saveBlogDoc() {
    this.isSubmitted = true;
    this.docFormControl.patchValue(this.fileName);
    if (this.blogDocForm.valid) {
      if (!this.docId) {
        this._objService.saveDocument(this.blogId, this.blogDocForm.value, this.file)
          .subscribe(res => this.resStatusMessage(res),
            error => this.errorMessage(error));
      }
      else {
        this._objService.updateDocumnet(this.blogId, this.blogDocForm.value, this.file, this.fileDeleted)
          .subscribe(res => this.resStatusMessage(res),
            error => this.errorMessage(error));
         }

    }
  }

  resStatusMessage(objSave: any) {
    swal("Success !", objSave.message, "success")
    this.location.back();
  }

  triggerCancelForm() {
    this.location.back();
  }

  errorMessage(objResponse: any) {
    swal("Alert !", objResponse.message, "info");
  }

  /*file handler */
  onFileSelect(args) {
    this.file = args;
    if (this.file)
      this.fileName = this.file.name;
  }

  onDeleteFile(id: string) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Document !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      ()=> {
        this._objService.deleteDoc(this.objBlogDoc.documentName, this.objBlogDoc.docProperties.documentMimeType, this.objBlogDoc.docProperties.docPath)
          .subscribe(res=> {
              this.fileDeleted = true;
              this.fileName = "";
              swal("Deleted!", res.message, "success");
            },
            error=> {
              swal("Alert!", error.message, "info");

            });
      });

  }

  /* End File Handler */
}

