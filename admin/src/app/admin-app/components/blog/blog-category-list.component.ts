import {Component, ElementRef, OnInit, Output, Input, EventEmitter, OnChanges} from '@angular/core';
import {BlogService} from "./blog.service";
import {BlogCategoryModel, BlogCategoryResponse} from "./blog.model";
import {Paginator} from 'primeng/primeng';
import {BlogCategoryEditorComponent} from  "./blog-category-editor.component";
import {Router} from '@angular/router';

@Component({
  selector: 'blog-category-list',
  templateUrl: './blog-category-list.html'
})

export class BlogCategoryListComponent implements OnInit {
  objListResponse:BlogCategoryResponse = new BlogCategoryResponse();
  error:any;
  categoryId:string;
  /* Pagination */
  perPage:number = 10;
  first:number = 0;
  bindSort:boolean = false;
  currentPage:number = 1;
  totalPage:number = 1;
  preIndex: number = 0;

  ngOnInit() {
    this.perPage = 10;
    this.currentPage = 1;
    //   if (!this.isCanceled)
    this.getBlogCategoryList();
  }


  constructor(private _objService:BlogService,
              private router: Router) {
  }

  getBlogCategoryList() {
    this._objService.getBlogCategoryList(this.perPage, this.currentPage)
      .subscribe(objRes =>this.bindList(objRes),
        error => this.errorMessage(error));
  }

  errorMessage(objResponse:any) {
    swal("Alert !", objResponse.message, "info");
  }

  bindList(objRes:BlogCategoryResponse) {
    this.objListResponse = objRes;
    this.preIndex = (this.perPage * (this.currentPage - 1));
        if (objRes.dataList.length > 0) {
            let totalPage = objRes.totalItems / this.perPage;
            this.totalPage = totalPage > 1 ? Math.ceil(totalPage) : 1;
            if (!this.bindSort) {
                this.bindSort = true;
                this.sortTable();
            }
            else
                jQuery("table").trigger("update", [true]);
        }
  }

  sortTable() {
    setTimeout(()=> {
      jQuery('.tablesorter').tablesorter({
        headers: {
          2: {sorter: false},
          3: {sorter: false}
        }
      });
    }, 50);
  }

  edit(id:string) {
    this.router.navigate(['/admin/blog-management/blog-category/editor', id]);
  }

  addCategory() {
   this.router.navigate(['/admin/blog-management/blog-category/editor']);
  }

  showCategoryList(args) {
      this.getBlogCategoryList();
  }

  delete(id:string) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Blog Category !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      ()=> {
        let objTemp:BlogCategoryModel = new BlogCategoryModel();
        objTemp._id = id;
        objTemp.deleted = true;
        this._objService.deleteBlogCategory(objTemp)
          .subscribe(res=> {
              this.getBlogCategoryList();
              swal("Deleted!", res.message, "success");
            },
            error=> {
              swal("Alert!", error.message, "info");

            });
      });


  }

  pageChanged(event) {
    this.perPage = event.rows;
        this.currentPage = (Math.floor(event.first / event.rows)) + 1;
        this.first = event.first;
        if (event.first == 0)
            this.first = 1;
        this.getBlogCategoryList();
    }
  }
