import { Router } from '@angular/router';
import {Component, ElementRef, OnInit, Output, EventEmitter, Input, OnChanges} from '@angular/core';
import {BlogService} from "./blog.service";
import {BlogModel,  BlogResponse, BlogCategoryResponse} from "./blog.model";

@Component({
  selector: 'blog-list',
  templateUrl: './blog-list.html'
})

export class BlogListComponent implements OnInit {

  objListResponse:BlogResponse = new BlogResponse();
  objCatList:BlogCategoryResponse = new BlogCategoryResponse();
  @Input() autoCompleteData:string[];
 /* Pagination */
  perPage:number = 10;
  currentPage:number = 1;
  totalPage:number = 1;
  first:number = 0;
  bindSort:boolean = false;
  preIndex:number = 0;
  /* End Pagination */

  ngOnInit() {    //if (!this.isCanceled)
    this.getBlogList();
    this.getCategoryList();
  }

  // ngOnChanges() {
  //   if (this.showList) {
  //     this.showForm = !this.showList;
  //     this.getCategoryList();
  //   }

  constructor(private _objService:BlogService,
              private router: Router) {
  }

  getCategoryList() {
    this._objService.getBlogCategoryList(100, 1, true)
      .subscribe(res=>this.objCatList = res,
        error=>this.errorMessage(error)
      )
  }

  categoryFilter(args) {
    let categoryId = args.target.value;
    this.currentPage = 1;
    this._objService.getBlogList(this.perPage, this.currentPage, categoryId)
      .subscribe(res => this.bindList(res),
        error=>this.errorMessage(error));
  }

  getBlogList() {
    this._objService.getBlogList(this.perPage, this.currentPage)
      .subscribe(objRes =>this.bindList(objRes),
        error => this.errorMessage(error));
  }

  errorMessage(objResponse:any) {
    swal("Alert !", objResponse.message, "info");
  }

  bindList(objRes:BlogResponse) {
    this.objListResponse = objRes;
    this.preIndex = (this.perPage * (this.currentPage - 1));
    if (objRes.dataList.length > 0) {
      let totalPage = objRes.totalItems / this.perPage;
      this.totalPage = totalPage > 1 ? Math.ceil(totalPage) : 1;
      if (!this.bindSort) {
        this.bindSort = true;
        this.sortTable();
      }
      else
        jQuery("table").trigger("update", [true]);
    }
  }

  sortTable() {
    setTimeout(()=> {
      jQuery('.tablesorter').tablesorter({
        headers: {
          3: {sorter: false},
          4: {sorter: false}
        }
      });
    }, 50);
  }

  addNews(){
    this.router.navigate(['/admin/blog-management/blog/editor']);
  }


  editBlog(id:string){
    this.router.navigate(['/admin/blog-management/blog/editor',id]);
  }

  showBlogList(args)
    {
      if (!args) {
        this.router.navigate(['/admin/blog-management/blog']);
        // this.getBlogList();
      }
      // this.showForm = false;
      this.sortTable();
    }

  showDocList(blogId:string) {
 this.router.navigate(['/admin/blog-management/doc', blogId]);
  }

  showMetaForm(blogId: string) {
   this.router.navigate(['/admin/blog-management/meta-list', blogId]);
  }

  deleteBlog(id:string){
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Blog !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      ()=> {
        let objTemp:BlogModel = new BlogModel();
        objTemp._id = id;
        objTemp.deleted = true;
        this._objService.deleteBlog(objTemp)
          .subscribe(res=> {
              this.getBlogList();
              swal("Deleted!", res.message, "success");
            },
            error=> {
              swal("Alert!", error.message, "info");

            });
      });
  }

  pageChanged(event){
    this.perPage = event.rows;
    this.currentPage = (Math.floor(event.first / event.rows)) + 1;
    this.first = event.first;
    if (event.first == 0)
      this.first = 1;
    this.getBlogList();
  }


}

