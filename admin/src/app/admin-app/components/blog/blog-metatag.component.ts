import {Component, EventEmitter, Output, Input, OnInit} from '@angular/core';
import {FormControlMessages} from "../../../shared/components/control-valdation-message.component";
import {BlogMetaTagModel, BlogTagModel} from "./blog.model";
import {BlogService} from "./blog.service";
import {FormGroup, Validators, FormBuilder} from "@angular/forms";
import {TagInputComponent} from '../../../shared/components/tag-input/tag-input.component';
import { ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';

@Component({
    selector: 'blog-metatag-editor',
    templateUrl: './blog-metatag.html'
})

export class BlogMetaTagEditorComponent implements OnInit {
    objBlogMeta:BlogMetaTagModel = new BlogMetaTagModel();
    blogMetaForm:FormGroup;
    isSubmitted:boolean = false;
    blogId:string;
    @Output() showBlogListEvent:EventEmitter<any> = new EventEmitter();
    autoCompleteData:Array<string>;

    constructor(private _objService:BlogService,
                private _formBuilder:FormBuilder,
                private route: ActivatedRoute,
                private location: Location) {
        this.blogMetaForm = this._formBuilder.group({
            "metaKeyword": [''],
            "titleTag": ['', Validators.required],
            "metaDescription": ['', Validators.required],
            "metaAuthor": ['', Validators.required]
        });
        route.params.subscribe(params => this.blogId = params ['id']);
    }

    ngOnInit() {
        if (this.blogId)
            this.getBlogMetaTagDetail();
        this.getBlogTagList();

    }

    public typeaheadOnSelect(e:any):void {
    }

    getBlogTagList():void {
        this._objService.getBlogTagList()
            .subscribe(res =>this.bindTagForAutoComplete(res),
                error => this.errorMessage(error));
    }

    bindTagForAutoComplete(res:BlogTagModel[]) {
        let data:string[] = res.map(function (row) {
            return row.tag;
        });
        this.autoCompleteData = data;
    }

    getBlogMetaTagDetail() {
       this._objService.getBlogMetaTagDetail(this.blogId)
            .subscribe(res =>this.bindDetail(res),
                error => this.errorMessage(error));
    }

    bindDetail(objRes:BlogMetaTagModel) {
        objRes.metaKeyword = objRes.metaKeyword.split(',');
        this.objBlogMeta = objRes;
        this.blogMetaForm.setValue({
          "metaKeyword": objRes.metaKeyword,
          "titleTag": objRes.titleTag,
          "metaDescription": objRes.metaDescription,
          "metaAuthor": objRes.metaAuthor
        })

    }

    saveBlogMetaTag() {
        this.isSubmitted = true;
        if (this.blogMetaForm.valid) {
            this._objService.updateBlogMetaTag(this.objBlogMeta)
                .subscribe(res => this.resStatusMessage(res),
                    error =>this.errorMessage(error));
        }

    }

    resStatusMessage(res:any) {
     swal("Success !", res.message, "success");
      this.location.back();

    }

    errorMessage(objResponse:any) {
      swal("Alert !", objResponse.message, "info");
    }

    triggerCancelForm() {
      this.location.back();
    }


}

