/**
 * Created by sanedev on 6/27/16.
 */
import{Component}from'@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'blog-management',
  templateUrl: './blog-management.html'
})
export class BlogManagementComponent {


  constructor( private router: Router) {
  }
  public tabSwitch(args) {
    if (1 == args.index) {
    this.router.navigate(['/admin/blog-management/blog-category']);
    }
    else {this.router.navigate(['/admin/blog-management/blog']);
    }
  }


}

