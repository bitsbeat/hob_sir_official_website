import {Component, EventEmitter, Output, Input, AfterViewInit, OnInit} from '@angular/core';
import {BlogCategoryModel} from "./blog.model";
import {BlogService} from "./blog.service";
import {FormGroup, Validators, FormBuilder,} from "@angular/forms";
import { ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';

@Component({
    selector: 'blog-category-editor',
    templateUrl: './blog-category-editor.html'
})

export class BlogCategoryEditorComponent implements OnInit {
    objBlogCat:BlogCategoryModel = new BlogCategoryModel();
    blogCategoryForm:FormGroup;
    isSubmitted:boolean = false;
    blogCategoryId:string;

    constructor(private _objService:BlogService, private _formBuilder:FormBuilder, private activatedRoute: ActivatedRoute, private location: Location) {
        this.blogCategoryForm = _formBuilder.group({
                "categoryName": ['', Validators.required],
                "categoryDescription": ['', Validators.required],
                "active": ['']
          }
        );
        activatedRoute.params.subscribe(params => this.blogCategoryId =params['id']);
    }

    ngOnInit() {
        if (this.blogCategoryId)
            this.getBlogCategoryDetail();
    }

    getBlogCategoryDetail() {
        this._objService.getBlogCategoryDetail(this.blogCategoryId).subscribe(res => this.bindDetail(res)),
                error => this.errorMessage(error);
    }

    bindDetail(objRes: BlogCategoryModel) {
        this.blogCategoryForm.setValue({
            "categoryName": objRes.categoryName,
            "categoryDescription": objRes.categoryDescription,
            "active": objRes.active
        });
    }

    saveBlogCategory() {
        this.isSubmitted = true;
        if (this.blogCategoryForm.valid) {
           if (!this.blogCategoryId) {
                this._objService.saveBlogCategory(this.blogCategoryForm.value)
                    .subscribe(res => this.resStatusMessage(res),
                        error =>this.errorMessage(error));
            }
            else {
              this._objService.updateBlogCategory(this.blogCategoryId, this.blogCategoryForm.value)
                    .subscribe(res => this.resStatusMessage(res),
                        error =>this.errorMessage(error));
            }
        }
    }

    resStatusMessage(res:any) {
      swal("Success !", res.message, "success");
      this.blogCategoryForm.reset();
      this.location.back();
    }

    errorMessage(objResponse:any) {
      swal("Alert !", objResponse.message, "info");
    }

    triggerCancelForm() {
        this.location.back();
    }


}

