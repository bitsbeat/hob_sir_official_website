export class StudentForm {
  _id: string;
  firstName: string;
  middleName: string;
  lastName: string;
  dateOfBirth: string;
  nationality: string;
  gender:string;
  country: string;
  district: string;
  municipalityVdc: string;
  wardNo: string;
  email:string;
  qualification: Qualification[];
  parent: Pinfo[];
  addedOn: string;
  deleted: boolean;
  deletedBy: string;
  deletedOn: string;
  admissionStatus: string;
  remarks: string;
  userId: number;
  admissionFormNo: string;
}

export class Pinfo {
  pName: string;
  contact:string;
  address:string;
  job: string;
  emailid:string;
}

export class Qualification {
  schoolBoard:string;
  schoolCourse:string;
  schoolGrade:string;
  intermediateBoard:string;
  intermediateCourse:string;
  intermediateGrade:string;
  bachelorBoard:string;
  bachelorCourse:string;
  bachelorGrade:string;
}

export class formResponse {

  data:StudentForm[];
  currentPage: number = 1;
  totalItems: number = 0;

}
