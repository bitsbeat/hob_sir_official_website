import {Component,ElementRef, OnInit} from '@angular/core';
import {Paginator} from 'primeng/primeng';
import {formResponse, StudentForm} from './form.model';
import {ApplicationService} from './admission.service';
import {Router} from "@angular/router";

@Component({
  selector: '<view-application>',
  templateUrl: './applicant-list.html'
})
export class ApplicantListComponent implements OnInit {
  objContact:StudentForm = new StudentForm();
  showForm:boolean = false;
  showList:boolean = true;
  showView:boolean = false;
  objResponse:formResponse = new formResponse();
  contactId:string;
  /* Pagination */
  perPage:number = 10;
  currentPage:number = 1;
  totalPage:number = 1;
  first:number = 0;
  bindSort:boolean = false;
  preIndex:number = 1;
  /* End Pagination */

  constructor(private formService: ApplicationService, private router: Router) {
  }

  ngOnInit() {
    this.perPage = 10;
    this.currentPage = 1;
    this.getFormList();
  }

  getFormList() {
    this.formService.getList(this.perPage, this.currentPage)
      .subscribe(objRes =>this.bindList(objRes),
        error => this.errorMessage(error));
  }

  errorMessage(objResponse:any) {
    swal("Alert !", objResponse.message, "info");
  }

  bindList(objRes:formResponse) {
    this.objResponse = objRes;
    this.preIndex = (this.perPage * (this.currentPage - 1));
    if (objRes.totalItems > 0) {
      let totalPage = objRes.totalItems / this.perPage;
      this.totalPage = totalPage > 1 ? Math.ceil(totalPage) : 1;
      if (!this.bindSort) {
        this.bindSort = true;
        this.sortTable();
      }
      else
        jQuery("table").trigger("update", [true]);
    }
  }

  sortTable() {
    setTimeout(() => {
      jQuery('.tablesorter').tablesorter({
        headers: {
          3: {sorter: false},
          4: {sorter: false},
          5: {sorter: false}
        }
      });
    }, 50);
  }

  changeDateFormat(data:string) {
    return new Date(data).toLocaleString('en-GB', {
      month: "numeric",
      year: "numeric",
      day: "numeric",
      hour12: false,
      hour: "numeric",
      minute: "numeric"
    });
  }

  delete(id:string) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Applicant Detail !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      ()=> {
        let objTemp:StudentForm = new StudentForm();
        objTemp._id= id;
        this.formService.deleteList(objTemp)
          .subscribe(res =>
            {
              this.getFormList();
              swal("Deleted!", res.message, "success");
            },
            error=> {
              swal("Alert!", error.message, "info");
            }
          );
      });
  }


  showListView() {
    this.router.navigate(['/admin/list']);
  }
  showDetail(id:string){
    this.router.navigate(['/admin/list/view',id]);
  }

  handleCancel(args) {
    this.showList = false;
    this.sortTable();
  }

  pageChanged(event) {
    this.perPage = event.rows;
    this.currentPage = (Math.floor(event.first / event.rows)) + 1;
    this.first = event.first;
    if (event.first == 0)
      this.first = 1;
    this.getFormList();
  }
}
