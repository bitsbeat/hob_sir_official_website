import {Injectable} from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { StudentForm, formResponse } from './form.model';
import{ API_URL} from "../../../shared/configs/env.config";

import 'rxjs/add/operator/map';



@Injectable()
export class ApplicationService {

  apiRoute: string = 'admission-info';
  private headers = new Headers({'Content-Type': 'application/json'});
  constructor( private http: Http) {}

  private handleError(error: any): Observable<any> {
    return Observable.throw(error.json().error || 'Server error');

  }

  getApplication(): Observable<formResponse[]> {
    return this.http.get('api/admission-info')
      .map(response => response.json().data as formResponse[])
      .catch(this.handleError);
  }

  createApplication(formpost: formResponse): Observable<formResponse[]> {

    let body = JSON.stringify(formpost);
    // this.headers.append('Authorization', 'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Il9pZCI6IjU4NDk3YzE2MDcyZjZjMjBmNGZhYzMxOSIsImFjdGl2ZSI6dHJ1ZSwidXNlcm5hbWUiOiJzdXBlcmFkbWluIiwiZW1haWwiOiJoZWxwQG5vZGViZWF0cy5jb20iLCJ1c2VyUm9sZSI6InN1cGVyYWRtaW4iLCJhZGRlZE9uIjoiMjAxNi0xMi0wOFQxNToyODoyMi4wMzlaIiwidHdvRmFjdG9yQXV0aEVuYWJsZWQiOmZhbHNlfSwiY2xhaW1zIjp7InN1YmplY3QiOiI1ODQ5N2MxNjA3MmY2YzIwZjRmYWMzMTkiLCJpc3N1ZXIiOiJodHRwOi8vbG9jYWxob3N0OjMwMDAiLCJwZXJtaXNzaW9ucyI6WyJzYXZlIiwidXBkYXRlIiwicmVhZCIsImRlbGV0ZSJdfSwiaWF0IjoxNTAwNDUyNjc1LCJleHAiOjE1MDA1MzkwNzUsImlzcyI6IjU4NDk3YzE2MDcyZjZjMjBmNGZhYzMxOSJ9.emAvCZywGE0m58gXCW4GF_J31XTzIkhsMQU4mmw-jmlpWqdqgqndsQS_I8aLavBDAKpeq9dYdaJdbwmwrEwYCw');
    return this.http.post('api/admission-info', body)
      .map(res => res.json().data as formResponse[])
      .catch(this.handleError);

  }

  getList(perPage: number, currentPage: number): Observable<formResponse> {
    return this.http.get(API_URL + this.apiRoute + "?perpage=" + perPage + "&page=" + currentPage)
      .map(res =><formResponse>res.json())
      .catch(this.handleError);
  }

  getContactById(id:string):Observable<StudentForm> {
    return this.http.get(API_URL + this.apiRoute + "/" + id)
      .map(res =><StudentForm>res.json().result)
      .catch(this.handleError);
  }

  deleteList(objUpdate:StudentForm) {
    objUpdate.deleted = true;
    let body = JSON.stringify({});
    return this.http.patch(API_URL + this.apiRoute + "/" +  objUpdate._id, body)
      .map(res => res.json())
      .catch(this.handleError);
  }

  updateStatus(objUpdate: StudentForm): Observable<any> {
    let body = objUpdate;
      return this.http.put('api/admission-info-admin/' + objUpdate._id, body)
        .map(()=> objUpdate)
        .catch(this.handleError);
  }
}
