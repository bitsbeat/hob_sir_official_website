import {NgModule} from "@angular/core";
import{SharedModule} from '../../../shared/shared.module';
import { ApplicantListComponent } from './applicant-list.component';
import { ApplicantViewComponent } from './applicant-view.component';
import {ApplicationService} from './admission.service';

@NgModule({
  imports: [SharedModule],
  declarations: [ ApplicantListComponent, ApplicantViewComponent],
  providers: [ApplicationService]
})
 export class AdmissionModule {}
