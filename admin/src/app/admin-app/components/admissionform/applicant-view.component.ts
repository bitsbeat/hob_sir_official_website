import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import {ApplicationService} from './admission.service';
import {StudentForm} from './form.model';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import * as moment from 'moment';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'view-list',
  templateUrl: './applicant-view.html',
})

export class ApplicantViewComponent implements OnInit {
 studentId: string;
  ObjView: StudentForm = new StudentForm();
  error: any;

  status: string[] = ['Applied','Under-Review','Verified','Rejected'];
  editForm: FormGroup;

  constructor (private ObjService: ApplicationService,
               private fb: FormBuilder,
               private route: ActivatedRoute ,
               private location: Location) {
    route.params.subscribe(params => this.studentId = params['id']);
    this.updateForm();
  }
  updateForm() {
    this.editForm = this.fb.group(
      {"admissionStatus":'Applied',
      "remarks": '',
      "_id":this.studentId});
  }

  ngOnInit(): void {
    this.getStudentDetail();
    if(this.studentId){
      this.getUpdatedValue();
    }

  }

  getStudentDetail() {
    this.ObjService.getContactById(this.studentId)
      .subscribe(respUser => this.handleDetail(respUser),
        error => this.error = error);
  }
  handleDetail(ObjView: StudentForm) {
    ObjView.addedOn = moment(ObjView.addedOn).format('LLL');
    this.ObjView= ObjView;
  }
  triggerCancelView(event?: Event) {
       this.location.back();
  }

  getUpdatedValue() {
    this.ObjService.getContactById(this.studentId).subscribe(res =>this.bindDetail(res),
      error => this.errorMessage(error));
  }

  bindDetail(objRes: StudentForm) {
    this.editForm.setValue({
      "admissionStatus": objRes.admissionStatus,
      "remarks": objRes.remarks,
      "_id": this.studentId
    });

  }
  updateView() {
    this.ObjService.updateStatus(this.editForm.value).subscribe(newdata =>
        this.resStatusMessage(newdata),
      error => this.errorMessage(error));
  }
    resStatusMessage(objSave: any) {
      swal("Successfully Updated", objSave.message, "success");
      this.location.back();
  }
    errorMessage(objResp: any) {
      swal("Alert!", objResp.message, "info");
    }

}
