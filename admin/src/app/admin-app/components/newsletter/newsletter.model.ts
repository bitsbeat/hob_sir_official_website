/**
 * Created by lakhe on 11/24/16.
 */

export class NewsletterModel {
  constructor(){
    this.subscribed = true;
  }
  _id:string;
  email:string;
  subscribed:boolean;
  addedBy:string;
  addedOn:string;
  unSubscribedDate:string;
}

export class NewsletterResponse {
  dataList:NewsletterModel[];
  totalItems:number;
  currentPage:number;
}
