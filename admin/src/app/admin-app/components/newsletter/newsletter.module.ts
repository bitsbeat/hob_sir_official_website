/**
 * Created by bishwo on 15/06/2017.
 */
import { NgModule }      from '@angular/core';
import { NewsletterService } from "./newsletter.service";
import { NewsletterComponent } from"./newsletter-list.component";

import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  imports: [SharedModule.forRoot() ],
  declarations: [NewsletterComponent],
  providers: [NewsletterService]
})

export class NewsletterModule {
}
