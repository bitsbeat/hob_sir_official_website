/**
 * Created by lakhe on 11/24/16.
 */
import {Component, ElementRef, OnInit} from '@angular/core';
import {NewsletterService} from "./newsletter.service";
import {NewsletterModel, NewsletterResponse} from "./newsletter.model";

@Component({
  selector: 'newsletter-list',
  templateUrl: './newsletter-list.html',
  styleUrls: ['newsletter.css']
})

export class NewsletterComponent implements OnInit {

  objResponse: NewsletterResponse = new NewsletterResponse();
  subscribed: boolean = true;
  showInfo: boolean = false;
  /* Pagination */
  perPage: number = 10;
  currentPage: number = 1;
  totalPage: number = 1;
  first: number = 0;
  bindSort: boolean = false;
  preIndex: number = 1;
  /* End Pagination */
  showForm: boolean = false;

  ngOnInit() {
    this.perPage = 10;
    this.currentPage = 1;
    this.getNewsletterSubscribedUsers();
  }

  constructor(private _objService: NewsletterService) {
  }

  getNewsletterSubscribedUsers() {
    this._objService.getNewsletterSubscribedUsers(this.perPage, this.currentPage, this.subscribed)
      .subscribe(objRes =>this.bindList(objRes),
        error => this.errorMessage(error));
  }

  errorMessage(objResponse: any) {
    swal("Alert !", objResponse.message, "info");
  }

  bindList(objRes: NewsletterResponse) {
    this.objResponse = objRes;
    this.preIndex = (this.perPage * (this.currentPage - 1));
    if (objRes.totalItems > 0) {
      let totalPage = objRes.totalItems / this.perPage;
      this.totalPage = totalPage > 1 ? Math.ceil(totalPage) : 1;
      if (!this.bindSort) {
        this.bindSort = true;
        this.sortTable();
      }
      else
        jQuery("table").trigger("update", [true]);
    }
  }

  sortTable() {
    setTimeout(()=> {
      jQuery('.tablesorter').tablesorter({
        headers: {
          2: {sorter: false},
          3: {sorter: false},
          4: {sorter: false}
        }
      });
    }, 50);
  }

  pageChanged(event) {
    if (event.rows) {
      this.perPage = event.rows;
      this.currentPage = (Math.floor(event.first / event.rows)) + 1;
      this.first = event.first;
      if (event.first == 0)
        this.first = 1;
      this.getNewsletterSubscribedUsers();
    }
  }

  search() {
    this._objService.getNewsletterSubscribedUsers(this.perPage, this.currentPage, this.subscribed)
      .subscribe(objRes =>this.bindList(objRes),
        error => this.errorMessage(error));
  }
}

