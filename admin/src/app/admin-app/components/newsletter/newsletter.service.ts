/**
 * Created by lakhe on 11/24/16.
 */
import {NewsletterModel, NewsletterResponse} from './newsletter.model';
import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Observable} from "rxjs/Observable";
import{ API_URL} from "../../../shared/configs/env.config";
@Injectable()
export class NewsletterService {
  apiRoute:string = "newsletter/subscribe";

  constructor(private _http:Http) {
  }


  getNewsletterSubscribedUsers(perPage:number, currentPage:number, subscribed:boolean):Observable < NewsletterResponse> {
    return this._http.get(API_URL + this.apiRoute + "?perpage=" + perPage + "&page=" + currentPage + "&subscribed=" + subscribed)
      .map(res =><NewsletterResponse>res.json())
      .catch(this.handleError);
  }

  handleError(error) {
    console.log(error.json());
    return Observable.throw(error.json() || 'server error');
  }


}
