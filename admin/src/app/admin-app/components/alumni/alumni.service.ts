import {Injectable} from "@angular/core";
import {API_URL} from "../../../shared/configs/env.config";
import {Http} from "@angular/http";
import {FileOperrationService} from "../../../shared/services/fileOperation.service";
import {Observable} from "rxjs/Observable";
import {AlumniByYearModel, AlumniByYearResponse, AlumniModel, AlumniResponse} from "./alumni.model";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import {Config} from "../../../shared/configs/general.config";


@Injectable()
export class AlumniService{
   alumniCategoryApiRoute: string = "alumni-category";
   alumniApiRoute: string = "alumni";
   progressObserver:any;
   progress:any;

   constructor(private http: Http,
               private fileService: FileOperrationService){
     this.progress = Observable.create(observer => {
       this.progressObserver = observer
     }).share();
   }
   /* Handle alumni category */
   postAlumniCategory(objCategory: AlumniByYearModel): Observable<any>{
     return this.http.post(API_URL + this.alumniCategoryApiRoute, objCategory)
       .map(res => res.json()).catch(this.handleError);
   }

   getCategories(): Observable<any> {
     return this.http.get(API_URL + this.alumniCategoryApiRoute)
       .map(res => res.json())
       .catch(this.handleError);
   }

   getAllCategory(perPage: number, currentPage: number): Observable<AlumniByYearResponse> {
     let queryString: string = "";
     queryString += perPage ? "?perpage=" + perPage : "";
     queryString += currentPage ? "&page=" + currentPage : "";
     return this.http.get(API_URL + this.alumniCategoryApiRoute + queryString)
       .map(res => <AlumniByYearResponse>res.json())
       .catch(this.handleError);
   }

   deleteCategory(objId: AlumniByYearModel): Observable<any> {
     let body = JSON.stringify({});
     return this.http.patch(API_URL + this.alumniCategoryApiRoute + "/" + objId._id, body)
       .map(res => res.json())
       .catch(this.handleError);
   }

   /* Handle Alumni Details */
   postAlumni(objAlumni: AlumniModel, file: File): Observable<any> {
     return Observable.create(observer => {
       let formData: FormData = new FormData(),
           xhr: XMLHttpRequest = new XMLHttpRequest();

       if(file){
         formData.append('imageName', file);
       }
       formData.append('data', JSON.stringify(objAlumni));
       xhr.onreadystatechange = () => {
         if(xhr.readyState === 4){
           if(xhr.status === 200){
             observer.next(JSON.parse(xhr.response));
             observer.complete();
           }
           else {
             observer.error(JSON.parse(xhr.response));
           }
         }
       };
       xhr.upload.onprogress = (event) => {
         this.progress = Math.round(event.loaded / event.total * 100);
       };
       xhr.open('POST', API_URL + this.alumniApiRoute, true);
       xhr.setRequestHeader("Authorization", Config.AuthToken);
       xhr.send(formData);
     })
   }

   updateAlumni(alumniId: string, objAlumni: AlumniModel, file: File, imageDeleted: boolean): Observable<any>{
     return Observable.create(observer => {
       let formData: FormData = new FormData(),
            xhr: XMLHttpRequest = new XMLHttpRequest();
       if(file){
         formData.append('imageName', file);
       }
       formData.append('data', JSON.stringify(objAlumni));
       xhr.onreadystatechange = () => {
         if(xhr.readyState === 4){
           if(xhr.status === 200){
             observer.next(JSON.parse(xhr.response));
             observer.complete();
           }
           else {
             observer.error(JSON.parse(xhr.response));
           }
         }
       };
       xhr.upload.onprogress = (event) => {
         this.progress = Math.round(event.loaded / event.total *100);
       };
       xhr.open('PUT', API_URL + this.alumniApiRoute + "/" + alumniId + "?imagedeleted" + imageDeleted, true);
       xhr.setRequestHeader("Authorization", Config.AuthToken);
       xhr.send(formData);
     })
   }

   getAlumniList(perPage: number, currentPage: number, categoryId?:string): Observable<AlumniResponse>{
     let queryString: string = "";
     queryString += perPage? "?perpage=" + perPage : "";
     queryString += currentPage? "&page=" + currentPage : "";
     queryString += categoryId ? "&categoryid=" + categoryId : "";
     return this.http.get(API_URL + this.alumniApiRoute + queryString)
       .map(res => <AlumniResponse>res.json())
       .catch(this.handleError);
   }

   getAlumniDetail(perPage: number, currentPage: number, urlSlog: string): Observable<any>{
     let queryString: string = "";
     queryString += perPage? "?perpage=" + perPage: "";
     queryString += currentPage? "&page=" + currentPage: "";
     return this.http.get(API_URL + "filter/" + this.alumniCategoryApiRoute + "/" + urlSlog + "/" + queryString)
       .map(res => res.json())
       .catch(this.handleError);
   }

   getAlumniData(slog: string): Observable<any> {
     return this.http.get(API_URL +  this.alumniApiRoute + "/" + slog)
       .map(res => res.json())
       .catch(this.handleError);
   }

   deleteAlumni(alumniId: AlumniModel): Observable<AlumniResponse> {
     let body = JSON.stringify({});
     return this.http.patch(API_URL + this.alumniApiRoute + "/" + alumniId._id, body)
       .map(res => res.json())
       .catch(this.handleError);
   }

   deleteImage(fileName: string, orgExt: string, path: string): Observable<any>{
     return this.fileService.deleteFile(fileName, orgExt, path, "image");
   }

  handleError(error) {
    return Observable.throw(error.json() || 'server error');
  }
}
