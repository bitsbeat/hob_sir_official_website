import {NgModule} from "@angular/core";
import {AlumniService} from "./alumni.service";
import {AlumniListComponent} from "./alumni-list.component";
import {ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../../../shared/shared.module";
import {AlumniMgmtComponent} from "./alumni-mgmt.component";
import {AlumniEditorComponent} from "./alumni-editor.component";
import {AlumniCategoryListComponent} from "./alumni-category-list.component";
import {AddAlumniComponent} from "./add-alumni.component";

@NgModule({
  imports:[ReactiveFormsModule, CommonModule, SharedModule],
  declarations: [AlumniMgmtComponent, AlumniListComponent,
    AlumniEditorComponent, AlumniCategoryListComponent, AddAlumniComponent],
  providers:[AlumniService]
})

export class AlumniModule{}
