import {Component, OnInit} from "@angular/core";
import {AlumniByYearResponse, AlumniModel, AlumniResponse} from "./alumni.model";
import {AlumniService} from "./alumni.service";
import {Router} from "@angular/router";


@Component({
  selector: 'alumni-list',
  templateUrl: './alumni-list.html'
})

export class AlumniListComponent implements OnInit{
  objAlumniResponse: AlumniResponse = new AlumniResponse();
  objByYear: AlumniByYearResponse;
  perPage: number = 10;
  currentPage: number = 1;
  totalPage: number = 1;
  first: number = 0;
  bindSort: boolean = false;
  preIndex: number = 0;
  constructor(private objService: AlumniService,
              private router: Router){}

  ngOnInit(){
    this.perPage = 10;
    this.currentPage = 1;
    this.getAlumniList();
    this.getAlumniYear();
  }

  getAlumniYear(){
    this.objService.getCategories()
      .subscribe(res => this.objByYear = res,
        error => this.errorMessage(error));
  }

  categoryFilter(args) {
    let categoryId = args.target.value;
    this.currentPage = 1;
    this.objService.getAlumniList(this.perPage, this.currentPage, categoryId)
      .subscribe(res => this.bindList(res),
        error=>this.errorMessage(error));
  }

  getAlumniList(){
   this.objService.getAlumniList(this.perPage, this.currentPage)
     .subscribe(data => this.bindList(data),
       error => this.errorMessage(error))
  }

  errorMessage(objResponse:any) {
    swal("Alert !", objResponse.message, "info");
  }

  bindList(objResp: AlumniResponse){
    this.objAlumniResponse = objResp;
    this.preIndex = (this.perPage * (this.currentPage -1));
    if(objResp.dataList.length > 0){
      let totalPage = objResp.totalItems / this.perPage;
      this.totalPage = totalPage > 1 ? Math.ceil(totalPage): 1;
      if(!this.bindSort){
        this.bindSort = true;
        this.setTable();
      }
      else
        jQuery("table").trigger("update", [true]);
    }
    else
      jQuery(".tablesorter").find('thead th').unbind('click mousedown').removeClass('header headerSortDown headerSortUp');
  }
  setTable(){
   setTimeout(()=>{
     jQuery('.tablesorter').tablesorter({
       headers:{
         3: {sorter: false},
         4: {sorter: false}
       }
     });
   }, 50);
  }

  addAlumni(){
    this.router.navigate(['/admin/alumni/alumni-list/alumni-editor']);
  }

  edit(id:string){
    this.router.navigate(['/admin/alumni/alumni-list/alumni-editor', id]);
  }

  delete(id:string) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Info !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      ()=> {
        let objTemp:AlumniModel = new AlumniModel();
        objTemp._id = id;
        objTemp.deleted = true;
        this.objService.deleteAlumni(objTemp)
          .subscribe(res=> {
              this.getAlumniList();
              swal("Deleted!", res.message, "success");
            },
            error=> {
              swal("Alert!", error.message, "info");

            });
      });
  }
  pageChanged(event) {
    this.perPage = event.rows;
    this.currentPage = (Math.floor(event.first / event.rows)) + 1;
    this.first = event.first;
    if (event.first == 0)
      this.first = 1;
    this.getAlumniList();
  }
}
