// import {ImageProperties} from "../../../shared/models/image.model";

export class AlumniByYearModel{
  _id: string;
  year: string;
  urlSlogCategory: string;
  addedBy: string;
  addedOn: string;
  updatedBy: string;
  updatedOn: string;
  deleted: boolean;
  deletedBy: string;
  deletedOn: string;
}

export class AlumniByYearResponse{
  dataList: AlumniByYearModel[];
  totalItems: number;
  currentPage: number;
}

export class AlumniModel{
   _id: string;
  studentName: string;
  urlSlog: string;
  categoryId: string;
  batchYear: string;
  imageName: string;
  imageProperties: ImageProperties;
  organizationName: string;
  organizationPosition: string;
  addedBy: string;
  addedOn: string;
  updatedBy: string;
  updatedOn: string;
  deleted: boolean;
  deletedBy: string;
  deletedOn: string;
}

export class AlumniResponse{
  dataList: AlumniModel[];
  totalItems: number;
  currentPage: number;
  message: string;
}


class ImageProperties{
  imageExtension: string;
  imageMimeType: string;
  imageSize: string;
  imageOriginalName: string;
  imagePath: string;
}
