import {Component, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AlumniService} from "./alumni.service";
import {Location} from "@angular/common";


@Component({
  selector: 'add-alumni',
  templateUrl: './add-alumni.html'
})

export class AddAlumniComponent {
  alumniForm: FormGroup;
  isSubmitted: boolean = false;
  alumniCatList: any;

  constructor(private fb: FormBuilder,
              private objService: AlumniService,
              private location: Location){
    this.alumniForm = this.fb.group({
      "year": ['', Validators.required]
    });
  }

  saveAlumniCategory(){
    if(this.alumniForm.valid){
      this.isSubmitted = true;
      this.objService.postAlumniCategory(this.alumniForm.value)
        .subscribe(res =>  this.resStatusMessage(res),
          error => this.errorMessage(error)
        );
    }
  }

  resStatusMessage(res:any){
    swal("Success !", res.message, "success");
    this.alumniForm.reset();
    this.location.back();
  }

  errorMessage(objResponse:any){
    swal("Alert !", objResponse.message, "info");
  }

  triggerCancelForm(){
  this.location.back();
  }

}
