import {AfterViewInit, Component, OnInit} from "@angular/core";
import {AlumniByYearModel, AlumniModel} from "./alumni.model";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ImageCanvasSizeEnum} from "../../../shared/configs/enum.config";
import {Config} from "../../../shared/configs/general.config";
import {AlumniService} from "./alumni.service";
import {Location} from "@angular/common";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'alumni-editor',
  templateUrl: './alumni-editor.html'
})

export class AlumniEditorComponent implements OnInit, AfterViewInit{
  objAlumni: AlumniModel = new AlumniModel();
  objAlumniCat: AlumniByYearModel[];
  alumniForm: FormGroup;
  id: string = "";
  isSubmitted: boolean = false;
  imageDeleted: boolean = false;
  file: File;
  fileName: string = "";
  drawImagePath: string = Config.DefaultImage;
  imageFormControl: FormControl = new FormControl('', Validators.required);
  canvasSize: number = ImageCanvasSizeEnum.small;

  constructor(private fb: FormBuilder,
              private objService: AlumniService,
              private location: Location,
              private activated: ActivatedRoute){
    this.alumniForm = this.fb.group({
      "studentName": ['', Validators.required],
      "categoryId": ['', Validators.required],
      "batchYear": ['', Validators.required],
      "organizationName": '',
      "organizationPosition":'',
      "imageFormControl": this.imageFormControl
    });

    activated.params.subscribe(params=> this.id = params['id'])
  }

  ngOnInit(){
    this.getCategoryList();
    if(this.id) {
      this.getAlumniDtl();
    }
  }

  ngAfterViewInit(){
    if (!this.id)
      this.drawImageToCanvas(this.drawImagePath);
  }

  getCategoryList(){
    this.objService.getAllCategory(100, 1)
      .subscribe(res => {
        this.objAlumniCat = res.dataList;
      },
        error => this.errorMessage(error))
  }

  getAlumniDtl(){
    this.objService.getAlumniData(this.id)
      .subscribe(res =>
          this.bindAlumniData(res),
        error => this.errorMessage(error));
  }

  errorMessage(objResponse:any) {
    swal("Alert !", objResponse.message, "info");
  }

  bindAlumniData(objRes: AlumniModel){
    this.objAlumni = objRes;
    this.alumniForm.setValue({
      "studentName": objRes.studentName,
      "categoryId": objRes.categoryId,
      "batchYear": objRes.batchYear,
      "organizationName": objRes.organizationName,
      "organizationPosition": objRes.organizationPosition,
      "imageFormControl": objRes.imageName
    });
    this.objAlumni.imageProperties = objRes.imageProperties;
    (<FormControl>this.alumniForm.controls['imageFormControl']).patchValue(objRes.imageName);
    let path: string = "";
    if (this.objAlumni.imageName) {
      var cl = Config.Cloudinary;
      path = cl.url(this.fileName);
    }
    else {
      path = Config.DefaultWideImage;
      this.drawImageToCanvas(path);
    }
  }

  saveAlumni(){
    this.isSubmitted = true;
    if(this.alumniForm.valid){
     if(!this.id){
       this.objService.postAlumni(this.alumniForm.value, this.file)
         .subscribe(data => this.resStatusMessage(data),
           error => this.errorMessage(error));
     }
     else {
       this.objService.updateAlumni(this.id, this.alumniForm.value, this.file, this.imageDeleted)
         .subscribe(data => this.resStatusMessage(data),
           error => this.errorMessage(error));
     }

    }
  }
  resStatusMessage(objSave: any){
    swal("Success !", objSave.message, "success");
    this.location.back();
  }
  triggerCancelForm(){
    this.location.back();
  }

  /*Image Handler */
  changeFile(args) {
    this.file = args;
    this.fileName = this.file.name;
  }

  drawImageToCanvas(path:string) {
    this.drawImagePath = path;
  }

  deleteImage(imageId) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Image !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      ()=> {
        this.objService.deleteImage(this.objAlumni.imageName, this.objAlumni.imageProperties.imageExtension, this.objAlumni.imageProperties.imagePath)
          .subscribe(res=> {
              this.imageDeleted = true;
              this.objAlumni.imageName = "";
              this.fileName = "";
              this.drawImageToCanvas(Config.DefaultImage);
              swal("Deleted!", res.message, "success");
            },
            error=> {
              swal("Alert!", error.message, "info");

            });
      });
  }
}
