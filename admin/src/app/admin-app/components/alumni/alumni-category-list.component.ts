import {Component, OnInit} from "@angular/core";
import {AlumniByYearModel, AlumniByYearResponse} from "./alumni.model";
import {AlumniService} from "./alumni.service";
import {Router} from "@angular/router";

@Component({
  selector: 'alumni-category',
  templateUrl: './alumni-category-list.html'
})

export class AlumniCategoryListComponent implements OnInit{
  objCatResp: AlumniByYearResponse = new AlumniByYearResponse();
  catId: string;
  /* Pagination */
  perPage:number = 10;
  first:number = 0;
  bindSort:boolean = false;
  currentPage:number = 1;
  totalPage:number = 1;
  preIndex: number = 0;

  constructor(private objService: AlumniService,
              private router: Router){}

  ngOnInit(){
    this.perPage = 10;
    this.currentPage = 1;
    this.getAlumniCategory();
  }

  getAlumniCategory(){
    this.objService.getAllCategory(this.perPage, this.currentPage)
      .subscribe(response => this.bindCatList(response),
    error => this.errorMessage(error)
      );
 }

 errorMessage(objResponse:any){
   swal("Alert !", objResponse.message, "info");
 }
  bindCatList(objRes: AlumniByYearResponse){
    this.objCatResp = objRes;
    this.preIndex = (this.perPage * (this.currentPage - 1));
    if (objRes.dataList.length > 0) {
      let totalPage = objRes.totalItems / this.perPage;
      this.totalPage = totalPage > 1 ? Math.ceil(totalPage) : 1;
      if (!this.bindSort) {
        this.bindSort = true;
        this.sortTable();
      }
      else
        jQuery("table").trigger("update", [true]);
    }
  }
  sortTable(){
    setTimeout(()=> {
      jQuery('.tablesorter').tablesorter({
        headers: {
          2: {sorter: false},
          3: {sorter: false}
        }
      });
    }, 50);
  }

  addCategory(){
    this.router.navigate(['/admin/alumni/alumni-year/add-alumni']);
  }

  delete(id:string) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Alumni Category !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      ()=> {
        let objTemp:AlumniByYearModel = new AlumniByYearModel();
        objTemp._id = id;
        objTemp.deleted = true;
        this.objService.deleteCategory(objTemp)
          .subscribe(res=> {
              this.getAlumniCategory();
              swal("Deleted!", res.message, "success");
            },
            error=> {
              swal("Alert!", error.message, "info");

            });
      });
  }

  pageChanged(event) {
    this.perPage = event.rows;
    this.currentPage = (Math.floor(event.first / event.rows)) + 1;
    this.first = event.first;
    if (event.first == 0)
      this.first = 1;
    this.getAlumniCategory();
  }
}
