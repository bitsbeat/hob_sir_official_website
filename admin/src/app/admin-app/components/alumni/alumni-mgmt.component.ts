import {Component} from "@angular/core";
import {Router} from "@angular/router";

@Component({
  selector: 'alumni',
  templateUrl: './alumni-mgmt.html'
})

export class AlumniMgmtComponent {

  constructor(private router: Router) {
  }

  public tabSwitch(args) {
    if (1 == args.index) {
      this.router.navigate(['/admin/alumni/alumni-year']);
    }
    else {
      this.router.navigate(['/admin/alumni/alumni-list']);
    }
  }
}
