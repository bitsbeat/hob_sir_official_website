import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { OnInit, EventEmitter } from '@angular/core';
import { ImgAlbumResponse, ImgAlbumModel } from './img-gallery.model';
import { Component } from '@angular/core';
import { ImgGalleryService } from "./img-gallery.service";

@Component({
    selector: 'img-gallery-album-list',
    templateUrl: './img-gallery-album-list.component.html'
})

export class ImgGalleryAlbumListComponent implements OnInit {
    objListResponse: ImgAlbumResponse;
    albumId: string;

    perPage: number = 10;
    currentPage: number = 1;
    totalPage: number = 1;
    first: number = 0;
    bindSort: boolean = false;
    preIndex: number = 0;

    ngOnInit() {
        this.getAlbumList();
    }

    constructor(private _objService: ImgGalleryService, private router: Router, private location: Location) {
    }

    getAlbumList() {
        this._objService.getImgAlbumList(this.perPage, this.currentPage)
            .subscribe(res => this.bindList(res),
            error => this.errorMessage(error)
            )
    }

    errorMessage(objResponse: any) {
        swal("Alert !", objResponse.message, "info");
    }

    bindList(objRes: ImgAlbumResponse) {
        this.objListResponse = objRes;
        this.preIndex = (this.perPage * (this.currentPage - 1));

        if (objRes.dataList.length > 0) {
            let totalPage = objRes.totalItems / this.perPage;
            this.totalPage = totalPage > 1 ? Math.ceil(totalPage) : 1;

            /*End Pagination */
            if (!this.bindSort) {
                this.bindSort = true;
                this.sortTable();
            }
            else
                jQuery("table").trigger("update", [true]);
        }
    }

    sortTable() {
        setTimeout(() => {
            jQuery('.tablesorter').tablesorter({
                headers: {
                    2: { sorter: false },
                    3: { sorter: false }
                }
            });
        }, 50);
    }

    addImgAlbum(){
        this.router.navigate(['/admin/img-gallery/img-gallery-album-editor']);
    }

    edit(id: string){
        this.router.navigate(['/admin/img-gallery/img-gallery-album-editor', id]);
    }

    showImageList(id: string) {
        this.router.navigate(['/admin/img-gallery/img-gallery-list', id]);
    }

    delete(id: string) {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this album !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },
            () => {
                let objAlbum: ImgAlbumModel = new ImgAlbumModel();
                objAlbum._id = id;
                objAlbum.deleted = true;
                this._objService.deleteImgAlbum(objAlbum)
                    .subscribe(res => {
                        this.getAlbumList();
                        swal("Deleted!", res.message, "success");
                    },
                    error => {
                        swal("Alert!", error.message, "info");
                    });
            });
    }

    pageChanged(event) {
        this.perPage = event.rows;
        this.currentPage = (Math.floor(event.first / event.rows)) + 1;
        this.first = event.first;
        if (event.first == 0)
            this.first = 1;
        this.getAlbumList();
    }
}