import { Config } from './../../../shared/configs/general.config';
import { Observable } from 'rxjs/Observable';
import { ImgAlbumModel, ImgGalleryResponse, ImgGalleryModel } from './img-gallery.model';
import { ImgAlbumResponse } from './img-gallery.model';
import { API_URL } from './../../../shared/configs/env.config';
import { FileOperrationService } from './../../../shared/services/fileOperation.service';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";

@Injectable()

export class ImgGalleryService{
    imageAlbumApiRoute:string = "gallery/album";
    imageGalleryApiRoute:string = "gallery/albumimage";
    progressObserver:any;
    progress:any;

    constructor(private _http:Http, private fileService:FileOperrationService) {
        this.progress = Observable.create(observer => {
            this.progressObserver = observer
        }).share();
    }

    getAllAlbumWithCoverImage(perPage:number, currentPage:number): Observable<ImgAlbumResponse> {
      return this._http.get(API_URL + 'gallery/album-cover'+ "?perpage=" + perPage + "&page=" + currentPage)
        .map(res => <ImgAlbumResponse> res.json())
        .catch(this.handleError);
    }

    getImgAlbumDetail(objId: string): Observable<ImgAlbumModel> {
        return this._http.get(API_URL + this.imageAlbumApiRoute + "/" + objId)
                    .map(res => <ImgAlbumModel> res.json())
                    .catch(this.handleError);
    }

    getImgAlbumList(perPage:number, currentPage:number):Observable < ImgAlbumResponse> {
        return this._http.get(API_URL + this.imageAlbumApiRoute + "?perpage=" + perPage + "&page=" + currentPage)
            .map(res =><ImgAlbumResponse>res.json())
            .catch(this.handleError);
    }

    getAlbumImgList(albumId: string, perPage?: number, currentPage?: number): Observable<ImgGalleryResponse>{
        return this._http.get(API_URL + this.imageGalleryApiRoute + "/" + albumId + "?perpage=" + perPage + "&page=" + currentPage)
            .map(res =><ImgGalleryResponse>res.json())
            .catch(this.handleError);
    }

    getImgGalleryDetail(albumId: string, imageId: string){
        return this._http.get(API_URL + this.imageGalleryApiRoute + "/" + albumId + "/" + imageId)
            .map(res =><ImgGalleryModel>res.json())
            .catch(this.handleError);
    }

    saveImgAlbum(objSave:ImgAlbumModel) {
        let body = JSON.stringify(objSave);
        return this._http.post(API_URL + this.imageAlbumApiRoute, body)
            .map(res => res.json())
            .catch(this.handleError);
    }

    updateImgAlbum(albumId: string, albumDetails: ImgAlbumModel){
        let body = JSON.stringify(albumDetails);
        return this._http.put(API_URL + this.imageAlbumApiRoute + "/" + albumId, body)
                    .map(res => res.json())
                    .catch(this.handleError);
    }

    deleteImgAlbum(objAlbum: ImgAlbumModel) {
        let body = JSON.stringify({});
        return this._http.patch(API_URL + this.imageAlbumApiRoute + "/" + objAlbum._id, body)
                        .map(res => <any>res.json())
                        .catch(this.handleError);
    }

    saveImg(albumId:string, objSave:ImgGalleryModel, file:File):Observable<any> {
        return Observable.create(observer => {
            let formData:FormData = new FormData(),
                xhr:XMLHttpRequest = new XMLHttpRequest();

            if (file) {
                formData.append('imageName', file);
            }
            formData.append('data', JSON.stringify(objSave));
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        observer.next(JSON.parse(xhr.response));
                        observer.complete();
                    } else {
                        observer.error(JSON.parse(xhr.response));
                    }
                }
            };
            xhr.upload.onprogress = (event) => {
                this.progress = Math.round(event.loaded / event.total * 100);
                //this.progressObserver.next(this.progress);
            };
            xhr.open('POST', API_URL + this.imageGalleryApiRoute + "/" + albumId, true);
            xhr.setRequestHeader("Authorization", Config.AuthToken);
            xhr.send(formData);
        });
    }

    updateImg(albumId:string, imageId: string, objUpdate:ImgGalleryModel, file:File, imageDeleted:boolean):Observable<any> {
        return Observable.create(observer => {
            let formData:FormData = new FormData(),
                xhr:XMLHttpRequest = new XMLHttpRequest();

            if (file) {
                formData.append('imageName', file);
            }
            formData.append('data', JSON.stringify(objUpdate));
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        observer.next(JSON.parse(xhr.response));
                        observer.complete();
                    } else {
                        observer.error(JSON.parse(xhr.response));
                    }
                }
            };
            xhr.upload.onprogress = (event) => {
                this.progress = Math.round(event.loaded / event.total * 100);
                //this.progressObserver.next(this.progress);
            };
            xhr.open('PUT', API_URL + this.imageGalleryApiRoute + "/" + albumId + "/" + imageId + "?imagedeleted=" + imageDeleted, true);
            xhr.setRequestHeader("Authorization", Config.AuthToken);
            xhr.send(formData);
        });
    }

    deleteAlbumImg(albumId: string, imageId: string): Observable<any>{
        return this._http.delete(API_URL + this.imageGalleryApiRoute + "/" + albumId + "/" + imageId)
            .map(res =><any>res.json())
            .catch(this.handleError);
    }

    updateCoverImg(albumId:string, objImage:ImgGalleryModel) {
        let body = JSON.stringify({});
        return this._http.patch(API_URL + this.imageGalleryApiRoute + "/" + albumId + "/" + objImage._id, body)
            .map(res => res.json());
    }

    deleteImgFile(fileName:string, orgExt:string, path:string):Observable < any > {
        return this.fileService.deleteFile(fileName, orgExt, path, "image");

    }

    handleError(error) {
        console.log(error.json());
        return Observable.throw(error.json() || 'server error');
    }
}
