import { ImgGalleryService } from './img-gallery.service';
import { Validators } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Config } from './../../../shared/configs/general.config';
import { ImgGalleryModel } from './img-gallery.model';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Component, AfterViewInit } from '@angular/core';
import { Location } from '@angular/common';
import { ImageCanvasSizeEnum } from "../../../shared/configs/enum.config";

@Component({
    selector: 'img-gallery-editor',
    templateUrl: './img-gallery-editor.component.html'
})

export class ImgGalleryEditorComponent implements AfterViewInit {
    objImgGallery: ImgGalleryModel = new ImgGalleryModel();
    imgGalleryForm: FormGroup;
    isSubmitted: boolean = false;
    imageId: string;
    albumId: string;

    imageDeleted: boolean = false;
    file: File;
    fileName: string = "";
    drawImagePath: string = Config.DefaultImage;
    imageFormControl: FormControl = new FormControl('', Validators.required);
    canvasSize: number = ImageCanvasSizeEnum.wide;

    constructor(private _objService: ImgGalleryService, private _formBuilder: FormBuilder, private location: Location, private activatedRoute: ActivatedRoute) {
        this.imgGalleryForm = this._formBuilder.group({
            "imageTitle": ['', Validators.required],
            "imageAltText": ['', Validators.required],
            "imageFormControl": this.imageFormControl,
            "active": ['']
        });
        activatedRoute.params.subscribe(params => this.albumId = params['id']);
        activatedRoute.params.subscribe(params => this.imageId = params['imageId']);
    }

    ngAfterViewInit() {
        if (!this.imageId)
            this.drawImageToCanvas(Config.DefaultImage);
    }

    ngOnInit() {
        if (this.imageId)
            this.getAlbumImgDetail();
    }

    getAlbumImgDetail() {
        this._objService.getImgGalleryDetail(this.albumId, this.imageId)
            .subscribe(res => this.bindDetail(res),
            error => this.errorMessage(error));
    }

    bindDetail(objRes: ImgGalleryModel) {
        this.imgGalleryForm.setValue({
            "imageTitle": objRes.imageTitle,
            "imageAltText": objRes.imageAltText,
            "active": objRes.active,
            "imageFormControl": objRes.imageName
        });
        this.objImgGallery = objRes;
        this.objImgGallery.imageName = objRes.imageName;
        this.objImgGallery.imageProperties = objRes.imageProperties;
        let path: string = "";
        if (this.objImgGallery.imageName) {
            this.fileName = this.objImgGallery.imageName;
            (<FormControl>this.imgGalleryForm.controls['imageFormControl']).patchValue(this.fileName);
            var cl = Config.Cloudinary;
            path = cl.url(this.objImgGallery.imageName);
        }
        else
            path = Config.DefaultImage;
        this.drawImageToCanvas(path);
    }

    saveImgGallery() {
        this.isSubmitted = true;
        (<FormControl>this.imgGalleryForm.controls['imageFormControl']).patchValue(this.fileName);
        if (this.imgGalleryForm.valid) {
            if (!this.imageId) {
                this._objService.saveImg(this.albumId, this.imgGalleryForm.value, this.file)
                    .subscribe(res => this.resStatusMessage(res),
                    error => this.errorMessage(error));
            }
            else {
                this._objService.updateImg(this.albumId, this.imageId, this.imgGalleryForm.value, this.file, this.imageDeleted)
                    .subscribe(res => this.resStatusMessage(res),
                    error => this.errorMessage(error));
            }
        }
    }

    resStatusMessage(objSave: any) {
        swal("Success !", objSave.message, "success");
        this.location.back();
    }

    triggerCancelForm() {
        this.location.back();
    }

    errorMessage(objResponse: any) {
        swal("Alert !", objResponse.message, "info");
    }

    changeFile(args) {
        this.file = args;
        this.fileName = this.file.name;
    }

    drawImageToCanvas(path: string) {
        this.drawImagePath = path;
    }

    deleteImage(id:string) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Image !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      ()=> {
        this._objService.deleteImgFile(this.objImgGallery.imageName, this.objImgGallery.imageProperties.imageExtension, this.objImgGallery.imageProperties.imagePath)
          .subscribe(res=> {
              this.imageDeleted = true;
              this.objImgGallery.imageName = "";
              this.fileName = "";
              this.drawImageToCanvas(Config.DefaultImage);
              swal("Deleted!", res.message, "success");
            },
            error=> {
              swal("Alert!", error.message, "info");

            });
      });

  }

}