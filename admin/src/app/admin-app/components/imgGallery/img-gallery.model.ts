import {ImageProperties} from "../../../shared/models/image.model";

export class ImgGalleryModel {
    constructor(){
        this.active = false;
    }
    _id:string;
    imageName:string;
    imageTitle:string;
    imageAltText:string;
    imageDescription:string;
    coverImage:boolean;
    imageProperties:ImageProperties;
    active:boolean;
    addedBy:string;
    addedOn:string;
    updatedBy:string;
    updatedOn:string;
    deleted:boolean;
    deletedBy:string;
    deletedOn:string;
}

export class ImgGalleryResponse{
    dataList: ImgGalleryModel[];
    totalItems: number;
    currentPage: number;
}

export class ImgAlbumModel{
    constructor(){
        this.active = false;
    }
    _id:string;
    albumName:string;
    albumDescription:string;
    active:boolean;
    image: ImgGalleryModel;
    createdOn:string;
    addedBy:string;
    addedOn:string;
    updatedBy:string;
    updatedOn:string;
    deleted:boolean;
    deletedBy:string;
    deletedOn:string;
}

export class ImgAlbumResponse{
    dataList: ImgAlbumModel[];
    totalItems: number;
    currentPage: number;
}
