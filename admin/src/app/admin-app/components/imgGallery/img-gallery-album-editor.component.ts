import { OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from "@angular/common";
import { Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { ImgGalleryService } from './img-gallery.service';
import { FormGroup } from '@angular/forms';
import { Input } from '@angular/core';
import { ImgAlbumModel } from './img-gallery.model';
import { Component } from '@angular/core';

@Component({
    selector: 'img-gallery-album-editor',
    templateUrl: './img-gallery-album-editor.component.html'
})

export class ImgGalleryAlbumEditorComponent implements OnInit{
    objAlbum: ImgAlbumModel = new ImgAlbumModel();
    albumId: string;
    imgAlbumForm: FormGroup;
    isSubmitted: boolean = false;

    constructor(private _objService: ImgGalleryService, private _formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private location: Location) {
        this.imgAlbumForm = this._formBuilder.group({
            "albumName": ['', Validators.required],
            "albumDescription": ['', Validators.required],
            "active": ['']
        });
        activatedRoute.params.subscribe(params => this.albumId = params['id']);        
    }

    ngOnInit() {
      if(this.albumId)
        this.getImgAlbumDetail();
    }

    getImgAlbumDetail() {
      this._objService.getImgAlbumDetail(this.albumId)
          .subscribe(res => this.bindDetail(res),
          error => this.errorMessage(error));
    }

    bindDetail(objRes: ImgAlbumModel){
      this.imgAlbumForm.setValue({
        "albumName": objRes.albumName,
        "albumDescription": objRes.albumDescription,
        "active": objRes.active
      });
    }

    saveAlbum() {
    this.isSubmitted = true;
    if (this.imgAlbumForm.valid) {
      if (!this.albumId) {
        this._objService.saveImgAlbum(this.imgAlbumForm.value)
          .subscribe(res => this.resStatusMessage(res),
            error =>this.errorMessage(error));
      }
      else {
        this._objService.updateImgAlbum(this.albumId, this.imgAlbumForm.value)
          .subscribe(res => this.resStatusMessage(res),
            error =>this.errorMessage(error));
      }
    }
  }

  resStatusMessage(res:any) {
    //this.showListEvent.emit(false); // * isCanceled = false
    swal("Success !", res.message, "success");
    this.location.back();
  }

  errorMessage(objResponse:any) {
    swal("Alert !", objResponse.message, "info");
  }

  triggerCancelForm() {
    this.location.back();
  }
}