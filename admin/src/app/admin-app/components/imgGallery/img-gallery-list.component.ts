import { Location } from "@angular/common";
import { ActivatedRoute, Router } from '@angular/router';
import { OnInit, Input, ViewChild } from '@angular/core';
import { ImgGalleryService } from './img-gallery.service';
import { ImgGalleryResponse, ImgGalleryModel } from './img-gallery.model';
import { Component, ElementRef } from '@angular/core';

@Component({
    selector: 'img-gallery-list',
    templateUrl: './img-gallery-list.component.html'
})

export class ImgGalleryListComponent implements OnInit {
    imgListResponse: ImgGalleryResponse = new ImgGalleryResponse();
    error: any;
    @ViewChild('prevCoverImage') prevCoverImage: ElementRef;
    albumId: string;
    perPage: number = 10;
    currentPage: number = 1;
    totalPage: number = 1;
    first: number = 0;
    bindSort: boolean = false;
    preIndex: number = 0;

    constructor(private _objService: ImgGalleryService, private eleRef: ElementRef, private activatedRoute: ActivatedRoute, private location: Location, private router: Router) {
        activatedRoute.params.subscribe(params => this.albumId = params['id']);
    }

    ngOnInit() {
        if (this.albumId)
            this.getAlbumImgList();
    }

    getAlbumImgList() {
        this._objService.getAlbumImgList(this.albumId, this.perPage, this.currentPage)
            .subscribe(objRes => this.bindList(objRes),
            error => this.errorMessage(error));
    }

    errorMessage(objResponse: any) {
        swal("Alert !", objResponse.message, "info");
    }

    bindList(objRes: ImgGalleryResponse) {
        this.imgListResponse = objRes;
        this.preIndex = (this.perPage * (this.currentPage - 1));

        /* Pagination */
        if (objRes.dataList.length > 0) {
            let totalPage = objRes.totalItems / this.perPage;
            this.totalPage = totalPage > 1 ? Math.ceil(totalPage) : 1;

            /*End Pagination */
            if (!this.bindSort) {
                this.bindSort = true;
                this.sortTable();
            }
            else
                jQuery("table").trigger("update", [true]);
        }
    }

    sortTable() {
        setTimeout(() => {
            jQuery('.tablesorter').tablesorter({
                headers: {
                    2: { sorter: false },
                    3: { sorter: false },
                    4: { sorter: false }
                }
            });
        }, 50);

    }

    back() {
        this.location.back();
    }

    addImage() {
        this.router.navigate(['./admin/img-gallery/img-gallery-list/' + this.albumId + '/img-gallery-editor']);
    }

    edit(imageId: string) {
        this.router.navigate(['./admin/img-gallery/img-gallery-list/' + this.albumId + '/img-gallery-editor/' + imageId]);
    }

    delete(imageId: string) {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Image !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },
            () => {
                this._objService.deleteAlbumImg(this.albumId, imageId)
                    .subscribe(res => {
                        this.getAlbumImgList();
                        swal("Deleted!", res.message, "success");
                    },
                    error => {
                        swal("Alert!", error.message, "info");

                    });
            });

    }

    changeCoverImage(e) {
        let imageId = e.target.value;
        swal({
            title: "Are you sure?",
            text: "You are going to change the cover image !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, change it!",
            closeOnConfirm: false
        },
            (isConfirm) => {
                if (isConfirm) {
                    let prevCoverImageId = this.prevCoverImage ? this.prevCoverImage.nativeElement.value : "";
                    let objImage: ImgGalleryModel = new ImgGalleryModel();
                    objImage._id = imageId;
                    objImage.coverImage = true;
                    this._objService.updateCoverImg(this.albumId, objImage)
                        .subscribe(res => {
                            this.getAlbumImgList();
                            swal("Changed!", res.message, "success");
                        },
                        error => {
                            swal("Alert!", error.message, "info");

                        });

                } else {
                    let prevCoverImageId = "";
                    if (this.prevCoverImage.nativeElement.value)
                        jQuery('input[name=rdbCoverImage][value=' + this.prevCoverImage.nativeElement.value + ']').prop('checked', true);

                }
            });

    }

    pageChanged(event) {
        this.perPage = event.rows;
        this.currentPage = (Math.floor(event.first / event.rows)) + 1;
        this.first = event.first;
        if (event.first == 0)
            this.first = 1;
        this.getAlbumImgList();
    }
}