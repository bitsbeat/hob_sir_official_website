import { ImgGalleryEditorComponent } from './img-gallery-editor.component';
import { ImgGalleryListComponent } from './img-gallery-list.component';
import { ImgGalleryAlbumEditorComponent } from './img-gallery-album-editor.component';
import { ImgGalleryComponent } from './img-gallery.component';
import { ImgGalleryAlbumListComponent } from './img-gallery-album-list.component';
import { SharedModule } from './../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { ImgGalleryService } from "./img-gallery.service";

@NgModule({
    imports: [SharedModule],
    exports: [ImgGalleryListComponent, ImgGalleryEditorComponent, ImgGalleryAlbumListComponent, ImgGalleryComponent, ImgGalleryAlbumEditorComponent],
    declarations: [ImgGalleryListComponent, ImgGalleryEditorComponent, ImgGalleryAlbumListComponent, ImgGalleryComponent, ImgGalleryAlbumEditorComponent],
    providers: [ImgGalleryService]
})

export class ImgGalleryModule{

}