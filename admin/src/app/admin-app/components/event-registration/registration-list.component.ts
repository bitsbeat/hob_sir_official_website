import { Component, OnInit } from '@angular/core';
import { RegistrationModel, RegistrationResponse, VenueListReponse} from "./registration.model";
import { RegistrationService } from "./registration.service";
import {FormControl, FormGroup, Validators, FormBuilder} from "@angular/forms";

@Component({
    selector: 'registration-list',
    templateUrl: './registration-list.html'
})

export class RegistrationListComponent implements OnInit {

    objListResponse:RegistrationResponse;
    error:any;
    showDetail:boolean = false;
    registrationObj:any;
    /* Pagination */
    perPage:number = 10;
    currentPage:number = 1;
    totalPage:number = 1;
    first:number = 0;
    bindSort:boolean = false;
    preIndex:number = 1;
    showForm:boolean = false;
    objVenueList: VenueListReponse;
    filterForm: FormGroup
    /* End Pagination */

    constructor(private _objService:RegistrationService, private _formBuilder : FormBuilder) {

        this.filterForm = this._formBuilder.group({
            "fullName": [''],
            "contactNumder": [''],
            "eventName": [''],
        });

    }

    ngOnInit() {
        this.getRegistrationList();
        this.getVenueList();
    }

    getRegistrationList() {
        this._objService.getRegistrationList(this.perPage, this.currentPage, false)
            .subscribe(objRes =>this.bindList(objRes),
                error => this.errorMessage(error));
    }

    errorMessage(objResponse:any) {
      swal("Alert !", objResponse.message, "info");

    }

    bindList(objRes : RegistrationResponse) {
        this.objListResponse = objRes;
        this.preIndex = (this.perPage * (this.currentPage - 1));

        /* Pagination */
        if (objRes.dataList.length > 0) {
            let totalPage = objRes.totalItems / this.perPage;
            this.totalPage = totalPage > 1 ? Math.ceil(totalPage) : 1;

            /*End Pagination */
            if (!this.bindSort) {
                this.bindSort = true;
                this.sortTable();
            }
            else
                jQuery("table").trigger("update", [true]);
        }
    }

    sortTable() {
        setTimeout(()=> {
            jQuery('.tablesorter').tablesorter({
                headers: {
                    4: {sorter: false},
                    5: {sorter: false}
                }
            });
        }, 50);
    }

    view(obj:any) {
        this.showDetail = true;
        this.registrationObj = obj;
    }

    changeDateFormat(data:string) {
        return new Date(data).toLocaleString('en-GB', {
            month: "numeric",
            year: "numeric",
            day: "numeric",
            hour12: false,
            hour: "numeric",
            minute: "numeric"
        });
    }

    delete(id:string) {
      swal({
          title: "Are you sure?",
          text: "You will not be able to recover this Registration Detail !",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          closeOnConfirm: false
        },
        ()=> {
          let objTemp:RegistrationModel = new RegistrationModel();
          objTemp._id = id;
          objTemp.deleted = true;
          this._objService.deleteRegistration(objTemp)
            .subscribe(res=> {
                this.getRegistrationList();
                swal("Deleted!", res.message, "success");
              },
              error=> {
                swal("Alert!", error.message, "info");

              });
        });
    }

    handleCancel(args) {
        this.showDetail = false;
        this.sortTable();
    }

    vppChanged(event:Event) {
        this.perPage = Number((<HTMLSelectElement>event.srcElement).value);
        this.getRegistrationList();
    }

    pageChanged(event) {
        this.perPage = event.rows;
        this.currentPage = (Math.floor(event.first / event.rows)) + 1;
        this.first = event.first;
        if (event.first == 0)
            this.first = 1;
        this.getRegistrationList();
    }

    getVenueList() {
    this._objService.getVenueList(100, 1)
      .subscribe(res=> this.objVenueList = res,
        error=>this.errorMessage(error)
      )
  }

  filterRegistrations(){
      let filterParams = this.filterForm.value;
      this._objService.getRegistrationList(this.perPage, this.currentPage, filterParams)
      .subscribe(res => this.bindList(res),
        error=>this.errorMessage(error));
  }


}

