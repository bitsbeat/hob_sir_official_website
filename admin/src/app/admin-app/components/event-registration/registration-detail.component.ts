import {
  Component, EventEmitter, Output, Input, AfterViewInit, ViewChild, OnInit
} from '@angular/core';
import {RegistrationModel} from "./registration.model";
import{Config} from "../../../shared/configs/general.config";

@Component({
  selector: 'registration-detail',
  templateUrl: './registration-detail.html'
})
export class RegistrationDetailComponent implements OnInit {
    
  @Input() registrationObj: RegistrationModel;
  @Output() viewCancelEvent: EventEmitter<any> = new EventEmitter();
  isCanceled:false;
  constructor() {
    
  }

  ngOnInit() {

  }

  triggerCancelView(event?: Event) {
    let showDetail = false;
    this.viewCancelEvent.emit(showDetail);
  }

  errorMessage(objResponse: any) {
    swal("Alert !", objResponse.message, "info");
  }


}
