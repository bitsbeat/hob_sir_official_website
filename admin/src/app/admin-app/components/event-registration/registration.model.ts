export class RegistrationModel {
    constructor() {
        this.emailSent = false;
    }
    _id          : string;
    fullName     : string;
    email        : string;
    contactNumber: string;
    eventId      : string;
    eventName    : string;
    emailSent    : boolean;
    addedOn      : string;
    deleted      : boolean;
    deletedOn    : any;

}

export class RegistrationResponse {
    dataList    :RegistrationModel[];
    currentPage :number = 1;
    totalItems  :number = 0;
}

export class VenueListReponse {
    venue: string;
}