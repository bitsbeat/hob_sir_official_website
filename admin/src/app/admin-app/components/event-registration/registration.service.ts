import { RegistrationModel, RegistrationResponse, VenueListReponse } from './registration.model';
import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { Config } from "../../../shared/configs/general.config";
import { API_URL } from "../../../shared/configs/env.config";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";

@Injectable()
export class RegistrationService {
    apiRoute : string   = "event-registration";
    eventRoute : string = "event/";
    constructor( private _http : Http ) {
    }

    deleteRegistration(objUpdate:RegistrationModel) {
        objUpdate.deleted = true;
        objUpdate.deletedOn = new Date();

        let body = JSON.stringify({});
        return this._http.patch(API_URL + this.apiRoute + "/" + objUpdate._id, body)
            .map(res => res.json())
            .catch(this.handleError);
    }

    eventRegister(eventValues:RegistrationModel):Observable<any>{
      return  this._http.post("api/event-registration", eventValues)
        .map(res=>res.json())
        .catch(this.handleError);
    }

    getRegistrationList(perPage:number, currentPage:number, filterParams:any):Observable < RegistrationResponse> {
        let getUrl = API_URL + this.apiRoute + "?perpage=" + perPage + "&page=" + currentPage,
            searchString = "";
        if(filterParams){
            searchString += (filterParams.fullName) ? "&fullName=" + filterParams.fullName : '';
            searchString += (filterParams.eventName) ? "&eventName=" + filterParams.eventName : '';
            searchString += (filterParams.contactNumber) ? "&contactNumber=" + filterParams.contactNumber : '';
        }

        getUrl += searchString;
        // console.log("filterParams",filterParams);
        return this._http.get(getUrl)
            .map(res =><RegistrationResponse>res.json())
            .catch(this.handleError);
    }

    getRegistrationById(id:string):Observable < RegistrationModel> {
        return this._http.get(API_URL + this.apiRoute + "/" + id)
            .map(res =><RegistrationModel>res.json())
            .catch(this.handleError);
    }

    alertError(error) {
      alert(error.json().message);
      return Observable.throw(error.json() || 'server error');
    }

    handleError(error) {
        console.log(error.json());
        return Observable.throw(error.json() || 'server error');
    }

    getVenueList(perPage:number, currentPage:number): Observable<VenueListReponse> {
        return this._http.get(API_URL + this.eventRoute + "venues" + "?perpage=" + perPage + "&page=" + currentPage)
            .map(res =><VenueListReponse>res.json())
            .catch(this.handleError);
    }


}
