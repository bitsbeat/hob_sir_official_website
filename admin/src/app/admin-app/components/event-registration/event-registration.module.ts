import { NgModule }      from '@angular/core';

import { RegistrationDetailComponent } from"./registration-detail.component";
import { RegistrationListComponent } from  "./registration-list.component";

import { RegistrationService } from "./registration.service";
import{SharedModule} from '../../../shared/shared.module';

@NgModule({
  imports: [SharedModule],
  declarations: [
      RegistrationDetailComponent,
      RegistrationListComponent
  ],
  providers: [ RegistrationService ]
})

export class EventRegistrationModule {
}
