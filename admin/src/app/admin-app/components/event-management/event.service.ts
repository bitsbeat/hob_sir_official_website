///<reference path="../../../../../node_modules/rxjs/add/operator/catch.d.ts"/>
import {EventModel, EventResponse} from './event.model';
import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Observable} from "rxjs/Observable";
import{Config} from "../../../shared/configs/general.config";
import{ API_URL} from "../../../shared/configs/env.config";
import {FileOperrationService} from '../../../shared/services/fileOperation.service';
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import {XhrService} from "../../../shared/services/xhr.service";

@Injectable()
export class EventService {
    apiRoute:string = "event/info";
    progressObserver:any;
    progress:any;

    constructor(private _http:Http, private  fileService:FileOperrationService, private xhrService: XhrService) {
    }

    saveEvent(objSave:EventModel, file:File):Observable<any> {
        return this.xhrService.xhrRequest<EventModel,any>('POST', this.apiRoute, 'imageName', objSave, file);
    }

    updateEvent(objUpdate:EventModel, file:File, imageDeleted:boolean):Observable<any> {
        return this.xhrService.xhrRequest<EventModel,any>('PUT', this.apiRoute, 'imageName', objUpdate, file, objUpdate._id, imageDeleted);
    }

    deleteEvent(objUpdate:EventModel) {
        objUpdate.deleted = true;
        let body = JSON.stringify({});
        return this._http.patch(API_URL + this.apiRoute + "/" + objUpdate._id, body)
            .map(res => res.json())
            .catch(this.handleError);
    }

    getRecentEvents():Observable < EventModel[]> {
        return this._http.get(API_URL + this.apiRoute + "/recent-events")
            .map(res =><EventModel[]>res.json())
            .catch(this.handleError);
    }

    getEventListInDescendingOrder(perPage: number = null, currentPage: number = null): Observable<EventResponse>{
      return this._http.get(API_URL + this.apiRoute + '/all' + "?perpage=" + perPage + "&page=" + currentPage)
        .map(res =><EventResponse>res.json())
        .catch(this.handleError);
    }

    getEventList(perPage:number= null, currentPage:number=null):Observable < EventResponse> {
      return this._http.get(API_URL + this.apiRoute + "?perpage=" + perPage + "&page=" + currentPage)
        .map(res =><EventResponse>res.json())
        .catch(this.handleError);
    }

    getOnGoingEvents(): Observable<EventResponse>{
      return this._http.get(API_URL + this.apiRoute + "/ongoing")
        .map(res => <EventResponse>res.json())
        .catch(this.handleError);
    }

    getAllExceptUpcomingEvents(perPage: number=null, currentPage: number=null): Observable<EventResponse>{
      return this._http.get(API_URL + this.apiRoute + "/finished" + "?perpage=" + perPage + "&page=" + currentPage)
        .map(res => <EventResponse>res.json())
        .catch(this.handleError);
    }

    getUpcomingEvents(): Observable<EventModel[]>{
        return this._http.get('api/event/info/upcoming-events')
                    .map(res => <EventModel[]>res.json())
                    .catch(this.handleError);
    }

    getEventByTitle(date: string, titleSlog: string): Observable<EventModel> {
        return this._http.get("api/event/info/event-detail/" +date + "/" + titleSlog)
          .map(res => <EventModel>res.json())
    }

    getEventById(id:string):Observable < EventModel> {
        return this._http.get(API_URL + this.apiRoute + "/" + id)
            .map(res =><EventModel>res.json())
            .catch(this.handleError);
    }

    deleteImage(fileName:string, orgExt:string, path:string):Observable < any > {
        return this.fileService.deleteFile(fileName, orgExt, path, "image");
    }

    handleError(error) {
        console.log(error.json());
        return Observable.throw(error.json() || 'server error');
    }
}
