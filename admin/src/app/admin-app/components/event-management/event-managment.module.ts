import {NgModule} from '@angular/core';
import {EventService} from "./event.service";
import {EventEditorComponent} from"./event-editor.component";
import {EventComponent} from"./event-list.component";
import {SharedModule} from '../../../shared/shared.module';
import {XhrService} from "../../../shared/services/xhr.service";

@NgModule({
    imports: [SharedModule],
    declarations: [EventEditorComponent, EventComponent],
    providers:[EventService, XhrService]
})

export class EventManagementModule {
}
