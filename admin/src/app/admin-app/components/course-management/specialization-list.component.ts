import { Output, Input } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { SpecializationResponse, SpecializationModel } from './course.model';
import { CourseService } from './course.service';
import { Router } from '@angular/router';
@Component({
    selector:"specialization-list",
    templateUrl:"./specialization-list.html"
})


export class SpecializationListComponent implements OnInit {

    objListResponse:SpecializationResponse = new SpecializationResponse();
    bindSort: boolean = false;

    constructor(
        private courseService : CourseService,
        private router        : Router
    ){
        this.getSpecializationList()
    }

    ngOnInit(){
    }

    getSpecializationList(){
        this.courseService.getSpecializations()
        .subscribe(data => this.bindList(data));
    }

    bindList(objRes){
        this.objListResponse = objRes;
      if (!this.bindSort) {
        this.bindSort = true;
        this.sortTable();
      }
      else
        jQuery("table").trigger("update", [true]);
    }

    sortTable() {
        setTimeout(()=> {
        jQuery('.tablesorter').tablesorter({
            headers: {
            3: {sorter: false},
            4: {sorter: false}
            }
        });
        }, 50);
    }
    

    errorMessage(objResponse){
        swal("Alert !", objResponse.message, "info");
    }

  delete(id){
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Course !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      ()=> {
        let objTemp:SpecializationModel = new SpecializationModel();
        objTemp._id = id;
        this.courseService.deleteSpecialization(objTemp)
          .subscribe(res=> {
              this.getSpecializationList();
              swal("Deleted!", res.message, "success");
            },
            error=> {
              swal("Alert!", error.message, "info");

            });
      });
  }

  edit(id){
    this.router.navigate(['/admin/course/specialization/',id]);
  }

  addSemester(){
    this.router.navigate(['/admin/course/specialization']);
  }

}