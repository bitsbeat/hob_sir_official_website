import { Output, Input } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CourseResponse, SemesterResponse, CourseModel } from './course.model';
import { CourseService } from './course.service';
import { Router } from '@angular/router';
@Component({
    selector:"course-list",
    templateUrl:"./course-list.html"
})


export class CourseListComponent implements OnInit {

    @Input() showForm:boolean;

    objListResponse:CourseModel[];
    objSemList:SemesterResponse = new SemesterResponse();
    bindSort: boolean = false;
    semesterName:string;
    courseId:string;

    constructor(
        private courseService : CourseService,
        private router        : Router
    ){
        this.getSemesterList()
    }

    ngOnInit(){
    }

    getSemesterList(){
        this.courseService.getSemesters()
        .subscribe(data => {
            this.objSemList   = data;
            this.semesterName = this.objSemList.dataList[0].semesterName;
            this.getCourseList();
        })
    }

    getCourseList(){
        this.courseService.getCourseList(this.semesterName)
            .subscribe(objRes =>this.bindList(objRes),
                error => this.errorMessage(error));
    }

    bindList(objRes){
        this.objListResponse = objRes;
      if (!this.bindSort) {
        this.bindSort = true;
        this.sortTable();
      }
      else
        jQuery("table").trigger("update", [true]);
    }

    sortTable() {
        setTimeout(()=> {
        jQuery('.tablesorter').tablesorter({
            headers: {
            3: {sorter: false},
            4: {sorter: false}
            }
        });
        }, 50);
    }


    errorMessage(objResponse){
        swal("Alert !", objResponse.message, "info");
    }

    semesterFilter(args) {
        this.semesterName = (<HTMLSelectElement>args.srcElement).value;
        this.courseService.getCourseList(this.semesterName)
        .subscribe(res => this.bindList(res),
            error=>this.errorMessage(error));
  }

  delete(id){
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Course !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      ()=> {
        let objTemp:CourseModel = new CourseModel();
        objTemp._id = id;
        this.courseService.deleteCourse(objTemp)
          .subscribe(res=> {
              this.getCourseList();
              swal("Deleted!", res.message, "success");
            },
            error=> {
              swal("Alert!", error.message, "info");

            });
      });
  }

  edit(id){
    this.showForm = true;
    this.courseId = id;
  }

  addCourse(){
    this.showForm = true;
  }

  showCourseList(args) {
    if (!args) {
      this.getSemesterList();
    }
    this.showForm = false;
    this.courseId = null;
  }
}
