import {CountryListService} from './../../../shared/services/countrylist.service';
import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {CourseService} from './course.service';
import {FormControl, FormGroup, Validators, FormBuilder} from "@angular/forms";
import {SemesterModel, SemesterResponse} from './course.model';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';


@Component({
  selector: "semester-editor",
  templateUrl: "./semester-editor.html"
})

export class SemesterEditorComponent implements OnInit {

  objSemester: SemesterModel = new SemesterModel();
  semesterFormControl: FormControl = new FormControl();
  semesterForm: FormGroup;
  @Input() semesterId: string;
  @Output() showListEvent: EventEmitter<any> = new EventEmitter();
  isSubmitted: boolean;

  constructor(private _courseService: CourseService,
              private _formBuilder: FormBuilder,
              private activatedRoute: ActivatedRoute,
              private location: Location) {
    activatedRoute.params.subscribe(params => this.semesterId = params['id']);
  }

  ngOnInit() {
    if (this.semesterId)
      this.getSemesterDetail();
    this.generateForm();
  }

  generateForm() {
    this.semesterForm = this._formBuilder.group({
      "semesterName": ["", Validators.required],
      "semesterCredit": ["", Validators.required]
    })
  }

  getSemesterDetail() {
    this._courseService.getSemester(this.semesterId)
      .subscribe(res => this.bindForm(res),
        err => this.errorMessage(err));
  }

  bindForm(res: any) {
    this.objSemester = res;
    this.semesterForm.patchValue({
      "semesterName": this.objSemester.semesterName,
      "semesterCredit": this.objSemester.semesterCredit
    });
  }

  onSubmit() {
    if (this.semesterForm.valid) {
      if (this.semesterId) {
        this._courseService.putSemester(this.semesterId, this.semesterForm.value)
          .subscribe(res => this.resStatusMessage(res),
            err => this.errorMessage(err));
      }
      else {
        this._courseService.postSemester(this.semesterForm.value)
          .subscribe(res => this.resStatusMessage(res),
            err => this.errorMessage(err));
      }
    }
  }

  resStatusMessage(res: any) {
    swal("Success !", res.message, "success");
    this.location.back();

  }

  triggerCancelForm() {
    this.location.back();
  }

  errorMessage(objResponse: any) {
    swal("Alert !", objResponse.message, "info");
  }

}
