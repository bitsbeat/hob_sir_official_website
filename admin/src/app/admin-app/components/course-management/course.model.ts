
// program data is default for now no update delete or add. only system provided data
// need tabed interface for semester and specialization 
// semester tab ma semester dropdown for course list
//    actions: edit, add, delete course
// specialization tab ma actions: list, edit, delete, view


export class CourseModel {

    _id          : string;
    semesterName : string;
    courseName   : string;
    courseCode   : string;
    creditHours  : string;
    deleted      : boolean;
    deletedBy    : string;
    deletedOn    : any;

}

export class SemesterModel {
    _id             : string;
    semesterName    : string;
    semesterCredit  : string;
    deleted         : boolean;
    deletedBy       : string;
    deletedOn       : any;
}

export class SpecializationModel {
    _id         : string;
    name        : string;
    creditHours : string;
    description : string;
}

export class ProgramModel {
    programName     : string;
    totalCredit     : string;
    totalSemesters  : string;
    totalYears      : string;
    addedOn         : any;
    deleted         : boolean;
    deletedBy       : string;
    deletedOn       : any;
    // semester        : SemesterModel[];
    // specialization  : SpecializationModel[];
}

export class ProgramResponse {
    dataList : ProgramModel;
}


export class SpecializationResponse {
    dataList : SpecializationModel[];
}

export class SemesterResponse {
    dataList : SemesterModel[];
}

export class CourseResponse {
    dataList : CourseModel[];
}

