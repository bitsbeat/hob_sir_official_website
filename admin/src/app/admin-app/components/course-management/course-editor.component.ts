import {CountryListService} from './../../../shared/services/countrylist.service';
import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {CourseService} from './course.service';
import {FormControl, FormGroup, Validators, FormBuilder} from "@angular/forms";
import {CourseModel, CourseResponse, SemesterResponse} from './course.model';


@Component({
  selector: "course-editor",
  templateUrl: "./course-editor.html"
})

export class CourseEditorComponent implements OnInit {

  @Input() courseId: string;
  @Output() showListEvent: EventEmitter<any> = new EventEmitter();
  courseResponse: CourseModel = new CourseModel();
  courseForm: FormGroup;
  objCourse: any;
  objSemList: SemesterResponse = new SemesterResponse();
  isSubmitted: boolean;

  constructor(private _courseService: CourseService, private _formBuilder: FormBuilder) {
    this.getSemesterList();
  }

  ngOnInit() {
    if (this.courseId)
      this.getCourseDetail();
    this.generateForm();
  }

  getSemesterList() {
    this._courseService.getSemesters()
      .subscribe(data => {
        this.objSemList = data;
      });
  }

  generateForm() {
    this.courseForm = this._formBuilder.group({
      "courseName": ["", Validators.required],
      "creditHours": ["", Validators.required],
      "courseCode": ["", Validators.required],
      "semesterName": ["", Validators.required]
    });
  }

  getCourseDetail() {
    this._courseService.getCourseById(this.courseId)
      .subscribe(data => {
        this.courseResponse = data;
        this.patchForm();
      })
  }

  patchForm() {
    if (this.courseResponse) {
      this.courseForm.patchValue({
        "courseName": this.courseResponse.courseName,
        "creditHours": this.courseResponse.creditHours,
        "courseCode": this.courseResponse.courseCode,
        "semesterName": this.courseResponse.semesterName

      })
    }
  }

  triggerCancelForm() {
    let isCanceled = true;
    this.showListEvent.emit(isCanceled);
  }


  onSubmit() {
    if (this.courseForm.valid) {
      if (this.courseId) {
        this._courseService.putCourse(this.courseId, this.courseForm.value)
          .subscribe(res => this.resStatusMessage(res),
            error => this.errorMessage(error));
      }
      else {
        this._courseService.postCourse(this.courseForm.value)
          .subscribe(res => this.resStatusMessage(res),
            error => this.errorMessage(error));
      }

    }
  }

  resStatusMessage(objSave: any) {
    this.showListEvent.emit(false); // is Form Canceled
    swal("Success !", objSave.message, "success");
  }

  errorMessage(objResponse: any) {
    swal("Alert !", objResponse.message, "info");
  }

}
