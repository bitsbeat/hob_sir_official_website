import { Output, Input } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { SemesterResponse, SemesterModel } from './course.model';
import { CourseService } from './course.service';
import { Router } from '@angular/router';
@Component({
    selector:"semester-list",
    templateUrl:"./semester-list.html"
})


export class SemesterListComponent implements OnInit {

    @Input() showForm:boolean;

    objListResponse:SemesterResponse = new SemesterResponse();
    semesterId:string;
    bindSort: boolean = false;
    semesterName:string;
    courseId:string;

    constructor(
        private courseService : CourseService,
        private router        : Router
    ){
        this.getSemesterList()
    }

    ngOnInit(){
    }

    getSemesterList(){
        this.courseService.getSemesters()
        .subscribe(data => this.bindList(data));
    }

    bindList(objRes){
        this.objListResponse = objRes;
      if (!this.bindSort) {
        this.bindSort = true;
        this.sortTable();
      }
      else
        jQuery("table").trigger("update", [true]);
    }

    sortTable() {
        setTimeout(()=> {
        jQuery('.tablesorter').tablesorter({
            headers: {
            3: {sorter: false},
            4: {sorter: false}
            }
        });
        }, 50);
    }
    

    errorMessage(objResponse){
        swal("Alert !", objResponse.message, "info");
    }

  delete(id){
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Course !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      ()=> {
        let objTemp:SemesterModel = new SemesterModel();
        objTemp._id = id;
        this.courseService.deleteSemester(objTemp)
          .subscribe(res=> {
              this.getSemesterList();
              swal("Deleted!", res.message, "success");
            },
            error=> {
              swal("Alert!", error.message, "info");

            });
      });
  }

  edit(id){
    this.router.navigate(['/admin/course/semester/',id]);
      
    this.showForm = true;
    this.courseId = id;
  }

  addSemester(){
    this.router.navigate(['/admin/course/semester']);
  }

}