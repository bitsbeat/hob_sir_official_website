import { Router } from '@angular/router';
import { Component } from'@angular/core';

@Component({
  selector: 'course-management',
  templateUrl: './course-management.html'
})
export class CourseManagementComponent {
  isSemList: boolean = false;
  isCourseList: boolean = true;
  isSpecializationList: boolean = false;
  isCanceled: boolean = false;
  id: string;

  constructor(private _router: Router) {
  }


  public tabSwitch(args) {
    //  if (args.active) {
    if (0 == args.index) {
        this._router.navigate(['/admin/course']);     
      
    }
    else if (1 == args.index) {
        this._router.navigate(['/admin/course/specialization/list']);
    }
    else if (2 == args.index) {
        this._router.navigate(['/admin/course/semester/list']);
     
    }

  }


}

