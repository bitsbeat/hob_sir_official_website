import { NgModule }      from '@angular/core';
import { CourseEditorComponent } from './course-editor.component';
import { CourseListComponent } from './course-list.component';
import { CourseService } from './course.service';
import { SemesterEditorComponent } from './semester-editor.component';
import { CourseManagementComponent } from './course-management.component';
import { SemesterListComponent } from './semester-list.component';
import { SpecializationListComponent } from './specialization-list.component';
import { SpecializationEditorComponent } from './specialization-editor.component';

import{SharedModule} from '../../../shared/shared.module';

@NgModule({
  imports: [SharedModule],
  declarations: [
      SemesterListComponent,
      SemesterEditorComponent,
      CourseEditorComponent,
      CourseListComponent,
      CourseManagementComponent,
      SpecializationListComponent,
      SpecializationEditorComponent
  ],
  providers: [ CourseService ]
})

export class CourseManagementModule {
}
