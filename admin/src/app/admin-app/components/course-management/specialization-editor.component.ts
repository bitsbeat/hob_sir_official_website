import { CountryListService } from './../../../shared/services/countrylist.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CourseService } from './course.service';
import {FormControl, FormGroup, Validators, FormBuilder} from "@angular/forms";
import {  SpecializationModel, SpecializationResponse } from './course.model';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';


@Component({
    selector:"specialization-editor",
    templateUrl:"./specialization-editor.html"
})

export class SpecializationEditorComponent implements OnInit {

    objSpecialization   : SpecializationModel = new SpecializationModel();
    specializationForm  : FormGroup;
    specializationId    : string;
    editorFormControl   : FormControl = new FormControl('', Validators.required);
    isSubmitted         : boolean = false;

    constructor(
        private _courseService : CourseService, 
        private _formBuilder   : FormBuilder,
        private activatedRoute : ActivatedRoute,
        private location       : Location
    ){
        activatedRoute.params.subscribe(params => this.specializationId = params['id']);
    }
    ngOnInit(){
        if(this.specializationId)
            this.getSpecializationDetail();
        this.generateForm();
    }

    generateForm(){
        this.specializationForm = this._formBuilder.group({
            "name":["", Validators.required],
            "creditHours":["", Validators.required],
            "description": this.editorFormControl

        })       
    }

    getSpecializationDetail(){
        this._courseService.getSpecialization(this.specializationId)
                .subscribe(res => this.bindForm(res),
                           err => this.errorMessage(err));
    }

    bindForm(res:any){
        this.objSpecialization = res;
        this.specializationForm.patchValue({
            "name": this.objSpecialization.name,
            "creditHours": this.objSpecialization.creditHours
        });
    }

    onSubmit(){
        this.isSubmitted = true;

        if(this.specializationForm.valid){
            if(this.specializationId){
                this._courseService.putSpecialization(this.specializationId,this.specializationForm.value)
                .subscribe(res => this.resStatusMessage(res),
                           err => this.errorMessage(err) );
            }
            else{
                this._courseService.postSpecialization(this.specializationForm.value)
                .subscribe(res => this.resStatusMessage(res),
                           err => this.errorMessage(err) );
            }
        }
    }

    resStatusMessage(res:any){
        swal("Success !", res.message, "success");
        this.location.back();
        
    }

    triggerCancelForm() {
        this.location.back();
    }
    errorMessage(objResponse:any) {
      swal("Alert !", objResponse.message, "info");
    }

     editorValueChange(args) {
        this.objSpecialization.description = args;
    }


}