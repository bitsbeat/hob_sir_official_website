import { ProgramModel, ProgramResponse, CourseModel, SemesterResponse, CourseResponse } from './course.model';
import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { Config } from "../../../shared/configs/general.config";
import { API_URL } from "../../../shared/configs/env.config";

@Injectable()
export class CourseService {
    apiRoute : string   = "course/";
    constructor( private _http : Http ) {
    }

/*-----------------Course Services------------------------*/

    deleteCourse(objUpdate:any) {
        return this._http.patch(API_URL + this.apiRoute + "one/" + objUpdate._id, objUpdate)
            .map(res => res.json())
            .catch(this.handleError);
    }

    getCourseList(id:string) : Observable<any> {
        let getUrl = API_URL + this.apiRoute + id ;

        return this._http.get(getUrl)
            .map(res =><CourseResponse>res.json())
            .catch(this.handleError);
    }

    getCourseById(id:string){
        return this._http.get(API_URL + this.apiRoute + "/one/" + id)
            .map(res => res.json())
            .catch(this.handleError);
    }

    postCourse(formValues:any) {
        return this._http.post(API_URL + this.apiRoute , formValues)
            .map(res =>res.json())
            .catch(this.handleError);
    }

    putCourse(courseId:string, formValues:any) {
        return this._http.put(API_URL + this.apiRoute + "one/" + courseId, formValues)
            .map(res =>res.json())
            .catch(this.handleError);
    }

/*-----------------Semester Services------------------------*/


    postSemester(values:any) : Observable<any> {
        let body = JSON.stringify(values);

        return this._http.post(API_URL + this.apiRoute + "semester", body )
            .map(res => res.json())
            .catch(this.handleError);
    }

    putSemester(semId, values): Observable<any>{
          return this._http.put(API_URL + this.apiRoute +"semester/"+ semId, values)
            .map(res =>res.json())
            .catch(this.handleError);
    }

    getSemester(id: string): Observable<any> {
        return this._http.get(API_URL + this.apiRoute + "semester/" + id)
            .map(res => res.json())
            .catch(this.handleError);
    }

    getSemesters(): Observable<any> {
        return this._http.get(API_URL + this.apiRoute + "semester")
            .map(res => res.json())
            .catch(this.handleError);
    }

    deleteSemester(objUpdate:any) {
        return this._http.patch(API_URL + this.apiRoute + "semester/" + objUpdate._id, objUpdate)
            .map(res => res.json())
            .catch(this.handleError);
    }

/*-----------------Specialization Services------------------------*/
    getSpecialization(id: string): Observable<any> {
        return this._http.get(API_URL + this.apiRoute + "specialization/" + id)
            .map(res => res.json())
            .catch(this.handleError);
    }

    getSpecializations() : Observable<any>{
        return this._http.get(API_URL + this.apiRoute + "specialization")
            .map(res => res.json())
            .catch(this.handleError);
    }

    postSpecialization(values:any) : Observable<any> {
        let body = JSON.stringify(values);

        return this._http.post(API_URL + this.apiRoute + "specialization", body )
            .map(res => res.json())
            .catch(this.handleError);
    }

    putSpecialization(specializationId, values): Observable<any>{
          return this._http.put(API_URL + this.apiRoute +"specialization/"+ specializationId, values)
            .map(res =>res.json())
            .catch(this.handleError);
    }

    deleteSpecialization(objUpdate:any) {
        return this._http.patch(API_URL + this.apiRoute + "specialization/" + objUpdate._id, objUpdate)
            .map(res => res.json())
            .catch(this.handleError);
    }

    handleError(error) {
        return Observable.throw(error.json() || 'server error');
    }

}
