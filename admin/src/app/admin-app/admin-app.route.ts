import { ImgGalleryEditorComponent } from './components/imgGallery/img-gallery-editor.component';
import { ImgGalleryListComponent } from './components/imgGallery/img-gallery-list.component';
import { ImgGalleryAlbumEditorComponent } from './components/imgGallery/img-gallery-album-editor.component';
import { ImgGalleryAlbumListComponent } from './components/imgGallery/img-gallery-album-list.component';
import {Routes, RouterModule} from '@angular/router';
import {NewsManagementComponent} from './components/news/news-management.component';
import {EmailServiceComponent} from './components/email-service/email-service.component';
import {ContactListCompoent} from './components/contact/contact-list.component';
import {BlogManagementComponent} from './components/blog/blog.component';
import {BlogCategoryListComponent} from './components/blog/blog-category-list.component';
import {BlogCategoryEditorComponent} from './components/blog/blog-category-editor.component';
import {CloudinarySettingComponent} from './components/cloudinary/cloudinary.component';
import {GoogleAnalyticsComponent} from './components/google-analytics/google-analytics.component';
import {OrganizationInfoComponent} from './components/organization-information/orginfo.component';
import {HtmlContentComponent} from './components/html-content/html-content-list.component';
import {CommentSettingComponent} from './components/comment-setting/comment-setting.component';
import {AdminAppComponent} from './admin-app.component';
import {AuthGuardService} from '../login-app/auth.guard.service';
import {NgModule} from '@angular/core';
import {EmailTemplateListComponent} from './components/email-template/email-template-list.component';
import {EmailTemplateEditorComponent} from './components/email-template/email-template-editor.component';
import {BlogEditorComponent} from './components/blog/blog-editor.component';
import {BlogListComponent} from './components/blog/blog-list.component';
import {BlogDocListComponent} from './components/blog/blog-doc-list.component';
import {BlogDocEditorComponent} from './components/blog/blog-doc-editor.component';
import {BlogMetaTagEditorComponent} from './components/blog/blog-metatag.component';
import {NewsListComponent} from './components/news/news-list.component';
import {NewsEditorComponent} from './components/news/news-editor.component';
import {NewsImageListComponent} from './components/news/news-image-list.component';
import {NewsImageEditorComponent} from './components/news/news-image-editor.component';
import {PartnerComponent} from './components/partner/partner-list.component';

export const adminAppRoute: Routes = [
  {
    path: '',
    component: AdminAppComponent,
    canActivate: [AuthGuardService],
    children: [
      {
        path: '',
        canActivateChild: [AuthGuardService],
        children: [
          {path: 'dashboard', loadChildren: 'app/admin-app/components/dashboard/dashboard.module#DashboardModule'},
          {path: 'contact', component: ContactListCompoent},
          {path: 'email-service', component: EmailServiceComponent},
          {path: 'cloudinary', component: CloudinarySettingComponent},
          {path: 'analytics', component: GoogleAnalyticsComponent},
          {path: 'organization', component: OrganizationInfoComponent},
          {path: 'partner', component: PartnerComponent},
          {path: 'news-section', component: NewsManagementComponent,
          children: [
            {path: '', redirectTo: '/admin/news-section/news', pathMatch:'full'},
            {path: 'news',
            children: [
              {path: '', component:NewsListComponent},
              {path: 'editor', component: NewsEditorComponent},
              {path: 'editor/:id', component: NewsEditorComponent}
            ]},
            {
              path: 'image',
              children: [
                {path: ':id', component: NewsImageListComponent},
                {path: 'editor/:newsid', component: NewsImageEditorComponent},
                {path: 'editor/:newsid/:imgid', component: NewsImageEditorComponent}
              ]
            }
          ]},
          {path: 'img-gallery',
              children: [
                {path: '', component: ImgGalleryAlbumListComponent},
                {path: 'img-gallery-album-editor', component: ImgGalleryAlbumEditorComponent},
                {path: 'img-gallery-album-editor/:id', component: ImgGalleryAlbumEditorComponent},
                {path: 'img-gallery-list/:id', component: ImgGalleryListComponent},
                {path: 'img-gallery-list/:id/img-gallery-editor', component: ImgGalleryEditorComponent},
                {path: 'img-gallery-list/:id/img-gallery-editor/:imageId', component: ImgGalleryEditorComponent}
              ]
          },
          {
            path: 'email-template',
            children: [
              {path: '', component: EmailTemplateListComponent},
              {path: 'email-template-editor', component: EmailTemplateEditorComponent},
              {
                path: 'email-template-editor/:id', component: EmailTemplateEditorComponent
              }
            ]
          },
          {
            path: 'quote', loadChildren: 'app/admin-app/components/notice/notice.module#NoticeModule'
          },
          {path: 'blog-management', component: BlogManagementComponent,
            children: [
              {path: '', redirectTo:'/admin/blog-management/blog', pathMatch: 'full'},
              {
                path: 'blog',
                children: [
                  {path: '', component: BlogListComponent},
                  {path: 'editor', component: BlogEditorComponent},
                  {path: 'editor/:id', component: BlogEditorComponent}
                ]
              },
              {path: 'blog-category',
                children: [
                  {path: '', component: BlogCategoryListComponent},
                  {path: 'editor', component: BlogCategoryEditorComponent},
                  {path: 'editor/:id', component: BlogCategoryEditorComponent}
                  ]
                },
              {
                path: 'doc',
                children: [
                  {path: ':id', component:BlogDocListComponent},
                  {path: 'editor/:blogid', component: BlogDocEditorComponent},
                  {path: 'editor/:blogid/:docid', component: BlogDocEditorComponent}
                ]
              },
              {path: 'meta-list/:id', component: BlogMetaTagEditorComponent }
            ]
          },
          {path: 'html', component: HtmlContentComponent},
          {path: 'comment', component: CommentSettingComponent},
          {
          path: 'videoGallery', loadChildren: 'app/admin-app/components/videoGallery/videoGallery.module#VideoGalleryModule'
          },
          {path: '', redirectTo: 'dashboard', pathMatch: 'full'}
        ]
      }
    ]
  }

];

@NgModule({
  imports: [
  RouterModule.forChild(adminAppRoute)
  ],
  exports: [
    RouterModule
  ]
})
export class adminAppRouting {
}
