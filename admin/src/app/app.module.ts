import { HttpModule } from '@angular/http';
import { CloudinaryModule } from './admin-app/components/cloudinary/cloudinary.module';
import { NgModule, Inject, PLATFORM_ID, APP_ID } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginAppComponent } from './login-app/login-app.component';
import { LoginComponent } from './login-app/components/login/login.component';
import { ForgotPasswordComponent } from "./login-app/components/forgot-password/forgot-password.component";
import { PageNotFoundComponent } from "./shared/components/page-not-found/page-not-found";
import { AppRoutingModule } from "./app.route";
import { loginAppRoutingModule } from "./login-app/login-app.route";
import { isPlatformBrowser } from '@angular/common';

@NgModule({
  imports: [BrowserModule.withServerTransition({ appId: 'hob-khadka' }),
    HttpModule,
    BrowserAnimationsModule,
    SharedModule.forRoot(),
    loginAppRoutingModule,
    AppRoutingModule,
    CloudinaryModule
  ],
  declarations: [AppComponent,
    LoginAppComponent,
    LoginComponent,
    ForgotPasswordComponent,
    PageNotFoundComponent],
  bootstrap: [AppComponent]
})

export class AppModule {
  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string) {
    const platform = isPlatformBrowser(platformId) ?
      'on the server' : 'in the browser';
    console.log(`Running ${platform} with appId=${appId}`);
  }
}
