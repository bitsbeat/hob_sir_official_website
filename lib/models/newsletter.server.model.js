/**
 * Created by lakhe on 11/24/16.
 */

(function(){
    'use strict';

    var mongoose = require('mongoose'),
        regex = require('./regex.server.model'),
        Schema = mongoose.Schema;

    var newsletterSchema = new Schema({
        email: {
            type: String,
            trim: true,
            required: true,
            lowercase: true,
            validate: {
                validator: function(email) {
                    if(email){
                        return regex.emailRegex.test(email);
                    }else{
                        return true;
                    }
                },
                message: '{VALUE} is not a valid email address!'
            }
        },
        subscribed: {
            type: Boolean,
            default:true
        },
        addedBy: {
            type: String,
            trim: true
        },
        addedOn: {
            type: Date,
            default: Date.now
        },
        unSubscribedDate: {
            type: Date
        }
    });

    module.exports = mongoose.model('NewsLetter', newsletterSchema, 'NewsLetter');
})();