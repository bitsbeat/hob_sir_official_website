(function () {

    'use strict';

    var mongoose = require('mongoose'),
        Schema = mongoose.Schema;

    var courseSchema = new Schema({
        semesterName: {
            type: String,
            required:true,
            trim: true
        },
        courseCode:{
            type: String,
            required:true,
            trim: true
        },
        courseName:{
            type: String,
            required:true,
            trim: true
        },
        creditHours:{
            type: String,
            required:true,
            trim: true
        },
        deleted: {
            type: Boolean,
            default: false
        },
        deletedBy: {
            type:String,
            trim: true
        },
        deletedOn: {
            type: Date
        },
    });

    var semesterSchema = new Schema({
        semesterName: {
            type: String,
            enum: ['Semester I', 'Semester II', 'Semester III', 'Semester IV'],
            required:true,
            trim: true
        },
        semesterCredit: {
            type:String,
            required:true,
            trim: true
        },
        deleted: {
            type: Boolean,
            default: false
        },
        deletedBy: {
            type:String,
            trim: true
        },
        deletedOn: {
            type: Date
        },
    });

    var specializationSchema = new Schema({
        name: {
            type: String,
            required:true,
            trim: true
        },
        creditHours:{
            type: String,
            required:true,
            trim: true
        },
        description:{
            type: String,
            required:true,
            trim: true
        },
        deleted: {
            type: Boolean,
            default: false
        },
        deletedBy: {
            type:String,
            trim: true
        },
        deletedOn: {
            type: Date
        }
    })

    var programSchema=new Schema({
        programName: {
            type:String,
            required:true,
            trim: true
        },
        totalCourses: {
            type:String,
            required:true,
            trim: true
        },
        totalCredit: {
            type:String,
            required:true,
            trim: true
        },
        addedOn: {
            type: Date,
            default: Date.now
        },
        deleted: {
            type: Boolean,
            default: false
        },
        deletedBy: {
            type:String,
            trim: true
        },
        deletedOn: {
            type: Date
        }
    });

    module.exports = {
        Program  : mongoose.model('Program',  programSchema, 'Program'),
        Specialization  : mongoose.model('Specialization',  specializationSchema, 'Specialization'),
        Semester : mongoose.model('Semester', semesterSchema, 'Semester'),
        Course : mongoose.model('Course', courseSchema, 'Course')
    };

})();