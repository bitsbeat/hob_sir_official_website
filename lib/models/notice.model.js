(function () {

    'use strict';

    const mongoose   = require('mongoose');
    const Schema     = mongoose.Schema;

    let noticeSchema = new Schema({
        noticeTitle      : {
            type    : String,
            required: true,
            trim    : true
        },
        urlSlog          : {
            type    : String,
            trim    : true
        },
        noticeDescription: {
            type    : String,
            required: true,
            trim    : true
        },
        deadline         : {
            type    : Date,
            trim    : true
        },
        active           : {
            type    : Boolean,
            default : true
        },
        deleted          : {
            type    : Boolean,
            default : false
        },
        addedOn          : {
            type    : Date,
            default : new Date()
        },
        addedBy          : {
            type    : String
        },
        lastUpdated:        {
            type    : Boolean,
            default : false
        },
        updatedOn        : {
            type    : Date
        },
        updatedBy        : {
            type    : String
        },
        deletedOn        : {
            type    : Date
        },
        deletedBy        : {
            type    : String
        }
    });

    module.exports = mongoose.model('Notice', noticeSchema, 'Notice');

})();
