'use strict';

const mongoose = require('mongoose'),
      Schema = mongoose.Schema;

let VideoSchema = new Schema({
    title: {
        type: String,
        required: true,
        trim: true
    },
    urlSlog: {
        type: String,
        trim: true,
        required: true
    },
    videoUrl: {
        type: String,
        required: true,
        trim: true
    },
    active: {
        type: Boolean,
        default: true
    },
    addedOn: {
        type: Date,
        default: Date.now()
    },
    addedBy: {
        type: String,
        trim: true
    },
    updatedOn: {
        type: Date
    },
    updatedBy: {
        type: String,
        trim: true
    },
    deleted: {
        type: Boolean,
        default: false
    },
    deletedOn: {
        type: Date
    },
    deletedBy: {
        type: String,
        trim: true
    }
});

module.exports = { Video: mongoose.model('Video', VideoSchema, 'Video') };