(function () {

    'use strict';

    const mongoose = require('mongoose'),
          schema   = mongoose.Schema;

    let alumniCategorySchema = new schema({
        year        : {
            type    : Number,
            trim    : true,
            required: true
        },
        urlSlogCategory: {
            type    : String,
            trim    : true
        },
        deleted        : {
            type    : Boolean,
            default : false
        },
        addedOn        : {
            type    : Date,
            default : new Date()
        },
        addedBy        : {
            type    : String,
            trim    : true
        },
        updatedOn      : {
            type    : Date
        },
        updatedBy      : {
            type    : String,
            trim    : true
        },
        deletedOn      : {
            type    : Date
        },
        deletedBy      : {
            type    : String,
            trim    : true
        }
    });

    let alumniSchema        = new schema({
        studentName         : {
            type        : String,
            trim        : true,
            required    : true
        },
        urlSlog             : {
            type        : String,
            trim        : true
        },
        categoryId      : {
            type        : String,
            trim        : true,
            required    : true
        },
        batchYear       : {
            type        : Number,
            trim        : true,
            required    : true
        },
        organizationName    : {
            type        : String,
            trim        : true
        },
        organizationPosition: {
            type        : String,
            trim        : true
        },
        imageName           : {
            type        : String,
            required    : true
        },
        imageProperties     : {
            imageExtension: {
                type:String,
                trim: true
            },
            imagePath     : {
                type:String,
                trim: true
            }
        },
        deleted             : {
            type        : Boolean,
            default     : false
        },
        addedOn             : {
            type        : Date,
            default     : new Date()
        },
        addedBy             : {
            type        : String,
            trim        : true
        },
        updatedOn           : {
            type        : Date
        },
        updatedBy           : {
            type        : String,
            trim        : true
        },
        deletedOn           : {
            type        : Date
        },
        deletedBy           : {
            type        : String,
            trim        : true
        }
    });

    module.exports = {
        AlumniCategory: mongoose.model('AlumniCategory', alumniCategorySchema, 'AlumniCategory'),
        Alumni        : mongoose.model('Alumni', alumniSchema, 'Alumni')
    };
})();