/*created on 24/07/17 by bishwo*/
(function(){
    'use strict';

    var mongoose = require('mongoose'),
        Schema   = mongoose.Schema;

    var eventRegistrationSchema = new Schema({
        
        fullName: {
            type: String,
            required: true,
            trim: true
        },
        email: {
            type: String,
            required: true,
            trim: true
        },
        contactNumber: {
            type: String,
            required: true,
            trim: true
        },
        eventId : {
            type: String,
            required: true
        },
        eventName : {
            type: String,
            required: true
        },
        emailSent : {
            type: Boolean,
            default: false
        },
        deleted: {
            type: Boolean,
            default: false
        },
        addedOn:{
            type: Date,
            default: Date.now
        },
    });

    module.exports = mongoose.model('EventRegistration', eventRegistrationSchema, 'EventRegistration');

})();