(function(){

    'use strict';

    const mongoose = require('mongoose'),
          schema = mongoose.Schema;

    let documentCategorySchema = new schema({
        categoryName: {
            type: String,
            trim: true,
            required: true
        },
        urlSlogCategory: {
            type: String,
            trim: true
        },
        categoryDescription: {
            type: String,
            trim: true
        },
        active: {
            type: Boolean,
            default: true
        },
        addedBy: {
            type: String,
            trim: true
        },
        addedOn: {
            type: Date,
            default: Date.now
        },
        updatedBy: {
            type: String,
            trim: true
        },
        updatedOn: {
            type: Date
        },
        deleted: {
            type: Boolean,
            default: false
        },
        deletedBy: {
            type: String,
            trim: true
        },
        deletedOn: {
            type: Date
        }
    });

    let documentSchema = new schema({
        documentTitle: {
            type: String,
            trim: true,
            required: true
        },
        urlSlog: {
            type: String,
            trim: true
        },
        categoryId: {
            type: String,
            trim: true
        },
        documentDescription: {
            type: String,
            trim: true,
            required: true
        },
        documentName: {
            type: String,
            trim: true
        },
        docProperties: {
            documentMimeType: {
                type: String,
                trim: true
            },
            docPath: {
                type: String,
                trim: true
            }
        },
        documentAltText: {
            type: String,
            trim: true
        },
        active: {
            type: Boolean,
            default: true
        },
        addedBy: {
            type: String,
            trim: true
        },
        addedOn: {
            type: Date,
            default: Date.now()
        },
        updatedBy: {
            type: String,
            trim: true
        },
        updatedOn: {
            type: Date
        },
        deleted: {
            type: Boolean,
            default: false
        },
        deletedBy: {
            type: String,
            trim: true
        },
        deletedOn: {
            type: Date,
        }
    });

    module.exports = {
        DocumentCategory: mongoose.model('DocumentCategory', documentCategorySchema, 'DocumentCategory'),
        Document: mongoose.model('Document', documentSchema, 'Document')
    };

})();