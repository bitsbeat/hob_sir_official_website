(function() {

  'user strict';

  const mongoose = require('mongoose'),
        regex = require('./regex.server.model'),
        autoIncrement = require('mongoose-auto-increment'),
        Schema = mongoose.Schema;

        var userQualification = new Schema({
          schoolBoard: {
            type: String,
            trim: true
          },
          schoolCourse: {
            type: String,
            trim: true
          },
          schoolGrade: {
            type: String,
            trim: true
          },
          intermediateBoard: {
            type: String,
            trim: true
          },
          intermediateCourse: {
            type: String,
            trim: true
          },
          intermediateGrade: {
            type: String,
            trim: true
          },
          bachelorBoard: {
            type: String,
            trim: true
          },
          bachelorCourse: {
            type: String,
            trim: true
          },
          bachelorGrade: {
            type: String,
            trim: true
          }
        });

        var parentInfo = new Schema({
          parentFullName: {
            type: String,
            trim: true
          },
          parentContactNo: {
            type: String,
            trim: true
          },
          parentContactAddress: {
            type: String,
            trim:true
          },
          parentProfession: {
            type: String,
            trim: true
          },
          parentEmail: {
            type:String,
            trim: true,
            lowercase: true,
            validate: {
                validator: function(email) {
                    if(email){
                        return regex.emailRegex.test(email);
                    }else{
                        return true;
                    }
                },
                message: '{VALUE} is not a valid email address!'
            }
          }
        });

    var userSchema = new Schema({
        firstName: {
            type: String,
            required: true,
            trim: true
        },
        middleName: {
            type: String,
            trim: true
        },
        lastName: {
            type: String,
            required: true,
            trim: true
        },
        dateOfBirth: {
            type: Date,
            required: true
        },
        nationality: {
            type: String,
            enum: ['Nepalese','Indian','Chinese','Others'],
            required: true
        },
        gender: {
            type: String,
            enum: ['Male','Female','Other'],
            required: true
        },
        country: {
            type: String,
            enum: ['Nepal','India','China','Others'],
            required: true
        },
        district: {
            type: String,
            trim: true
        },
        municipalityVdc: {
            type: String,
            trim: true
        },
        wardNo: {
            type: String,
            trim: true
        },
        email: {
            type:String,
            required:true,
            trim: true,
            lowercase: true,
            validate: {
                validator: function(email) {
                    if(email){
                        return regex.emailRegex.test(email);
                    }else{
                        return true;
                    }
                },
                message: '{VALUE} is not a valid email address!'
            }
        },
        qualification: [userQualification],
        parent: [parentInfo],
        isDeleted: {
            type: Boolean,
            default: false
        },
        addedOn: {
            type: Date
        },
        updatedOn: {
            type: Date
        },
        deletedOn: {
            type: Date
        },
        admissionStatus: {
            type: String,
            enum: ['Applied', 'Under-Review', 'Verified', 'Rejected'],
            default: ['Applied']
        },
        remarks: {
            type: String,
            default: '',
            trim: true
        },
        userId: {
            type: Number,
            unique: true
        },
        admissionFormNo: {
            type: String,
            trim: true
        }

    });

        autoIncrement.initialize(mongoose.connection);
        userSchema.plugin(autoIncrement.plugin, {model: 'UserInfo', field: 'userId', startAt: 1, incrementBy: 1});
        module.exports = mongoose.model('UserInfo', userSchema, 'UserInfo');
})();
