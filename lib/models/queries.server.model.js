(function () {

    'use strict';

    var mongoose = require('mongoose'),
        regex = require('./regex.server.model'),
        Schema = mongoose.Schema;

    var QuerySchema = new Schema({
        name: {
            type:String,
            required:true,
            trim: true
        },
        email: {
            type:String,
            required:true,
            trim: true,
            lowercase: true,
            validate: {
                validator: function(email) {
                    if(email){
                        return regex.emailRegex.test(email);
                    }else{
                        return true;
                    }
                },
                message: '{VALUE} is not a valid email address!'
            }
        },
        query: {
            type:String,
            trim: true
        },
        addedOn: {
            type: Date,
            default: Date.now
        },
        deleted: {
            type: Boolean,
            default: false
        },
        deletedBy: {
            type:String,
            trim: true
        },
        emailSent : {
            type: Boolean,
            default: false
        },
        deletedOn: {
            type: Date
        }
    });

    module.exports = mongoose.model('Query', QuerySchema, 'Query');

})();