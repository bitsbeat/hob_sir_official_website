(function () {

    "use strict";

    module.exports = {
        "userUnBlock"                       : "5702528b2bab8ed05c17f0be",
        "userConfirmRegistration"           : "5700ed1a31ba89ba3f49c663",
        "emailResponseToInterestedParties"  : "57076563033dc5944676b803",
        "emailResponseToSiteAdmin"          : "570767b94a8b142c49db4682",
        "passwordChangeConfirmation"        : "577e875a609df04d3a72cace",
        "eventSubscriptionEmail"            : "596afdf9fb109a20b63bcbda",
        "queryEmailToInterestedParties"     : "59788993a0c674505231774d",
        "queryEmailToSiteAdmin"             : "59788a8da0c674505231774e",
        "emailResponseToApplicant"          : "59844d24532dd82f0554440f",
        "admissionInfoEmailToSiteAdmin"     : "5986abc4167dc514c546c345",
        "updateInfoToApplicant"             : "5986f59fc34de12c5ddb1d3a",
        "newsletterSubscription"            : "583687df3c8c5d6c684d1188",
        "newsletterUnSubscription"          : "583688313c8c5d6c684d1189"
    };
})();