(function () {

    'use strict';

    module.exports = {

        development: {
            username: 'hob_khadka',
            password: 'hob_khadka',
            host: 'ds259105.mlab.com',
            port: '59105',
           dbName: 'prj_hobkhadka'
        },
        production: {
            username: 'HOBKhadkaBitcoin2017',
            password: 'pwd#HOBKhadkaBitcoin2017user369',
            host: '127.0.0.1',
            port: '27081',
            dbName: 'prj_hobkhadka'
        },
        test: {
            username: 'demouser',
            password: 'demouser',
            host: 'ds119728.mlab.com',
            port: '19728',
            dbName: 'prj_nodebeats'
        },
        dbBackup: {
            opt1: {
                type: 'archive',
                name: 'prj_nodebeats.archive',
                active: false,
                gzip: true
            },
            opt2: {
                type: 'bson',
                name: 'prj_nodebeats',
                active: true
            }
        }
    };
})();
