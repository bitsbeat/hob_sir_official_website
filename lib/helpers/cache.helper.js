(function (cacheHelper) {
    
        'use strict';
    
        var Promise = require('bluebird');

        cacheHelper.getDataFromCache = (appCache, cacheString) => {
            try {
                return appCache.get(cacheString);
            }
            catch (err) {
                // return  next(err);
            }
        };
    
        cacheHelper.storeDataInCache = (resObj, appCache, cacheString) => {
            try {
                return new Promise(function (resolve, reject) {
                    appCache.set(cacheString, resObj, function (err, success) {
                        if (!err && success) {
                            resolve(success);
                        } else {
                            reject(err);
                        }
                    });
                });
            }
            catch (err) {
                // return  next(err);
            }
        };
    
    })(module.exports);