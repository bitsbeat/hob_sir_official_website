var courseManagementRoutes = (function () {

    'use strict';

    var HTTPStatus                  = require('http-status'),
        express                     = require('express'),
        tokenAuthMiddleware         = require('../middlewares/token.authentication.middleware'),
        roleAuthMiddleware          = require('../middlewares/role.authorization.middleware'),
        messageConfig               = require('../configs/api.message.config'),
        courseManagementController  = require('../controllers/course.management.server.controller'),
        courseManagementRouter      = express.Router();


/*------------------------------------------ All ----------------------------------------*/

        courseManagementRouter.route('/detail')

        .get(courseManagementController.getAllData)
    
/*------------------------------------------ Specialization Section----------------------------------------*/
        courseManagementRouter.route('/specialization')

        .get(courseManagementController.getSpecializations)

        .post(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize,  courseManagementController.postSpecialization)

        courseManagementRouter.use('/specialization/:id', function(req,res, next){
                courseManagementController.getSpecializationById(req)
                .then(function(specializationInfo){

                    //saving in request object so that it can be used for other operations like updating data using put and patch method
                    if (specializationInfo) {
                        req.specializationInfo = specializationInfo;
                        next();
                        return null;// return a non-undefined value to signal that we didn't forget to return promise
                    } else {
                        res.status(HTTPStatus.NOT_FOUND);
                        res.json({
                            message: messageConfig.blog.notFoundDoc
                        });
                    }
                })
                .catch(function(err){
                    return next(err);
                });
        })
        courseManagementRouter.route('/specialization/:id')

        .get(function (req, res) {
            res.status(HTTPStatus.OK);
            res.json(req.specializationInfo);
        })

        .put(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize,  courseManagementController.putSpecialization)

        .patch(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize,  courseManagementController.patchSpecialization)
        
/*------------------------------------------ Semester Section----------------------------------------*/

        courseManagementRouter.route('/semester')

        .get(courseManagementController.getSemesters)

        .post(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize,  courseManagementController.postSemester)


        courseManagementRouter.use('/semester/:id', function(req,res, next){
                courseManagementController.getSemesterById(req)
                .then(function(semesterInfo){

                    //saving in request object so that it can be used for other operations like updating data using put and patch method
                    if (semesterInfo) {
                        req.semesterInfo = semesterInfo;
                        next();
                        return null;// return a non-undefined value to signal that we didn't forget to return promise
                    } else {
                        res.status(HTTPStatus.NOT_FOUND);
                        res.json({
                            message: messageConfig.blog.notFoundDoc
                        });
                    }
                })
                .catch(function(err){
                    return next(err);
                });
        })
        courseManagementRouter.route('/semester/:id')

        .get(function (req, res) {
            res.status(HTTPStatus.OK);
            res.json(req.semesterInfo);
        })

        .put(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize,  courseManagementController.putSemester)

        .patch(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize,  courseManagementController.patchSemester)

/*------------------------------------------ Course Section----------------------------------------*/

        courseManagementRouter.use('/one/:id', function(req,res, next){
                courseManagementController.getCourseById(req)
                .then(function(courseInfo){
                    if (courseInfo) {
                        req.courseInfo = courseInfo;
                        next();
                        return null;// return a non-undefined value to signal that we didn't forget to return promise
                    } else {
                        res.status(HTTPStatus.NOT_FOUND);
                        res.json({
                            message: messageConfig.blog.notFoundDoc
                        });
                    }
                })
                .catch(function(err){
                    return next(err);
                });
        })
        courseManagementRouter.route('/one/:id')

        .get(function (req, res) {
            res.status(HTTPStatus.OK);
            res.json(req.courseInfo);
        })

        .put(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize,  courseManagementController.updateCourse)

        .patch(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize,  courseManagementController.patchCourse)

        courseManagementRouter.use('/:id', function(req,res, next){
                courseManagementController.getCourses(req)
                .then(function(courseInfo){
                    if (courseInfo) {
                        req.courseInfo = courseInfo;
                        next();
                        return null;// return a non-undefined value to signal that we didn't forget to return promise
                    } else {
                        res.status(HTTPStatus.NOT_FOUND);
                        res.json({
                            message: messageConfig.blog.notFoundDoc
                        });
                    }
                })
                .catch(function(err){
                    return next(err);
                });
        });

        courseManagementRouter.route('/:id')

        .get(function (req, res) {
            res.status(HTTPStatus.OK);
            res.json(req.courseInfo);
        })

        courseManagementRouter.route('/')

        .post(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize,  courseManagementController.postCourse)


    return courseManagementRouter;

})();

module.exports = courseManagementRoutes;

