var documentManagementRoutes = (function () {

    'use strict';

    const HTTPStatus = require('http-status'),
          express = require('express'),
          tokenAuthMiddleware = require('../middlewares/token.authentication.middleware'),
          roleAuthMiddleware = require('../middlewares/role.authorization.middleware'),
          messageConfig = require('../configs/api.message.config'),
          documentFilePath = './public/uploads/documents/documentManagement/',
          uploadPrefix = 'doc',
          fileUploadHelper = require('../helpers/file.upload.helper')("",documentFilePath, uploadPrefix),
          documentUploader = fileUploadHelper.documentUploader,
          documentManagementController = require('../controllers/document.management.controller'),
          documentManagementRouter = express.Router();

          documentManagementRouter.route('/document-category/')

          /**
           * @api {post} /api/documentcategory/ Post Document category data
           * @apiPermission admin
           * @apiName postDocumentCategory
           * @apiGroup Document
           *
           * @apiParam {String} categoryName  name of the category of document.
           *
           * @apiDescription saves the document category information to the database
           * @apiVersion 0.0.1
           * @apiHeader (AuthorizationHeader) {String} authorization x-access-token value.
           * @apiHeaderExample {json} Header-Example:
           *{
           *  "x-access-token": "yJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c"
           * }
           *
           * @apiExample {curl} Example usage:
           *
           *
           * curl \
           * -v \
           * -X POST  \
           * http://localhost:3000/api/documentcategory/ \
           * -H 'Content-Type: application/json' \
           * -H "x-access-token: eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c2JlYXQuY29tIiwidXNlcm5hbWUiOiJzdXBlcmFkbWluIiwiX2lkIjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwidXNlckNvbmZpcm1lZCI6ZmFsc2UsImJsb2NrZWQiOmZhbHNlLCJkZWxldGVkIjpmYWxzZSwiYWRkZWRPbiI6IjIwMTYtMDctMDhUMDc6NTQ6MDMuNzY2WiIsInR3b0ZhY3RvckF1dGhFbmFibGVkIjpmYWxzZSwidXNlclJvbGUiOiJhZG1pbiIsImFjdGl2ZSI6dHJ1ZX0sImNsYWltcyI6eyJzdWJqZWN0IjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwiaXNzdWVyIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIiwicGVybWlzc2lvbnMiOlsic2F2ZSIsInVwZGF0ZSIsInJlYWQiLCJkZWxldGUiXX0sImlhdCI6MTQ2OTAwMDE4NCwiZXhwIjoxNDY5MDIwMTg0LCJpc3MiOiI1NzdmNWMxYjU4Njk5MDJkNjdlYjIyYTgifQ.Npy3LZnsOqOqH7RBfl3oYAdVOYdlU3_6i8izGo7xkHzI610NjnCaljiMxb7s71RJsRoqVqNqB-gai8vzWlofrQ" \
           * -d '{"categoryName": "Course", "categoryDescription": "This is course","active": true}'
           *
           * @apiSuccess {String} message Document category saved successfully.
           *
           * @apiSuccessExample Success-Response:
           *     HTTP/1.1 200 OK
           *     {
           *         "message": "Document category saved successfully"
           *       }
           *
           * @apiError (UnAuthorizedError) {String} message authentication token was not supplied
           * @apiError (UnAuthorizedError) {Boolean} isToken to check if token is supplied or not , if token is supplied , returns true else returns false
           * @apiError (UnAuthorizedError) {Boolean} success true if jwt token verifies
           *
           * @apiErrorExample Error-Response:
           *     HTTP/1.1 401 Unauthorized
           *     {
           *       "isToken": true,
           *        "success": false,
           *        "message": "Authentication failed"
           *     }
           *
           *
           * @apiError  (UnAuthorizedError_1) {String} message  You are not authorized to access this api route.
           *
           * @apiErrorExample Error-Response:
           *     HTTP/1.1 401 Unauthorized
           *     {
           *       "message": "You are not authorized to access this api route."
           *     }
           *
           * @apiError  (UnAuthorizedError_2) {String} message  You are not authorized to perform this action.
           *
           * @apiErrorExample Error-Response:
           *     HTTP/1.1 401 Unauthorized
           *     {
           *       "message": "You are not authorized to perform this action"
           *     }
           *
           *
           *
           * @apiError  (BadRequest) {String} message Document category is required
           *
           * @apiErrorExample Error-Response:
           *     HTTP/1.1 400 Bad Request
           *
           *     {
           *       "message": "Document category is required"
           *     }
           *
           *
           *
           * @apiError  (AlreadyExists)  {String} message  Document category with same name already exists
           *
           * @apiErrorExample Error-Response:
           *     HTTP/1.1 409 Conflict
           *     {
           *       "message": "Document category with same name already exists"
           *     }
           *
           *
           *
           * @apiError (InternalServerError)  {String} message  Internal Server Error
           *
           * @apiErrorExample Error-Response:
           *     HTTP/1.1 500 Internal Server Error
           *     {
           *       "message": "Internal Server Error"
           *     }
           *
           */

              .post(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, documentManagementController.postDocumentCategory)

              /**
               * @api {get} /api/documentcategory/ Get document category list
               * @apiPermission public
               * @apiName getAllDocumentCategories
               * @apiGroup Document
               *
               *
               * @apiParam {Int} perpage Number of data to return on each request
               * @apiParam {Int} page Current page number of pagination system.
               * @apiParam {String} categoryname name of document category to filter document categories
               * @apiParam {Boolean} active whether to get data with active bit true or false, if true, then returns data list with active bit set to true only
               *
               *
               *   @apiParamExample {json} Request-Example:
               *     {
               *       "perpage": 10,
               *       "page": 1,
               *       "categoryname": "technology"
               *     }
               *
               *  * @apiParamExample {json} Request-Example:
               *     {
               *       "active": true,
               *       "categoryname": "technology"
               *     }
               *
               * @apiDescription Retrieves the document category list, if exists, else return empty array
               * @apiVersion 0.0.1
               *
               * @apiExample {curl} Example usage:
               * curl \
               * -G \
               * -v \
               * "http://localhost:3000/api/documentcategory" \
               * --data-urlencode "perpage=10" \
               * --data-urlencode "page=1"
               *
               * @apiExample {curl} Example usage:
               * curl \
               * -G \
               * -v \
               * "http://localhost:3000/api/documentcategory" \
               * --data-urlencode "active=true" \
               * --data-urlencode "categoryname=technology"
               *
               *
               * @apiSuccess {Array} dataList list of document categories
               * @apiSuccess {String} dataList._id  object id of document category data.
               * @apiSuccess {String} dataList.categoryName  name of the category of document.
               * @apiSuccess {String} dataList.categoryDescription   brief description about document category.
               * @apiSuccess {String} dataList.urlSlogCategory  clean URL of document category.
               * @apiSuccess {String} dataList.addedOn  date on which category is added.
               * @apiSuccess {Boolean} dataList.active  active bit status.
               * @apiSuccess {Int} totalItems  total no of document categories in the related collection in database.
               * @apiSuccess {Int} currentPage  current page number of client pagination system.
               * @apiSuccess {Int} perpage per page limitation of data.
               *
               * @apiSuccessExample Success-Response:
               *     HTTP/1.1 200 OK
               *     {
               *           "dataList": [
               *               {
               *                   "_id": "578f2ca535102e0c5d5c3ce2",
               *                   "categoryDescription": "contains food related document",
               *                   "urlSlogCategory": "food",
               *                   "categoryName": "Food",
               *                   "addedOn": "2016-07-20T07:47:49.232Z",
               *                   "active": false
               *               }
               *           ],
               *           "totalItems": 1,
               *           "currentPage": 1,
               *           "perPage": 10
               *       }
               *
               *
               * @apiError  (NotFound) {String} message  Category not found
               *
               * @apiErrorExample Error-Response:
               *     HTTP/1.1 404 Not Found
               *     {
               *       "message": "Category not found"
               *     }
               *
               * @apiError  (InternalServerError) {String} message  Internal Server Error
               *
               * @apiErrorExample Error-Response:
               *     HTTP/1.1 500 Internal Server Error
               *     {
               *       "message": "Internal Server Error"
               *     }
               *
               */

              .get(getAllDocumentCategories);

        function getAllDocumentCategories(req, res, next) {
            documentManagementController.getDocumentCategories (req, next)
                .then((documentCategoryList) => {
                if(documentCategoryList) {
                    res.status(HTTPStatus.OK);
                    res.json(documentCategoryList);
                }
                else {
                    res.status(HTTPStatus.NOT_FOUND);
                    res.json({
                        message: messageConfig.document.notFoundCategory
                    })
                }
                })
                .catch((err) => {
                    return next(err);
                });
        };

        documentManagementRouter.use('/document-category/:categoryId', (req, res, next) => {
            documentManagementController.getDocumentCategoryById(req)
                .then((documentCategoryInfo) => {
                if(documentCategoryInfo) {
                    req.documentCategoryInfo = documentCategoryInfo;
                    next();
                    return null;
                }
                else {
                    res.status(HTTPStatus.NOT_FOUND);
                    res.json({
                        message: messageConfig.document.notFoundCategory
                    });
                }
                })
                .catch((err) => {
                    return next(err);
                });
        });

        documentManagementRouter.route('/document-category/:categoryId')

        /**
         * @api {get} /api/documentcategory/:categoryId Get document category object by ID
         * @apiPermission public
         * @apiName getDocumentCategoryByID
         * @apiGroup Document
         *
         * @apiParam {String} categoryId  object id of the blog category data
         *
         *
         *   @apiParamExample {json} Request-Example:
         *     {
         *       "categoryId": "578f2c9535102e0c5d5c3ce1"
         *  }
         *
         *
         * @apiDescription Retrieves the blog category object by Id, if exists, else return empty object
         * @apiVersion 0.0.1
         *
         * @apiExample {curl} Example usage:
         * curl \
         * -G \
         * -v \
         * "http://localhost:3000/api/documentcategory/578f2c9535102e0c5d5c3ce1"
         *
         *
         * @apiSuccess {String} _id  object id of document category data.
         * @apiSuccess {String} categoryName  name of the category of document.
         * @apiSuccess {String} categoryDescription   brief description about document category.
         * @apiSuccess {String} urlSlogCategory  clean URL of blog category.
         * @apiSuccess {String} addedOn  date on which category is added.
         * @apiSuccess {Boolean} active  active bit status.
         *
         * @apiSuccessExample Success-Response:
         *     HTTP/1.1 200 OK
         *     {
         *           "_id": "578f2c9535102e0c5d5c3ce1",
         *           "categoryDescription": "contains technology related to document",
         *           "urlSlogCategory": "technology",
         *           "categoryName": "technology",
         *           "addedOn": "2016-07-20T07:47:33.132Z",
         *           "active": true
         *       }
         *
         *
         * @apiError  (NotFound) {String} message  Category not found
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 404 Not Found
         *     {
     *       "message": "Category not found"
     *     }
         *
         * @apiError  (InternalServerError) {String} message  Internal Server Error
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 500 Internal Server Error
         *     {
     *       "message": "Internal Server Error"
     *     }
         *
         */

            .get((req, res) => {
            res.status(HTTPStatus.OK);
            res.json(req.documentCategoryInfo);

            })

            /**
             * @api {put} /api/documentcategory/:categoryId Updates existing document category data
             * @apiPermission admin
             * @apiName updateDocumentCategory
             * @apiGroup Document
             *
             * @apiParam {String} categoryId  object id of the document category data
             *
             *
             *   @apiParamExample {json} Request-Example:
             *     {
             *       "categoryId": "578f32bca4d436d76bc751cf"
             *     }
             *
             * @apiDescription Updates existing document category information to the database
             * @apiVersion 0.0.1
             * @apiHeader (AuthorizationHeader) {String} authorization x-access-token value.
             * @apiHeaderExample {json} Header-Example:
             *{
             *  "x-access-token": "yJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c"
             * }
             *
             * @apiExample {curl} Example usage:
             *
             *
             * curl \
             * -v \
             * -X PUT  \
             * http://localhost:3000/api/documentcategory/578f32bca4d436d76bc751cf \
             * -H 'Content-Type: application/json' \
             * -H "x-access-token: eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c2JlYXQuY29tIiwidXNlcm5hbWUiOiJzdXBlcmFkbWluIiwiX2lkIjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwidXNlckNvbmZpcm1lZCI6ZmFsc2UsImJsb2NrZWQiOmZhbHNlLCJkZWxldGVkIjpmYWxzZSwiYWRkZWRPbiI6IjIwMTYtMDctMDhUMDc6NTQ6MDMuNzY2WiIsInR3b0ZhY3RvckF1dGhFbmFibGVkIjpmYWxzZSwidXNlclJvbGUiOiJhZG1pbiIsImFjdGl2ZSI6dHJ1ZX0sImNsYWltcyI6eyJzdWJqZWN0IjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwiaXNzdWVyIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIiwicGVybWlzc2lvbnMiOlsic2F2ZSIsInVwZGF0ZSIsInJlYWQiLCJkZWxldGUiXX0sImlhdCI6MTQ2OTAwMDE4NCwiZXhwIjoxNDY5MDIwMTg0LCJpc3MiOiI1NzdmNWMxYjU4Njk5MDJkNjdlYjIyYTgifQ.Npy3LZnsOqOqH7RBfl3oYAdVOYdlU3_6i8izGo7xkHzI610NjnCaljiMxb7s71RJsRoqVqNqB-gai8vzWlofrQ" \
             * -d '{"categoryName": "Sports Articles", "categoryDescription": "Contains sports related blog articles and tips","active": true}'
             *
             * @apiSuccess {String} message Document category updated successfully.
             *
             * @apiSuccessExample Success-Response:
             *     HTTP/1.1 200 OK
             *     {
             *         "message": "Document category updated successfully"
             *       }
             *
             * @apiError (UnAuthorizedError) {String} message authentication token was not supplied
             * @apiError (UnAuthorizedError) {Boolean} isToken to check if token is supplied or not , if token is supplied , returns true else returns false
             * @apiError (UnAuthorizedError) {Boolean} success true if jwt token verifies
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 401 Unauthorized
             *     {
             *       "isToken": true,
             *        "success": false,
             *        "message": "Authentication failed"
             *     }
             *
             *
             * @apiError  (UnAuthorizedError_1) {String} message  You are not authorized to access this api route.
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 401 Unauthorized
             *     {
             *       "message": "You are not authorized to access this api route."
             *     }
             *
             * @apiError  (UnAuthorizedError_2) {String} message  You are not authorized to perform this action.
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 401 Unauthorized
             *     {
             *       "message": "You are not authorized to perform this action"
             *     }
             *
             *
             *
             * @apiError  (BadRequest) {String} message Document category name is required
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 400 Bad Request
             *
             *     {
             *       "message": "Document category name is required"
             *     }
             *
             *
             *
             * @apiError  (AlreadyExists)  {String} message  Document category with same name already exists
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 409 Conflict
             *     {
             *       "message": "Document category with same name already exists"
             *     }
             *
             *
             *
             * @apiError (InternalServerError)  {String} message  Internal Server Error
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 500 Internal Server Error
             *     {
             *       "message": "Internal Server Error"
             *     }
             *
             */

            .put(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, documentManagementController.updateDocumentCategory)

            /**
             * @api {patch} /api/documentcategory/:categoryId Deletes existing Document category data
             * @apiPermission admin
             * @apiName deleteDocumentCategory
             * @apiGroup Document
             *
             * @apiParam {String} categoryId  object id of the document category data
             *
             *
             *   @apiParamExample {json} Request-Example:
             *     {
             *       "categoryId": "578f32bca4d436d76bc751cf"
             *     }
             *
             * @apiDescription Deletes existing document category information from the database
             * @apiVersion 0.0.1
             * @apiHeader (AuthorizationHeader) {String} authorization x-access-token value.
             * @apiHeaderExample {json} Header-Example:
             *{
             *  "x-access-token": "yJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c"
             * }
             *
             * @apiExample {curl} Example usage:
             *
             *
             * curl \
             * -v \
             * -X PATCH  \
             * http://localhost:3000/api/documentcategory/578f32bca4d436d76bc751cf \
             * -H "x-access-token: eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c2JlYXQuY29tIiwidXNlcm5hbWUiOiJzdXBlcmFkbWluIiwiX2lkIjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwidXNlckNvbmZpcm1lZCI6ZmFsc2UsImJsb2NrZWQiOmZhbHNlLCJkZWxldGVkIjpmYWxzZSwiYWRkZWRPbiI6IjIwMTYtMDctMDhUMDc6NTQ6MDMuNzY2WiIsInR3b0ZhY3RvckF1dGhFbmFibGVkIjpmYWxzZSwidXNlclJvbGUiOiJhZG1pbiIsImFjdGl2ZSI6dHJ1ZX0sImNsYWltcyI6eyJzdWJqZWN0IjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwiaXNzdWVyIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIiwicGVybWlzc2lvbnMiOlsic2F2ZSIsInVwZGF0ZSIsInJlYWQiLCJkZWxldGUiXX0sImlhdCI6MTQ2OTAwMDE4NCwiZXhwIjoxNDY5MDIwMTg0LCJpc3MiOiI1NzdmNWMxYjU4Njk5MDJkNjdlYjIyYTgifQ.Npy3LZnsOqOqH7RBfl3oYAdVOYdlU3_6i8izGo7xkHzI610NjnCaljiMxb7s71RJsRoqVqNqB-gai8vzWlofrQ"
             *
             * @apiSuccess {String} message Document category deleted successfully.
             *
             * @apiSuccessExample Success-Response:
             *     HTTP/1.1 200 OK
             *     {
             *         "message": "Document category deleted successfully"
             *       }
             *
             * @apiError (UnAuthorizedError) {String} message authentication token was not supplied
             * @apiError (UnAuthorizedError) {Boolean} isToken to check if token is supplied or not , if token is supplied , returns true else returns false
             * @apiError (UnAuthorizedError) {Boolean} success true if jwt token verifies
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 401 Unauthorized
             *     {
             *       "isToken": true,
             *        "success": false,
             *        "message": "Authentication failed"
             *     }
             *
             * @apiError  (UnAuthorizedError_1) {String} message  You are not authorized to access this api route.
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 401 Unauthorized
             *     {
             *       "message": "You are not authorized to access this api route."
             *     }
             *
             * @apiError  (UnAuthorizedError_2) {String} message  You are not authorized to perform this action.
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 401 Unauthorized
             *     {
             *       "message": "You are not authorized to perform this action"
             *     }
             *
             *
             * @apiError (InternalServerError)  {String} message  Internal Server Error
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 500 Internal Server Error
             *     {
             *       "message": "Internal Server Error"
             *     }
             *
             */


            .patch(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, documentManagementController.deleteDocumentCategory);

        documentManagementRouter.route('/document')

            .post(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, documentUploader.single('documentName'), documentManagementController.postDocument)
            .get(getAllDocuments);

        function getAllDocuments(req, res, next) {
            documentManagementController.getDocuments (req, next)
                .then((documentList) => {
                    if(documentList) {
                        res.status(HTTPStatus.OK);
                        res.json(documentList);
                    }
                    else {
                        res.status(HTTPStatus.NOT_FOUND);
                        res.json({
                        message: messageConfig.document.notFoundDocument
                        });
                    }
                })
                .catch((err) => {
                    return next(err);
                });
        };

        documentManagementRouter.use('/document/:documentId', (req, res, next) => {
            documentManagementController.getDocumentById(req)
                .then((documentInfo) => {
                if(documentInfo) {
                    req.documentInfo = documentInfo;
                    next();
                    return null;
                }
                else {
                    res.status(HTTPStatus.NOT_FOUND);
                    res.json({
                        message: messageConfig.document.notFoundDocument
                    });
                }
                })
                .catch((err) => {
                return next(err);
                })
        });

        documentManagementRouter.route('/document/:documentId')


            .get((req, res) => {
            res.status(HTTPStatus.OK);
            res.json(req.documentInfo);
            })

            .patch(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, documentManagementController.deleteDocument)

            .put(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, documentUploader.single('documentName'), documentManagementController.updateDocument);


        documentManagementRouter.route('/filter/document-category/:documentCategory')
            .get(getDocumentByCategory);

        function getDocumentByCategory(req, res, next) {
            documentManagementController.getDocumentByCategory(req, next)
                .then((documentList) => {
                if(documentList) {
                    res.status(HTTPStatus.OK);
                    res.json(documentList);
                }
                else {
                    res.status(HTTPStatus.NOT_FOUND);
                    res.json({
                        message: messageConfig.document.notFoundDocument
                    });
                }
                })
                .catch((err) => {
                return next(err);
                });
    };
    return documentManagementRouter;
})();

module.exports = documentManagementRoutes;