/**
 * Created by lakhe on 11/24/16.
 */

var newsletterRoutes = (function () {

    'use strict';

    var HTTPStatus = require('http-status'),
        express = require('express'),
        tokenAuthMiddleware = require('../middlewares/token.authentication.middleware'),
        roleAuthMiddleware = require('../middlewares/role.authorization.middleware'),
        messageConfig = require('../configs/api.message.config'),
        newsletterController = require('../controllers/newsletter.server.controller'),
        newsletterRouter = express.Router();

    newsletterRouter.route('/subscribe')
        .get( tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, getAllNewsLetterSubscribedUsers)
        .post( newsletterController.subscribeNewsletter );


    newsletterRouter.route('/unsubscribe/:newsletterId')
        .patch( newsletterController.unSubscribeNewsletter );



    function getAllNewsLetterSubscribedUsers(req, res, next) {
        newsletterController.getAllNewsLetterSubscribedUsers(req, next)
            .then(function(lstSubscribedUsers){
                //if exists, return data in json format
                if (lstSubscribedUsers) {
                    res.status(HTTPStatus.OK);
                    res.json(lstSubscribedUsers);
                } else {
                    res.status(HTTPStatus.NOT_FOUND);
                    res.json({
                        message: messageConfig.newsletter.notFound
                    });
                }
            })
            .catch(function(err){
                return next(err);
            });
    }

    return newsletterRouter;

})();

module.exports = newsletterRoutes;