let videoRoutes = (function () {
'use strict';

let HTTPStatus = require('http-status'),
    express = require('express'),
    tokenAuthMiddleware = require('../middlewares/token.authentication.middleware'),
    roleAuthMiddleware = require('../middlewares/role.authorization.middleware'),
    messageConfig = require('../configs/api.message.config'),
    videoRouter = express.Router(),
    videoController = require('../controllers/video.server.controller');

videoRouter.route('/video')

    .post(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, videoController.postVideoGallery)

    .get(videoController.getAllVideoGallery);

videoRouter.use('/video/:videoId', (req, res, next) => {
    videoController.getVideoById(req)
        .then((videoDetail) => {
            if(videoDetail) {
                req.videoDetail = videoDetail;
                next();
                return null;                
            }
            else {
                res.status(HTTPStatus.NOT_FOUND);
                res.json({
                    message: "No details found"
                });
            }
        })
        .catch((err) => {
            return next(err);
        })
});

videoRouter.route('/video/:videoId')

    .get((req, res) => {
        res.status(HTTPStatus.OK);
        res.json(req.videoDetail);
    })

    .put(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, videoController.updateVideo)

    .patch(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, videoController.deleteVideo);

    return videoRouter;
})();

module.exports = videoRoutes;