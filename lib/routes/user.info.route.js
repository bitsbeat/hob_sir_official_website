

const express = require('express');
const userInfoRouter = express.Router();
const user = require('../controllers/user.info.controller');
const mw = require('../middlewares/response.middleware');
const tokenAuthMiddleware = require('../middlewares/token.authentication.middleware');
      roleAuthMiddleware = require('../middlewares/role.authorization.middleware');


/**
 * @api {post} /api/admission-info Register User information
 * @apiPermission public
 * @apiName register
 * @apiGroup UserInfo
 *
 * @apiParam {String} firstName Mandatory first name of the user
 * @apiParam {String} [middleName] Optional middle name of the user
 * @apiParam {String} lastName Mandatory last name of the user
 * @apiParam {String} dateOfBirth Mandatory date of birth of the user
 * @apiParam {enum} nationality Mandatory nationality of the user
 * @apiParam {enum} gender Mandatory gender of the user
 * @apiParam {enum} maritalStatus Mandatory marital status of the user
 * @apiParam {enum} religion Mandatory religion of the user
 * @apiParam {enum} country Mandatory country of the user
 * @apiParam {String} [district] Optional district of the user
 * @apiParam {String} [municipalityVdc] Optional municipality or vdc of the user
 * @apiParam {String} [wardNo] Optional ward number of the user
 * @apiParam {String} email Mandatory email of the user
 * @apiParam {Array} qualification Optional qualification information of the user
 * @apiParam {Array} parent Optional parent information of the user
 *
 *
 * @apiDescription saves user information to the database
 * @apiHeader (AuthorizationHeader) {String} Authorization
 * @apiHeaderExample {json} Header-example:
 * {
 *      "Authorization": "yJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c"
 * }
 * @apiExample {curl} Example usage:
 *
 *
 *  curl \
 * -v \
 * -X POST  \
 * http://localhost:3000/api/admission-info \
 * -H "Content-Type: application/json"  \
 * -H "Authorization: eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c2JlYXQuY29tIiwidXNlcm5hbWUiOiJzdXBlcmFkbWluIiwiX2lkIjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwidXNlckNvbmZpcm1lZCI6ZmFsc2UsImJsb2NrZWQiOmZhbHNlLCJkZWxldGVkIjpmYWxzZSwiYWRkZWRPbiI6IjIwMTYtMDctMDhUMDc6NTQ6MDMuNzY2WiIsInR3b0ZhY3RvckF1dGhFbmFibGVkIjpmYWxzZSwidXNlclJvbGUiOiJhZG1pbiIsImFjdGl2ZSI6dHJ1ZX0sImNsYWltcyI6eyJzdWJqZWN0IjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwiaXNzdWVyIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIiwicGVybWlzc2lvbnMiOlsic2F2ZSIsInVwZGF0ZSIsInJlYWQiLCJkZWxldGUiXX0sImlhdCI6MTQ2OTE4MzQ0MCwiZXhwIjoxNDY5MjAzNDQwLCJpc3MiOiI1NzdmNWMxYjU4Njk5MDJkNjdlYjIyYTgifQ.KSp3_S2LEhixyzTnNJE9AtD5u7-8ljImr-Np0kzxkoNWWjqB66a_T67NTCTMoMZys7PIYPFNBR9VwLOhkrX7BQ" \
 * @apiSuccess {String} Message User registered successfully.
 * @apiSuccessExample Success-response:
 * {
 *   "Success": 1,
 *   "Message": "User registered successfully",
 *   "data": {
 *       "__v": 0,
 *       "firstName": "Sajan",
 *       "middleName": "",
 *       "lastName": "Awale",
 *       "dateOfBirth": "1995-05-09T00:00:00.000Z",
 *       "nationality": "Nepalese",
 *       "gender": "Male",
 *       "maritalStatus": "Single",
 *       "religion": "Hinduism",
 *       "country": "Nepal",
 *       "district": "Lalitpur",
 *       "municipalityVdc": "LMC",
 *       "wardNo": "09",
 *       "email": "sanjeevmagar49@gmail.com",
 *       "_id": "596da247619e6000269a8ecd",
 *       "isDeleted": false,
 *       "parent": [
 *           {
 *               "parentFullName": "Dev Maharjan",
 *               "parentContactNo": "9841780434",
 *               "parentContactAddress": "Old Baneshwor",
 *               "parentProfession": "Scaffolding",
 *               "parentEmail": "manumagar28@gmail.com",
 *               "_id": "596da247619e6000269a8ecf"
 *           }
 *       ],
 *       "qualification": [
 *           {
 *               "schoolBoard": "SEE",
 *               "schoolCourse": "SEE",
 *               "schoolGrade": "Distinction",
 *               "intermediateBoard": "HSEB",
 *               "intermediateCourse": "High School",
 *               "intermediateGrade": "First Division",
 *               "bachelorBoard": "University",
 *               "bachelorCourse": "B.Sc.IT",
 *               "bachelorGrade": "Pass",
 *               "_id": "596da247619e6000269a8ece"
 *           }
 *       ]
 *   }
 *   }
 * @apiErrorExample Error-Response:
 *     Unauthorized
 *     {
 *       "isToken": true,
 *        "success": false,
 *        "message": "Authentication failed"
 *     }
 * @apiError (BadRequest) {String[]} Messaage Validation errors for not entering values for mandatory fields: firstName, lastName, dateOfBirth, nationality, gender, maritalStatus, religion, country, email
 * @apiErrorExample Error Response:
 *      BadRequest
 *      {
 *   "result": "failure",
 *   "success": 0,
 *   "error": 1,
 *   "errorMsg": [
 *       "First name not provided",
 *       "Last name not provided",
 *       "Date of birth not provided",
 *       "Nationality not provided",
 *       "Gender not provided",
 *       "Marital status not provided",
 *       "Religion not provided",
 *       "Country not provided",
 *       "Email not provided"
 *   ],
 *   "statusCode": 400
 */
userInfoRouter.post('/admission-info', user.collectToRegister, user.register, mw.respond,mw.error);

/**
 * @api {get} /api/admission-info get all the user's information
 * @apiPermission admin
 * @apiName retrieve
 * @apiGroup UserInfo
 *
 * @apiDescription Retrieves the information of the users, if exists, else return empty array.
 * @apiExample {curl} Example usage:
 * curl \
 * -G \
 * -v \
 * "http://localhost:3000/api/admission-info" \
 *
 * @apiSuccess {String} _id object id of user info.
 * @apiSuccess {String} firstName first name of the user.
 * @apiSuccess {String} middleName middle name of the user.
 * @apiSuccess {String} lastName last name of the user.
 * @apiSuccess {String} dateOfBirth date of birth of the user.
 * @apiSuccess {enum} nationality nationality of the user.
 * @apiSuccess {enum} gender gender of the user.
 * @apiSuccess {enum} maritalStatus marital status of the user.
 * @apiSuccess {enum} religion religion of the user.
 * @apiSuccess {enum} country country of the user.
 * @apiSuccess {String} district district of the user.
 * @apiSuccess {String} municipalityVdc municipality or vdc of the user.
 * @apiSuccess {String} wardNo ward number of the place where the user resides.
 * @apiSuccess {String} email email of the user.
 * @apiSuccess {Array} qualification information about the qualification of the user.
 * @apiSuccess {Array} parent parent's information of the user.
 *
 * @apiSuccessExample Success-Response:
 *   {
 *   "Message": "Here are the list of user's information",
 *   "data": [
 *       {
 *           "_id": "596c58a625d9e2014c67c755",
 *           "firstName": "Mahesh",
 *           "middleName": "",
 *           "lastName": "Magar",
 *           "dateOfBirth": "1995-05-06T00:00:00.000Z",
 *           "nationality": "Nepalese",
 *           "gender": "Male",
 *           "maritalStatus": "Married",
 *           "religion": "Buddhism",
 *           "country": "Nepal",
 *           "district": "Pokhara",
 *           "municipalityVdc": "KMC",
 *           "wardNo": "34",
 *           "email": "sanjeevmagar49@gmail.com",
 *           "__v": 3,
 *           "isDeleted": false,
 *           "parent": [
 *               {
 *                   "parentFullName": "Madan Bdr Khapangi Magar",
 *                   "parentContactNo": "9841780434",
 *                   "parentContactAddress": "Katyani Chowk",
 *                   "parentProfession": "Scaffolding",
 *                   "parentEmail": "manumagar28@gmail.com",
 *                   "_id": "596dab181ec2d8005196de88"
 *               },
 *               {
 *                   "parentFullName": "Maya Devi Khapangi Magar",
 *                   "parentContactNo": "9808858267",
 *                   "parentContactAddress": "Bhimsengola",
 *                   "parentProfession": "Housewife",
 *                   "parentEmail": "manumagar28@gmail.com",
 *                   "_id": "596dab181ec2d8005196de87"
 *               }
 *           ],
 *           "qualification": [
 *               {
 *                   "schoolBoard": "See",
 *                   "schoolCourse": "See",
 *                   "schoolGrade": "Distinction",
 *                   "intermediateBoard": "HSEB",
 *                   "intermediateCourse": "High School",
 *                   "intermediateGrade": "First Division",
 *                   "bachelorBoard": "University",
 *                   "bachelorCourse": "B.Sc.IT",
 *                   "bachelorGrade": "Pass",
 *                   "_id": "596dab181ec2d8005196de86"
 *               }
 *           ]
 *       }
 *   }
 * @apiErrorExample Error-Response:
 *     Unauthorized
 *     {
 *       "isToken": true,
 *        "success": false,
 *        "message": "Authentication failed"
 *     }
 */
userInfoRouter.get('/admission-info',tokenAuthMiddleware.authenticate,roleAuthMiddleware.authorize,user.retrieve,mw.respond,mw.error);

/**
 * @api {put} /api/admission-info/:id update the information of the existing user
 * @apiName update
 * @apiGroup UserInfo
 *
 * @apiParam {String} firstName Mandatory first name of the user
 * @apiParam {String} [middleName] Optional middle name of the user
 * @apiParam {String} lastName Mandatory last name of the user
 * @apiParam {String} dateOfBirth Mandatory date of birth of the user
 * @apiParam {enum} nationality Mandatory nationality of the user
 * @apiParam {enum} gender Mandatory gender of the user
 * @apiParam {enum} maritalStatus Mandatory marital status of the user
 * @apiParam {enum} religion Mandatory religion of the user
 * @apiParam {enum} country Mandatory country of the user
 * @apiParam {String} [district] Optional district of the user
 * @apiParam {String} [municipalityVdc] Optional municipality or vdc of the user
 * @apiParam {String} [wardNo] Optional ward number of the user
 * @apiParam {String} email Mandatory email of the user
 * @apiParam {Array} qualification Optional qualification information of the user
 * @apiParam {Array} parent Optional parent information of the user
 *
 *
 * @apiDescription updates user's information in the database
 * @apiHeader (AuthorizationHeader) {String} Authorization
 * @apiHeaderExample {json} Header-example:
 * {
 *      "Authorization": "yJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c"
 * }
 * @apiExample {curl} Example usage:
 *
 *
 *  curl \
 * -v \
 * -X POST  \
 * http://localhost:3000/api/admission-info/:id \
 * -H "Content-Type: application/json"  \
 * -H "Authorization: eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c2JlYXQuY29tIiwidXNlcm5hbWUiOiJzdXBlcmFkbWluIiwiX2lkIjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwidXNlckNvbmZpcm1lZCI6ZmFsc2UsImJsb2NrZWQiOmZhbHNlLCJkZWxldGVkIjpmYWxzZSwiYWRkZWRPbiI6IjIwMTYtMDctMDhUMDc6NTQ6MDMuNzY2WiIsInR3b0ZhY3RvckF1dGhFbmFibGVkIjpmYWxzZSwidXNlclJvbGUiOiJhZG1pbiIsImFjdGl2ZSI6dHJ1ZX0sImNsYWltcyI6eyJzdWJqZWN0IjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwiaXNzdWVyIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIiwicGVybWlzc2lvbnMiOlsic2F2ZSIsInVwZGF0ZSIsInJlYWQiLCJkZWxldGUiXX0sImlhdCI6MTQ2OTE4MzQ0MCwiZXhwIjoxNDY5MjAzNDQwLCJpc3MiOiI1NzdmNWMxYjU4Njk5MDJkNjdlYjIyYTgifQ.KSp3_S2LEhixyzTnNJE9AtD5u7-8ljImr-Np0kzxkoNWWjqB66a_T67NTCTMoMZys7PIYPFNBR9VwLOhkrX7BQ" \
 * @apiSuccess {String} Message User's Information updated successfully
 * @apiSuccessExample Success-response:
 * {
 *   "Success": 1,
 *   "Message": "User's Information updated successfully",
 *   "data": {
 *       "_id": "596da247619e6000269a8ecd",
 *       "firstName": "Sajan",
 *       "middleName": "",
 *       "lastName": "Awale",
 *       "dateOfBirth": "1995-05-09T00:00:00.000Z",
 *       "nationality": "Nepalese",
 *       "gender": "Male",
 *       "maritalStatus": "Single",
 *       "religion": "Hinduism",
 *       "country": "Nepal",
 *       "district": "Lalitpur",
 *       "municipalityVdc": "LMC",
 *       "wardNo": "09",
 *       "email": "sanjeevmagar49@gmail.com",
 *       "__v": 1,
 *       "isDeleted": false,
 *       "parent": [
 *           {
 *               "parentFullName": "Dev Maharjan",
 *               "parentContactNo": "9841780434",
 *               "parentContactAddress": "Old Baneshwor",
 *               "parentProfession": "Scaffolding",
 *               "parentEmail": "manumagar28@gmail.com",
 *               "_id": "596da247619e6000269a8ecf"
 *           }
 *       ],
 *       "qualification": [
 *           {
 *               "schoolBoard": "SEE",
 *               "schoolCourse": "SEE",
 *               "schoolGrade": "Distinction",
 *               "intermediateBoard": "HSEB",
 *               "intermediateCourse": "High School",
 *               "intermediateGrade": "First Division",
 *               "bachelorBoard": "University",
 *               "bachelorCourse": "B.Sc.IT",
 *               "bachelorGrade": "Pass",
 *               "_id": "596da247619e6000269a8ece"
 *           }
 *       ]
 *   }
 *   }
 * @apiErrorExample Error-Response:
 *     Unauthorized
 *     {
 *       "isToken": true,
 *        "success": false,
 *        "message": "Authentication failed"
 *     }
 */
userInfoRouter.put('/admission-info/:id',tokenAuthMiddleware.authenticate,roleAuthMiddleware.authorize,user.collectToRegister,user.update,mw.respond,mw.error);

/**
 * @api {put} /api/admission-info/:id delete the information of the existing user
 * @apiName delete
 * @apiGroup UserInfo
 *
 * @apiDescription deletes user's information from the database
 * @apiHeader (AuthorizationHeader) {String} Authorization
 * @apiHeaderExample {json} Header-example:
 * {
 *      "Authorization": "yJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c"
 * }
 * @apiExample {curl} Example usage:
 *
 *
 *  curl \
 * -v \
 * -X POST  \
 * http://localhost:3000/api/admission-info/:id \
 * -H "Content-Type: application/x-www-form-urlencoded"  \
 * -H "Authorization: eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c2JlYXQuY29tIiwidXNlcm5hbWUiOiJzdXBlcmFkbWluIiwiX2lkIjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwidXNlckNvbmZpcm1lZCI6ZmFsc2UsImJsb2NrZWQiOmZhbHNlLCJkZWxldGVkIjpmYWxzZSwiYWRkZWRPbiI6IjIwMTYtMDctMDhUMDc6NTQ6MDMuNzY2WiIsInR3b0ZhY3RvckF1dGhFbmFibGVkIjpmYWxzZSwidXNlclJvbGUiOiJhZG1pbiIsImFjdGl2ZSI6dHJ1ZX0sImNsYWltcyI6eyJzdWJqZWN0IjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwiaXNzdWVyIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIiwicGVybWlzc2lvbnMiOlsic2F2ZSIsInVwZGF0ZSIsInJlYWQiLCJkZWxldGUiXX0sImlhdCI6MTQ2OTE4MzQ0MCwiZXhwIjoxNDY5MjAzNDQwLCJpc3MiOiI1NzdmNWMxYjU4Njk5MDJkNjdlYjIyYTgifQ.KSp3_S2LEhixyzTnNJE9AtD5u7-8ljImr-Np0kzxkoNWWjqB66a_T67NTCTMoMZys7PIYPFNBR9VwLOhkrX7BQ" \
 * @apiSuccess {String} Message User's Information updated successfully
 * @apiSuccessExample Success-response:
 * {
 *   "Success": 1,
 *   "Messsage": "User's information deleted successfully"
 * }
 * @apiErrorExample Error-Response:
 *     Unauthorized
 *     {
 *       "isToken": true,
 *        "success": false,
 *        "message": "Authentication failed"
 *     }
 */
userInfoRouter.patch('/admission-info/:id',tokenAuthMiddleware.authenticate,roleAuthMiddleware.authorize,user.delete,mw.respond,mw.error);

/**
 * @api {get} /api/admission-info/:id get the particular user's information
 * @apiName find_id
 * @apiGroup UserInfo
 *
 * @apiDescription Retrieves the information of the particular user, if exists, else return empty array.
 * @apiExample {curl} Example usage:
 * curl \
 * -G \
 * -v \
 * "http://localhost:3000/api/admission-info/:id" \
 *
 * @apiSuccess {String} _id object id of user info.
 * @apiSuccess {String} firstName first name of the user.
 * @apiSuccess {String} middleName middle name of the user.
 * @apiSuccess {String} lastName last name of the user.
 * @apiSuccess {String} dateOfBirth date of birth of the user.
 * @apiSuccess {enum} nationality nationality of the user.
 * @apiSuccess {enum} gender gender of the user.
 * @apiSuccess {enum} maritalStatus marital status of the user.
 * @apiSuccess {enum} religion religion of the user.
 * @apiSuccess {enum} country country of the user.
 * @apiSuccess {String} district district of the user.
 * @apiSuccess {String} municipalityVdc municipality or vdc of the user.
 * @apiSuccess {String} wardNo ward number of the place where the user resides.
 * @apiSuccess {String} email email of the user.
 * @apiSuccess {Array} qualification information about the qualification of the user.
 * @apiSuccess {Array} parent parent's information of the user.
 *
 * @apiSuccessExample Success-Response:
 *   {
 *   "result": [
 *       {
 *           "_id": "596c58a625d9e2014c67c755",
 *           "firstName": "Mahesh",
 *           "middleName": "",
 *           "lastName": "Magar",
 *           "dateOfBirth": "1995-05-06T00:00:00.000Z",
 *           "nationality": "Nepalese",
 *           "gender": "Male",
 *           "maritalStatus": "Married",
 *           "religion": "Buddhism",
 *           "country": "Nepal",
 *           "district": "Pokhara",
 *           "municipalityVdc": "KMC",
 *           "wardNo": "34",
 *           "email": "sanjeevmagar49@gmail.com",
 *           "__v": 3,
 *           "isDeleted": false,
 *           "parent": [
 *               {
 *                   "parentFullName": "Madan Bdr Khapangi Magar",
 *                   "parentContactNo": "9841780434",
 *                   "parentContactAddress": "Katyani Chowk",
 *                   "parentProfession": "Scaffolding",
 *                   "parentEmail": "manumagar28@gmail.com",
 *                   "_id": "596dab181ec2d8005196de88"
 *               },
 *               {
 *                   "parentFullName": "Maya Devi Khapangi Magar",
 *                   "parentContactNo": "9808858267",
 *                   "parentContactAddress": "Bhimsengola",
 *                   "parentProfession": "Housewife",
 *                   "parentEmail": "manumagar28@gmail.com",
 *                   "_id": "596dab181ec2d8005196de87"
 *               }
 *           ],
 *           "qualification": [
 *               {
 *                   "schoolBoard": "See",
 *                   "schoolCourse": "See",
 *                   "schoolGrade": "Distinction",
 *                   "intermediateBoard": "HSEB",
 *                   "intermediateCourse": "High School",
 *                   "intermediateGrade": "First Division",
 *                   "bachelorBoard": "University",
 *                   "bachelorCourse": "B.Sc.IT",
 *                   "bachelorGrade": "Pass",
 *                   "_id": "596dab181ec2d8005196de86"
 *               }
 *           ]
 *       }
 *   }
 *
 * @apiError (Not Found) {String} null
 * @apiErrorExample Error-response:
 *      {
 *          "result": null
 *      }
 * @apiError  (InternalServerError) {String} message  Internal Server Error
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "message": "Internal Server Error"
 *     }
 * @apiErrorExample Error-Response:
 *     Unauthorized
 *     {
 *       "isToken": true,
 *        "success": false,
 *        "message": "Authentication failed"
 *     }
 */
userInfoRouter.get('/admission-info/:id',tokenAuthMiddleware.authenticate,roleAuthMiddleware.authorize,user.find_id,mw.respond,mw.error);

/**
 * @api {put} /api/admission-info-admin/:id update admissionStatus and remarks of the application submitted
 * @apiName adminUpdate
 * @apiGroup UserInfo
 *
 * @apiParam {String} [admissionStatus] admission status of the user
 * @apiParam {String} [remarks] remarks of the user
 *
 * @apiDescription updates user's admissionStatus and remarks in the database
 * @apiHeader (AuthorizationHeader) {String} Authorization
 * @apiHeaderExample {json} Header-example:
 * {
 *      "Authorization": "yJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c"
 * }
 * @apiExample {curl} Example usage:
 *
 *
 *  curl \
 * -v \
 * -X POST  \
 * http://localhost:3000/api/admission-info-admin/:id \
 * -H "Content-Type: application/json"  \
 * -H "Authorization: eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c2JlYXQuY29tIiwidXNlcm5hbWUiOiJzdXBlcmFkbWluIiwiX2lkIjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwidXNlckNvbmZpcm1lZCI6ZmFsc2UsImJsb2NrZWQiOmZhbHNlLCJkZWxldGVkIjpmYWxzZSwiYWRkZWRPbiI6IjIwMTYtMDctMDhUMDc6NTQ6MDMuNzY2WiIsInR3b0ZhY3RvckF1dGhFbmFibGVkIjpmYWxzZSwidXNlclJvbGUiOiJhZG1pbiIsImFjdGl2ZSI6dHJ1ZX0sImNsYWltcyI6eyJzdWJqZWN0IjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwiaXNzdWVyIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIiwicGVybWlzc2lvbnMiOlsic2F2ZSIsInVwZGF0ZSIsInJlYWQiLCJkZWxldGUiXX0sImlhdCI6MTQ2OTE4MzQ0MCwiZXhwIjoxNDY5MjAzNDQwLCJpc3MiOiI1NzdmNWMxYjU4Njk5MDJkNjdlYjIyYTgifQ.KSp3_S2LEhixyzTnNJE9AtD5u7-8ljImr-Np0kzxkoNWWjqB66a_T67NTCTMoMZys7PIYPFNBR9VwLOhkrX7BQ" \
 * @apiSuccess {String} Message User's Information updated successfully
 * @apiSuccessExample Success-response:
 * {
 *   "Success": 1,
 *   "Message": "User's Information updated successfully",
 *   "data": {
 *       "_id": "596da247619e6000269a8ecd",
 *       "firstName": "Sajan",
 *       "middleName": "",
 *       "lastName": "Awale",
 *       "dateOfBirth": "1995-05-09T00:00:00.000Z",
 *       "nationality": "Nepalese",
 *       "gender": "Male",
 *       "maritalStatus": "Single",
 *       "religion": "Hinduism",
 *       "country": "Nepal",
 *       "district": "Lalitpur",
 *       "municipalityVdc": "LMC",
 *       "wardNo": "09",
 *       "email": "sanjeevmagar49@gmail.com",
 *       "admissionStatus": "Verified",
 *       "remarks": "Very good",
 *       "__v": 1,
 *       "isDeleted": false,
 *       "parent": [
 *           {
 *               "parentFullName": "Dev Maharjan",
 *               "parentContactNo": "9841780434",
 *               "parentContactAddress": "Old Baneshwor",
 *               "parentProfession": "Scaffolding",
 *               "parentEmail": "manumagar28@gmail.com",
 *               "_id": "596da247619e6000269a8ecf"
 *           }
 *       ],
 *       "qualification": [
 *           {
 *               "schoolBoard": "SEE",
 *               "schoolCourse": "SEE",
 *               "schoolGrade": "Distinction",
 *               "intermediateBoard": "HSEB",
 *               "intermediateCourse": "High School",
 *               "intermediateGrade": "First Division",
 *               "bachelorBoard": "University",
 *               "bachelorCourse": "B.Sc.IT",
 *               "bachelorGrade": "Pass",
 *               "_id": "596da247619e6000269a8ece"
 *           }
 *       ]
 *   }
 *   }
 * @apiErrorExample Error-Response:
 *     Unauthorized
 *     {
 *       "isToken": true,
 *        "success": false,
 *        "message": "Authentication failed"
 *     }
 */
userInfoRouter.put('/admission-info-admin/:id', tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, user.adminUpdate, mw.respond, mw.error);


module.exports = userInfoRouter;
