let noticeRoutes = (() => {

    'use strict';

    const HTTPStatus            =   require('http-status'),
          express               =   require('express'),
          tokenAuthMiddleware   =   require('../middlewares/token.authentication.middleware'),
          roleAuthMiddleware    =   require('../middlewares/role.authorization.middleware'),
          noticeController      =   require('../controllers/notice.controller'),
          messageConfig         =   require('../configs/api.message.config'),
          noticeRouter          =   express.Router();

    noticeRouter.route('/quote')

        .get(noticeController.getCacheQuote);
    
    noticeRouter.route('/notice/')

        /**
         * @api {get} /api/notice/ Get all notice list
         * @apiPermission public
         * @apiName getAllNotice
         * @apiGroup Notice
         *
         *
         * @apiParam {Int} totalItems total number of data
         * @apiParam {Int} perPage Number of data to return on each request
         * @apiParam {Int} currentPage Current page number of pagination system.*
         *
         *@apiParamExample {json} Request-Example:
         *     {
         *       "totalItems": 1,
         *       "currentPage": 1,
         *       "perPage": 10
         *     }
         *
         * @apiDescription Retrieves the notice list, if exists, else return empty array
         * @apiVersion 0.0.1
         *
         * @apiExample {curl} Example usage:
         * curl \
         * -G \
         * -v \
         * "http://localhost:3000/api/notice/" \
         * --data-urlencode "totalItems=1" \
         * --data-urlencode "perPage=10" \
         * --data-urlencode "currentPage=1"
         *
         * @apiExample {curl} Example usage:
         * curl \
         * -G \
         * -v \
         * "http://localhost:3000/api/notice/" \
         * --data-urlencode "active=true"
         *
         *
         * @apiSuccess {Array} dataList list of notice
         * @apiSuccess {String} dataList._id  object id of document category data.
         * @apiSuccess {String} dataList.noticeTitle  title of the notice.
         * @apiSuccess {String} dataList.noticeDescription  description about notice.
         * @apiSuccess {String} dataList.urlSlog  clean URL of notice.
         * @apiSuccess {String} dataList.addedOn  date on which notice is added.
         * @apiSuccess {Boolean} dataList.active  active bit status.
         * @apiSuccess {Int} totalItems  total no of notice in the related collection in database.
         * @apiSuccess {Int} currentPage  current page number of client pagination system.
         * @apiSuccess {Int} perPage per page limitation of data.
         *
         * @apiSuccessExample Success-Response:
         *     HTTP/1.1 200 OK
         *     {
         *      "dataList": [
         *           {
         *                  "_id": "598d3d696c0fa11680e3e14e",
         *                  "noticeDescription": "All of the students have to submit their assignment by 31st August.",
         *                  "noticeTitle": "Submission",
         *                  "addedOn": "2017-08-11T05:15:21.445Z",
         *                  "active": true
         *           }
         *          ],
         *              "totalItems": 1,
         *              "currentPage": 1,
         *              "perPage": 10
         *     }
         *
         * @apiError  (NotFound) {String} message  Notice not found
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 404 Not Found
         *     {
                   *       "message": "Notice not found"
                   *     }
         *
         * @apiError  (InternalServerError) {String} message  Internal Server Error
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 500 Internal Server Error
         *     {
                   *       "message": "Internal Server Error"
                   *     }
         *
         */
        .get(getAllNotice)

        /**
         * @api {post} /api/notice/ Post notice details
         * @apiPermission admin
         * @apiName postNotice
         * @apiGroup Notice
         *
         * @apiParam {String} noticeTitle  title of notice.
         *
         * @apiDescription saves the notice information to the database
         * @apiVersion 0.0.1
         * @apiHeader (AuthorizationHeader) {String} authorization x-access-token value.
         * @apiHeaderExample {json} Header-Example:
         *{
           *  "x-access-token": "yJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c"
           * }
         *
         * @apiExample {curl} Example usage:
         *
         *
         * curl \
         * -v \
         * -X POST  \
         * http://localhost:3000/api/notice/ \
         * -H 'Content-Type: application/json' \
         * -H "x-access-token: eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c2JlYXQuY29tIiwidXNlcm5hbWUiOiJzdXBlcmFkbWluIiwiX2lkIjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwidXNlckNvbmZpcm1lZCI6ZmFsc2UsImJsb2NrZWQiOmZhbHNlLCJkZWxldGVkIjpmYWxzZSwiYWRkZWRPbiI6IjIwMTYtMDctMDhUMDc6NTQ6MDMuNzY2WiIsInR3b0ZhY3RvckF1dGhFbmFibGVkIjpmYWxzZSwidXNlclJvbGUiOiJhZG1pbiIsImFjdGl2ZSI6dHJ1ZX0sImNsYWltcyI6eyJzdWJqZWN0IjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwiaXNzdWVyIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIiwicGVybWlzc2lvbnMiOlsic2F2ZSIsInVwZGF0ZSIsInJlYWQiLCJkZWxldGUiXX0sImlhdCI6MTQ2OTAwMDE4NCwiZXhwIjoxNDY5MDIwMTg0LCJpc3MiOiI1NzdmNWMxYjU4Njk5MDJkNjdlYjIyYTgifQ.Npy3LZnsOqOqH7RBfl3oYAdVOYdlU3_6i8izGo7xkHzI610NjnCaljiMxb7s71RJsRoqVqNqB-gai8vzWlofrQ" \
         * -d '{"noticeTitle": "Submission", "categoryDescription": "submission deadline on 31st August","active": true}'
         *
         * @apiSuccess {String} message Notice saved successfully.
         *
         * @apiSuccessExample Success-Response:
         *     HTTP/1.1 200 OK
         *     {
         *         "message": "Notice saved successfully"
         *     }
         *
         * @apiError (UnAuthorizedError) {String} message authentication token was not supplied
         * @apiError (UnAuthorizedError) {Boolean} isToken to check if token is supplied or not , if token is supplied , returns true else returns false
         * @apiError (UnAuthorizedError) {Boolean} success true if jwt token verifies
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 401 Unauthorized
         *     {
           *       "isToken": true,
           *        "success": false,
           *        "message": "Authentication failed"
           *     }
         *
         *
         * @apiError  (UnAuthorizedError_1) {String} message  You are not authorized to access this api route.
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 401 Unauthorized
         *     {
           *       "message": "You are not authorized to access this api route."
           *     }
         *
         * @apiError  (UnAuthorizedError_2) {String} message  You are not authorized to perform this action.
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 401 Unauthorized
         *     {
         *       "message": "You are not authorized to perform this action"
         *     }
         *
         *
         *
         * @apiError  (BadRequest) {String} message Notice title is required
         * @apiError  (BadRequest) {String} message Notice description is required
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 400 Bad Request
         *
         *     {
         *       "message": "Notice title is required",
         *       "message": "Notice description is required"
         *     }
         *
         *
         *
         * @apiError  (AlreadyExists)  {String} message  Notice with same title already exists
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 409 Conflict
         *     {
         *       "message": "Notice with same title already exists"
         *     }
         *
         *
         *
         * @apiError (InternalServerError)  {String} message  Internal Server Error
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 500 Internal Server Error
         *     {
           *       "message": "Internal Server Error"
           *     }
         *
         */
        .post(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, noticeController.postNotice);

    noticeRouter.route('/notice-admin')


        .get(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, getNoticeByAdmin);

    noticeRouter.route('/notice-list/latest-week')

    /**
     * @api {get} /api/notice-list/latest-week Get all notice list of the latest week
     * @apiPermission public
     * @apiName getNoticeByLatestWeek
     * @apiGroup Notice
     *
     * @apiDescription Retrieves the notice list of the latest week, if exists, else return empty array
     * @apiVersion 0.0.1
     *
     * @apiExample {curl} Example usage:
     * curl \
     * -G \
     * -v \
     * "http://localhost:3000/api/notice-list/latest-week" \
     *
     * @apiExample {curl} Example usage:
     * curl \
     * -G \
     * -v \
     * "http://localhost:3000/api/notice-list/latest-week" \
     * --data-urlencode "active=true"
     *
     *
     * @apiSuccess {Array} list of notice
     * @apiSuccess {String} dataList._id  object id of document category data.
     * @apiSuccess {String} dataList.noticeTitle  title of the notice.
     * @apiSuccess {String} dataList.noticeDescription  description about notice.
     * @apiSuccess {String} dataList.urlSlog  clean URL of notice.
     * @apiSuccess {String} dataList.addedOn  date on which notice is added.
     * @apiSuccess {Boolean} dataList.active  active bit status.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *           {
     *                  "_id": "598d3d696c0fa11680e3e14e",
     *                  "noticeDescription": "All of the students have to submit their assignment by 31st August.",
     *                  "noticeTitle": "Submission",
     *                  "addedOn": "2017-08-11T05:15:21.445Z",
     *                  "active": true
     *           }
     *     }
     *
     * @apiError  (NotFound) {String} message  Notice not found
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
                   *       "message": "Notice not found"
                   *     }
     *
     * @apiError  (InternalServerError) {String} message  Internal Server Error
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
                   *       "message": "Internal Server Error"
                   *     }
     *
     */
        .get(noticeController.getNoticeByLatestWeek);

    noticeRouter.use('/notice/:noticeId', (req, res, next) => {
        noticeController.getNoticeById(req)
            .then((noticeInfo) => {
                if (noticeInfo) {
                    req.noticeInfo = noticeInfo;
                    next();
                    return null;
                }
                else {
                    res.status(HTTPStatus.NOT_FOUND);
                    res.json({
                        message: messageConfig.notice.notFound
                    });
                }
            })
            .catch((err) => {
                return next(err);
            })
    });

    noticeRouter.route('/notice/:noticeId')

    /**
     * @api {get} /api/notice/:noticeId Get notice details according to the id.
     * @apiPermission public
     * @apiName getNoticeById
     * @apiGroup Notice
     *
     * @apiDescription Retrieves the notice detail according to the id
     * @apiVersion 0.0.1
     *
     * @apiExample {curl} Example usage:
     * curl \
     * -G \
     * -v \
     * "http://localhost:3000/api/notice/:noticeId" \
     *
     * @apiExample {curl} Example usage:
     * curl \
     * -G \
     * -v \
     * "http://localhost:3000/api/notice/:noticeId" \
     * --data-urlencode "active=true"
     *
     *
     * @apiSuccess {String} dataList._id  object id of document category data.
     * @apiSuccess {String} dataList.noticeTitle  title of the notice.
     * @apiSuccess {String} dataList.noticeDescription  description about notice.
     * @apiSuccess {String} dataList.urlSlog  clean URL of notice.
     * @apiSuccess {String} dataList.addedOn  date on which notice is added.
     * @apiSuccess {Boolean} dataList.active  active bit status.
     * @apiSuccess {Int} totalItems  total no of notice in the related collection in database.
     * @apiSuccess {Int} currentPage  current page number of client pagination system.
     * @apiSuccess {Int} perPage per page limitation of data.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *           {
     *                  "_id": "598d3d696c0fa11680e3e14e",
     *                  "noticeDescription": "All of the students have to submit their assignment by 31st August.",
     *                  "noticeTitle": "Submission",
     *                  "addedOn": "2017-08-11T05:15:21.445Z",
     *                  "active": true
     *           }
     *
     * @apiError  (NotFound) {String} message  Notice not found
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
                   *       "message": "Notice not found"
                   *     }
     *
     * @apiError  (InternalServerError) {String} message  Internal Server Error
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
                   *       "message": "Internal Server Error"
                   *     }
     *
     */
        .get((req, res) => {
            res.status(HTTPStatus.OK);
            res.json(req.noticeInfo);
        })

        /**
         * @api {get} /api/notice/:noticeId updates the notice details according to the id
         * @apiPermission public
         * @apiName updateNoticeById
         * @apiGroup Notice
         *
         *
         * @apiDescription updates the details of the notice through the id requested
         * @apiVersion 0.0.1
         *
         * @apiExample {curl} Example usage:
         * curl \
         * -G \
         * -v \
         * "http://localhost:3000/api/notice/:noticeId" \
         *
         * @apiExample {curl} Example usage:
         * curl \
         * -G \
         * -v \
         * "http://localhost:3000/api/notice/:noticeId" \
         * --data-urlencode "active=true"
         *
         *
         * @apiSuccess {String} dataList.noticeTitle  title of the notice.
         * @apiSuccess {String} dataList.noticeDescription  description about notice.
         * @apiSuccess {Boolean} dataList.active  active bit status.
         *
         * @apiSuccessExample Success-Response:
         *     HTTP/1.1 200 OK
         *         {
         *              message: "Notice updated successfully"
         *         }
         *
         * @apiError  (NotFound) {String} message  Notice not found
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 404 Not Found
         *     {
                   *       "message": "Notice not found"
                   *     }
         *
         * @apiError  (InternalServerError) {String} message  Internal Server Error
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 500 Internal Server Error
         *     {
                   *       "message": "Internal Server Error"
                   *     }
         *
         */
        .put(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, noticeController.updateNoticeById)

        /**
         * @api {get} /api/notice/:noticeId deletes the notice
         * @apiPermission public
         * @apiName deleteNoticeById
         * @apiGroup Notice
         *
         *
         * @apiDescription deletes the notice as per the id requested
         * @apiVersion 0.0.1
         *
         * @apiExample {curl} Example usage:
         * curl \
         * -G \
         * -v \
         * "http://localhost:3000/api/notice/:noticeId" \
         *
         * @apiExample {curl} Example usage:
         * curl \
         * -G \
         * -v \
         * "http://localhost:3000/api/notice/:noticeId" \
         * --data-urlencode "active=true"
         *
         * @apiSuccessExample Success-Response:
         *     HTTP/1.1 200 OK
         *           {
         *              message: "Notice deleted successfully"
         *           }
         *
         * @apiError  (NotFound) {String} message  Notice not found
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 404 Not Found
         *     {
                   *       "message": "Notice not found"
                   *     }
         *
         * @apiError  (InternalServerError) {String} message  Internal Server Error
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 500 Internal Server Error
         *     {
                   *       "message": "Internal Server Error"
                   *     }
         *
         */
        .patch(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, noticeController.deleteNoticeById);

    noticeRouter.use('/notice-detail/:titleSlog', (req, res, next) => {
        noticeController.getNoticeByTitleSlog(req)
            .then((noticeDetail) => {
                if (noticeDetail) {
                    req.noticeDetail = noticeDetail
                    next();
                }
                else {
                    res.status(HTTPStatus.NOT_FOUND);
                    res.json({
                        message: messageConfig.notice.notFound
                    });
                }
            })
            .catch((err) => {
                return next(err)
            })
    });

    noticeRouter.route('/notice-detail/:titleSlog')

    /**
     * @api {get} /api/notice-detail/:titleSlog Get notice details according to the urlSlog
     * @apiPermission public
     * @apiName getNoticeByTitleSlog
     * @apiGroup Notice
     *
     * @apiDescription Retrieves the notice details according to the urlSlog
     * @apiVersion 0.0.1
     *
     * @apiExample {curl} Example usage:
     * curl \
     * -G \
     * -v \
     * "http://localhost:3000/api/notice-detail/:titleSlog" \
     *
     * @apiExample {curl} Example usage:
     * curl \
     * -G \
     * -v \
     * "http://localhost:3000/api/notice-detail/:titleSlog" \
     * --data-urlencode "active=true"
     *
     *
     * @apiSuccess {String} dataList.noticeTitle  title of the notice.
     * @apiSuccess {String} dataList.noticeDescription  description about notice.
     * @apiSuccess {String} dataList.urlSlog  clean URL of notice.
     * @apiSuccess {String} dataList.addedOn  date on which notice is added.
     * @apiSuccess {Boolean} dataList.active  active bit status.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *           {
     *                  "_id": "598d3d696c0fa11680e3e14e",
     *                  "noticeDescription": "All of the students have to submit their assignment by 31st August.",
     *                  "noticeTitle": "Submission",
     *                  "addedOn": "2017-08-11T05:15:21.445Z",
     *                  "active": true
     *           }
     *
     * @apiError  (NotFound) {String} message  Notice not found
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "message": "Notice not found"
     *     }
     *
     * @apiError  (InternalServerError) {String} message  Internal Server Error
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *       "message": "Internal Server Error"
     *     }
     *
     */
        .get((req, res) => {
            res.status(HTTPStatus.OK);
            res.json(req.noticeDetail);
        });

    function getAllNotice(req, res, next) {
        noticeController.getAllNotice(req)
            .then((noticeList) => {
            if (noticeList) {
                res.status(HTTPStatus.OK);
                res.json(noticeList);
            }
            else {
                res.status(HTTPStatus.NOT_FOUND);
                res.json({
                    message: messageConfig.notice.notFound
                });
            }
            })
            .catch((err) => {
            return next(err);
            })
    }

    function getNoticeByAdmin(req, res, next) {
        noticeController.getNoticeByAdmin(req)
            .then((noticeList) => {
            if (noticeList) {
                res.status(HTTPStatus.OK);
                res.json(noticeList);
            }
            else {
                res.status(HTTPStatus.NOT_FOUND);
                res.json({
                    message: messageConfig.notice.notFound
                });
            }
            })
            .catch((err) => {
            return next(err);
            })
    }

    return noticeRouter;
})();

module.exports = noticeRoutes;