var eventVenueRoute = (function () {

    'use strict';

    var HTTPStatus = require('http-status'),
        express = require('express'),
        messageConfig = require('../configs/api.message.config'),
        tokenAuthMiddleware = require('../middlewares/token.authentication.middleware'),
        roleAuthMiddleware = require('../middlewares/role.authorization.middleware'),
        eventManagementController = require('../controllers/event.management.server.controller'),
        eventVenueRouter = express.Router();



        eventVenueRouter.route('/')

        .get(getAllVenues)

    function getAllVenues(req, res, next){
        eventManagementController.getEventVenueList(req)
        .then(function(venueList){
            //saving in request object so that it can be used for other operations like updating data using put and patch method
            if (venueList) {
                res.status(HTTPStatus.OK);
                res.json(venueList);
            } else {
                res.status(HTTPStatus.NOT_FOUND);
                res.json({
                    message: messageConfig.eventManager.notFound
                });
            }
        })
        .catch(function(err){
            return next(err);
        });
    }

    return eventVenueRouter;

})();

module.exports = eventVenueRoute;