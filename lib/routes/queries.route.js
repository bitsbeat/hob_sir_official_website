var queryRoutes = (function() {
    'use strict';

    var HTTPStatus = require('http-status'),
        express = require('express'),
        messageConfig = require('../configs/api.message.config'),
        queriesController = require('../controllers/queries.server.controller'),
        responseMiddleware = require('../middlewares/response.middleware'),
        queriesRouter = express.Router(),
        tokenAuthMiddleware = require('../middlewares/token.authentication.middleware'),
        roleAuthMiddleware = require('../middlewares/role.authorization.middleware');

    queriesRouter.route('/')

        .get( tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, getAllQueries )

        .post( queriesController.collectToRegister, queriesController.postQuery, responseMiddleware.respond, responseMiddleware.error )

    queriesRouter.use('/:queryId', function(req, res, next){
        queriesController.getQueryByID(req)
            .then(function(queryInfo){
                if (queryInfo) {
                    req.queryInfo = queryInfo;
                    next();
                    return null;
                } else {
                    res.status(HTTPStatus.NOT_FOUND);
                    res.json({
                        message: messageConfig.roleManager.notFound
                    });
                }
            })
            .catch(function(err){
                return next(err);
            });
    });


    queriesRouter.route('/:queryId')

    .get(function (req, res) {
            res.status(HTTPStatus.OK);
            res.json(req.queryInfo);
    })

    .patch( tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, queriesController.patchQuery )


    
    //function declaration to return events list to the client if exists else return  empty array
    function getAllQueries(req, res, next) {
        queriesController.getAllQueries(req, next)
            .then(function(events){
                //if exists, return data in json format
                if (events) {
                    res.status(HTTPStatus.OK);
                    res.json(events);
                } else {
                    res.status(HTTPStatus.NOT_FOUND);
                    res.json({
                        message: messageConfig.eventManager.notFound
                    });
                }
            })
            .catch(function(err){
                return next(err);
            });
    }

    return queriesRouter;

})();

module.exports = queryRoutes;
