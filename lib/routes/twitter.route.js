
    'use strict';

    let express = require('express'),
        twitterRouter = express.Router(),
        twitterController = require('../controllers/twitter.controller');

twitterRouter.route('/twitter')

    .get(twitterController.getTweetFeeds)

    module.exports = twitterRouter;