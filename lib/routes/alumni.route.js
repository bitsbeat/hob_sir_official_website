let alumniRoutes = function () {

    'use strict';

    const HTTPStatus            = require('http-status'),
          express               = require('express'),
          alumniRouter          = express.Router(),
          tokenAuthMiddleware   = require('../middlewares/token.authentication.middleware'),
          roleAuthMiddleware    = require('../middlewares/role.authorization.middleware'),
          messageConfig         = require('../configs/api.message.config'),
          imageFilePath         = './public/uploads/image/alumni',
          uploadPrefix          = 'alumni',
          fileUploadHelper      = require('../helpers/file.upload.helper')(imageFilePath, '', uploadPrefix),
          uploader              = fileUploadHelper.uploader,
          alumniController      = require('../controllers/alumni.controller');

        alumniRouter.route('/alumni-category')

        /**
         * @api {get} /api/alumni-category Get list of alumni category
         * @apiPermission public
         * @apiName getAllAlumniCategory
         * @apiGroup AlumniCategory
         *
         * @apiParam {Int} totalItems Total number of data
         * @apiParam {Int} perPage Number of data to return on each request
         * @apiParam {Int} page Current page number of pagination system.
         *
         * @apiParamExample {json} Request-Example:
         *     {
         *       "totalItems": 5
         *       "perPage": 10,
         *       "page": 1
         *     }
         *
         * @apiDescription Retrieves the alumni-category list if exists, else return empty array
         * @apiVersion 0.0.1
         *
         * @apiExample {curl} Example usage:
         * curl \
         * -G \
         * -v \
         * "http://localhost:3000/api/alumni-category" \
         * --data-urlencode "perpage=10" \
         * --data-urlencode "page=1"
         *
         * @apiSuccess {Array}  dataList list of alumni-category
         * @apiSuccess {String} dataList._id  object id of alumni-category data.
         * @apiSuccess {String} dataList.year   year of the alumni.
         * @apiSuccess {Int} totalItems  total no of alumni-category in the event collection in database.
         * @apiSuccess {Int} currentPage  current page number of client pagination system.
         *
         * @apiSuccessExample Success-Response:
         *     HTTP/1.1 200 OK
         *      {
         *              "dataList": [
         *                          {
         *                              "_id": "59a3dbdeda90dd1d1f3bfb1d",
         *                              "urlSlogCategory": "2016",
         *                              "year": "2016",
         *                              "addedOn": "2017-08-28T09:01:18.425Z"
         *                          },
         *                          {
         *                              "_id": "59a3d806da90dd1d1f3bfb1c",
         *                              "urlSlogCategory": "2015",
         *                              "year": "2015",
         *                              "addedOn": "2017-08-28T08:44:54.254Z"
         *                          },
         *                          {
         *                              "_id": "59a3d782da90dd1d1f3bfb1b",
         *                              "urlSlogCategory": "2017",
         *                              "year": "2017",
         *                              "addedOn": "2017-08-28T08:42:42.976Z"
         *                          }
         *                          ],
         *              "totalItems": 3,
         *              "currentPage": 1,
         *              "perPage": 10
         *          }
         *
         * @apiError  (NotFound) {String} message  Alumni category not found
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 404 Not Found
         *     {
         *       "message": "Alumni category not found"
         *     }
         *
         * @apiError  (InternalServerError) {String} message  Internal Server Error
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 500 Internal Server Error
         *     {
         *       "message": "Internal Server Error"
         *     }
         *
         */
            .get(getAllAlumniCategories)

            /**
             * @api {post} /api/alumni-category Post Alumni category data
             * @apiPermission admin
             * @apiName postAlumniCategory
             * @apiGroup AlumniCategory
             *
             *
             * @apiParam {String} year Mandatory year of the alumni.
             *
             * @apiDescription saves alumni category information to the database
             * @apiVersion 0.0.1
             * @apiHeader (AuthorizationHeader) {String} authorization x-access-token value.
             * @apiHeaderExample {json} Header-Example:
             *{
             *  "x-access-token": "yJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c"
             * }
             *
             * @apiExample {curl} Example usage:
             *
             *
             * curl \
             * -v \
             * -X POST  \
             * http://localhost:3000/api/alumni-category/ \
             * -H 'Content-Type: application/json' \
             * -H "x-access-token: eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c2JlYXQuY29tIiwidXNlcm5hbWUiOiJzdXBlcmFkbWluIiwiX2lkIjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwidXNlckNvbmZpcm1lZCI6ZmFsc2UsImJsb2NrZWQiOmZhbHNlLCJkZWxldGVkIjpmYWxzZSwiYWRkZWRPbiI6IjIwMTYtMDctMDhUMDc6NTQ6MDMuNzY2WiIsInR3b0ZhY3RvckF1dGhFbmFibGVkIjpmYWxzZSwidXNlclJvbGUiOiJhZG1pbiIsImFjdGl2ZSI6dHJ1ZX0sImNsYWltcyI6eyJzdWJqZWN0IjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwiaXNzdWVyIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIiwicGVybWlzc2lvbnMiOlsic2F2ZSIsInVwZGF0ZSIsInJlYWQiLCJkZWxldGUiXX0sImlhdCI6MTQ2ODkyMTU1MiwiZXhwIjoxNDY4OTQxNTUyLCJpc3MiOiI1NzdmNWMxYjU4Njk5MDJkNjdlYjIyYTgifQ.wyt4E06UCFRkBpKD7ThHcWODCxGfO0j3Zv0k-cRTh0nYTHsKjVS4WgpefcLy-o_67aeSh42-wEqq0tBvxk603A" \
             * -d '{"year": 2017}'
             *
             *
             * @apiSuccess {String} message Alumni category saved successfully.
             *
             * @apiSuccessExample Success-Response:
             *     HTTP/1.1 200 OK
             *     {
             *         "message": "Alumni category saved successfully"
             *       }
             *
             * @apiError (UnAuthorizedError) {String} message authentication token was not supplied
             * @apiError (UnAuthorizedError) {Boolean} isToken to check if token is supplied or not , if token is supplied , returns true else returns false
             * @apiError (UnAuthorizedError) {Boolean} success true if jwt token verifies
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 401 Unauthorized
             *     {
             *       "isToken": true,
             *        "success": false,
             *        "message": "Authentication failed"
             *     }
             *
             *
             * @apiError  (UnAuthorizedError_1) {String} message  You are not authorized to access this api route.
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 401 Unauthorized
             *     {
             *       "message": "You are not authorized to access this api route."
             *     }
             *
             * @apiError  (UnAuthorizedError_2) {String} message  You are not authorized to perform this action.
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 401 Unauthorized
             *     {
             *       "message": "You are not authorized to perform this action"
             *     }
             *
             *
             *
             * @apiError  (BadRequest) {String} message year is required
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 400 Bad Request
             *
             *     {
             *       "message": "year is required"
             *     }
             *
             *
             *
             * @apiError  (AlreadyExists)  {String} message  Alumni category with same name already exists
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 409 Conflict
             *     {
             *       "message": "Category with same title already exists"
             *     }
             *
             *
             *
             * @apiError (InternalServerError)  {String} message  Internal Server Error
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 500 Internal Server Error
             *     {
             *       "message": "Internal Server Error"
             *     }
             *
             */
            .post(tokenAuthMiddleware.authenticate,roleAuthMiddleware.authorize, alumniController.postAlumniCategory);

        alumniRouter.route('/filter/alumni-category/:alumniCategory')

        /**
         * @api {get} /api/filter/alumni-category/:alumniCategory Get list of alumni as per the alumni category
         * @apiPermission public
         * @apiName getAllAlumniByCategoryTitle
         * @apiGroup Alumni
         *
         * @apiParam {Int} totalItems Total number of data
         * @apiParam {Int} perPage Number of data to return on each request
         * @apiParam {Int} page Current page number of pagination system.
         *
         * @apiParamExample {json} Request-Example:
         *     {
         *       "totalItems": 5
         *       "perPage": 10,
         *       "page": 1
         *     }
         *
         * @apiDescription Retrieves the alumni list as per the alumni category if exists, else return empty array
         * @apiVersion 0.0.1
         *
         * @apiExample {curl} Example usage:
         * curl \
         * -G \
         * -v \
         * "http://localhost:3000/api/filter/alumni-category/:alumniCategory" \
         * --data-urlencode "perpage=10" \
         * --data-urlencode "page=1"
         *
         * @apiSuccess {Array}  dataList list of alumni as per the category
         * @apiSuccess {String} dataList._id  object id of alumni data.
         * @apiSuccess {String} dataList.studentName   Name of the student.
         * @apiSuccess {String} dataList.batchYear   batch of the student.
         * @apiSuccess {String} dataList.organizationName   Name of the organization the student is currently working.
         * @apiSuccess {String} dataList.organizationPosition   Position of the student in the organization.
         * @apiSuccess {String} dataList.categoryId   Id of the category in which the alumni falls in.
         * @apiSuccess {String} dataList.imageName   image uploaded.
         * @apiSuccess {Int} totalItems  total no of alumni-category in the event collection in database.
         * @apiSuccess {Int} currentPage  current page number of client pagination system.
         *
         * @apiSuccessExample Success-Response:
         *     HTTP/1.1 200 OK
         *      {
         *          "dataList": [
         *                      {
         *                          "_id": "59a5074627545119b24204bd",
         *                          "imageName": "alumni-1503987526026.jpg",
         *                          "organizationPosition": "trainee",
         *                          "organizationName": "bitsbeat",
         *                          "batchYear": 2011,
         *                          "categoryId": "59a3d806da90dd1d1f3bfb1c",
         *                          "urlSlog": "sabita",
         *                          "studentName": "sabita",
         *                          "addedOn": "2017-08-29T06:18:46.443Z",
         *                          "imageProperties": {
         *                              "imageExtension": "jpg",
         *                              "imagePath": "/home/sabita/bitsbeat/DAV_MBA_School/public/uploads/image/alumni/alumni-1503987526026.jpg"
         *                      }
         *                      }
         *                      ],
         *           "totalItems": 3,
         *           "currentPage": 1,
         *           "perPage": 10
         *       }
         *
         * @apiError  (NotFound) {String} message  Alumni not found
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 404 Not Found
         *     {
         *       "message": "Alumni not found"
         *     }
         *
         * @apiError  (InternalServerError) {String} message  Internal Server Error
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 500 Internal Server Error
         *     {
         *       "message": "Internal Server Error"
         *     }
         *
         */
            .get(getAllAlumniByCategoryTitle);

        alumniRouter.use('/alumni-category/:alumniCategoryId', (req, res, next) => {
            alumniController.getAlumniCategoryById(req)
                .then((alumniCategory) => {
                if (alumniCategory) {
                    req.alumniCategory = alumniCategory;
                    next();
                    return null;
                }
                else {
                    res.status(HTTPStatus.NOT_FOUND);
                    res.json({
                        Message: messageConfig.alumni.notFoundAlumniCategory
                    });
                }
                })
                .catch((err) => {
                return next(err)
                })
        });

        alumniRouter.route('/alumni-category/:alumniCategoryId')

        /**
         * @api {get} /api/alumni-category/:alumniCategoryId Get particular alumni category as per the id.
         * @apiPermission public
         * @apiName getAlumniCategoryById
         * @apiGroup AlumniCategory
         *
         * @apiParamExample {json} Request-Example:
         *     {
         *       "totalItems": 5
         *       "perPage": 10,
         *       "page": 1
         *     }
         *
         * @apiDescription Retrieves the particular alumni-category as per the id.
         * @apiVersion 0.0.1
         *
         * @apiExample {curl} Example usage:
         * curl \
         * -G \
         * -v \
         * "http://localhost:3000/api/alumni-category/:alumniCategoryId" \
         * --data-urlencode "perpage=10" \
         * --data-urlencode "page=1"
         *
         * @apiSuccess {String} dataList._id  object id of alumni-category data.
         * @apiSuccess {String} dataList.year   year of the alumni.
         *
         * @apiSuccessExample Success-Response:
         *     HTTP/1.1 200 OK
         *
         *      {
         *            "_id": "59a3dbdeda90dd1d1f3bfb1d",
         *            "urlSlogCategory": "2016",
         *            "year": "2016",
         *            "addedOn": "2017-08-28T09:01:18.425Z"
         *      }
         *
         *
         * @apiError  (NotFound) {String} message  Alumni category not found
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 404 Not Found
         *     {
         *       "message": "Alumni category not found"
         *     }
         *
         * @apiError  (InternalServerError) {String} message  Internal Server Error
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 500 Internal Server Error
         *     {
         *       "message": "Internal Server Error"
         *     }
         *
         */
            .get((req, res) => {
            res.status(HTTPStatus.OK);
            res.json(req.alumniCategory);
            })

            /**
             * @api {post} /api/alumni-category/:alumniCategoryId Update Alumni category data
             * @apiPermission admin
             * @apiName updateAlumniCategory
             * @apiGroup AlumniCategory
             *
             *
             * @apiParam {String} year Mandatory year of the alumni.
             *
             * @apiDescription updates alumni category information to the database
             * @apiVersion 0.0.1
             * @apiHeader (AuthorizationHeader) {String} authorization x-access-token value.
             * @apiHeaderExample {json} Header-Example:
             *{
             *  "x-access-token": "yJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c"
             * }
             *
             * @apiExample {curl} Example usage:
             *
             *
             * curl \
             * -v \
             * -X POST  \
             * http://localhost:3000/api/alumni-category/:alumniCategoryId \
             * -H 'Content-Type: application/json' \
             * -H "x-access-token: eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c2JlYXQuY29tIiwidXNlcm5hbWUiOiJzdXBlcmFkbWluIiwiX2lkIjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwidXNlckNvbmZpcm1lZCI6ZmFsc2UsImJsb2NrZWQiOmZhbHNlLCJkZWxldGVkIjpmYWxzZSwiYWRkZWRPbiI6IjIwMTYtMDctMDhUMDc6NTQ6MDMuNzY2WiIsInR3b0ZhY3RvckF1dGhFbmFibGVkIjpmYWxzZSwidXNlclJvbGUiOiJhZG1pbiIsImFjdGl2ZSI6dHJ1ZX0sImNsYWltcyI6eyJzdWJqZWN0IjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwiaXNzdWVyIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIiwicGVybWlzc2lvbnMiOlsic2F2ZSIsInVwZGF0ZSIsInJlYWQiLCJkZWxldGUiXX0sImlhdCI6MTQ2ODkyMTU1MiwiZXhwIjoxNDY4OTQxNTUyLCJpc3MiOiI1NzdmNWMxYjU4Njk5MDJkNjdlYjIyYTgifQ.wyt4E06UCFRkBpKD7ThHcWODCxGfO0j3Zv0k-cRTh0nYTHsKjVS4WgpefcLy-o_67aeSh42-wEqq0tBvxk603A" \
             * -d '{"year": 2017}'
             *
             *
             * @apiSuccess {String} message Alumni category updated successfully.
             *
             * @apiSuccessExample Success-Response:
             *     HTTP/1.1 200 OK
             *     {
             *         "message": "Alumni category updated successfully"
             *       }
             *
             * @apiError (UnAuthorizedError) {String} message authentication token was not supplied
             * @apiError (UnAuthorizedError) {Boolean} isToken to check if token is supplied or not , if token is supplied , returns true else returns false
             * @apiError (UnAuthorizedError) {Boolean} success true if jwt token verifies
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 401 Unauthorized
             *     {
             *       "isToken": true,
             *        "success": false,
             *        "message": "Authentication failed"
             *     }
             *
             *
             * @apiError  (UnAuthorizedError_1) {String} message  You are not authorized to access this api route.
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 401 Unauthorized
             *     {
             *       "message": "You are not authorized to access this api route."
             *     }
             *
             * @apiError  (UnAuthorizedError_2) {String} message  You are not authorized to perform this action.
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 401 Unauthorized
             *     {
             *       "message": "You are not authorized to perform this action"
             *     }
             *
             *
             *
             * @apiError  (BadRequest) {String} message year is required
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 400 Bad Request
             *
             *     {
             *       "message": "year is required"
             *     }
             *
             *
             *
             * @apiError  (AlreadyExists)  {String} message  Alumni category with same name already exists
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 409 Conflict
             *     {
             *       "message": "Category with same title already exists"
             *     }
             *
             *
             *
             * @apiError (InternalServerError)  {String} message  Internal Server Error
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 500 Internal Server Error
             *     {
             *       "message": "Internal Server Error"
             *     }
             *
             */
            .put(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, alumniController.updateAlumniCategory)

            /**
             * @api {post} /api/alumni-category/:alumniCategoryId Delete Alumni category data
             * @apiPermission admin
             * @apiName deleteAlumniCategory
             * @apiGroup AlumniCategory
             *
             * @apiDescription deletes alumni category from the database
             * @apiVersion 0.0.1
             * @apiHeader (AuthorizationHeader) {String} authorization x-access-token value.
             * @apiHeaderExample {json} Header-Example:
             *{
             *  "x-access-token": "yJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c"
             * }
             *
             * @apiExample {curl} Example usage:
             *
             *
             * curl \
             * -v \
             * -X POST  \
             * http://localhost:3000/api/alumni-category/:alumniCategoryId \
             * -H 'Content-Type: application/json' \
             * -H "x-access-token: eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c2JlYXQuY29tIiwidXNlcm5hbWUiOiJzdXBlcmFkbWluIiwiX2lkIjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwidXNlckNvbmZpcm1lZCI6ZmFsc2UsImJsb2NrZWQiOmZhbHNlLCJkZWxldGVkIjpmYWxzZSwiYWRkZWRPbiI6IjIwMTYtMDctMDhUMDc6NTQ6MDMuNzY2WiIsInR3b0ZhY3RvckF1dGhFbmFibGVkIjpmYWxzZSwidXNlclJvbGUiOiJhZG1pbiIsImFjdGl2ZSI6dHJ1ZX0sImNsYWltcyI6eyJzdWJqZWN0IjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwiaXNzdWVyIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIiwicGVybWlzc2lvbnMiOlsic2F2ZSIsInVwZGF0ZSIsInJlYWQiLCJkZWxldGUiXX0sImlhdCI6MTQ2ODkyMTU1MiwiZXhwIjoxNDY4OTQxNTUyLCJpc3MiOiI1NzdmNWMxYjU4Njk5MDJkNjdlYjIyYTgifQ.wyt4E06UCFRkBpKD7ThHcWODCxGfO0j3Zv0k-cRTh0nYTHsKjVS4WgpefcLy-o_67aeSh42-wEqq0tBvxk603A" \
             * -d '{"year": 2017}'
             *
             *
             * @apiSuccess {String} message Alumni category deleted successfully.
             *
             * @apiSuccessExample Success-Response:
             *     HTTP/1.1 200 OK
             *     {
             *         "message": "Alumni category deleted successfully"
             *       }
             *
             * @apiError (UnAuthorizedError) {String} message authentication token was not supplied
             * @apiError (UnAuthorizedError) {Boolean} isToken to check if token is supplied or not , if token is supplied , returns true else returns false
             * @apiError (UnAuthorizedError) {Boolean} success true if jwt token verifies
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 401 Unauthorized
             *     {
             *       "isToken": true,
             *        "success": false,
             *        "message": "Authentication failed"
             *     }
             *
             *
             * @apiError  (UnAuthorizedError_1) {String} message  You are not authorized to access this api route.
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 401 Unauthorized
             *     {
             *       "message": "You are not authorized to access this api route."
             *     }
             *
             * @apiError  (UnAuthorizedError_2) {String} message  You are not authorized to perform this action.
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 401 Unauthorized
             *     {
             *       "message": "You are not authorized to perform this action"
             *     }
             *
             * @apiError (InternalServerError)  {String} message  Internal Server Error
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 500 Internal Server Error
             *     {
             *       "message": "Internal Server Error"
             *     }
             *
             */
            .patch(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, alumniController.deleteAlumniCategory);

        alumniRouter.route('/alumni')

        /**
         * @api {get} /api/alumni Get list of alumni
         * @apiPermission public
         * @apiName getAllAlumni
         * @apiGroup Alumni
         *
         * @apiParam {Int} totalItems Total number of data
         * @apiParam {Int} perPage Number of data to return on each request
         * @apiParam {Int} page Current page number of pagination system.
         *
         * @apiParamExample {json} Request-Example:
         *     {
         *       "totalItems": 5
         *       "perPage": 10,
         *       "page": 1
         *     }
         *
         * @apiDescription Retrieves the alumni list if exists, else return empty array
         * @apiVersion 0.0.1
         *
         * @apiExample {curl} Example usage:
         * curl \
         * -G \
         * -v \
         * "http://localhost:3000/api/alumni" \
         * --data-urlencode "perpage=10" \
         * --data-urlencode "page=1"
         *
         * @apiSuccess {Array}  dataList list of alumni
         * @apiSuccess {String} dataList._id  object id of alumni data.
         * @apiSuccess {String} dataList.studentName   Name of the student.
         * @apiSuccess {String} dataList.batchYear   batch of the student.
         * @apiSuccess {String} dataList.organizationName   Name of the organization the student is currently working.
         * @apiSuccess {String} dataList.organizationPosition   Position of the student in the organization.
         * @apiSuccess {String} dataList.categoryId   Id of the category in which the alumni falls in.
         * @apiSuccess {String} dataList.imageName   image uploaded.
         * @apiSuccess {Int} totalItems  total no of alumni-category in the event collection in database.
         * @apiSuccess {Int} currentPage  current page number of client pagination system.
         *
         * @apiSuccessExample Success-Response:
         *     HTTP/1.1 200 OK
         *      {
         *          "dataList": [
         *                      {
         *                          "_id": "59a5074627545119b24204bd",
         *                          "imageName": "alumni-1503987526026.jpg",
         *                          "organizationPosition": "trainee",
         *                          "organizationName": "bitsbeat",
         *                          "batchYear": 2011,
         *                          "categoryId": "59a3d806da90dd1d1f3bfb1c",
         *                          "urlSlog": "sabita",
         *                          "studentName": "sabita",
         *                          "addedOn": "2017-08-29T06:18:46.443Z",
         *                          "imageProperties": {
         *                              "imageExtension": "jpg",
         *                              "imagePath": "/home/sabita/bitsbeat/DAV_MBA_School/public/uploads/image/alumni/alumni-1503987526026.jpg"
         *                      }
         *                      }
         *                      ],
         *           "totalItems": 3,
         *           "currentPage": 1,
         *           "perPage": 10
         *       }
         *
         * @apiError  (NotFound) {String} message  Alumni not found
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 404 Not Found
         *     {
         *       "message": "Alumni not found"
         *     }
         *
         * @apiError  (InternalServerError) {String} message  Internal Server Error
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 500 Internal Server Error
         *     {
         *       "message": "Internal Server Error"
         *     }
         *
         */
            .get(getAllAlumni)

            /**
             * @api {post} /api/alumni Post alumni data
             * @apiPermission admin
             * @apiName postAlumni
             * @apiGroup Alumni
             *
             * @apiParam {String} studentName Mandatory name of the student.
             * @apiParam {String} categoryId Mandatory object id of the related alumni category.
             * @apiParam {String} batchYear Mandatory batch year of the student.
             *
             * @apiDescription saves alumni information to the database
             * @apiVersion 0.0.1
             * @apiHeader (AuthorizationHeader) {String} authorization x-access-token value.
             * @apiHeaderExample {json} Header-Example:
             *{
             *  "x-access-token": "yJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c"
             * }
             *
             * @apiExample {curl} Example usage:
             *
             *
             * curl \
             * -v \
             * -X POST  \
             * http://localhost:3000/api/alumni/ \
             * -H "Content-Type: multipart/form-data"  \
             * -H "x-access-token: eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c2JlYXQuY29tIiwidXNlcm5hbWUiOiJzdXBlcmFkbWluIiwiX2lkIjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwidXNlckNvbmZpcm1lZCI6ZmFsc2UsImJsb2NrZWQiOmZhbHNlLCJkZWxldGVkIjpmYWxzZSwiYWRkZWRPbiI6IjIwMTYtMDctMDhUMDc6NTQ6MDMuNzY2WiIsInR3b0ZhY3RvckF1dGhFbmFibGVkIjpmYWxzZSwidXNlclJvbGUiOiJhZG1pbiIsImFjdGl2ZSI6dHJ1ZX0sImNsYWltcyI6eyJzdWJqZWN0IjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwiaXNzdWVyIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIiwicGVybWlzc2lvbnMiOlsic2F2ZSIsInVwZGF0ZSIsInJlYWQiLCJkZWxldGUiXX0sImlhdCI6MTQ2ODkyMTU1MiwiZXhwIjoxNDY4OTQxNTUyLCJpc3MiOiI1NzdmNWMxYjU4Njk5MDJkNjdlYjIyYTgifQ.wyt4E06UCFRkBpKD7ThHcWODCxGfO0j3Zv0k-cRTh0nYTHsKjVS4WgpefcLy-o_67aeSh42-wEqq0tBvxk603A" \
             * -F imageName=@public/images/404_banner.png  \
             *
             * @apiSuccess {String} message Alumni saved successfully.
             *
             * @apiSuccessExample Success-Response:
             *     HTTP/1.1 200 OK
             *     {
             *         "message": "Alumni saved successfully"
             *       }
             *
             * @apiError (UnAuthorizedError) {String} message authentication token was not supplied
             * @apiError (UnAuthorizedError) {Boolean} isToken to check if token is supplied or not , if token is supplied , returns true else returns false
             * @apiError (UnAuthorizedError) {Boolean} success true if jwt token verifies
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 401 Unauthorized
             *     {
             *       "isToken": true,
             *        "success": false,
             *        "message": "Authentication failed"
             *     }
             *
             *
             * @apiError  (UnAuthorizedError_1) {String} message  You are not authorized to access this api route.
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 401 Unauthorized
             *     {
             *       "message": "You are not authorized to access this api route."
             *     }
             *
             * @apiError  (UnAuthorizedError_2) {String} message  You are not authorized to perform this action.
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 401 Unauthorized
             *     {
             *       "message": "You are not authorized to perform this action"
             *     }
             *
             *
             *
             * @apiError  (BadRequest) {String[]} message validation errors due to not entering values for studentName, categoryId and batchYear
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 400 Bad Request
             *
             *     {
             *       "message":[
             *                      {"param":"studentName","msg":"studentName is required","value":""},
             *                      {"param":"categoryId","msg":"CategoryId is required"},
             *                      {"param":"batchYear","msg":"batchYear is required","value":""}
             *                 ]
             *     }
             *
             *
             *
             * @apiError  (AlreadyExists)  {String} message  student with same name already exists
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 409 Conflict
             *     {
             *       "message": "student with same name already exists"
             *     }
             *
             *
             *
             * @apiError (InternalServerError)  {String} message  Internal Server Error
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 500 Internal Server Error
             *     {
             *       "message": "Internal Server Error"
             *     }
             *
             */
            .post(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, uploader.single('imageName'), fileUploadHelper.imageUpload, alumniController.postAlumni);

        alumniRouter.use('/alumni/:alumniId', (req, res, next) => {
            alumniController.getAlumniById(req)
                .then((alumniData) => {
                if (alumniData) {
                    req.alumniData = alumniData;
                    next();
                    return null;
                }
                else {
                    res.status(HTTPStatus.NOT_FOUND);
                    res.json({
                        Message: messageConfig.alumni.notFoundAlumni
                    });
                }
                })
                .catch((err) => {
                return next(err)
                })
        });

        alumniRouter.route('/alumni/:alumniId')

        /**
         * @api {get} /api/alumni/:alumniId Get particular alumni as per id
         * @apiPermission public
         * @apiName getAllAlumniById
         * @apiGroup Alumni
         *
         * @apiParam {Int} totalItems Total number of data
         * @apiParam {Int} perPage Number of data to return on each request
         * @apiParam {Int} page Current page number of pagination system.
         *
         * @apiParamExample {json} Request-Example:
         *     {
         *       "totalItems": 5
         *       "perPage": 10,
         *       "page": 1
         *     }
         *
         * @apiDescription Retrieves the particular alumni as per the id sent as parameter.
         * @apiVersion 0.0.1
         *
         * @apiExample {curl} Example usage:
         * curl \
         * -G \
         * -v \
         * "http://localhost:3000/api/alumni/:alumniId" \
         * --data-urlencode "perpage=10" \
         * --data-urlencode "page=1"
         *
         * @apiSuccess {Array}  dataList list of alumni as per the id
         * @apiSuccess {String} dataList._id  object id of alumni data.
         * @apiSuccess {String} dataList.studentName   Name of the student.
         * @apiSuccess {String} dataList.batchYear   batch of the student.
         * @apiSuccess {String} dataList.organizationName   Name of the organization the student is currently working.
         * @apiSuccess {String} dataList.organizationPosition   Position of the student in the organization.
         * @apiSuccess {String} dataList.categoryId   Id of the category in which the alumni falls in.
         * @apiSuccess {String} dataList.imageName   image uploaded.
         * @apiSuccess {Int} totalItems  total no of alumni-category in the event collection in database.
         * @apiSuccess {Int} currentPage  current page number of client pagination system.
         *
         * @apiSuccessExample Success-Response:
         *     HTTP/1.1 200 OK
         *      {
         *          "dataList": [
         *                      {
         *                          "_id": "59a5074627545119b24204bd",
         *                          "imageName": "alumni-1503987526026.jpg",
         *                          "organizationPosition": "trainee",
         *                          "organizationName": "bitsbeat",
         *                          "batchYear": 2011,
         *                          "categoryId": "59a3d806da90dd1d1f3bfb1c",
         *                          "urlSlog": "sabita",
         *                          "studentName": "sabita",
         *                          "addedOn": "2017-08-29T06:18:46.443Z",
         *                          "imageProperties": {
         *                              "imageExtension": "jpg",
         *                              "imagePath": "/home/sabita/bitsbeat/DAV_MBA_School/public/uploads/image/alumni/alumni-1503987526026.jpg"
         *                      }
         *                      }
         *                      ],
         *           "totalItems": 3,
         *           "currentPage": 1,
         *           "perPage": 10
         *       }
         *
         * @apiError  (NotFound) {String} message  Alumni not found
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 404 Not Found
         *     {
         *       "message": "Alumni not found"
         *     }
         *
         * @apiError  (InternalServerError) {String} message  Internal Server Error
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 500 Internal Server Error
         *     {
         *       "message": "Internal Server Error"
         *     }
         *
         */
            .get((req, res) => {
            res.status(HTTPStatus.OK);
            res.json(req.alumniData);
            })

            /**
             * @api {patch} /api/alumni/:alumniId  Deletes existing alumni data
             * @apiPermission admin
             * @apiName patchAlumni
             * @apiGroup Alumni
             *
             * @apiParam {String} newsId  object id of the alumni data
             *
             *   @apiParamExample {json} Request-Example:
             *     {
             *       "alumniId": "578dd7a1772dc45d60c4f8f0"
             *       }
             *
             *
             * @apiDescription Deletes existing alumni information from the database
             * @apiVersion 0.0.1
             * @apiHeader (AuthorizationHeader) {String} authorization x-access-token value.
             * @apiHeaderExample {json} Header-Example:
             *{
             *  "x-access-token": "yJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c"
             * }
             *
             * @apiExample {curl} Example usage:
             *
             *
             * curl \
             * -v \
             * -X PATCH  \
             * http://localhost:3000/api/alumni/578dfaeae392bd75218e743a \
             * -H "x-access-token: eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c2JlYXQuY29tIiwidXNlcm5hbWUiOiJzdXBlcmFkbWluIiwiX2lkIjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwidXNlckNvbmZpcm1lZCI6ZmFsc2UsImJsb2NrZWQiOmZhbHNlLCJkZWxldGVkIjpmYWxzZSwiYWRkZWRPbiI6IjIwMTYtMDctMDhUMDc6NTQ6MDMuNzY2WiIsInR3b0ZhY3RvckF1dGhFbmFibGVkIjpmYWxzZSwidXNlclJvbGUiOiJhZG1pbiIsImFjdGl2ZSI6dHJ1ZX0sImNsYWltcyI6eyJzdWJqZWN0IjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwiaXNzdWVyIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIiwicGVybWlzc2lvbnMiOlsic2F2ZSIsInVwZGF0ZSIsInJlYWQiLCJkZWxldGUiXX0sImlhdCI6MTQ2ODkyMTU1MiwiZXhwIjoxNDY4OTQxNTUyLCJpc3MiOiI1NzdmNWMxYjU4Njk5MDJkNjdlYjIyYTgifQ.wyt4E06UCFRkBpKD7ThHcWODCxGfO0j3Zv0k-cRTh0nYTHsKjVS4WgpefcLy-o_67aeSh42-wEqq0tBvxk603A"
             *
             * @apiSuccess {String} message Alumni deleted successfully.
             *
             * @apiSuccessExample Success-Response:
             *     HTTP/1.1 200 OK
             *     {
             *         "message": "Alumni deleted successfully"
             *       }
             *
             * @apiError (UnAuthorizedError) {String} message authentication token was not supplied
             * @apiError (UnAuthorizedError) {Boolean} isToken to check if token is supplied or not , if token is supplied , returns true else returns false
             * @apiError (UnAuthorizedError) {Boolean} success true if jwt token verifies
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 401 Unauthorized
             *     {
             *       "isToken": true,
             *        "success": false,
             *        "message": "Authentication failed"
             *     }
             *
             *
             * @apiError  (UnAuthorizedError_1) {String} message  You are not authorized to access this api route.
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 401 Unauthorized
             *     {
             *       "message": "You are not authorized to access this api route."
             *     }
             *
             * @apiError  (UnAuthorizedError_2) {String} message  You are not authorized to perform this action.
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 401 Unauthorized
             *     {
             *       "message": "You are not authorized to perform this action"
             *     }
             *
             *
             * @apiError (InternalServerError)  {String} message  Internal Server Error
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 500 Internal Server Error
             *     {
             *       "message": "Internal Server Error"
             *     }
             *
             */
            .patch(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, alumniController.deleteAlumni)

            /**
             * @api {patch} /api/alumni/:alumniId  Updates existing alumni data
             * @apiPermission admin
             * @apiName updateAlumni
             * @apiGroup Alumni
             *
             * @apiParam {String} newsId  object id of the alumni data
             *
             *   @apiParamExample {json} Request-Example:
             *     {
             *       "alumniId": "578dd7a1772dc45d60c4f8f0"
             *       }
             *
             *
             * @apiDescription Updates existing alumni information from the database
             * @apiVersion 0.0.1
             * @apiHeader (AuthorizationHeader) {String} authorization x-access-token value.
             * @apiHeaderExample {json} Header-Example:
             *{
             *  "x-access-token": "yJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c"
             * }
             *
             * @apiExample {curl} Example usage:
             *
             *
             * curl \
             * -v \
             * -X PATCH  \
             * http://localhost:3000/api/alumni/578dfaeae392bd75218e743a \
             * -H "x-access-token: eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiaGVsbG9AYml0c2JlYXQuY29tIiwidXNlcm5hbWUiOiJzdXBlcmFkbWluIiwiX2lkIjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwidXNlckNvbmZpcm1lZCI6ZmFsc2UsImJsb2NrZWQiOmZhbHNlLCJkZWxldGVkIjpmYWxzZSwiYWRkZWRPbiI6IjIwMTYtMDctMDhUMDc6NTQ6MDMuNzY2WiIsInR3b0ZhY3RvckF1dGhFbmFibGVkIjpmYWxzZSwidXNlclJvbGUiOiJhZG1pbiIsImFjdGl2ZSI6dHJ1ZX0sImNsYWltcyI6eyJzdWJqZWN0IjoiNTc3ZjVjMWI1ODY5OTAyZDY3ZWIyMmE4IiwiaXNzdWVyIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIiwicGVybWlzc2lvbnMiOlsic2F2ZSIsInVwZGF0ZSIsInJlYWQiLCJkZWxldGUiXX0sImlhdCI6MTQ2ODkyMTU1MiwiZXhwIjoxNDY4OTQxNTUyLCJpc3MiOiI1NzdmNWMxYjU4Njk5MDJkNjdlYjIyYTgifQ.wyt4E06UCFRkBpKD7ThHcWODCxGfO0j3Zv0k-cRTh0nYTHsKjVS4WgpefcLy-o_67aeSh42-wEqq0tBvxk603A"
             *
             * @apiSuccess {String} message Alumni updated successfully.
             *
             * @apiSuccessExample Success-Response:
             *     HTTP/1.1 200 OK
             *     {
             *         "message": "Alumni updated successfully"
             *       }
             *
             * @apiError (UnAuthorizedError) {String} message authentication token was not supplied
             * @apiError (UnAuthorizedError) {Boolean} isToken to check if token is supplied or not , if token is supplied , returns true else returns false
             * @apiError (UnAuthorizedError) {Boolean} success true if jwt token verifies
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 401 Unauthorized
             *     {
             *       "isToken": true,
             *        "success": false,
             *        "message": "Authentication failed"
             *     }
             *
             *
             * @apiError  (UnAuthorizedError_1) {String} message  You are not authorized to access this api route.
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 401 Unauthorized
             *     {
             *       "message": "You are not authorized to access this api route."
             *     }
             *
             * @apiError  (UnAuthorizedError_2) {String} message  You are not authorized to perform this action.
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 401 Unauthorized
             *     {
             *       "message": "You are not authorized to perform this action"
             *     }
             *
             *
             * @apiError (InternalServerError)  {String} message  Internal Server Error
             *
             * @apiErrorExample Error-Response:
             *     HTTP/1.1 500 Internal Server Error
             *     {
             *       "message": "Internal Server Error"
             *     }
             *
             */
            .put(tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, uploader.single('imageName'), fileUploadHelper.imageUpload, alumniController.updateAlumni);

    function getAllAlumniCategories(req, res, next) {
        alumniController.getAllAlumniCategories(req)
            .then((alumniCategoryList) => {
            if (alumniCategoryList) {
                res.status(HTTPStatus.OK);
                res.json(alumniCategoryList);
            }
            else {
                res.status(HTTPStatus.NOT_FOUND);
                res.json({
                    Message: messageConfig.alumni.notFoundAlumniCategory
                });
            }
            })
            .catch((err) => {
            return next(err)
            })
    }

    function getAllAlumni(req, res, next) {
        alumniController.getAllAlumni(req)
            .then((AlumniList) => {
            if (AlumniList) {
                res.status(HTTPStatus.OK);
                res.json(AlumniList);
            }
            else {
                res.status(HTTPStatus.NOT_FOUND);
                res.json({
                    Message: messageConfig.alumni.notFoundAlumni
                });
            }
            })
            .catch((err) => {
            return next(err)
            })
    }

    function getAllAlumniByCategoryTitle(req, res, next) {
        alumniController.getAllAlumniByCategoryTitle(req, next)
            .then((alumniListByCategory) => {
            if (alumniListByCategory) {
                res.status(HTTPStatus.OK);
                res.json(alumniListByCategory);
            }
            else {
                res.status(HTTPStatus.NOT_FOUND);
                res.json({
                    Message: messageConfig.alumni.notFoundAlumni
                });
            }
            })
            .catch((err) => {
            return next(err);
            })
    }

    return alumniRouter;
}();

    module.exports = alumniRoutes;