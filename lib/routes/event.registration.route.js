var eventRegistrationRoutes = (function() {
    'use strict';

     var HTTPStatus = require('http-status'),
        express = require('express'),
        messageConfig = require('../configs/api.message.config'),
        eventRegistrationController = require('../controllers/event.registration.server.controller'),
        responseMiddleware = require('../middlewares/response.middleware'),
        eventRegistrationRouter = express.Router(),
        tokenAuthMiddleware = require('../middlewares/token.authentication.middleware'),
        roleAuthMiddleware = require('../middlewares/role.authorization.middleware');

    eventRegistrationRouter.route('/')

        .get( getAllEventRegistrations )

        .post( eventRegistrationController.collectToRegister, eventRegistrationController.postRegister, responseMiddleware.respond, responseMiddleware.error )

        //function declaration to return events list to the client if exists else return  empty array
        function getAllEventRegistrations(req, res, next) {
            eventRegistrationController.getAllEventRegistrations(req, next)
                .then(function(events){
                    //if exists, return data in json format
                    if (events) {
                        res.status(HTTPStatus.OK);
                        res.json(events);
                    } else {
                        res.status(HTTPStatus.NOT_FOUND);
                        res.json({
                            message: messageConfig.eventManager.notFound
                        });
                    }
                })
                .catch(function(err){
                    return next(err);
                });
        }

        eventRegistrationRouter.use('/:registrationId', function(req, res, next){
            eventRegistrationController.getEventRegistrationByID(req)
                .then(function(registrationInfo){
                    if (registrationInfo) {
                        req.registrationInfo = registrationInfo;
                        next();
                        return null;
                    } else {
                        res.status(HTTPStatus.NOT_FOUND);
                        res.json({
                            message: messageConfig.roleManager.notFound
                        });
                    }
                })
                .catch(function(err){
                    return next(err);
                });
        });

        eventRegistrationRouter.route('/:registrationId')

        .patch( tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, eventRegistrationController.patchEventRegistration );


    return eventRegistrationRouter;

})();

module.exports = eventRegistrationRoutes;
