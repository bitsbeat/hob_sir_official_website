let noticeController = (() => {

    'use strict';

    const Notice             = require('../models/notice.model'),
          dataProviderHelper = require('../data/mongo.provider.helper'),
          HTTPStatus         = require('http-status'),
          messageConfig      = require('../configs/api.message.config'),
          utilityHelper      = require('../helpers/utilities.helper'),
          cacheHelper        = require('../helpers/cache.helper'),
          errorHelper        = require('../helpers/error.helper'),
          NodeCache          = require('node-cache'),
          moment             = require('moment'),
          Promise            = require('bluebird');

    let noticeFields = '_id noticeTitle noticeDescription active addedOn lastUpdated';

    let appCache = new NodeCache( { stdTTL: 86400, checkperiod: 0 });
    let prevValue = "";
    
    function NoticeModule() {}

    NoticeModule.CreateNotice = (noticeObj, _slogVal, loggedInUser) => {
        let newNotice = new Notice();

        newNotice.noticeTitle       = noticeObj.noticeTitle;
        newNotice.urlSlog           = _slogVal;
        newNotice.noticeDescription = noticeObj.noticeDescription;
        newNotice.deadline          = noticeObj.deadline;
        newNotice.active            = noticeObj.active;
        newNotice.addedOn           = new Date();
        newNotice.addedBy           = loggedInUser;

        return newNotice;
    };

    let _p = NoticeModule.prototype;

    _p.checkValidationErrors = (req) => {
        req.checkBody('noticeTitle', messageConfig.notice.validationErrorMessage.noticeTitle).notEmpty();
        req.checkBody('noticeDescription', messageConfig.notice.validationErrorMessage.noticeDescription).notEmpty();

        return req.validationErrors();
    };

    _p.postNotice = (req, res, next) => {
        if (req.body.noticeTitle) {
            let modelInfo = utilityHelper.sanitizeUserInput(req, next);
            let _slogVal  = utilityHelper.getCleanURL(modelInfo.noticeTitle, next);
            let query     = {};
            query.urlSlog = _slogVal;
            query.deleted = false;

            dataProviderHelper.checkForDuplicateEntry(Notice, query)
                .then((count) => {
                if (count > 0) {
                    throw new Promise.CancellationError('{ "statusCode":"' + HTTPStatus.CONFLICT + '", "message": "' + messageConfig.notice.alreadyExistsNotice + '"}');
                }
                else {
                    let noticeObj = NoticeModule.CreateNotice(modelInfo, _slogVal, req.decoded.user.username);
                    return dataProviderHelper.save(noticeObj);
                }
                })
                .then(() => {
                res.status(HTTPStatus.OK);
                res.json({
                    message: messageConfig.notice.saveMessage
                });
                })
                .catch(Promise.CancellationError, (CancellationErr) => {
                    errorHelper.customErrorResponse(res, CancellationErr, next);
                })
                .catch((err) => {
                return next(err);
                })
        }
        else {
            res.status(HTTPStatus.BAD_REQUEST);
            res.json({
                message: messageConfig.notice.fieldRequiredNotice
            });
        }
    };

    _p.getAllNotice = (req, res, next) => {
        let pagerOpts = utilityHelper.getPaginationOpts(req, next);
        let query = {};
         query.active = true;
         query.deleted = false;
        let sortOpts = {addedOn: -1};

        return dataProviderHelper.getAllWithDocumentFieldsPagination(Notice, query, pagerOpts, noticeFields, sortOpts);
    };

    _p.getNoticeByAdmin = (req, res, next) => {
        let pagerOpts = utilityHelper.getPaginationOpts(req, next);
        let query     = {};
            query.deleted = false;

        let sortOpts  = {addedOn: -1};

        return dataProviderHelper.getAllWithDocumentFieldsPagination(Notice, query, pagerOpts, noticeFields, sortOpts);
    };

    let getAllQuotes = (req, res, next) => {
        let query = {};
            query.active = true;
            query.deleted = false;
            query.lastUpdated = false;
        let sortOpts  = {addedOn: -1};        
        return dataProviderHelper.getAllWithDocumentFieldsNoPagination(Notice, query, noticeFields, sortOpts)
    }

    function randomIntFromInterval(min,max)
    {
       return Math.floor(Math.random()*(max-min+1)+min);
    }

    const getQuotes = () => {
        return new Promise(function (resolve, reject) {            
        getAllQuotes()
            .then((res) => {
                if(res) {
                    const length = res.length;
                    if(length>0) {
                        const index = randomIntFromInterval(0, length-1);
                        const quoteResponse = res[index];
                        if(length === 1){
                            dataProviderHelper.updateModelData(Notice, {deleted: false}, {$set:{lastUpdated: false}}, true);
                        }
                        return resolve(quoteResponse);
                    }else {
                        return resolve({});
                    }
                }
            })
            .catch((err) => {
                return resolve({});
            })
        });
    }

    const updatePreviousQuote = (_id) => {
        const query = {};
            query._id = _id;
        return dataProviderHelper.updateModelData(Notice, query, {$set:{lastUpdated: true}}, false);
    }

    _p.getQuotesFromCache = (req, res, next) => {
        return new Promise(function (resolve, reject) {
            var quoteFeedRes = cacheHelper.getDataFromCache(appCache, "quoteCache");
            if (quoteFeedRes !== null && quoteFeedRes !== undefined) {
                res.json(quoteFeedRes);
            } else {
                getQuotes()
                    .then((resData) => {
                        if(resData)
                            return [resData, cacheHelper.storeDataInCache(resData, appCache, "quoteCache"), updatePreviousQuote(resData._id)];
                        else
                            return [{}];
                    })
                    .then((quote) => {
                        res.json(quote[0]);
                    })
                    .catch((err) => {
                        return next(err);
                    });
                }
            });
    }

    _p.getNoticeByLatestWeek = (req, res, next) => {
        let query      = {};
        let date       = new Date();
        let now        = moment(date);
        let OneWeekAgo = now.subtract(7, 'days');

        return _p.getAllNotice(req, res, next)
            .then((noticeList) => {
            if (noticeList) {
                    query.addedOn = {$gte: OneWeekAgo};
                    query = { $or: [ { deadline: { $gt: new Date() } }, { deadline: '' } ] }
                    query.active   = true;
                    query.deleted  = false;

                    let sortOpts   = {addedOn: -1};
                    return dataProviderHelper.getAllWithDocumentFieldsNoPagination(Notice, query, noticeFields, sortOpts)
                        .then((filteredNotices) => {
                        if (filteredNotices) {
                            res.status(HTTPStatus.OK);
                            res.json(filteredNotices);
                        }
                        else {
                            res.status(HTTPStatus.NOT_FOUND);
                            res.json({
                                Message: messageConfig.notice.notFound
                            });
                        }
                        })
            }
            else {
                res.status(HTTPStatus.NOT_FOUND);
                res.json({
                    Message: messageConfig.notice.notFound
                });
            }
            })
            .catch((err) => {
            return next(err);
            })
    };

    _p.getNoticeById = (req) => {
        return dataProviderHelper.findById(Notice, req.params.noticeId, noticeFields);
    };

    _p.deleteNoticeById = (req, res, next) => {
        req.noticeInfo.deleted   = true;
        req.noticeInfo.deletedBy = req.decoded.user.username;
        req.noticeInfo.deletedOn = new Date();

        let _query = {
            'noticeId': req.params.noticeId,
            'deleted' : false
        };

        dataProviderHelper.checkForDuplicateEntry(Notice, _query)
            .then((count) => {
                if(count > 0) {
                    return new Promise.CancellationError('{"status": "' + HTTPStatus.CONFLICT + '", "message": "' + messageConfig.notice.alreadyExistsNotice + '"}');
                }
                else {
                    dataProviderHelper.save(req.noticeInfo);
                }
            })
            .then(() => {
                res.status(HTTPStatus.OK);
                res.json({
                    message: messageConfig.notice.deleteMessage
                });
            })
            .catch(Promise.CancellationError, (CancellationErr) => {
                errorHelper.customErrorResponse(res, CancellationErr, next)
            })
            .catch((err) => {
                return next(err);
            });
    };

    _p.updateNoticeById = (req, res, next) => {
        if (req.body.noticeTitle) {
            let modelInfo   = utilityHelper.sanitizeUserInput(req, next);
            let _slogVal    = utilityHelper.getCleanURL(modelInfo.noticeTitle, next);

            if (req.noticeInfo.urlSlog !== _slogVal) {
                let query   = {};
                query.urlSlog   = _slogVal;
                query.deleted   = false;

                dataProviderHelper.checkForDuplicateEntry(Notice, query)
                    .then((count) => {
                        if(count > 0) {
                            return new Promise.CancellationError('{"status": "' + HTTPStatus.CONFLICT + '", "message": "' + messageConfig.notice.alreadyExistsNotice + '"}');
                        }
                        else {
                            return _p.updateFunction(req, res, modelInfo, _slogVal);
                        }
                    })
                    .then(() => {
                        res.status(HTTPStatus.OK);
                        res.json({
                            message: messageConfig.notice.updateMessage
                        });
                    })
                    .catch(Promise.CancellationError, (CancellationErr) => {
                        errorHelper.customErrorResponse(res, CancellationErr, next);
                    })
                    .catch((err) => {
                        return next(err);
                    });
            }
            else {
                _p.updateFunction(req, res, modelInfo, _slogVal)
                    .then(() => {
                        res.status(HTTPStatus.OK);
                        res.json({
                            message: messageConfig.notice.updateMessage
                        });
                    })
                    .catch((err) => {
                        return next(err);
                    });
            }
        }
        else {
            res.status(HTTPStatus.BAD_REQUEST);
            res.json({
                message: messageConfig.notice.fieldRequiredNotice
            });
        }
    };

    _p.updateFunction = (req, res, modelInfo, _slogVal) => {
        req.noticeInfo.noticeTitle       = modelInfo.noticeTitle;
        req.noticeInfo.urlSlog           = _slogVal;
        req.noticeInfo.noticeDescription = modelInfo.noticeDescription;
        req.noticeInfo.deadline          = modelInfo.deadline;
        req.noticeInfo.active            = modelInfo.active;
        req.noticeInfo.updatedBy         = req.decoded.user.username;
        req.noticeInfo.updatedOn         = new Date();

        return dataProviderHelper.save(req.noticeInfo);
    };

    _p.getNoticeByTitleSlog = (req, res, next) => {
        let query = {
            'active': true,
            'deleted': false,
            'urlSlog': req.params.titleSlog
        };

        return new Promise((resolve, reject) => {
            dataProviderHelper.getAllWithDocumentFieldsNoPagination(Notice, query, noticeFields, {})
                .then((noticeObj) => {
                resolve(noticeObj)
                })
                .catch((err) => {
                reject(err)
                })
        })
    };

    return {
        postNotice            : _p.postNotice,
        getAllNotice          : _p.getAllNotice,
        getNoticeByAdmin      : _p.getNoticeByAdmin,
        getNoticeByLatestWeek : _p.getNoticeByLatestWeek,
        getNoticeById         : _p.getNoticeById,
        getCacheQuote         : _p.getQuotesFromCache,
        deleteNoticeById      : _p.deleteNoticeById,
        updateNoticeById      : _p.updateNoticeById,
        getNoticeByTitleSlog  : _p.getNoticeByTitleSlog
    };
})();

module.exports = noticeController;