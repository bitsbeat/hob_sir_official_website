/**
 * Created by lakhe on 11/27/16.
 */
    'use strict';

var twitterController = (function () {
    'use strict';

    const Promise = require('bluebird'),
        OAuth = require('oauth').OAuth,
        errorLogController = require('./error.log.server.controller'),
        utililtyHelper = require('../helpers/utilities.helper'),
        cacheHelper = require('../helpers/cache.helper'),
        NodeCache = require('node-cache'),
        twitterConfig = require('../configs/twitter.config'),
        HttpStatus = require('http-status');

    let appCache = new NodeCache( { stdTTL: twitterConfig.cacheExpiryTime, checkperiod: 0 });

    let getTwitterOAuthConfig = () => {
        return new OAuth(
            twitterConfig.requestTokenApiURL,
            twitterConfig.accessTokenApiURL,
            twitterConfig.consumerKey,
            twitterConfig.consumerSecret,
            "1.0A",
            null,
            twitterConfig.encryptionAlgorithm
        );
    };

    let formatTwitterFeeds = (twitterResponseJsonData, next) => {
        var tweetHtml = '';
        for (var x = 0; x < twitterResponseJsonData.length; x++) {
            tweetHtml += '<div class="twitter-tweets">';
            tweetHtml += '<div class="twitter-tweet-head clearfix">';
            tweetHtml += '<div class="twitter-tweet-user-img">';
            tweetHtml += '<img src="';
            tweetHtml += twitterResponseJsonData[x]["user"]["profile_image_url_https"].toString();
            tweetHtml += '" alt="';
            tweetHtml += twitterResponseJsonData[x]["user"]["name"].toString();
            tweetHtml += '">';
            tweetHtml += '</div>';
            tweetHtml += '<div class="twitter-tweet-user-info">';
            tweetHtml += '<a target="_blank" href="';
            tweetHtml += 'https://twitter.com/';
            tweetHtml += twitterResponseJsonData[x]["user"]["screen_name"].toString();
            tweetHtml += '/" title="Follow ';
            tweetHtml += twitterResponseJsonData[x]["user"]["screen_name"].toString();
            tweetHtml += '">';
            tweetHtml += '<h5>@';
            tweetHtml += twitterResponseJsonData[x]["user"]["screen_name"].toString();
            tweetHtml += '</h5>';
            tweetHtml += '</a>';
            tweetHtml += '<span>';
            var postDate = twitterResponseJsonData[x]["created_at"].toString();
            var dateParts = postDate.split(' ');
            tweetHtml += dateParts[1];
            tweetHtml += ' ';
            tweetHtml += dateParts[2];
            tweetHtml += ', ';
            tweetHtml += dateParts[5];
            tweetHtml += '</span>';
            tweetHtml += '<br />';
            tweetHtml += '<span>';
            var _MS_PER_DAY = 1000 * 60 * 60 * 24;
            var dateDiff = Math.floor(new Date() - new Date(twitterResponseJsonData[x]["created_at"].toString()))/_MS_PER_DAY;

            var hours= 0;
            if(parseInt(dateDiff) >= 1){
                tweetHtml += parseInt(dateDiff);
                tweetHtml += parseInt(dateDiff) > 1 ? ' days' : ' day';
                hours = (parseFloat(dateDiff) - parseInt(dateDiff)) * 24;
                if(hours >= 1){
                    tweetHtml += ', ';
                    tweetHtml += parseInt(hours);
                    tweetHtml += parseInt(hours) > 1 ? ' hours' : ' hour';
                }
            }else{
                hours = parseFloat(dateDiff) * 24;
                if(hours >= 1){
                    tweetHtml += parseInt(hours);
                    tweetHtml += parseInt(hours) > 1 ? ' hours' : ' hour';
                }else{
                    tweetHtml += ', ';
                    var minutes = (parseFloat(hours) - parseInt(hours)) * 60;
                    tweetHtml += parseInt(minutes);
                    tweetHtml += ' min';
                }
            }
            tweetHtml += ' ago';
            tweetHtml += '</span>';
            tweetHtml += '</div>';
            tweetHtml += '</div>';
            tweetHtml += '<div class="twitter-tweet-content">';
            tweetHtml += '<p>';
            var tweetArr = twitterResponseJsonData[x]["text"].toString().split(' ');
            var wordCount = 0;
            for (var i = 0; i < tweetArr.length; i++) {
                if (utililtyHelper.containsChar(tweetArr[i], 'http', next)) {
                    tweetHtml += '<a target="_blank" href="';
                    tweetHtml += tweetArr[i];
                    tweetHtml += '">';
                    tweetHtml  += tweetArr[i];
                    tweetHtml += '</a>';
                    tweetHtml += ' ';
                    wordCount++;
                } else {
                    if (utililtyHelper.containsChar(tweetArr[i], '#', next)) {
                        tweetHtml += '<a target="_blank" href="https://twitter.com/hashtag/';
                        if (tweetArr[i].length > 1) {
                            tweetHtml += replaceSpecialCharacters(tweetArr[i].substring(1));
                        }
                        tweetHtml += '?src=hash">';
                        tweetHtml  += tweetArr[i];
                        tweetHtml += '</a>';
                        tweetHtml += ' ';
                    } else {
                        tweetHtml  += tweetArr[i];
                        tweetHtml += ' ';
                    }
                }
            }
            tweetHtml += '</p>';
            tweetHtml += '</div>';
            tweetHtml += '</div>';
        }
        return tweetHtml;
    };

    let replaceSpecialCharacters = (string) => {
        return string.replace(",", "")
            .replace("_", "")
            .replace("(", "")
            .replace(")", "")
            .replace(".", "")
            .replace("/", "")
            .replace("'", "")
            .replace("\"", "")
            .replace(":", "")
            .replace("’", "")
            .replace("‘", "")
            .replace("\\", "")
            .replace("#", "")
            .replace("!", "")
            .replace("@", "")
            .replace("$", "")
            .replace("&", "")
            .replace("~", "")
            .replace("`", "")
            .replace("%", "")
            .replace(",", "")
            .replace(";", "")
            .replace("?", "");
    };

    let manageTwitterFeedCache = (next) => {
        return new Promise(function (resolve, reject) {
            var _twitterFeedRes = cacheHelper.getDataFromCache(appCache, "twitterFeedCache");
            if (_twitterFeedRes !== null && _twitterFeedRes !== undefined) {
                resolve(_twitterFeedRes);
            } else {
                accessTwitterTimelineApi(next)
                    .then(function (resData) {
                        return [resData, cacheHelper.storeDataInCache(resData, appCache, "twitterFeedCache")];
                    })
                    .then(function (twitterFeedData) {
                        resolve(twitterFeedData[0]);
                    })
                    .catch(function (err) {
                        reject(err);
                    });
            }
        });
    };


    let accessTwitterTimelineApi = (next) => {
        var _url = twitterConfig.apiURL +"?screen_name=" + twitterConfig.username + "&count=" + twitterConfig.tweetCount;
        return new Promise(function (resolve, reject){
            getTwitterOAuthConfig().get(_url, twitterConfig.accessToken, twitterConfig.accessTokenSecret,
                function (err, twitterFeeds) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(JSON.parse(twitterFeeds));
                    }
                });
        });
    };

    function twitterModule(){}

    var _p = twitterModule.prototype;

    _p.getTweetFeeds = function (req, res, next) {
        //For reference, follow https://dev.twitter.com/web/sign-in/implementing
        manageTwitterFeedCache(next)
            .then(function (twitterResponseData) {
                if (twitterResponseData) {
                    res.status(HttpStatus.OK);
                    res.json(twitterResponseData);
                }
                else {
                    res.send(twitterResponseData)
                }
            })
            .catch(function (err) {
                var errorObj = {
                    message : err.data,
                    errorStack : err.data,
                    errorType : err.statusCode
                };
                errorLogController.postErrorLogs(errorObj, req, next);
                res.send([]);
            });
    };

    return{
        getTweetFeeds : _p.getTweetFeeds
    };

})();

module.exports = twitterController;





