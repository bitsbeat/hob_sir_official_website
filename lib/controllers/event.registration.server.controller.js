/*created on 24/07/17 by bishwo*/

var eventRegistrationController = (function(){
    'use strict';

    var dataProviderHelper      = require('../data/mongo.provider.helper'),
        HTTPStatus              = require('http-status'),
        messageConfig           = require('../configs/api.message.config'),
        EventRegistration       = require('../models/event.registration.server.model'),
        utilityHelper           = require('../helpers/utilities.helper'),
        errorHelper             = require('../helpers/error.helper'),
        Promise                 = require("bluebird"),
        emailTemplateController = require('./email.template.server.controller'),
        emailConfig             = require('../configs/email.template.config'),
        mailHelper              = require('../helpers/mail.helper'),
        collect                 = require('../helpers/collect.helper'),
        documentFields          = '_id fullName email contactNumber eventId emailSent eventName addedOn';
    function EventRegistrationModule(){}

        var _p = EventRegistrationModule.prototype;

        _p.collectToRegister = function (req, res, next) {
            let collectInstance = new collect();
            collectInstance.setBody([
                'fullName',
                'email',
                'contactNumber',
                'eventId',
                'eventName'
            ])

            collectInstance.setMandatoryFields({
                fullName     : 'Full Name not provided',
                contactNumber: 'Contact number not provided',
                email        : 'Email not provided',
                eventId      : 'Event Id not provided',
                eventName    : 'Event Name not provided'
            })

            collectInstance.collect(req).then((data) => {
                req.participantData = data
                next();
            }).catch((err) => {
                err.status = 400
                next(err)
            })
        }

        _p.getAllEventRegistrations = function(req, next){
            var pagerOpts = utilityHelper.getPaginationOpts(req, next),
                query     = { deleted: false },
                sortOpts  = {};

            if(req.query.fullName)
                query.fullName = req.query.fullName;
            
            if(req.query.contactNumber)
                query.contactNumber = req.query.contactNumber;
            
            if(req.query.eventName)
                query.eventName = req.query.eventName;
            
            if(req.query.sortOption){
                sortOpts = req.query.sortOption
            }else{
                sortOpts = { addedOn: -1 }
            }

            return dataProviderHelper.getAllWithDocumentFieldsPagination(EventRegistration, query, pagerOpts, documentFields, sortOpts);
        };

        _p.getEventRegistrationByID = function(req,next){
            return dataProviderHelper.findById(EventRegistration, req.params.registrationId, documentFields);
        };

        _p.postRegister = function(req,res,next){
            let eventRegistrationModel = new EventRegistration(req.participantData);

            var query = req.participantData;
            dataProviderHelper.checkForDuplicateEntry(EventRegistration, query)
                .then(function(count){
                    if(count > 0){
                        throw new Promise.CancellationError('{ "statusCode":"' + HTTPStatus.CONFLICT + '", "message": "' + messageConfig.eventSubscription.alreadyExists + '"}');
                    } else {
                        return [ eventRegistrationModel, dataProviderHelper.save(eventRegistrationModel)];
                    }
                })
                .spread(function(eventRegistrationModel){
                    /*Send email here and update emailSent to true*/
                    if(eventRegistrationModel.emailSent === false){
                        _p.sendEventJoinedEmail( eventRegistrationModel, next);
                        _p.updateEventEmailSent( eventRegistrationModel );
                    }
                })
                .then(function(){
                    res.status(HTTPStatus.OK);
                    res.json({
                        message: messageConfig.eventSubscription.saveMessage
                    });
                })
                .catch(Promise.CancellationError, function (cancellationErr) {
                    errorHelper.customErrorResponse(res, cancellationErr, next);
                })
                .catch(function(err){
                    return next(err);
                });

        };

        _p.sendEventJoinedEmail = function (req, next){
            var req = {
                email  : req.email, 
                params : {
                    templateId : emailConfig.eventSubscriptionEmail
                }
            };

            return new Promise(function(resolve, reject) {
                emailTemplateController.getEmailTemplateDataByID(req)
                .then(function(emailTemplate){
                    var messageBody = '';
                    if(emailTemplate){
                        messageBody     =  emailTemplate.templateBody;
                        var mailOptions = {
                            fromEmail           : " DAV MBA School < "+emailTemplate.emailFrom+" >", // sender address
                            toEmail             : [req.email], // list of receivers
                            subject             : emailTemplate.emailSubject, // Subject line
                            textMessage         : messageBody, // plaintext body
                            htmlTemplateMessage : messageBody
                        };
                        return mailHelper.sendEmailWithNoAttachment(req, mailOptions, next);
                    }else{
                        return;
                    }
                })
                .then(function (data) {
                    resolve(data);
                })
                .catch(function(err){
                    reject(err);
                });
            });
        }

        _p.updateEventEmailSent = function(req){
            let queryData = { _id       : req._id},
            reqData       = { emailSent : true};

            return dataProviderHelper.updateModelData(EventRegistration,queryData,reqData, false);
        };

        _p.patchEventRegistration = function(req, res, next){
                req.registrationInfo.deleted   = true;
                req.registrationInfo.deletedOn = new Date();
                req.registrationInfo.deletedBy = req.decoded.user.username;

                return dataProviderHelper.save(req.registrationInfo)
                    .then(function () {
                        res.status(HTTPStatus.OK);
                        res.json({
                            message: messageConfig.eventSubscription.deleteMessage
                        });
                    })
                    .catch(function (err) {
                        return next(err);
                    });
     
        }

    return{
        collectToRegister       : _p.collectToRegister,
        getAllEventRegistrations: _p.getAllEventRegistrations,
        getEventRegistrationByID: _p.getEventRegistrationByID,
        postRegister            : _p.postRegister,
        updateEventEmailSent    : _p.updateEventEmailSent,
        patchEventRegistration  : _p.patchEventRegistration
    };

})();

module.exports = eventRegistrationController;