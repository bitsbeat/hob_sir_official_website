var documentManagementController = (function () {

    'use strict';

    const dataProviderHelper = require('../data/mongo.provider.helper'),
          HTTPStatus = require('http-status'),
          messageConfig = require('../configs/api.message.config'),
          DocumentManagement = require('../models/document.management.model'),
          Document = DocumentManagement.Document,
          DocumentCategory = DocumentManagement.DocumentCategory,
          utilityHelper = require('../helpers/utilities.helper'),
          errorHelper = require('../helpers/error.helper'),
          mongoose = require('mongoose'),
          ObjectId = mongoose.Types.ObjectId,
          Promise = require('bluebird'),
          join = Promise.join;

    var documentFieldsCategory = '_id categoryName urlSlogCategory categoryDescription active addedOn';
    var documentFieldsDocument = '_id documentTitle urlSlog categoryId documentDescription documentName' +
                                 ' docProperties documentAltText active addedOn';

    function DocumentModule () {}

    DocumentModule.createDocumentCategory = (documentCategoryObj, _slogVal, loggedInUser) => {
        var documentCategory = new DocumentCategory();

        documentCategory.categoryName = documentCategoryObj.categoryName;
        documentCategory.urlSlogCategory = _slogVal;
        documentCategory.categoryDescription = documentCategoryObj.categoryDescription;
        documentCategory.active = documentCategoryObj.active;
        documentCategory.addedBy = loggedInUser;
        documentCategory.addedOn = new Date();

        return documentCategory;
    };

    DocumentModule.createDocument = (documentObj, modelHtmlObj, _slogVal, documentInfo, loggedInUser) => {
        var document = new Document();

        document.documentTitle = documentObj.documentTitle;
        document.urlSlog = _slogVal;
        document.categoryId = documentObj.categoryId;
        document.documentDescription = modelHtmlObj.documentDescription;
        document.documentName = documentInfo._documentName;
        document.docProperties = {
            documentMimeType: documentInfo._documentMimeType,
            docPath: documentInfo._documentPath
        };
        document.documentAltText = documentObj.documentAltText;
        document.addedBy = loggedInUser;
        document.addedOn = new Date();

        return document;
    };

    var _p = DocumentModule.prototype;

    _p.checkValidationErrors = (req) => {
        // req.checkBody('documentCategoryTitle', messageConfig.document.validationErrMessage.documentCategoryTitle).notEmpty();
        req.checkBody('documentTitle', messageConfig.document.validationErrMessage.documentTitle).notEmpty();
        req.checkBody('documentDescription', messageConfig.document.validationErrMessage.documentDescription).notEmpty();

        return req.validationErrors();
    }

    _p.postDocumentCategory = (req,res,next) => {
        if(req.body.categoryName) {
            var modelInfo = utilityHelper.sanitizeUserInput(req,next);
            var _slogVal = utilityHelper.getCleanURL(modelInfo.categoryName, next);
            var query = {};
            query.urlSlogCategory = _slogVal;
            query.deleted = false;

            dataProviderHelper.checkForDuplicateEntry(DocumentCategory, query)
                .then((count) => {
                if(count > 0) {
                    throw new Promise.CancellationError('{ "statusCode":"' + HTTPStatus.CONFLICT + '", "message": "' + messageConfig.document.alreadyExistsCategory + '"}');
                }
                else {
                    var documentCategoryObj = DocumentModule.createDocumentCategory(modelInfo, _slogVal, req.decoded.user.username);
                    return dataProviderHelper.save(documentCategoryObj);
                }
                })
                .then(function () {
                    res.status(HTTPStatus.OK);
                    res.json({
                        messsage: messageConfig.document.saveMessageCategory
                    });
                })
                .catch(Promise.CancellationError, (cancellationErr) => {
                    errorHelper.customErrorResponse(res,cancellationErr,next);
                })
                .catch((err) => {
                    return next(err);
                });
        }
        else {
            res.status(HTTPStatus.BAD_REQUEST);
            res.json({
                message: messageConfig.document.fieldRequiredCategory
            });
        }
    };

    _p.getDocumentCategories = (req, next) => {
        var pagerOpts = utilityHelper.getPaginationOpts(req, next);

        var query = {};
            //matches anything that exactly matches the input category name,
            //case sensitive
        if(req.query.categoryname) {
            query.categoryName = {$regex: new RegExp('^' + req.query.categoryname + '$', "i")};
        }
        if(req.query.active) {
            query.active = true;
        }

        query.deleted = false;
        var sortOpts = {addedOn: -1};

        return dataProviderHelper.getAllWithDocumentFieldsPagination(DocumentCategory, query, pagerOpts, documentFieldsCategory, sortOpts);
    };

    _p.getDocumentCategoryById = (req) => {
        return dataProviderHelper.findById(DocumentCategory, req.params.categoryId, documentFieldsCategory);
    };

    _p.updateDocumentCategory = (req,res,next) => {
        if(req.body.categoryName) {
            var modelInfo = utilityHelper.sanitizeUserInput(req,next);
            var _slogVal = utilityHelper.getCleanURL(modelInfo.categoryName, next);

            if(req.documentCategoryInfo.urlSlogCategory !== _slogVal) {
                var query = {};
                query.urlSlogCategory = _slogVal;
                query.deleted = false;

                dataProviderHelper.checkForDuplicateEntry(DocumentCategory, query)
                    .then((count) => {
                    if(count > 0) {
                        return new Promise.CancellationError('{"status": "' + HTTPStatus.CONFLICT + '", "message": "' + messageConfig.document.alreadyExistsCategory + '"}');
                    }
                    else {
                        return _p.updateFunction(req, res, modelInfo, _slogVal);
                    }
                    })
                    .then(() => {
                    res.status(HTTPStatus.OK);
                    res.json({
                        message: messageConfig.document.updateMessageCategory
                    });
                    })
                    .catch(Promise.CancellationError, (CancellationErr) => {
                        errorHelper.customErrorResponse(res, CancellationErr, next);
                    })
                    .catch((err) => {
                    return next(err);
                    });
            }
            else {
                _p.updateFunction(req, res, modelInfo, _slogVal)
                    .then(() => {
                    res.status(HTTPStatus.OK);
                    res.json({
                        message: messageConfig.document.updateMessageCategory
                    });
                    })
                    .catch((err) => {
                    return next(err);
                    });
            }
        }
        else {
            res.status(HTTPStatus.BAD_REQUEST);
            res.json({
                message: messageConfig.document.fieldRequiredCategory
            });
        }
    };

    _p.updateFunction = (req, res, modelInfo, _slogVal) => {
        req.documentCategoryInfo.categoryName = modelInfo.categoryName;
        req.documentCategoryInfo.urlSlogCategory = _slogVal;
        req.documentCategoryInfo.categoryDescription = modelInfo.categoryDescription;
        req.documentCategoryInfo.active = modelInfo.active;
        req.documentCategoryInfo.updatedBy = req.decoded.user.username;
        req.documentCategoryInfo.updatedOn = new Date();

        return dataProviderHelper.save(req.documentCategoryInfo);
    };

    _p.deleteDocumentCategory = (req, res, next) => {
        req.documentCategoryInfo.deleted = true;
        req.documentCategoryInfo.deletedBy = req.decoded.user.username;
        req.documentCategoryInfo.deletedOn = new Date();

        var _query = {
            'categoryId': req.params.categoryId,
            'deleted': false
        };

        dataProviderHelper.checkForDuplicateEntry(Document, _query)
            .then((count) => {
            if(count > 0) {
                return new Promise.CancellationError('{"status": "' + HTTPStatus.CONFLICT + '", "message": "' + messageConfig.document.alreadyExistsCategory + '"}');
            }
            else {
                dataProviderHelper.save(req.documentCategoryInfo);
            }
            })
            .then(() => {
            res.status(HTTPStatus.OK);
            res.json({
                message: messageConfig.document.deleteMessageCategory
            });
            })
            .catch(Promise.CancellationError, (CancellationErr) => {
                errorHelper.customErrorResponse(res, CancellationErr, next)
            })
            .catch((err) => {
            return next(err);
            });
    };

    _p.postDocument = (req, res, next) => {
        // console.log(req.body);
        req.body = JSON.parse(req.body.data);

        var errors = _p.checkValidationErrors(req);
        if(errors) {
            res.status(HTTPStatus.BAD_REQUEST);
            res.json({
                message: errors
            });
           
        }
        else {
            var modelInfo = utilityHelper.sanitizeUserInput(req, next);
            var _slogVal = utilityHelper.getCleanURL(modelInfo.documentTitle, next);
            var query = {};
                query.urlSlog = _slogVal;
                query.deleted = false;
                
            dataProviderHelper.checkForDuplicateEntry(Document, query)
                .then((count) => {
                if(count > 0) {
                    return new Promise.CancellationError('{ "statusCode":"' + HTTPStatus.CONFLICT + '", "message": "' + messageConfig.document.alreadyExistsDocument + '"}');
                }
                else {
                    var documentInfo = utilityHelper.getDocumentFileInfo(req, null, next);
                    var contentInfo = {};
                    contentInfo.documentDescription = req.body.documentDescription;
                    var modelHtmlInfo = utilityHelper.sanitizeUserHtmlBodyInput(contentInfo, next);
                  
                    var documentObj = DocumentModule.createDocument(modelInfo, modelHtmlInfo, _slogVal, documentInfo, req.decoded.user.username );

                    return dataProviderHelper.save(documentObj);
                }
                })
                .then(() => {
                res.status(HTTPStatus.OK);
                res.json({
                    message: messageConfig.document.saveMessageDocument
                });
                })
                .catch(Promise.CancellationError, (CancellationErr) => {
                    errorHelper.customErrorResponse(res, CancellationErr, next);
                })
                .catch((err) => {
                return next(err);
                });
        }
    };

    _p.getDocuments = (req, res, next) => {
        var pagerOpts = utilityHelper.getPaginationOpts(req, next);
        var query = {};

        if(req.query.documenttitle) {
            query.documentTitle = { $regex: new RegExp('.*'+ req.documenttitle, "i") };
        }
        if(req.query.categoryid) {
            query.categoryId = req.query.categoryid;
        }
        if(req.query.active) {
            query.active = true;
        }
        query.deleted = false;
        var sortOpts = {addedOn: -1};

        return dataProviderHelper.getAllWithDocumentFieldsPagination(Document, query, pagerOpts, documentFieldsDocument, sortOpts);
        };

    _p.getDocumentById = (req) => {

        return dataProviderHelper.findById(Document, req.params.documentId, documentFieldsDocument);
    };

    _p.getDocumentByCategory = (req, res, next) => {
        var category = '';
        if(req.params.documentCategory) {
            category = req.params.documentCategory;
        }
        var categorySlog = utilityHelper.getCleanURL(category, next);
        var query = {
            'active': true,
            'deleted': false
        };
        query.urlSlogCategory = categorySlog;
        return new Promise((resolve, reject) => {
            dataProviderHelper.findOne(DocumentCategory, query, documentFieldsCategory)
                .then((categoryObj) => {
                    if(categoryObj)  {
                        req.query.categoryid = categoryObj._id;
                        req.query.active = true;
                        return _p.getDocuments(req, next);
                    }
                    else {
                        return [];
                    }
                })
                .then((documentList) => {
                resolve(documentList);
                })
                .catch((err) => {
                reject(err);
                });
        });
    };

    _p.updateDocument = (req, res, next) => {
        req.body =JSON.parse(req.body.data);

        var errors = _p.checkValidationErrors(req);
        if(errors) {
            res.status(HTTPStatus.BAD_REQUEST);
            res.json({
                message: errors
            });
        }
        else {
            var modelInfo = utilityHelper.sanitizeUserInput(req, next);
            var _slogVal = utilityHelper.getCleanURL(modelInfo.documentTitle, next);
            if(req.documentInfo.urlSlog !== _slogVal) {
                var query = {};
                query.urlSlog = _slogVal;
                query.deleted = false;

                dataProviderHelper.checkForDuplicateEntry(Document, query)
                    .then((count) => {
                    if(count > 0) {
                        return new Promise.CancellationError('{ "statusCode":"' + HTTPStatus.CONFLICT + '", "message": "' + messageConfig.document.alreadyExistsDocument + '" }')
                    }
                    else {
                        return _p.updateDocumentFunc(req, res, modelInfo, _slogVal, next);
                    }
                    })
                    .then(() => {
                    res.status(HTTPStatus.OK);
                    res.json({
                        message: messageConfig.document.updateMessageDocument
                    });
                    })
                    .catch(Promise.CancellationError, (cancellationErr) => {
                        errorHelper.customErrorResponse(res, cancellationErr, next);
                    })
                    .catch((err) => {
                    return next(err);
                    })
            }
            else {
                return _p.updateDocumentFunc(req, res, modelInfo, _slogVal, next);
            }
        }

        };

    _p.updateDocumentFunc = (req, res, modelInfo, _slogVal, next) => {
        var fileDataObj = {
            documentName: req.documentInfo.documentName,
            docProperties: {
                documentMimeType: req.documentInfo.docProperties.documentMimeType,
                docPath: req.documentInfo.docProperties.docPath
            }
        };
        var fileInfo = utilityHelper.getDocumentFileInfo(req, fileDataObj, next);
        var contentInfo = {};
        contentInfo.documentDescription = req.body.documentDescription;
        var modelHtmlInfo = utilityHelper.sanitizeUserHtmlBodyInput(contentInfo, next);

        req.documentInfo.documentTitle = modelInfo.documentTitle;
        req.documentInfo.urlSlog = _slogVal;
        req.documentInfo.categoryId = modelInfo.categoryId;
        req.documentInfo.documentDescription = modelHtmlInfo.documentDescription;
        req.documentInfo.documentAltText = modelInfo.documentAltText;
        req.documentInfo.active = modelInfo.active;
        req.documentInfo.updatedBy = req.decoded.user.username;
        req.documentInfo.updatedOn = new Date();

        req.documentInfo.documentName = fileInfo._documentName;
        req.documentInfo.docProperties.documentMimeType = fileInfo._documentMimeType;
        req.documentInfo.docProperties.docPath = fileInfo._documentPath;

        return dataProviderHelper.save(req.documentInfo);
    };

    _p.deleteDocument = (req, res, next) => {
        req.documentInfo.deleted = true;
        req.documentInfo.deletedOn = new Date();
        req.documentInfo.deletedBy = req.decoded.user.username;

        dataProviderHelper.save(req.documentInfo)
            .then(() => {
            res.status(HTTPStatus.OK);
            res.json({
                message: messageConfig.document.deleteMessageDocument
            });
            })
            .catch((err) => {
            return next(err);
            })
    };

    return {
        postDocumentCategory: _p.postDocumentCategory,
        getDocumentCategories: _p.getDocumentCategories,
        getDocumentCategoryById: _p.getDocumentCategoryById,
        updateDocumentCategory: _p.updateDocumentCategory,
        deleteDocumentCategory: _p.deleteDocumentCategory,
        postDocument: _p.postDocument,
        getDocuments: _p.getDocuments,
        getDocumentById: _p.getDocumentById,
        getDocumentByCategory: _p.getDocumentByCategory,
        updateDocument: _p.updateDocument,
        deleteDocument: _p.deleteDocument
    };

})();

module.exports= documentManagementController;