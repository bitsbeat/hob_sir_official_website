/**
 * Created by lakhe on 11/24/16.
 */

'use strict';

var newsLetterController = (function () {

    var NewsLetter = require('../models/newsletter.server.model'),
        HTTPStatus = require('http-status'),
        messageConfig = require('../configs/api.message.config'),
        dataProviderHelper = require('../data/mongo.provider.helper'),
        emailTemplateConfigs = require('../configs/email.template.config'),
        mailHelper = require('../helpers/mail.helper'),
        errorHelper = require('../helpers/error.helper'),
        emailTemplateController = require('./email.template.server.controller'),
        utilityHelper = require('../helpers/utilities.helper'),
        Promise = require("bluebird");

    var documentFields = '_id email subscribed addedOn';

    function NewsLetterModule() {
    }

    NewsLetterModule.CreateNewsLetter = function (newsLetterObj) {
        var newsLetterInfo = new NewsLetter();
        newsLetterInfo.email = newsLetterObj.email;
        newsLetterInfo.subscribed = true;
        newsLetterInfo.addedBy = 'system';
        newsLetterInfo.addedOn = new Date();
        return newsLetterInfo;
    };

    var _p = NewsLetterModule.prototype;

    _p.checkValidationErrors = function (req) {
        req.checkBody('email', messageConfig.newsletter.validationErrMessage.email).notEmpty();
        req.checkBody('email', messageConfig.newsletter.validationErrMessage.emailValid).isEmail();

        return req.validationErrors();
    };

    _p.getAllNewsLetterSubscribedUsers = function (req, next) {
        var pagerOpts = null,
            sortOpts = {addedOn: -1},
            query = {};
        if (req.query && req.query.subscribed) {
            query.subscribed = req.query.subscribed;
            pagerOpts = utilityHelper.getPaginationOpts(req, next);
            return dataProviderHelper.getAllWithDocumentFieldsPagination(NewsLetter, query, pagerOpts, documentFields, sortOpts);
        }
        else if(req.params && req.params.subscribed) {
            query.subscribed = req.params.subscribed;
            documentFields = "email";
            
            return dataProviderHelper.getAllWithDocumentFieldsNoPagination(NewsLetter, query, documentFields, sortOpts);
        }

    };

    _p.getNewsLetterSubscribedUserInfoById = function (req) {
        return dataProviderHelper.findById(NewsLetter, req.params.newsletterId, documentFields);
    };

    _p.unSubscribeNewsletter = function (req, res, next) {

        var queryOpts = {
            _id: req.params.newsletterId
        };
        var updateOpts = {
            subscribed: false,
            unSubscribedDate: new Date()
        };
        return _p.updateNewsletterSubscription(queryOpts, updateOpts)
            .then(function () {
                res.status(HTTPStatus.OK);
                res.json({
                    message: messageConfig.newsletter.unSubscribedMessage
                });
                return _p.getNewsLetterSubscribedUserInfoById(req);
            })
            .then(function (newsLetterInfo) {
                return _p.sendEmailToUnSubscribedUser(req, newsLetterInfo, next);
            })
            .then(function () {
                console.log('email sent');
            })
            .catch(function (err) {
                return next(err);
            });
    };

    _p.subscribeNewsletter = function (req, res, next) {
        var errors = _p.checkValidationErrors(req);
        if (errors) {
            res.status(HTTPStatus.BAD_REQUEST);
            res.json({
                message: errors
            });
        } else {
            var modelInfo = utilityHelper.sanitizeUserInput(req, next);
            var newsLetterInfo = NewsLetterModule.CreateNewsLetter(modelInfo);
            var _query = {
                email: modelInfo.email
            };
            dataProviderHelper.getAllWithoutDocumentFieldsNoPagination(NewsLetter, _query)
                .then(function (newsletterData) {
                    if (newsletterData && newsletterData.length > 0) {
                        if (newsletterData[0].subscribed === true) {
                            throw new Promise.CancellationError('{ "statusCode":"' + HTTPStatus.CONFLICT + '", "message": "' + messageConfig.newsletter.alreadyExists + '"}');
                        } else {
                            var queryOpts = {
                                _id: newsletterData[0]._id
                            };
                            var updateOpts = {
                                subscribed: true,
                                unSubscribedDate: ""
                            };
                            return _p.updateNewsletterSubscription(queryOpts, updateOpts);
                        }
                    } else {
                        return dataProviderHelper.save(newsLetterInfo);
                    }
                })
                .then(function () {
                    res.status(HTTPStatus.OK);
                    res.json({
                        message: messageConfig.newsletter.saveMessage
                    });
                    return _p.sendEmailToSubscribedUser(req, newsLetterInfo, next);
                })
                .then(function () {
                    console.log('email sent');
                })
                .catch(Promise.CancellationError, function (cancellationErr) {
                    errorHelper.customErrorResponse(res, cancellationErr, next);
                })
                .catch(function (err) {
                    return next(err);
                });
        }
    };

    _p.updateNewsletterSubscription = function (queryOpts, updateOpts) {

        var multiOpts = false;

        return new Promise(function (resolve, reject) {
            dataProviderHelper.updateModelData(NewsLetter, queryOpts, updateOpts, multiOpts)
                .then(function () {
                    resolve();
                })
                .catch(function (err) {
                    reject(err);
                });
        });
    };

    _p.sendEmailToSubscribedUser = function (req, newsLetterInfo, next) {
        req.params.templateId = emailTemplateConfigs.newsletterSubscription;
        return new Promise(function (resolve, reject) {
            emailTemplateController.getEmailTemplateDataByID(req)
                .then(function (emailTemplate) {
                    var messageBody = '';
                    if (emailTemplate) {
                        if (emailTemplate.templateBody) {
                            var _url = req.protocol + '://' + req.hostname + '/api/newsletter/unsubscribe/' + newsLetterInfo._id;
                            if (emailTemplate.templateBody.indexOf("{{ interestedClient.unsubscribedURL }}") > -1) {
                                messageBody = emailTemplate.templateBody.replace('{{ interestedClient.unsubscribedURL }}', _url);
                            }
                            else
                                messageBody = emailTemplate.templateBody;
                        }
                        var mailOptions = {
                            fromEmail: "DAV MBA Business School" + "<" + emailTemplate.emailFrom + ">", // sender address
                            toEmail: newsLetterInfo.email, // list of receivers
                            subject: emailTemplate.emailSubject, // Subject line
                            textMessage: messageBody, // plaintext body
                            htmlTemplateMessage: messageBody
                        };

                        return mailHelper.sendEmailWithNoAttachment(req, mailOptions, next);
                    } else {
                        return;
                    }
                })
                .then(function (data) {
                    resolve(data);
                })
                .catch(function (err) {
                    reject(err);
                });
        });
    };

    _p.sendEmailToUnSubscribedUser = function (req, newsLetterInfo, next) {
        var userEmail = newsLetterInfo.email;
        req.params.templateId = emailTemplateConfigs.newsletterUnSubscription;

        return new Promise(function (resolve, reject) {
            emailTemplateController.getEmailTemplateDataByID(req)
                .then(function (emailTemplate) {
                    var messageBody = '';
                    if (emailTemplate) {
                        messageBody = emailTemplate.templateBody;
                        var mailOptions = {
                            fromEmail: emailTemplate.emailFrom, // sender address
                            toEmail: userEmail, // list of receivers
                            subject: emailTemplate.emailSubject, // Subject line
                            textMessage: messageBody, // plaintext body
                            htmlTemplateMessage: messageBody
                        };
                        return mailHelper.sendEmailWithNoAttachment(req, mailOptions, next);
                    } else {
                        return;
                    }
                })
                .then(function (data) {
                    resolve(data);
                })
                .catch(function (err) {
                    reject(err);
                });
        });
    };

    return {
        getAllNewsLetterSubscribedUsers: _p.getAllNewsLetterSubscribedUsers,
        unSubscribeNewsletter: _p.unSubscribeNewsletter,
        subscribeNewsletter: _p.subscribeNewsletter
    };
})();

module.exports = newsLetterController;