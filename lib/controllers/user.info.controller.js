var userInfoController = (function() {

  'use strict'

const UserInfo = require('../models/user.info');
const Promise = require('bluebird');
const config = require('../configs/database.config');
const collect = require('../helpers/collect.helper');
const utilityHelper = require('../helpers/utilities.helper');
const emailTemplateConfigs = require('../configs/email.template.config');
const emailTemplateController = require('./email.template.server.controller');
const mailHelper = require('../helpers/mail.helper');
const dataProviderHelper = require('../data/mongo.provider.helper');
const HTTPStatus = require('http-status');
const join = Promise.join();


    function UserInfoModule() {}

    var _p = UserInfoModule.prototype;

    _p.collectToRegister = (req,res,next)=>{
    let collectInstance = new collect();
    collectInstance.setBody([
      'firstName',
      'middleName',
      'lastName',
      'dateOfBirth',
      'nationality',
      'gender',
      'country',
      'district',
      'municipalityVdc',
      'wardNo',
      'email'
    ])



    collectInstance.setMandatoryFields({
      firstName: 'First name not provided',
      lastName: 'Last name not provided',
      dateOfBirth: 'Date of birth not provided',
      nationality: 'Nationality not provided',
      gender: 'Gender not provided',
      country: 'Country not provided',
      email: 'Email not provided'
    })

    collectInstance.collect(req).then((data)=>{
      req.userData = data
      next();
    }).catch((err)=>{
      err.status = 400
      next(err)
    })
  };


  _p.register = (req,res,next)=>{
    return new Promise((resolve,reject)=> {
        let newUser = new UserInfo(req.userData);
        if (req.body.qualification) {
            newUser.qualification = req.body.qualification
        }

        if (req.body.parent) {
            newUser.parent = req.body.parent
        }

        UserInfo.findOne({"isDeleted": false})
            .sort({userId: -1})
            .then((user) => {
            if(user !== null) {
                var prefixVal = new Date().getFullYear().toString().substr(1, 3);
                var n = user.userId + 1;

                switch (n.toString().length) {
                    case 1:
                        newUser.admissionFormNo = prefixVal + '-' + '0000' + n;
                        break;
                    case 2:
                        newUser.admissionFormNo = prefixVal + '-' + '000' + n;
                        break;
                    case 3:
                        newUser.admissionFormNo = prefixVal + '-' + '00' + n;
                        break;
                    case 4:
                        newUser.admissionFormNo = prefixVal + '-' + '0' + n;
                        break;
                    case 5:
                        newUser.admissionFormNo = prefixVal + '-' + n;
                        break;
                    default:
                        newUser.admissionFormNo = null;
                }
            }
                else {
                    newUser.admissionFormNo = (new Date().getFullYear().toString().substr(1, 3)) + '-' + '00001';
                }
            })
            .then(() => {
                let date = new Date();

                newUser.addedOn = date;
                newUser.updatedOn = date;

                dataProviderHelper.save(newUser)
                    .then(() => {
                        res.status(HTTPStatus.OK);
                        res.json({
                            message: "Admission form submitted successfully"
                        });
                        return join(
                            _p.sendEmailToSiteAdmin(req, newUser, next),
                            _p.sendEmailToApplicant(req, newUser, next)
                        );
                    })
                    .catch((err) => {
                        return next(err);
                    })
            })
            .catch((err) => {
                return next(err);
            })
    })
  };

  _p.retrieve = (req,res,next)=>{
      var pagerOpts = utilityHelper.getPaginationOpts(req, next);
    UserInfo.find({"isDeleted":false})
        .sort({addedOn: -1})
        .skip(pagerOpts.perPage * (pagerOpts.page-1))
        .limit(pagerOpts.perPage)
        .exec()
        .then(data => {
            req.cdata = {
                Message: "Here are the list of user's information",
                data
            }
            return UserInfo.count({"isDeleted": false}).exec()
        }).then(total => {
            req.cdata.totalItems = total
            req.cdata.page = pagerOpts.page;
            req.cdata.perPage = pagerOpts.perPage;

            next();
        })
        .catch(err=> next(err))
  };

  _p.update = (req,res,next)=>{
    UserInfo.findOneAndUpdate({'_id': req.params.id},{$set: req.userData},{new:true},(err,user)=>{
      if(err){
        return next(err);
      }

      user.qualification = req.body.qualification;
      user.parent = req.body.parent;

        user.updatedOn = new Date();

      user.save((err,user)=>{
        if(err){
          return next(err);
        }
          req.cdata = {
              Success: 1,
              Message: "Admission Information updated successfully",
              user
          }
          next();
      })
    })
  };

    _p.adminUpdate = (req,res,next) => {
      let id = req.params.id;

      UserInfo.findById(id, (err,user) => {
        if(err) {
          return next(err);
        }

        user.admissionStatus = req.body.admissionStatus;
        user.remarks = req.body.remarks;

          dataProviderHelper.save(user)
              .then(() => {
                res.status(HTTPStatus.OK);
                res.json({
                    message: "Admission status and remarks updated successfully"
                });
                return join(_p.sendUpdateEmailToApplicant(req, user, next));
              })
              .catch((err) => {
                return next(err);
              });
        })
    };

    _p.delete = (req,res,next)=>{
      UserInfo.findOneAndUpdate({'_id':req.params.id},{$set: {"isDeleted":1}},{new:false},(err,user)=>{
        if(err){
          return next(err);
        }

        user.deletedOn = new Date();

        user.save((err, user) => {
          if(err) {
              return next(err);
          }
            req.cdata = {
            Success: 1,
            Messsage: "Admission information deleted successfully"
          }
          next();
         })
      })
    };

    _p.find_id = (req,res,next)=>{

      let id = req.params.id;

      UserInfo.findById(id,(err,result)=>{
        if(err){
          return next(err);
        }
        else {
          req.cdata = {
            result
          }
          next();
        }
      })
    };


    _p.sendEmailToSiteAdmin = (req, userInfo, next) => {
        req.params.templateId = emailTemplateConfigs.admissionInfoEmailToSiteAdmin;

        return new Promise((resolve, reject) => {
            emailTemplateController.getEmailTemplateDataByID(req)
                .then((emailTemplate) => {
                    var messageBody = '';
                    if(emailTemplate) {
                        if(emailTemplate.templateBody) {
                            messageBody = emailTemplate.templateBody;
                            if(messageBody.indexOf("{{Applicant.firstName}}") > -1) {
                                messageBody = messageBody.replace('{{Applicant.firstName}}', userInfo.firstName);
                            }
                            if(messageBody.indexOf("{{Applicant.middleName}}") > -1) {
                                messageBody = messageBody.replace('{{Applicant.middleName}}', userInfo.middleName);
                            }
                            if(messageBody.indexOf("{{Applicant.lastName}}") > -1) {
                                messageBody = messageBody.replace('{{Applicant.lastName}}', userInfo.lastName);
                            }
                            if(messageBody.indexOf("{{Applicant.email}}") > -1) {
                                messageBody = messageBody.replace('{{Applicant.email}}', userInfo.email);
                            }
                        }

                        var mailOptions = {
                            fromEmail: userInfo.email, // sender address
                            toEmail: emailTemplate.emailFrom, // list of receivers
                            subject: emailTemplate.emailSubject, // Subject line
                            textMessage: messageBody, // plaintext body
                            htmlTemplateMessage: messageBody
                        };

                        return mailHelper.sendEmailWithNoAttachment(req, mailOptions, next);
                    }
                    else {
                        return;
                    }
                })
                .then((data) => {
                    resolve(data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    };

    _p.sendEmailToApplicant = (req, userInfo, next) => {
    req.params.templateId = emailTemplateConfigs.emailResponseToApplicant;

    return new Promise((resolve, reject) => {
        emailTemplateController.getEmailTemplateDataByID(req)
            .then((emailTemplate) => {
                var messageBody = '';

                if(emailTemplate) {
                    messageBody = emailTemplate.templateBody;

                    if(messageBody.indexOf("{{Applicant.firstName}}") > -1) {
                        messageBody = messageBody.replace('{{Applicant.firstName}}', userInfo.firstName);
                    }

                    var mailOptions = {
                        fromEmail: emailTemplate.emailFrom, // sender address
                        toEmail: userInfo.email, // list of receivers
                        subject: emailTemplate.emailSubject, // Subject line
                        textMessage: messageBody, // plaintext body
                        htmlTemplateMessage: messageBody
                    };

                    return mailHelper.sendEmailWithNoAttachment(req, mailOptions, next);
                }
                else {
                    return;
                }
            })
            .then((data) => {
                resolve(data);
            })
            .catch((err) => {
                reject(err);
            });
    });
};

    _p.sendUpdateEmailToApplicant = (req, userInfo, next) => {
      req.params.templateId = emailTemplateConfigs.updateInfoToApplicant;

      return new Promise((resolve, reject) => {
        emailTemplateController.getEmailTemplateDataByID(req)
            .then((emailTemplate) => {
              var messageBody = '';

              if(emailTemplate) {
                messageBody = emailTemplate.templateBody;

                if(messageBody.indexOf("{{Applicant.firstName}}") > -1) {
                  messageBody = messageBody.replace('{{Applicant.firstName}}', userInfo.firstName);
                }
                if(messageBody.indexOf("{{Applicant.admissionStatus}}") > -1) {
                  messageBody = messageBody.replace('{{Applicant.admissionStatus}}', userInfo.admissionStatus);
                }
                if(messageBody.indexOf("{{Applicant.remarks}}") > -1) {
                  messageBody = messageBody.replace('{{Applicant.remarks}}', userInfo.remarks);
                }

                var mailOptions = {
                    fromEmail: emailTemplate.emailFrom, // sender address
                    toEmail: userInfo.email, // list of receivers
                    subject: emailTemplate.emailSubject, // Subject line
                    textMessage: messageBody, // plaintext body
                    htmlTemplateMessage: messageBody
                };

                return mailHelper.sendEmailWithNoAttachment(req, mailOptions, next);
              }
              else {
                return;
              }
            })
            .then((data) => {
              resolve(data);
            })
            .catch((err) => {
              reject(err);
            });
      })
    };

    return {
        collectToRegister: _p.collectToRegister,
        register: _p.register,
        retrieve: _p.retrieve,
        update: _p.update,
        adminUpdate: _p.adminUpdate,
        delete: _p.delete,
        find_id: _p.find_id
    }


})();

module.exports = userInfoController;
