let videoController = (() => {
'use strict';

let HTTPStatus = require('http-status'),
    messageConfig = require('../configs/api.message.config'),
    dataProviderHelper = require('../data/mongo.provider.helper'),
    Video = require('../models/video.server.model'),
    videoModel = Video.Video,
    utilityHelper = require('../helpers/utilities.helper'),
    errorHelper = require('../helpers/error.helper'),
    Promise = require('bluebird');

let documentFields = '_id title videoUrl urlSlog active';

function VideoModule() {} 

VideoModule.createVideo = (videoObj, _slogVal, loggedInUser) => {
    let videoInfo = new videoModel();
    videoInfo.title = videoObj.title;
    videoInfo.urlSlog = _slogVal;
    videoInfo.videoUrl = videoObj.videoUrl;
    videoInfo.active = videoObj.active;
    videoInfo.addedOn = new Date();
    videoInfo.addedBy = loggedInUser;

    return videoInfo;
}

let _p = VideoModule.prototype;

_p.checkValidationErrors = (req) => {
    req.checkBody('title', messageConfig.videoGallery.validationErrMessage.title).notEmpty();
    req.checkBody('videoUrl', messageConfig.videoGallery.validationErrMessage.videoUrl).notEmpty();

    return req.validationErrors();
};

_p.postVideo = (req, res, next) => {
    let errors = _p.checkValidationErrors(req);
    
    if(errors) {
        res.status(HTTPStatus.BAD_REQUEST);
        res.json({
            message: errors
        });
    } 
    else {
        let modelInfo = utilityHelper.sanitizeUserInput(req, next);
        let _slogVal = utilityHelper.getCleanURL(modelInfo.title, next);
        let query = {};
        query.urlSlog = _slogVal;
        query.deleted = false;

        dataProviderHelper.checkForDuplicateEntry(videoModel, query)
            .then((count) => {
                if(count>0) {
                    throw new Promise.CancellationError('{ "statusCode":"' + HTTPStatus.CONFLICT + '", "message": "' + messageConfig.videoGallery.alreadyExists + '"}')
                }
                else {
                    let createVideoObj = VideoModule.createVideo(modelInfo, _slogVal, req.decoded.user.username);
                    return dataProviderHelper.save(createVideoObj)
                    .then(() => {
                        res.status(HTTPStatus.OK);
                        res.json({
                            message: messageConfig.videoGallery.saveMessage
                        });
                    })
                    .catch((err) => {
                        return next(err);
                    })
              }
            })
            .catch(Promise.CancellationError, function (cancellationErr) {
                errorHelper.customErrorResponse(res, cancellationErr, next);
            })
            .catch((err) => {
                return next(err);
            })
    }
};

_p.getAllVideo = (req, res, next) => {
    let pagerOpts = utilityHelper.getPaginationOpts(req, next);
    let query = {};
    let sortOpts = {addedOn: -1};

    if(req.query.title) {
        query.title = {$regex: new RegExp('.*' + req.query.title, "i")};
    }
    if(req.query.active) {
        query.active = true;
    }
    query.deleted = false;
    return dataProviderHelper.getAllWithDocumentFieldsPagination(videoModel, query, pagerOpts, documentFields, sortOpts)
        .then((video) => {
            if(video){
                res.status(HTTPStatus.OK);
                res.json(video);
            }
            else{
                res.status(HTTPStatus.NOT_FOUND);
                res.json({
                    message: messageConfig.videoGallery.notFoundMessage
                });
            }
        })
        .catch((err) => {
            return next(err);
        })
};

_p.getVideoById = (req) => {
    let query = {
        '_id': req.params.videoId
    };
    return dataProviderHelper.findOne(videoModel, query, documentFields);
}

_p.updateVideo = (req, res, next) => {
    let errors = _p.checkValidationErrors(req);
    if(errors) {
        res.status(HTTPStatus.OK);
        res.json({
            message: errors
        });
    }
    else {
        let modelInfo = utilityHelper.sanitizeUserInput(req, next);
        let _slogVal = utilityHelper.getCleanURL(modelInfo.title, next);

        if(req.videoDetail.urlSlog !== _slogVal) {
            let query = {};

            query.urlSlog = _slogVal;
            query.deleted = false;

            dataProviderHelper.checkForDuplicateEntry(videoModel, query)
                .then((count) => {
                    if(count > 0 ) {
                        throw new Promise.CancellationError('{ "statusCode":"' + HTTPStatus.CONFLICT + '", "message": "' + "Video already exists" + '"}');
                    }
                    else {
                        _p.updateVideoFunction(req, res, modelInfo, _slogVal)
                            .then(() => {
                                res.status(HTTPStatus.OK);
                                res.json({
                                    message: "Video updated successfully"
                                });
                            })
                            .catch((err) => {
                                return next(err);
                            })
                    }
                })
                .catch(Promise.CancellationError, (cancellationErr) => {
                    errorHelper.customErrorResponse(res, cancellationErr, next);
                })
                .catch((err) => {
                    return next(err);
                });
        }
        else {
            _p.updateVideoFunction(req, res, modelInfo, _slogVal, next)
                .then(() => {
                    res.status(HTTPStatus.OK);
                    res.json({
                        message: "Video updated successfully"
                    });
                })
                .catch((err) => {
                    return next(err);
                })
        }
    }
};

_p.updateVideoFunction = (req, res, modelInfo, _slogVal) => {
    req.videoDetail.title = modelInfo.title;
    req.videoDetail.urlSlog = _slogVal;
    req.videoDetail.videoUrl = modelInfo.videoUrl;
    req.videoDetail.updatedOn = new Date();
    req.videoDetail.updatedBy = req.decoded.user.username;
    req.videoDetail.active = modelInfo.active;

    return dataProviderHelper.save(req.videoDetail);
};

_p.deleteVideo = (req, res, next) => {
    req.videoDetail.deleted = true;
    req.videoDetail.deletedOn = new Date();
    req.videoDetail.deletedBy = req.decoded.user.username;
    dataProviderHelper.save(req.videoDetail)
        .then(() => {
            res.status(HTTPStatus.OK);
            res.json({
                message: messageConfig.videoGallery.deletedMessage
            });
        })
        .catch((err) => {
            return next(err);
        })
}

return {
    postVideoGallery: _p.postVideo,
    getAllVideoGallery: _p.getAllVideo,
    getVideoById: _p.getVideoById,
    updateVideo: _p.updateVideo,
    deleteVideo: _p.deleteVideo
};

})();

module.exports = videoController;