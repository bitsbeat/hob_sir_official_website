var imageSliderController = (function () {

    'use strict';

    var dataProviderHelper = require('../data/mongo.provider.helper'),
        HTTPStatus = require('http-status'),
        messageConfig = require('../configs/api.message.config'),
        AdmissionOverlay = require('../models/admission.overlay.server.model'),
        utilityHelper = require('../helpers/utilities.helper'),
        Promise = require("bluebird");

    var documentFields = '_id imageName imageTitle imageAltText imageHeight imageWidth active';

    function AdmissionOverlayModule() {}

    AdmissionOverlayModule.CreateAdmissionOverlay = function (admissionOverlayObj, modelHtmlContentInfo, loggedInUser, imageInfo) {
        var admissionOverlayInfo = new AdmissionOverlay();
        admissionOverlayInfo.imageName = imageInfo._imageName;
        admissionOverlayInfo.imageTitle = admissionOverlayObj.imageTitle;
        admissionOverlayInfo.imageAltText = admissionOverlayObj.imageAltText;
        admissionOverlayInfo.active = admissionOverlayObj.active;
        admissionOverlayInfo.imageHeight = admissionOverlayObj.imageHeight;
        admissionOverlayInfo.imageWidth = admissionOverlayObj.imageWidth;
        admissionOverlayInfo.imageProperties = {
            imageExtension: imageInfo._imageExtension,
            imagePath: imageInfo._imagePath
        };
        admissionOverlayInfo.addedBy = loggedInUser;
        admissionOverlayInfo.addedOn = new Date();
        return admissionOverlayInfo;
    };

    var _p = AdmissionOverlayModule.prototype;

    _p.getAdmissionOverlayDetail = function (req, next) {
        var pagerOpts = utilityHelper.getPaginationOpts(req, next);
        var query = {};
        if (req.query.active) {
            query.active = true;
        }
        query.deleted = false;
        var sortOpts = {addedOn: -1}; 
        return dataProviderHelper.getAllWithDocumentFieldsPagination(AdmissionOverlay, query, pagerOpts, documentFields, sortOpts);
    };

    _p.getAdmissionOverlayById = function (req) {

        var selectFields = documentFields + ' imageProperties';

        return dataProviderHelper.findById(AdmissionOverlay, req.params.admissionOverlayId, selectFields);
    };

    _p.patchAdmissionOverlayImage = function (req, res, next) {
        req.admissionOverlayInfo.deleted = true;
        req.admissionOverlayInfo.deletedOn = new Date();
        req.admissionOverlayInfo.deletedBy = req.decoded.user.username;
        _p.saveFunc(req, res, req.admissionOverlayInfo, next, messageConfig.admissionOverlay.deleteMessage);
    };

    _p.postAdmissionOverlayImage = function (req, res, next) {
        req.body = JSON.parse(req.body.data);
        var imageInfo = utilityHelper.getFileInfo(req, null, next);
        if (imageInfo._imageName) {
            var modelInfo = utilityHelper.sanitizeUserInput(req, next);
            var htmlContentInfo = {};
            var modelHtmlContentInfo = utilityHelper.sanitizeUserHtmlBodyInput(htmlContentInfo, next);

            var admissionOverlayInfo = AdmissionOverlayModule.CreateAdmissionOverlay(modelInfo, modelHtmlContentInfo, req.decoded.user.username, imageInfo);

            _p.saveFunc(req, res, admissionOverlayInfo, next, messageConfig.admissionOverlay.saveMessage);
        } else {
            res.status(HTTPStatus.BAD_REQUEST);
            res.json({
                message: messageConfig.admissionOverlay.fieldRequired
            });
        }
    };

    _p.saveFunc = function (req, res, newAdmissionOverlayInfo, next, msg) {
        dataProviderHelper.save(newAdmissionOverlayInfo)
            .then(function () {
                res.status(HTTPStatus.OK);
                res.json({
                    message: msg
                });
            })
            .catch(function (err) {
                return next(err);
            });
    };
    _p.updateAdmissionOverlayImage = function (req, res, next) {
        var imageInfo = utilityHelper.getFileInfo(req, req.admissionOverlayInfo, next);
        if (imageInfo._imageName !== "") {
            req.body = JSON.parse(req.body.data);
            var modelInfo = utilityHelper.sanitizeUserInput(req, next);
            var htmlContentInfo = {};
            htmlContentInfo.imagePrimaryContent = req.body.imagePrimaryContent;
            htmlContentInfo.imageSecondaryContent = req.body.imageSecondaryContent;
            var modelHtmlContentInfo = utilityHelper.sanitizeUserHtmlBodyInput(htmlContentInfo, next);

            req.admissionOverlayInfo.imageTitle = modelInfo.imageTitle;
            req.admissionOverlayInfo.imageAltText = modelInfo.imageAltText;
            req.admissionOverlayInfo.active = modelInfo.active;
            req.admissionOverlayInfo.imageName = imageInfo._imageName;
            req.admissionOverlayInfo.imageHeight = modelInfo.imageHeight;
            req.admissionOverlayInfo.imageWidth = modelInfo.imageWidth;
            req.admissionOverlayInfo.imageProperties.imageExtension = imageInfo._imageExtension;
            req.admissionOverlayInfo.imageProperties.imagePath = imageInfo._imagePath;
            req.admissionOverlayInfo.updatedBy = req.decoded.user.username;
            req.admissionOverlayInfo.updatedOn = new Date();

            _p.saveFunc(req, res, req.admissionOverlayInfo, next, messageConfig.admissionOverlay.updateMessage);
        } else {
            res.status(HTTPStatus.BAD_REQUEST);
            res.json({
                message: messageConfig.admissionOverlay.fieldRequired
            });
        }
    };


    return {
        getAdmissionOverlayById     : _p.getAdmissionOverlayById,
        getAdmissionOverlayDetail   : _p.getAdmissionOverlayDetail,
        patchAdmissionOverlayImage  : _p.patchAdmissionOverlayImage,
        postAdmissionOverlayImage   : _p.postAdmissionOverlayImage,
        updateAdmissionOverlayImage : _p.updateAdmissionOverlayImage
    };

})();

module.exports = imageSliderController;