/*created on 24/07/17 by bishwo*/

var queriesController = (function(){
    'use strict';

    var dataProviderHelper      = require('../data/mongo.provider.helper'),
        HTTPStatus              = require('http-status'),
        messageConfig           = require('../configs/api.message.config'),
        QueriesModel            = require('../models/queries.server.model'),
        utilityHelper           = require('../helpers/utilities.helper'),
        errorHelper             = require('../helpers/error.helper'),
        Promise                 = require("bluebird"),
        emailTemplateController = require('./email.template.server.controller'),
        emailConfig             = require('../configs/email.template.config'),
        mailHelper              = require('../helpers/mail.helper'),
        collect                 = require('../helpers/collect.helper'),
        documentFields          = '_id name email query emailSent addedOn';
    function QueryModule(){}

        var _p = QueryModule.prototype;

        _p.collectToRegister = function (req, res, next) {
            let collectInstance = new collect();
            collectInstance.setBody([
                'name',
                'email',
                'query'
            ])

            collectInstance.setMandatoryFields({
                name  : 'Name not provided',
                email : 'Email not provided',
                query : 'Query not provided'
            })

            collectInstance.collect(req).then((data) => {
                req.providedFields = data
                next();
            }).catch((err) => {
                err.status = 400
                next(err)
            })
        }

        _p.getAllQueries = function(req, next){
            var pagerOpts = utilityHelper.getPaginationOpts(req, next),
                query     = { 'deleted' : false },
                sortOpts  = {};

            if(req.query.sortOption){
                sortOpts = req.query.sortOption
            }else{
                sortOpts = { addedOn: -1 }
            }

            return dataProviderHelper.getAllWithDocumentFieldsPagination(QueriesModel, query, pagerOpts, documentFields, sortOpts);
        };

        _p.getQueryByID = function(req,next){
            return dataProviderHelper.findById(QueriesModel, req.params.queryId, documentFields);
        };

        _p.postQuery = function(req,res,next){
            var queryModel = new QueriesModel(req.providedFields);
            dataProviderHelper.save(queryModel)
            .then(function(){
                    /*Send email here and update emailSent to true*/
                    if(queryModel.emailSent === false){
                        _p.sendEmailToEndUser( queryModel, next);
                        _p.sendEmailToAdmin( req, queryModel, next);
                        _p.updateQueryEmailSent( queryModel );
                    }
                })
                .then(function(){
                    res.status(HTTPStatus.OK);
                    res.json({
                        message: messageConfig.queries.saveMessage
                    });
                })
                .catch(Promise.CancellationError, function (cancellationErr) {
                    errorHelper.customErrorResponse(res, cancellationErr, next);
                })
                .catch(function(err){
                    return next(err);
                });

        };

        _p.sendEmailToEndUser = function (req, next){
            var request = {
                email  : req.email, 
                params : {
                    templateId : emailConfig.queryEmailToInterestedParties
                }
            };

            return new Promise(function(resolve, reject) {
                emailTemplateController.getEmailTemplateDataByID(request)
                .then(function(emailTemplate){
                    var messageBody = '';
                    if(emailTemplate){
                        messageBody     =  emailTemplate.templateBody;
                        var mailOptions = {
                            fromEmail           : emailTemplate.emailFrom, // sender address
                            toEmail             : [request.email], // list of receivers
                            subject             : emailTemplate.emailSubject, // Subject line
                            textMessage         : messageBody, // plaintext body
                            htmlTemplateMessage : messageBody
                        };
                        return mailHelper.sendEmailWithNoAttachment(request, mailOptions, next);
                    }else{
                        return;
                    }
                })
                .then(function (data) {
                    resolve(data);
                })
                .catch(function(err){
                    reject(err);
                });
            });
        }

        _p.updateQueryEmailSent = function(req){
            let queryData = { _id       : req._id},
            reqData       = { emailSent : true};

            return dataProviderHelper.updateModelData(QueriesModel,queryData,reqData, false);
        };

        _p.sendEmailToAdmin = function (req, queryInfo, next) {
            req.params.templateId = emailConfig.queryEmailToSiteAdmin;

            return new Promise(function(resolve, reject) {
                emailTemplateController.getEmailTemplateDataByID(req)
                .then(function(emailTemplate){
                    var messageBody = '';
                    if(emailTemplate){
                        if(emailTemplate.templateBody){
                            messageBody = emailTemplate.templateBody;
                            if(messageBody.indexOf("{{ interestedPeople.name }}") > -1) {
                                messageBody = messageBody.replace('{{ interestedPeople.name }}', queryInfo.fullName);
                            }
                            if(messageBody.indexOf("{{ interestedPeople.email }}") > -1) {
                                messageBody = messageBody.replace('{{ interestedPeople.email }}', queryInfo.email);
                            }
                            if(messageBody.indexOf("{{ interestedPeople.message }}") > -1) {
                                messageBody = messageBody.replace('{{ interestedPeople.message }}', queryInfo.message);
                            }
                        }

                        messageBody     =  emailTemplate.templateBody;
                        var mailOptions = {
                            fromEmail: queryInfo.email, // sender address
                            toEmail: [emailTemplate.emailFrom], // list of receivers
                            subject: emailTemplate.emailSubject, // Subject line
                            textMessage: messageBody, // plaintext body
                            htmlTemplateMessage: messageBody
                        };

                        return mailHelper.sendEmailWithNoAttachment(req, mailOptions, next);
                    }else{
                        return;
                    }
                })
                .then(function (data) {
                    resolve(data);
                })
                .catch(function(err){
                    reject(err);
                });
            });
        }

        _p.patchQuery = function(req, res, next){
                req.queryInfo.deleted = true;
                req.queryInfo.deletedOn = new Date();
                req.queryInfo.deletedBy = req.decoded.user.username;

                return dataProviderHelper.save(req.queryInfo)
                    .then(function () {
                        res.status(HTTPStatus.OK);
                        res.json({
                            message: messageConfig.queries.deleteMessage
                        });
                    })
                    .catch(function (err) {
                        return next(err);
                    });
     
        }

    return{
        collectToRegister       : _p.collectToRegister,
        getAllQueries           : _p.getAllQueries,
        getQueryByID            : _p.getQueryByID,
        postQuery               : _p.postQuery,
        updateQueryEmailSent    : _p.updateQueryEmailSent,
        sendEmailToAdmin        : _p.sendEmailToAdmin,
        patchQuery              : _p.patchQuery
    };

})();

module.exports = queriesController;