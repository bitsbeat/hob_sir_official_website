var courseManagementController = (function () {

    'use strict';

    var CourseModel             = require('../models/course.management.server.model'),
        HTTPStatus              = require('http-status'),
        messageConfig           = require('../configs/api.message.config'),
        dataProviderHelper      = require('../data/mongo.provider.helper'),
        utilityHelper           = require('../helpers/utilities.helper'),
        errorHelper             = require('../helpers/error.helper'),
        Promise                 = require("bluebird"),
        join                    = Promise.join,
        Semester                = CourseModel.Semester,
        Course                  = CourseModel.Course,
        Specialization          = CourseModel.Specialization,
        Program                 = CourseModel.Program;

    var semesterDropDownFields       = 'semesterName semesterCredit',
        courseDocumentFields         = 'courseCode courseName creditHours semesterName',
        specializationDropDownFields = 'name creditHours description',
        programFields                = 'programName totalCredit totalCourses';

    function CourseModule(){}
/*------------------------------------------ Create Model Section----------------------------------------*/

    CourseModule.CreateSemester = function (semesterObj) {
        var semInfo            = new Semester();
        semInfo.semesterName   = semesterObj.semesterName;
        semInfo.semesterCredit = semesterObj.semesterCredit;
        return semInfo;
    };

    CourseModule.CreateCourse = function (courseObj) {
        var courseInfo           = new Course();
        courseInfo.semesterName  = courseObj.semesterName;
        courseInfo.creditHours   = courseObj.creditHours;
        courseInfo.courseName    = courseObj.courseName;
        courseInfo.courseCode    = courseObj.courseCode;
        return courseInfo;
    };

    CourseModule.CreateSpecialization = function(specializationObj) {
        var specializationInfo          = new Specialization();
        specializationInfo.name         = specializationObj.name;
        specializationInfo.creditHours  = specializationObj.creditHours;
        specializationInfo.description  = specializationObj.description;
        return specializationInfo;
    }

    var _p = CourseModule.prototype;
/*------------------------------------------ Course Section----------------------------------------*/
    _p.getCourses = function( req, res, next) {
        var queryOpts = { 'semesterName' : req.params.id, deleted: false },
            sortField = {courseCode : 1};
        return dataProviderHelper.getAllWithDocumentFieldsNoPagination(Course, queryOpts, courseDocumentFields, sortField);
        
    };

    _p.getCourseById = function (req, res, next){
        return dataProviderHelper.findById(Course, req.params.id, courseDocumentFields);
    };

    _p.postCourse = function( req, res, next) {

         var courseParams = req.body,
            modelInfo      = utilityHelper.sanitizeUserInput(req, next)

        dataProviderHelper.checkForDuplicateEntry(Course, courseParams)
            .then(function(count){
                if(count > 0)
                    throw new Promise.CancellationError('{ "statusCode":"' + HTTPStatus.CONFLICT + '", "message": "' + messageConfig.course.alreadyExists + '"}');
            })
            .then(function(){
                var semObj = CourseModule.CreateCourse(modelInfo);
                return [dataProviderHelper.save(semObj)];
            })
            .then(function () {
                res.status(HTTPStatus.OK);
                res.json({
                    message: messageConfig.course.saveMessage
                });
            })
            .catch(Promise.CancellationError, function (cancellationErr) {
                errorHelper.customErrorResponse(res, cancellationErr, next);
            })
            .catch(function (err) {
                return next(err);
            });


    };

    _p.updateCourse = function( req, res, next) {
        var errors = _p.checkValidationErrors(req);
        if (errors) {
            res.status(HTTPStatus.BAD_REQUEST);
            res.json({
                message: errors
            });
        }else {
            var _id = {_id :req.params.id};
            var modelInfo = utilityHelper.sanitizeUserInput(req, next);

            dataProviderHelper.updateModelData(Course, _id, modelInfo, false)
            .then(function () {
                res.status(HTTPStatus.OK);
                res.json({
                    message: messageConfig.course.updateMessageCourse
                });
            })
            .catch(function (err) {
                return next(err);
            });

        }
    };

    _p.patchCourse = function( req, res, next) {
        req.body.deleted   = true;
        req.body.deletedOn = new Date();
        req.body.deletedBy = req.decoded.user.username;


        var _id = req.params.id;
        delete req.body._id;

        dataProviderHelper.removeModelData(Course,{ _id: _id})
            .then(function () {
                res.status(HTTPStatus.OK);
                res.json({
                    message: messageConfig.course.deleteMessage
                });
            })
            .catch(Promise.CancellationError, function (cancellationErr) {
                errorHelper.customErrorResponse(res, cancellationErr, next);
            })
            .catch(function (err) {
                return next(err);
            });
    };

/*------------------------------------------ Semester Section----------------------------------------*/

    _p.getSemesters = function( req, res, next) {
        var queryOpts = {deleted : false};
        dataProviderHelper.find(Semester, queryOpts, semesterDropDownFields)
        .then(function(data){
            res.status(HTTPStatus.OK);
            res.json({
                dataList: data
            });
        });
    };

    _p.postSemester = function( req, res, next) {
        var semesterParams = req.body,
            modelInfo      = utilityHelper.sanitizeUserInput(req, next),
            query          = {'semesterName' : modelInfo.semesterName};

        dataProviderHelper.checkForDuplicateEntry(Semester, query)
            .then(function(count){
                if(count > 0)
                    throw new Promise.CancellationError('{ "statusCode":"' + HTTPStatus.CONFLICT + '", "message": "' + messageConfig.course.semester.alreadyExists + '"}');
            })
            .then(function(){
                var semObj = CourseModule.CreateSemester(modelInfo);
                return [dataProviderHelper.save(semObj)];
            })
            .then(function () {
                res.status(HTTPStatus.OK);
                res.json({
                    message: messageConfig.course.semester.saveMessage
                });
            })
            .catch(Promise.CancellationError, function (cancellationErr) {
                errorHelper.customErrorResponse(res, cancellationErr, next);
            })
            .catch(function (err) {
                return next(err);
            });
    };

    _p.putSemester = function( req, res, next) {
        var errors = _p.checkSemesterValidationErrors(req);
        if (errors) {
            res.status(HTTPStatus.BAD_REQUEST);
            res.json({
                message: errors
            });
        }else {
            var _id = {_id :req.params.id};
            var modelInfo = utilityHelper.sanitizeUserInput(req, next);

            dataProviderHelper.updateModelData(Semester, _id, modelInfo, false)
            .then(function () {
                res.status(HTTPStatus.OK);
                res.json({
                    message: messageConfig.course.semester.updateMessageSemester
                });
            })
            .catch(function (err) {
                return next(err);
            });

        }
    };

    _p.getSemesterById = function (req, res, next){
        return dataProviderHelper.findById(Semester, req.params.id, semesterDropDownFields);
    };

    _p.patchSemester = function(req, res, next){
        req.body.deleted   = true;
        req.body.deletedOn = new Date();
        req.body.deletedBy = req.decoded.user.username;


        var _id = req.params.id;
        delete req.body._id;

        dataProviderHelper.updateModelData(Semester,{ _id: _id}, req.body, false)
            .then(function () {Specialization
                res.status(HTTPStatus.OK);
                res.json({
                    message: messageConfig.course.semester.deleteMessage
                });
            })
            .catch(Promise.CancellationError, function (cancellationErr) {
                errorHelper.customErrorResponse(res, cancellationErr, next);
            })
            .catch(function (err) {
                return next(err);
            });
    }

/*------------------------------------------ Specialization Section----------------------------------------*/
    _p.getSpecializations = function( req, res, next) {
        var queryOpts = {deleted : false};
        dataProviderHelper.find(Specialization, queryOpts, specializationDropDownFields)
        .then(function(data){
            res.status(HTTPStatus.OK);
            res.json({
                dataList: data
            });
        });
    };

    _p.postSpecialization = function( req, res, next) {
        var specializationParams = req.body,
            modelInfo      = utilityHelper.sanitizeUserInput(req, next),
            query          = {'name' : modelInfo.name};

        dataProviderHelper.checkForDuplicateEntry(Specialization, query)
            .then(function(count){
                if(count > 0)
                    throw new Promise.CancellationError('{ "statusCode":"' + HTTPStatus.CONFLICT + '", "message": "' + messageConfig.course.specialization.alreadyExists + '"}');
            })
            .then(function(){
                var semObj = CourseModule.CreateSpecialization(modelInfo);
                return [dataProviderHelper.save(semObj)];
            })
            .then(function () {
                res.status(HTTPStatus.OK);
                res.json({
                    message: messageConfig.course.specialization.saveMessage
                });
            })
            .catch(Promise.CancellationError, function (cancellationErr) {
                errorHelper.customErrorResponse(res, cancellationErr, next);
            })
            .catch(function (err) {
                return next(err);
            });
    };

    _p.putSpecialization = function( req, res, next) {
        var errors = _p.checkSpecializationValidationErrors(req);
        if (errors) {
            res.status(HTTPStatus.BAD_REQUEST);
            res.json({
                message: errors
            });
        }else {
            var _id = {_id :req.params.id};
            var modelInfo = utilityHelper.sanitizeUserInput(req, next);

            dataProviderHelper.updateModelData(Specialization, _id, modelInfo, false)
            .then(function () {
                res.status(HTTPStatus.OK);
                res.json({
                    message: messageConfig.course.specialization.updateMessageSpecialization
                });
            })
            .catch(function (err) {
                return next(err);
            });

        }
    };

    _p.getSpecializationById = function (req, res, next){
        return dataProviderHelper.findById(Specialization, req.params.id, specializationDropDownFields);
    };

    _p.patchSpecialization = function(req, res, next){
        req.body.deleted   = true;
        req.body.deletedOn = new Date();
        req.body.deletedBy = req.decoded.user.username;


        var _id = req.params.id;
        delete req.body._id;

        dataProviderHelper.updateModelData(Specialization,{ _id: _id}, req.body, false)
            .then(function () {Specialization
                res.status(HTTPStatus.OK);
                res.json({
                    message: messageConfig.course.specialization.deleteMessage
                });
            })
            .catch(Promise.CancellationError, function (cancellationErr) {
                errorHelper.customErrorResponse(res, cancellationErr, next);
            })
            .catch(function (err) {
                return next(err);
            });
    }

/*------------------------------------------ All Data ----------------------------------------*/

    _p.getAllData = function(req, res, next){
        var semQueryOpts     = {deleted : false},
            programQueryOpts = {},
            semesters = null;

        dataProviderHelper.findOne(Program, programQueryOpts, programFields)
        .then(function(programData){
            return [programData, _p.getSemesterData(req, next), _p.getSpecializationData(req,next)];
        })
        .spread(function(programData, semesters, specialization){
            var returnData = {
                program        : programData,
                semester       : semesters,
                specialization : specialization
            }
            res.status(HTTPStatus.OK);
            res.json(returnData);
        });

        // resume@cmsjob.com 
    }

    _p.getSemesterData = function(req, next){
        return new Promise(function(resolve, reject){
            Semester.aggregate([
                {
                    $lookup: {
                        from		 : 'Course',
                        localField   : 'semesterName',
                        foreignField : 'semesterName',
                        as			 : 'course'
                    }
                },
                { $sort: {semesterName : 1} },
                { $match : { deleted : false } }
                ], function(err, result){
                    if(err && result.length === 0)
                        errorHelper.customErrorResponse(res, cancellationErr, next);				
                    else{
                        resolve(result);
                    }
                });

        })
    }

    _p.getSpecializationData = function( req, next) {
        var queryOpts = {deleted : false};
        return new Promise(function(resolve, reject){
            dataProviderHelper.find(Specialization, queryOpts, specializationDropDownFields)
            .then(function(data){
                if(data)
                    resolve(data);
                else
                    resolve(null);
            });

        });
    };


/*------------------------------------------ Validation Check----------------------------------------*/
     _p.checkValidationErrors = function (req) {
        req.checkBody('courseName', messageConfig.course.validationErrMessage.courseName).notEmpty();
        req.checkBody('creditHours', messageConfig.course.validationErrMessage.creditHours).notEmpty();
        req.checkBody('courseCode', messageConfig.course.validationErrMessage.courseCode).notEmpty();
        return req.validationErrors();
    };

    _p.checkSemesterValidationErrors = function (req) {
        req.checkBody('semesterName', messageConfig.course.semester.validationErrMessage.semesterName).notEmpty();
        req.checkBody('semesterCredit', messageConfig.course.semester.validationErrMessage.semesterCredit).notEmpty();
        return req.validationErrors();
    };

    _p.checkSpecializationValidationErrors = function (req) {
        req.checkBody('name', messageConfig.course.semester.validationErrMessage.name).notEmpty();
        req.checkBody('creditHours', messageConfig.course.semester.validationErrMessage.credit).notEmpty();
        req.checkBody('description', messageConfig.course.semester.validationErrMessage.description).notEmpty();
        return req.validationErrors();
    };
    

    return{
        getCourses                    : _p.getCourses,
        postCourse                    : _p.postCourse,
        updateCourse                  : _p.updateCourse,
        patchCourse                   : _p.patchCourse,
        getCourseById                 : _p.getCourseById,

        getSemesters                  : _p.getSemesters,
        postSemester                  : _p.postSemester,
        putSemester                   : _p.putSemester,
        getSemesterById               : _p.getSemesterById,
        patchSemester                 : _p.patchSemester, 

        getSpecializations            : _p.getSpecializations,
        postSpecialization            : _p.postSpecialization,
        putSpecialization             : _p.putSpecialization,
        getSpecializationById         : _p.getSpecializationById,
        patchSpecialization           : _p.patchSpecialization,

        getAllData                    : _p.getAllData,
        getSemesterData               : _p.getSemesterData,

        checkValidationErrors         : _p.checkValidationErrors,
        checkSemesterValidationErrors : _p.checkSemesterValidationErrors

    };


})();

module.exports = courseManagementController;