let alumniController = function () {

    'use strict';

    const cloudinary         = require('cloudinary'),
          HTTPStatus         = require('http-status'),
          messageCofig       = require('../configs/api.message.config'),
          dataProviderHelper = require('../data/mongo.provider.helper'),
          alumniModel        = require('../models/alumni.model'),
          AlumniCategory     = alumniModel.AlumniCategory,
          Alumni             = alumniModel.Alumni,
          cloudinaryHelper   = require('../helpers/cloudinary.helper'),
          utilityHelper      = require('../helpers/utilities.helper'),
          errorHelper        = require('../helpers/error.helper'),
          Promise            = require('bluebird');

    let alumniCategoryFields = '_id year urlSlogCategory addedOn';
    let alumniFields         = '_id studentName urlSlog categoryId batchYear organizationName organizationPosition imageName imageProperties addedOn';

    function AlumniModel() {}

    AlumniModel.CreateAlumniCategory = (alumniCategoryObj, loggedInUser, _slogVal) => {
        let alumniCategoryInfo = new AlumniCategory();
        alumniCategoryInfo.year            = alumniCategoryObj.year;
        alumniCategoryInfo.urlSlogCategory = _slogVal;
        alumniCategoryInfo.addedOn         = new Date();
        alumniCategoryInfo.addedBy         = loggedInUser;

        return alumniCategoryInfo;
    };

    AlumniModel.CreateAlumni         = (alumniObj, imageInfo, loggedInUser, _slogVal) => {
        let alumniInfo        = new Alumni();
        alumniInfo.studentName          = alumniObj.studentName;
        alumniInfo.urlSlog              = _slogVal;
        alumniInfo.categoryId           = alumniObj.categoryId;
        alumniInfo.batchYear            = alumniObj.batchYear;
        alumniInfo.organizationName     = alumniObj.organizationName;
        alumniInfo.organizationPosition = alumniObj.organizationPosition;
        alumniInfo.imageName            = imageInfo._imageName;
        alumniInfo.imageProperties      = {
            imageExtension: imageInfo._imageExtension,
            imagePath     : imageInfo._imagePath
        };
        alumniInfo.addedOn              = new Date();
        alumniInfo.addedBy              = loggedInUser;

        return alumniInfo;
    };

    let _p = AlumniModel.prototype;

    _p.checkValidationErrors = (req) => {
        req.checkBody('studentName', messageCofig.alumni.validationErrMessage.studentName).notEmpty();
        req.checkBody('categoryId', messageCofig.alumni.validationErrMessage.categoryId).notEmpty();

        return req.validationErrors();
    };

    _p.getAllAlumniCategories = (req, next) => {
        let pagerOpts = utilityHelper.getPaginationOpts(req, next);
        let query = {};
        query.deleted = false;

        let sortOpts = {year: -1};

        return dataProviderHelper.getAllWithDocumentFieldsPagination(AlumniCategory, query, pagerOpts, alumniCategoryFields, sortOpts);
    };

    _p.getAlumniCategoryById = (req) => {
        return dataProviderHelper.findById(AlumniCategory, req.params.alumniCategoryId, alumniCategoryFields);
    };

    _p.deleteAlumniCategory = (req, res, next) => {
        req.alumniCategory.deleted = true;
        req.alumniCategory.deletedOn = new Date();
        req.alumniCategory.deletedBy = req.decoded.user.username;

        let query = {
            'alumniCategoryId': req.params.alumniCategoryId,
            'deleted': false
        };

        dataProviderHelper.checkForDuplicateEntry(AlumniCategory, query)
            .then(function (count) {
                if (count > 0) {
                    throw new Promise.CancellationError('{ "statusCode":"' + HTTPStatus.CONFLICT + '", "message": "' + messageCofig.alumni.categoryDeleteDeny + '"}');
                } else {
                    dataProviderHelper.save(req.alumniCategory)
                }
            })
            .then(() => {
                res.status(HTTPStatus.OK);
                res.json({
                    Message: messageCofig.alumni.deleteMessageAlumniCategory
                });
            })
            .catch(Promise.CancellationError, function (cancellationErr) {
                errorHelper.customErrorResponse(res, cancellationErr, next);
            })
            .catch ((err) => {
                return next(err)
            })
    };

    _p.postAlumniCategory = (req, res, next) => {
        if (req.body.year) {
            let modelInfo   = utilityHelper.sanitizeUserInput(req, next);
            let query       = {};
            let _slogVal        = utilityHelper.getCleanURL(modelInfo.year, next);
            query.urlSlogCateogry = _slogVal;
            query.deleted         = false;

            dataProviderHelper.checkForDuplicateEntry(AlumniCategory, query)
                .then(function (count) {
                    if (count > 0) {
                        throw new Promise.CancellationError('{ "statusCode":"' + HTTPStatus.CONFLICT + '", "message": "' + messageConfig.alumni.alreadyExistsAlumniCategory + '"}');
                    } else {
                        var alumniCategoryInfo = AlumniModel.CreateAlumniCategory(modelInfo, req.decoded.user.username, _slogVal);
                        return dataProviderHelper.save(alumniCategoryInfo);
                    }
                })
                .then(function () {
                    res.status(HTTPStatus.OK);
                    res.json({
                        Message: messageCofig.alumni.saveMessageAlumniCategory
                    });
                })
                .catch(Promise.CancellationError, function (cancellationErr) {
                    errorHelper.customErrorResponse(res, cancellationErr, next);
                })
                .catch(function (err) {
                    return next(err);
                });
        } else {
            res.status(HTTPStatus.BAD_REQUEST);
            res.json({
                Message: messageCofig.alumni.fieldRequiredAlumniCategory
            });
        }
    };

    _p.updateAlumniCategory = (req, res, next) => {
        if (req.body.year) {
            let modelInfo = utilityHelper.sanitizeUserInput(req, next);
            let _slogVal  = utilityHelper.getCleanURL(modelInfo.year, next);

            if (req.alumniCategory.urlSlogCategory !== _slogVal) {
                let query = {};
                query.year      = {$regex: new RegExp('^' + modelInfo.year + '$', "i")};
                query.deleted   = false;

                dataProviderHelper.checkForDuplicateEntry(AlumniCategory, query)
                    .then(function (count) {
                        if (count > 0) {
                            throw new Promise.CancellationError('{ "statusCode":"' + HTTPStatus.CONFLICT + '", "message": "' + messageCofig.alumni.alreadyExistsAlumniCategory + '"}');
                        } else {
                            return _p.updateFunc(req, res, modelInfo, _slogVal);
                        }
                    })
                    .then(function () {
                        res.status(HTTPStatus.OK);
                        res.json({
                            message: messageCofig.alumni.updateMessageAlumniCategory
                        });
                    })
                    .catch(Promise.CancellationError, function (cancellationErr) {
                        errorHelper.customErrorResponse(res, cancellationErr, next);
                    })
                    .catch(function (err) {
                        return next(err);
                    });
                    } else {
                        _p.updateFunc(req, res, modelInfo, _slogVal)
                            .then(function () {
                                res.status(HTTPStatus.OK);
                                res.json({
                                    message: messageCofig.alumni.updateMessageAlumniCategory
                                });
                            })
                            .catch(function (err) {
                                return next(err);
                            });
                    }
                    } else {
                        res.status(HTTPStatus.BAD_REQUEST);
                        res.json({
                        message: messageCofig.alumni.fieldRequiredAlumniCategory
                    });
        }
    };

    _p.updateFunc = (req, res, modelInfo, _slogVal) => {
        req.alumniCategory.year             = modelInfo.year;
        req.alumniCategory.urlSlogCategory  = _slogVal;
        req.alumniCategory.updatedOn        = new Date();
        req.alumniCategory.updatedBy        = req.decoded.user.username;

        return dataProviderHelper.save(req.alumniCategory);
    };

    _p.getAllAlumni = (req, next) => {
        let pagerOpts   = utilityHelper.getPaginationOpts(req, next);
        let query       = {};
        if (req.query.studentname) {
            query.studentName = {$regex: new RegExp('.*' + req.query.studentname, "i")};
        }
        if (req.query.categoryid) {
            query.categoryId = req.query.categoryid;
        }
        query.deleted   = false;

        let sortOpts    = {addedOn: -1};

        return dataProviderHelper.getAllWithDocumentFieldsPagination(Alumni, query, pagerOpts, alumniFields, sortOpts);
    };

    _p.getAlumniById = (req) => {
        return dataProviderHelper.findById(Alumni, req.params.alumniId, alumniFields);
    };

    _p.getAllAlumniByCatergoryTitle = (req, next) => {
        let category = '';
        if (req.params.alumniCategory) {
            category = req.params.alumniCategory
        }
        let categorySlog = utilityHelper.getCleanURL(category, next);
        let query = {
            'deleted': false
        };
        query.urlSlogCategory = categorySlog;

        return new Promise((resolve, reject) => {
            dataProviderHelper.findOne(AlumniCategory, query, alumniCategoryFields)
                .then((categoryObj) => {
                if(categoryObj) {
                    req.query.categoryid = categoryObj._id;
                    return _p.getAllAlumni(req, next);
                }
                else {
                    return [];
                }
                })
                .then((alumniList) => {
                resolve(alumniList);
                })
                .catch((err) => {
                reject(err)
                })
        })
    };

    _p.deleteAlumni = (req, res, next) => {
        req.alumniData.deleted      = true;
        req.alumniData.deletedOn    = new Date();
        req.alumniData.deletedBy    = req.decoded.user.username;

        dataProviderHelper.save(req.alumniData)
            .then(() => {
            res.status(HTTPStatus.OK);
            res.json({
                Message: messageCofig.alumni.deleteMessageAlumni
            });
            })
            .catch((err) => {
            return next(err);
            })
    };

    _p.postAlumni = (req, res, next) => {
        req.body = JSON.parse(req.body.data);

        let errors = _p.checkValidationErrors(req);
        if (errors) {
            res.status(HTTPStatus.BAD_REQUEST);
            res.json({
                Message: errors
            })
        }
        else {
            let modelInfo   = utilityHelper.sanitizeUserInput(req, next);
            let query       = {};
            let _slogVal    = utilityHelper.getCleanURL(modelInfo.studentName, next);
            query.urlSlog   = _slogVal;
            query.deleted   = false;

            dataProviderHelper.checkForDuplicateEntry(Alumni, query)
                .then((count) => {
                if (count > 0) {
                    throw new Promise.CancellationError('{"statusCode": "' + HTTPStatus.CONFLICT + '", "message": "' + messageCofig.alumni.alreadyExistsAlumni +'"}');
                }
                else {
                    let imageInfo   = utilityHelper.getFileInfo(req, null, next);
                    let alumniInfo  = AlumniModel.CreateAlumni(modelInfo, imageInfo, req.decoded.user.username, _slogVal);
                    return dataProviderHelper.save(alumniInfo);
                }
                })
                .then(() => {
                res.status(HTTPStatus.OK);
                res.json({
                    Message: messageCofig.alumni.saveMessageAlumni
                });
                })
                .catch(Promise.CancellationError, (CancellationErr) => {
                    errorHelper.customErrorResponse(res, CancellationErr, next)
                })
                .catch((err) => {
                return next(err)
                })
        }
    };

    _p.updateAlumni = (req, res, next) => {
        req.body = JSON.parse(req.body.data);

        let errors = _p.checkValidationErrors(req);
        if (errors) {
            res.status(HTTPStatus.BAD_REQUEST);
            res.json({
                message: errors
            });
        }else {
            let modelInfo = utilityHelper.sanitizeUserInput(req, next);
            let _slogVal = utilityHelper.getCleanURL(modelInfo.studentName, next);

            if (req.alumniData.urlSlog !== _slogVal) {
                let query = {};
                query.urlSlog = _slogVal;
                query.deleted = false;

                dataProviderHelper.checkForDuplicateEntry(Alumni, query)
                    .then(function (count) {
                        if (count > 0) {
                            throw new Promise.CancellationError('{ "statusCode":"' + HTTPStatus.CONFLICT + '", "message": "' + messageCofig.alumni.alreadyExistsAlumni + '"}');
                        } else {
                            return _p.AlumniUpdateFunction(req, res, next, modelInfo, _slogVal);
                        }
                    })
                    .then(function () {
                        res.status(HTTPStatus.OK);
                        res.json({
                            message: messageCofig.alumni.updateMessageAlumni
                        });
                    })
                    .catch(Promise.CancellationError, function (cancellationErr) {
                        errorHelper.customErrorResponse(res, cancellationErr, next);
                    })
                    .catch(function (err) {
                        return next(err);
                    });
            } else {
                _p.AlumniUpdateFunction(req, res, next, modelInfo, _slogVal)
                    .then(function () {
                        res.status(HTTPStatus.OK);
                        res.json({
                            message: messageCofig.alumni.updateMessageAlumni
                        });
                    })
                    .catch(function (err) {
                        return next(err);
                    });
            }
        }
    };

    _p.AlumniUpdateFunction = (req, res, next, modelInfo, _slogVal) => {
        let imageDataObj = {
            imageName       : req.alumniData.imageName,
            imageProperties : {
                imageExtension  : req.alumniData.imageProperties.imageExtension,
                imagePath       : req.alumniData.imageProperties.imagePath
            }
        };
        let imageInfo = utilityHelper.getFileInfo(req, imageDataObj, next);
        req.alumniData.studentName          = modelInfo.studentName;
        req.alumniData.urlSlog              = _slogVal;
        req.alumniData.categoryId           = modelInfo.categoryId;
        req.alumniData.batchYear            = modelInfo.batchYear;
        req.alumniData.organizationName     = modelInfo.organizationName;
        req.alumniData.organizationPosition = modelInfo.organizationPosition;
        req.alumniData.imageName            = imageInfo._imageName;
        req.alumniData.imageProperties      = {
            imageExtension: imageInfo._imageExtension,
            imagePath     : imageInfo._imagePath
        };
        req.alumniData.updatedOn            = new Date();
        req.alumniData.updatedBy            = req.decoded.user.username;

        return dataProviderHelper.save(req.alumniData);
    };

    return {
        getAllAlumniCategories      : _p.getAllAlumniCategories,
        postAlumniCategory          : _p.postAlumniCategory,
        getAlumniCategoryById       : _p.getAlumniCategoryById,
        deleteAlumniCategory        : _p.deleteAlumniCategory,
        updateAlumniCategory        : _p.updateAlumniCategory,
        postAlumni                  : _p.postAlumni,
        getAllAlumni                : _p.getAllAlumni,
        getAllAlumniByCategoryTitle : _p.getAllAlumniByCatergoryTitle,
        getAlumniById               : _p.getAlumniById,
        updateAlumni                : _p.updateAlumni,
        deleteAlumni                : _p.deleteAlumni
    };

}();

    module.exports = alumniController;