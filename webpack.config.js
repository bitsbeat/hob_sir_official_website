const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const WebpackMd5Hash = require('webpack-md5-hash');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');
const CompressionPlugin = require("compression-webpack-plugin");
const path = require('path');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const LodashModuleReplacementPlugin = require('lodash-webpack-plugin');
const PreloadWebpackPlugin = require('preload-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const StyleExtHtmlWebpackPlugin = require('style-ext-html-webpack-plugin');
const GLOBALS = {
  'process.env.NODE_ENV': JSON.stringify('production'),
  __DEV__: false
};



var appRootPath = __dirname + "/client";
// const DedupePlugin = require('webpack/lib/optimize/DedupePlugin');
var Debug = process.env.NODE_ENV != "production";
module.exports = {
    entry: {
        main: appRootPath + '/vendor-scripts',
        css: appRootPath + '/vendor-css'
    },

    output: {
        path: path.join(__dirname, 'public/package/'),
        filename: '[name].bundle.js'
   },
    module: {
        loaders: [

            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract('style', 'css?sourceMap')
            },
            {
                test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/,
                loader: 'file?name=assets/[hash].[ext]'
            },
            // {
            //     test: /\.(eot|svg|ttf|woff|woff2)$/,
            //     loader: 'file?name=assets/fonts/[name].[ext]'
            // },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: [
                    'file?hash=sha512&digest=hex&name=assets/[hash].[ext]',
                    'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
                ]
            }

        ]
    },
    debug: Debug,
    devtool: Debug ? "cheap-module-source-map" : false,
    resolve: {

        /*
         * An array of extensions that should be used to resolve modules.
         *
         * See: http://webpack.github.io/docs/configuration.html#resolve-extensions
         */
        extensions: ['', '.js', '.css'],

        // Make sure root is src
        root: appRootPath,

        // remove other default values
        modulesDirectories: ['node_modules'],
        alias: {
            'offline': __dirname + '/public/offline',
            'public': __dirname + '/public',
            'scripts': __dirname + '/node_modules/',
            'scrollreveal': __dirname + '/public/plugins/scrollreveal/scrollreveal.js',
            'client': __dirname + '/client'

        }
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            'window.jQuery': "jquery",
            "ScrollReveal": "scrollreveal"
        }),
        // new webpack.optimize.CommonsChunkPlugin({
        //     name: ['main'].reverse()
        // }),
        // /*Single Css */
        // new ExtractTextPlugin("style.css", {
        //     allChunks: true
        // }),
        // /*
        //  * Plugin: HtmlWebpackPlugin
        //  * Description: Simplifies creation of HTML files to serve your webpack bundles.
        //  * This is especially useful for webpack bundles that include a hash in the filename
        //  * which changes every compilation.
        //  *
        //  * See: https://github.com/ampedandwired/html-webpack-plugin
        //  */
        // /**
        //  * Plugin: DedupePlugin
        //  * Description: Prevents the inclusion of duplicate code into your bundle
        //  * and instead applies a copy of the function at runtime.
        //  *
        //  * See: https://webpack.github.io/docs/list-of-plugins.html#defineplugin
        //  * See: https://github.com/webpack/docs/wiki/optimization#deduplication
        //  */
        // new DedupePlugin(),

        // /**
        //  * Plugin: UglifyJsPlugin
        //  * Description: Minimize all JavaScript output of chunks.
        //  * Loaders are switched into minimizing mode.
        //  *
        //  * See: https://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
        //  */
        // // NOTE: To debug prod builds uncomment //debug lines and comment //prod lines
        // new UglifyJsPlugin({
        //     warnings: false,
        //     beautify: false, //prod
        //     mangle: {screw_ie8: true}, //prod
        //     compress: {screw_ie8: true}, //prod
        //     comments: false //prod
        // })
        new webpack.ProvidePlugin({
            Promise: 'imports-loader?this=>global!exports-loader?global.Promise!es6-promise',
            fetch: 'imports-loader?this=>global!exports-loader?global.fetch!whatwg-fetch'
          }),
          new webpack.optimize.ModuleConcatenationPlugin(),
          new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            filename: 'vendor.[chunkhash].js',
            minChunks (module) {
              return module.context && module.context.indexOf('node_modules') >= 0;
            }
          }),
        //   new webpack.ProvidePlugin({
        //     $: "jquery",
        //     jQuery: "jquery"
        //   }),
          new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
          // new webpack.optimize.CommonsChunkPlugin({
          //   name: "vendor",
          //   children: true,
          //   minChunks: 2,
          //   async: true
          // }),
          // Hash the files using MD5 so that their names change when the content changes.
          new WebpackMd5Hash(),
      
          // Tells React to build in prod mode. https://facebook.github.io/react/downloads.html
          new webpack.DefinePlugin(GLOBALS),
      
          new BundleAnalyzerPlugin(),
      
          // Generate an external css file with a hash in the filename
          new ExtractTextPlugin('[name].[contenthash].css'),
          // Minify JS
          new webpack.optimize.UglifyJsPlugin({
            mangle: true,
            compress: {
              warnings: false, // Suppress uglification warnings
              pure_getters: true,
              unsafe: true,
              unsafe_comps: true,
              screw_ie8: true,
              conditionals: true,
              unused: true,
              comparisons: true,
              sequences: true,
              dead_code: true,
              evaluate: true,
              if_return: true,
              join_vars: true
            },
            output: {
              comments: false
            },
            exclude: [/\.min\.js$/gi] // skip pre-minified libs
          }),
          new webpack.optimize.AggressiveMergingPlugin(),
          new webpack.NoErrorsPlugin(),
          new CompressionPlugin({
            asset: "[path].gz[query]",
            algorithm: "gzip",
            test: /\.js$|\.css$|\.html$|\.eot?.+$|\.ttf?.+$|\.woff?.+$|\.svg?.+$/,
            threshold: 10240,
            minRatio: 0.8
          }),
          new PreloadWebpackPlugin({
            rel: 'preload',
            as: 'script',
            include: 'all',
            fileBlacklist: [/\.(css|map)$/, /base?.+/]
          }),
          new ScriptExtHtmlWebpackPlugin({
            defaultAttribute: 'defer'
          }),
          new StyleExtHtmlWebpackPlugin({
            minify: true
          }),
          // Generate HTML file that contains references to generated bundles. See here for how this works: https://github.com/ampedandwired/html-webpack-plugin#basic-usage
          new HtmlWebpackPlugin({
            template: 'src/index.ejs',
            favicon: 'src/favicon.ico',
            minify: {
              removeComments: true,
              collapseWhitespace: true,
              removeRedundantAttributes: true,
              useShortDoctype: true,
              removeEmptyAttributes: true,
              removeStyleLinkTypeAttributes: true,
              keepClosingSlash: true,
              minifyJS: true,
              minifyCSS: true,
              minifyURLs: true
            },
            inject: true//,
            // Note that you can add custom options here if you need to handle other custom logic in index.html
            // To track JavaScript errors via TrackJS, sign up for a free trial at TrackJS.com and enter your token below.
            //trackJSToken: ''
          }),
          // optimizations
          new LodashModuleReplacementPlugin({
            'caching': true,
            'collections': true,
            'paths': true
          }),
          new webpack.LoaderOptionsPlugin({
            minimize: true,
            debug: false,
            noInfo: true, // set to false to see a list of every file being bundled.
            options: {
              sassLoader: {
                includePaths: [path.resolve(__dirname, 'src', 'scss')]
              },
              context: '/',
              postcss: () => [autoprefixer],
            }
          })
    ],
    module: {
        rules: [
          {test: /\.jsx?$/, exclude: /node_modules/, loader: 'babel-loader'},
          {test: /\.eot(\?v=\d+.\d+.\d+)?$/, loader: 'url-loader?name=[name].[ext]'},
          {test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url-loader?limit=10000&mimetype=application/font-woff&name=[name].[ext]'},
          {test: /\.[ot]tf(\?v=\d+.\d+.\d+)?$/, loader: 'url-loader?limit=10000&mimetype=application/octet-stream&name=[name].[ext]'},
          {test: /\.svg(\?v=\d+.\d+.\d+)?$/, loader: 'url-loader?limit=10000&mimetype=image/svg+xml&name=[name].[ext]'},
          {test: /\.(jpe?g|png|gif)$/i, loader: 'file-loader?name=[name].[ext]'},
          {test: /\.ico$/, loader: 'file-loader?name=[name].[ext]'},
          {test: /(\.css|\.scss|\.sass)$/, loader: ExtractTextPlugin.extract('css-loader?sourceMap!postcss-loader!sass-loader?sourceMap')}
        ]
      }
};
